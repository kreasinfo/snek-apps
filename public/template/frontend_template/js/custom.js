/*
	Author: Umair Chaudary @ Pixel Art Inc.
	Author URI: http://www.pixelartinc.com/
*/


$(document).ready(function(e) {

    $ = jQuery;

    /* CART */
    $("#daftarbelanja, #daftarbelanjaf").load(base_url+'cart/countdaftarbelanja/');
    $("div#daftarcart").load(base_url+'cart/countdaftarbelanja_list/');
    
    $("[id^=quantity_]").bind("change", recalc2);
    // run the calculation function now
    recalc();
    // $('div#keranjang').hide();
    /* CART */

    /* UKURAN PRODUCT */
    var initvalueukuran = $('#ukuranproduct').val();
    load_size_stock(initvalueukuran);
    $('#ukuranproduct').change(function(){
        $('#quantityblock').html('Loading...');
        load_size_stock($(this).val());
    });
    /* UKURAN PRODUCT */

    $('#quantityblock button.plus').click(function(e){
        console.log('test')
    });


    $.extend($.fx.step,{
        backgroundPosition: function(fx) {
            if (fx.pos === 0 && typeof fx.end == 'string') {
                var start = $.css(fx.elem,'backgroundPosition');
                start = toArray(start);
                fx.start = [start[0],start[2]];
                var end = toArray(fx.end);
                fx.end = [end[0],end[2]];
                fx.unit = [end[1],end[3]];
            }
            var nowPosX = [];
            nowPosX[0] = ((fx.end[0] - fx.start[0]) * fx.pos) + fx.start[0] + fx.unit[0];
            nowPosX[1] = ((fx.end[1] - fx.start[1]) * fx.pos) + fx.start[1] + fx.unit[1];
            fx.elem.style.backgroundPosition = nowPosX[0]+' '+nowPosX[1];

            function toArray(strg){
                strg = strg.replace(/left|top/g,'0px');
                strg = strg.replace(/right|bottom/g,'100%');
                strg = strg.replace(/([0-9\.]+)(\s|\)|$)/g,"$1px$2");
                var res = strg.match(/(-?[0-9\.]+)(px|\%|em|pt)\s(-?[0-9\.]+)(px|\%|em|pt)/);
                return [parseFloat(res[1],10),res[2],parseFloat(res[3],10),res[4]];
            }
        }
    });


});
function load_size_stock(valuedata){
    var postdata = "id="+valuedata;
    $.ajax({
        type: 'POST',
        url: base_url+'produk/load_quantitiy/',
        data: postdata,
        success: function(data) {
            console.log(data);
            if(data !=''){
                $('#quantityblock').html(data);
                $('#quantityblock_temp').hide();
            }else{
                $('#quantityblock_temp').show();
            }
            // $(".select").selectBox();
        }
    })
}
function addtocart(iddata,ukuranproduct){ 
    var qty = $('#qty').val();
    var ukuranproduct = $('#ukuranproduct').val();
    
    if(qty != 0 && ukuranproduct != 0){
        $.ajax({
            type: 'POST',
            url: base_url+"cart/addproduct/",
            data: {productid:iddata, quantity:qty, size:ukuranproduct},
            success: function(response) {
                if(response == 'exist'){
                    toastr.error("Item yang Anda tambahkan telah ada dalam database kami.", "ERROR");
                }else if(response == 'stock'){
                    toastr.error("Maaf stok produk ini tidak tersedia saat ini.", "ERROR");
                }else if(response == 'success'){
                    toastr.success("Produk telah dimasukkan ke keranjang belanja.", "SUCCESS");
                }else if(response == 'notequal'){
                    toastr.info("Produk ini tidak bisa diorder dikarenakan perbedaan wilayah.", "INFO");
                }
                $("#daftarbelanja").load(base_url+'cart/countdaftarbelanja/');
            }
        });
    }else{
        toastr.error("Silahkan memilih rasa dan jumlah barang.", "ERROR");
    }
}
function addtocartbeli(iddata,ukuranproduct){ 
    var qty = $('#qty').val();
    var ukuranproduct = $('#ukuranproduct').val();
    
    if(qty != 0 && ukuranproduct != 0){
        $.ajax({
            type: 'POST',
            url: base_url+"cart/addproduct/",
            data: {productid:iddata, quantity:qty, size:ukuranproduct},
            success: function(response) {
                if(response == 'exist'){
                    toastr.error("Item yang Anda tambahkan telah ada dalam database kami.", "ERROR");
                }else if(response == 'stock'){
                    toastr.error("Maaf stok produk ini tidak tersedia saat ini.", "ERROR");
                }else if(response == 'success'){
                    toastr.success("Produk telah dimasukkan ke keranjang belanja.", "SUCCESS");
                     window.location.replace(base_url+"cart/view");
                }else if(response == 'notequal'){
                    toastr.info("Produk ini tidak bisa diorder dikarenakan perbedaan wilayah.", "INFO");
                }
                $("#daftarbelanja").load(base_url+'cart/countdaftarbelanja/');
            }
        });
    }else{
        toastr.error("Silahkan memilih rasa dan jumlah barang.", "ERROR");
    }
}

function removecart(id){
    var posting = "productid="+id;
    $.ajax({
        type: 'POST',
        url: base_url+"cart/remove_product/",
        data: posting,
        success: function(response) {
            $("#daftarbelanja").load(base_url+'cart/countdaftarbelanja/');
            $("div#daftarcart").load(base_url+'cart/countdaftarbelanja_list/');
            $("div#"+id).remove();
            var tabel = $('.isicart').length;
            if(tabel == '0'){
                $('div#keranjang').show();
            }
            recalc();
        }
    });
}

function updateqty(id, qty){
    $.ajax({
        type: 'POST',
        url: base_url+"cart/update_qty/",
        data: {productid:id, qty:qty},
        success: function(response) {
            recalc();
        }
    });
}
function recalc(){

    $("[id^=totalthis_]").calc(
        // the equation to use for the calculation
        "qty * price",
        // define the variables used in the equation, these can be a jQuery object
        {
            qty: $("[id^=quantity_]"),
            price: $("[id^=productprice_]")
        },
        // define the formatting callback, the results of the calculation are passed to this function
        function (s){
            // return the number as a dollar amount
            //return "$" + s.toFixed(2);
            // return 'IDR '+format_hasil(s.toFixed());
            return s.toFixed(3);
        },
        // define the finish callback, this runs after the calculation has been complete
        function ($this){
            // sum the total of the $("[id^=total_item]") selector
            var sum = $this.sum();
            $("#total_all").text(
                // round the results to 2 digits
                //"$" + sum.toFixed(2)
                'IDR '+format_hasil(sum.toFixed(0))
            );
            $("[id^=totalthis_]").formatCurrency({  digitGroupSymbol:'.', symbol:'IDR ', roundToDecimalPlace:'-2' });
            // $("[id^=productprice_]").formatCurrency({ symbol:'IDR ', roundToDecimalPlace:'-2' });
            $("#totalhrgaproduk").val(sum.toFixed(0));
            
            $(".totalharga").text(
                'IDR '+format_hasil(sum.toFixed(0))
            );
        }
    );

    $("[id^=totalberat_]").calc(
        "qty * berat",
        {
            qty: $("[id^=quantity_]"),
            berat: $("[id^=productberat_]")
        },
        function (s){
            return s.toFixed(0)+' gram';
        },
        function ($this){
            var sum = $this.sum();
            
            $(".berattotal").text(
                sum.toFixed(0)
            );
            
            $("#berattotal").val(
                sum.toFixed(0)
            );
        }
    );
    
    var totalhrgaproduk = $("#totalhrgaproduk").val();
    var harga_pengiriman = $("#harga_pengiriman").val();
    var berattotal = $("#berattotal").val();
    var total_pengiriman = parseInt(harga_pengiriman) * parseInt(Math.ceil(berattotal/1000));
    $("#total_pengirimansidemenu").text(total_pengiriman).formatCurrency({ digitGroupSymbol:'.', symbol:'IDR ' ,roundToDecimalPlace:'0' });
    $("#total_pengiriman").text(total_pengiriman).formatCurrency({ digitGroupSymbol:'.', symbol:'IDR ' ,roundToDecimalPlace:'0' });
    $("#totalhargaongkir").val(total_pengiriman);
    var total_harga = parseInt(total_pengiriman) + parseInt(totalhrgaproduk);
    $("#total_all").text(total_harga).formatCurrency({ digitGroupSymbol:'.', symbol:'IDR ' ,roundToDecimalPlace:'0' });
    
}
function format_hasil(jumlahtot){
    jumlahtot = jumlahtot.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(jumlahtot))
        jumlahtot = jumlahtot.replace(pattern, "$1.$2");
    return jumlahtot;
}
function recalc2(){
    $("[id^=totalthis_]").calc(
        // the equation to use for the calculation
        "qty * price",
        // define the variables used in the equation, these can be a jQuery object
        {
            qty: $("[id^=quantity_]"),
            price: $("[id^=productprice_]")
        },
        // define the formatting callback, the results of the calculation are passed to this function
        function (s){
            // return the number as a dollar amount
            //return "$" + s.toFixed(2);
            return s.toFixed(3);
        },
        // define the finish callback, this runs after the calculation has been complete
        function ($this){
            // sum the total of the $("[id^=total_item]") selector
            var sum = $this.sum();
            $("#total_all").text(
                // round the results to 2 digits
                //"$" + sum.toFixed(2)
                'IDR '+format_hasil(sum.toFixed(0))
            );
            $("[id^=totalthis_]").formatCurrency({  digitGroupSymbol:'.', symbol:'IDR ', roundToDecimalPlace:'-2' });
            // $("[id^=productprice_]").formatCurrency({ symbol:'IDR ', roundToDecimalPlace:'-2' });
            $("#totalhrgaproduk").val(sum.toFixed(0));
            
            $(".totalharga").text(
                'IDR '+format_hasil(sum.toFixed(0))
            );
        }
    );
    
    updateqty($(this).attr('data-id'), $(this).val());

    $("[id^=totalberat_]").calc(
        "qty * berat",
        {
            qty: $("[id^=quantity_]"),
            berat: $("[id^=productberat_]")
        },
        function (s){
            return s.toFixed(0)+' gram';
        },
        function ($this){
            var sum = $this.sum();
            
            $(".berattotal").text(
                sum.toFixed(0)
            );
            
            $("#berattotal").val(
                sum.toFixed(0)
            );
        }
    );

    var totalhrgaproduk = $("#totalhrgaproduk").val();
    var harga_pengiriman = $("#harga_pengiriman").val();
    var berattotal = $("#berattotal").val();
    var total_pengiriman = parseInt(harga_pengiriman) * parseInt(Math.ceil(berattotal/1000));
    $("#total_pengirimansidemenu").text(total_pengiriman).formatCurrency({ digitGroupSymbol:'.', symbol:'IDR ' ,roundToDecimalPlace:'0' });
    $("#total_pengiriman").text(total_pengiriman).formatCurrency({ digitGroupSymbol:'.', symbol:'IDR ' ,roundToDecimalPlace:'0' });
    $("#totalhargaongkir").val(total_pengiriman);
    var total_harga = parseInt(total_pengiriman) + parseInt(totalhrgaproduk);
    $("#total_all").text(total_harga).formatCurrency({ digitGroupSymbol:'.', symbol:'IDR ' ,roundToDecimalPlace:'0' });
    
}