<?php
function cutting_words($string, $max_length){  
    if (strlen($string) > $max_length){  
           return substr($string, 0, $max_length)."..."; 
    }else{  
        return $string;  
    }  
}
function cut_paragraf($oldstring, $wordsreturned){
  $retval = '';
  $string = preg_replace('/(?<=\S,)(?=\S)/', ' ', $oldstring);
  $string = str_replace("\n", " ", $string);
  $array = explode(" ", $string);
  if (count($array)<=$wordsreturned)
  {
    $retval = $string;
  }
  else
  {
    array_splice($array, $wordsreturned);
    $retval = implode(" ", $array)." ...";
  }
  return $retval;
} 
function clean_str($string){
    $string = str_replace(array("\\r\\n", "\\r", "\\n"), "", $string);
    $string = stripslashes($string);
    return $string;
}

function clean_str_html($string){
    $string = str_replace(array("\\r\\n", "\\r", "\\n"), "", $string);
    $string = htmlentities(stripslashes($string));
    return $string;
}
function replace_slash($string){
    $string = str_replace("/", "_", $string);
    $string = htmlentities(stripslashes($string));
    return $string;
}
function replace_dash($string){
    $string = str_replace("_", "/", $string);
    $string = htmlentities(stripslashes($string));
    return $string;
}
function rupiah($number) {
    $rupiah = number_format($number, 0, ',', '.');
    return "IDR ".$rupiah;
}
function format_weight($weight) {
    return number_format($weight, 0, ',', '.');
}

function format_price($price, $code) {
    return $code.' '.number_format($price, 0, ',', '.');
}
