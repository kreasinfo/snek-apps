<?php 
if ( ! function_exists('writeLog')) {
	function writeLog($params = array()) {
		$CI =& get_instance(); 
		$CI->load->library('session');
		$CI->load->database();	
		
		$module = isset($params['module'])?$params['module']:'';
		$details = isset($params['details'])?$params['details']:'';
		
		$data ['id'] = "Null";
		$data ['date_created'] = date("Y-m-d H:i:s");
		$data ['ip_address'] = $CI->input->ip_address();
		$data ['username'] = $CI->session->userdata('userid');
		$data ['module'] = $CI->db->escape_str($module);
		$data ['details'] = $CI->db->escape_str($details);
		$doInsert = $CI->db->insert('logs', $data);
		if($doInsert){		
			return true;
		}else{
			return false;
		}
	}

	function sizeName($iddata) {
		$CI =& get_instance(); 
		$CI->load->library('session');
		$CI->load->database();	
		
		$q = $CI->db->query("
			SELECT
				size
			FROM
				produk_ukuran
			WHERE id = '".$iddata."'
			ORDER BY id DESC
		");
		$result = $q->first_row('array');
		if(count($result) > 0){
			return $result['size'];
		}else{
			return '-';
		}
	}

	function sizeStock($iddata) {
		$CI =& get_instance(); 
		$CI->load->library('session');
		$CI->load->database();	
		
		$q = $CI->db->query("
			SELECT
				stock
			FROM
				produk_ukuran
			WHERE id = '".$iddata."'
			ORDER BY id DESC
		");
		$result = $q->first_row('array');
		if(count($result) > 0){
			return $result['stock'];
		}else{
			return '-';
		}
	}
}