<?php 
if ( ! function_exists('thumb_image')) {
	function thumb_image($path = "", $size = "", $type = ""){
		$CI =& get_instance();
		$webconfig = $CI->config->item('webconfig');

		## DEFINE THE IMAGE ##
		if($type == 'profile'){
			$path_thumbs     = $webconfig['media-path-profile-thumb'];
			$server_thumbs   = $webconfig['media-server-profile-thumb'];
			$path_original   = $webconfig['media-path-profile'];
			$server_original = $webconfig['media-server-profile'];
		}else if($type == 'blog'){
			$path_thumbs     = $webconfig['media-path-blog-thumb'];
			$server_thumbs   = $webconfig['media-server-blog-thumb'];
			$path_original   = $webconfig['media-path-blog'];
			$server_original = $webconfig['media-server-blog'];
		}else if($type == 'product'){
			$path_thumbs     = $webconfig['media-path-product-thumb'];
			$server_thumbs   = $webconfig['media-server-product-thumb'];
			$path_original   = $webconfig['media-path-product'];
			$server_original = $webconfig['media-server-product'];
		}else{
			$path_thumbs     = $webconfig['media-path-images-thumb'];
			$server_thumbs   = $webconfig['media-server-images-thumb'];
			$path_original   = $webconfig['media-path-images-original'];
			$server_original = $webconfig['media-server-images-original'];
		}

		if($path != "") {
			$str_path = "";
			$exp_img = explode(".", $path);
			
			
			if(isset($size) && $size != "") {
				if(file_exists($path_thumbs.$exp_img[0]."_".$size."_thumb.".$exp_img[1])) {
					$str_path = $server_thumbs.$exp_img[0]."_".$size."_thumb.".$exp_img[1];
				} else {
					if($type == 'profile'){
						$str_path = $webconfig['back_images']."no_picture_profile.jpg";
					}else{
						$str_path = $webconfig['back_images']."no_image_news.jpg";
					}
				}
			} else {
				if(file_exists($path_original.$path)) {
					$str_path = $server_original.$path;
				} else {
					if($type == 'profile'){
						$str_path = $webconfig['back_images']."no_picture_profile.jpg";
					}else{
						$str_path = $webconfig['back_images']."no_image_news.jpg";
					}
				}
			}

			return $str_path;
		} else {
			if($type == 'profile'){
				return $webconfig['back_images']."no_picture_profile.jpg";
			}else{
				return $webconfig['back_images']."no_image_news.jpg";
			}
		}
	}
}
if ( ! function_exists('thumb_onthefly')) {
	function thumb_onthefly($path = "", $width = "", $height = "", $type = "crop"){
		$CI =& get_instance();
		$webconfig = $CI->config->item('webconfig');

		## LOAD IMG LIBRARY ##
		require_once(dirname(__FILE__).'/../libraries/resize.php');
		$resizeObj = new resize($webconfig['media-path-images-original'].$path); 
		// *** (options: exact, portrait, landscape, auto, crop)
		$resizeObj -> resizeImage($width, $height, $type);

		# RESIZE and CROP #
        $thepath = $webconfig['media-path-images-thumb'].$path;                       
        $exp = explode("/",$thepath);
        $way ='';
        $i = 0; foreach($exp as $n){ $i++;
        	if($i < count($exp)){
            	$way.=$n.'/';
            	@mkdir($way);     
        	}  
        }
		$resizeObj -> saveImage($webconfig['media-path-images-thumb'].$path, 100);

		return $webconfig['media-server-images-thumb'].$path;
	}
}