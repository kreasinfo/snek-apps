<?php
if ( ! function_exists('send_email')) {
	function send_email($params = array()) {
		include_once(dirname(__FILE__)."/../libraries/phpmailer/class.phpmailer.php");
		include_once(dirname(__FILE__)."/../libraries/phpmailer/class.smtp.php");
		
		$email_tujuan = isset($params["email_tujuan"])?$params["email_tujuan"]:'';
		$email_tujuan_text = isset($params["email_tujuan_text"])?$params["email_tujuan_text"]:'';
		$body 		  = isset($params["body"])?$params["body"]:'';
		$subject 	  = isset($params["subject"])?$params["subject"]:'';
		
		$setfrom_email	  = isset($params["setfrom_email"])?$params["setfrom_email"]:'admin@snekbook.com';
		$setfrom_text	  = isset($params["setfrom_text"])?$params["setfrom_text"]:'Snekbook Website';
		$addreply_email	  = isset($params["addreply_email"])?$params["addreply_email"]:'admin@snekbook.com';
		$addreply_text	  = isset($params["addreply_text"])?$params["addreply_text"]:'Snekbook Admin';
		
		$attachment	  = isset($params["attachment"])?$params["attachment"]:array();
		
		$mail             = new PHPMailer();

		//$body             = file_get_contents(base_url().'register/bodyemail/');
		
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
												   // 1 = errors and messages
												   // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->Host       = HOST_EMAIL; // sets the SMTP server
		$mail->Port       = PORT_EMAIL;                    // set the SMTP port for the GMAIL server
		$mail->Username   = USER_EMAIL; // SMTP account username
		$mail->Password   = PASS_EMAIL;        // SMTP account password
		
		$mail->SetFrom($setfrom_email, $setfrom_text);
		$mail->AddReplyTo($addreply_email, $addreply_text);
		
		$mail->Subject    = $subject;
		
		$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		
		$mail->MsgHTML($body);
		
		$address = $email_tujuan;
		$mail->AddAddress($address, $email_tujuan_text);
		
		## ATTACHMENT ##
		if(count($attachment) > 0){
			foreach($attachment as $listdata){
				$mail->AddAttachment($listdata);
			}
		}
		
		if(!$mail->Send()) {
		  // echo "Mailer Error: " . $mail->ErrorInfo;
		  return false;
		} else {
		  return true;
		}
	}
}