<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class MY_Exceptions extends CI_Exceptions {

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->ob_level = ob_get_level();
		// Note:  Do not log messages from this constructor.
	}


	function show_404($page = '', $log_error = TRUE)
	{
		header("location: http://".APP_DOMAIN."/notfoundpage");
		exit;
	}

	// function show_error($heading, $message, $template = 'error_general', $status_code = 500)
	// {
	// 	header("location: http://".APP_DOMAIN."/error_page");
	// 	exit;
	// }


	// function show_php_error($severity, $message, $filepath, $line)
	// {
	// 	header("location: http://".APP_DOMAIN."/error_page");
	// 	exit;
	// }
}
// END Exceptions Class

/* End of file Exceptions.php */
/* Location: ./system/core/Exceptions.php */