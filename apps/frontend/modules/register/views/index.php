<?php if(isset($error_hash) && count($error_hash) > 0){ ?>
    <?php foreach($error_hash as $inp_err){ ?>
        <script type="text/javascript">
        jQuery(document).ready(function($) {
            toastr.error("<?php echo $inp_err; ?>");
            });
        </script>
    <?php } ?>
<?php } ?>
<?php if(isset($success)){ ?>
    <script type="text/javascript">
    jQuery(document).ready(function($) {
        toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
        });
    </script>
<?php } ?>
<div class="container">
    <div class="row margin-top-40">
        <div class="col-lg-6 col-lg-offset-3">
            <div class="bg-white">
                <center><img src="<?php echo $this->webconfig['frontend_template']; ?>images/Logo.png" class="img-responsive" /></center>
                <p class="login-ttl">REGISTRASI <span class="ft-red">PEMBELI</span></p>
                <p class="login-des">Halaman ini diperuntukan bagi pembeli produk di Snekbook.com, Silahkan Mengisi data Anda dengan Benar pada Form Berikut ini :</p>
                <form method="POST">
                    <div class="row">
                        <div class="col-md-12"><input type="text" class="form-control margin-bottom-10" name="fullname" placeholder="Nama Lengkap" value="<?php echo isset($fullname)?$fullname:'';?>"/></div>
                    </div>
                    <?php /* <div class="row">
                        <div class="col-md-6"><input type="text" class="form-control margin-bottom-10" name="username" placeholder="Username" /></div>
                        <div class="col-md-6"><input type="password" class="form-control margin-bottom-10" name="password" placeholder="Password" /></div>
                    </div> */?>
                    <div class="row">
                        <div class="col-md-6"><input type="email" class="form-control margin-bottom-10" name="email" placeholder="Email" value="<?php echo isset($email)?$email:'';?>"/></div>
                        <div class="col-md-6"><input type="password" class="form-control margin-bottom-10" name="password" placeholder="Password" value="<?php echo isset($password)?$password:'';?>"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"><input type="text" class="form-control margin-bottom-10" name="phone" placeholder="No. Telp" value="<?php echo isset($phone)?$phone:'';?>"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"><textarea class="form-control margin-bottom-10" name="address" placeholder="Alamat"><?php echo isset($address)?$address:'';?></textarea></div>
                    </div>
                    <?php /* <div class="row">
                        <div class="col-xs-6 col-xs-offset-3" >
                            <input type="file" class="form-control margin-bottom-10" placeholder="Pilih File Foto" />
                        </div>
                    </div> */?>
                    <div class="row">
                        <div class="col-xs-4">
                            <img id="img" src="<?php echo base_url(); ?>captcha/" class="img-responsive captcha"/>
                        </div>
                        <div class="col-xs-8">
                            <input type="text" class="form-control margin-bottom-10" name="captcha" placeholder="captcha" value="<?php echo isset($captcha)?$captcha:'';?>"/>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-login btn-block" value="DAFTAR" /><br />
                </form>
                <div class="border margin-bottom-30"></div>
                <!-- <a href="<?php echo $fb_login_url; ?>" class="btn-facebook margin-bottom-10">LOG IN VIA FACEBOOK</a>
                <a href="" class="btn-twitter margin-bottom-10">LOG IN VIA TWITTER</a> -->
                
                <div class="row">
                    <div class="col-xs-6">
                        <div class="box-link">
                            <a href="<?php echo base_url(); ?>">
                                <i class="fa fa-home"></i>
                                <p>Kembali ke Home</p>
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-xs-6">
                        <div class="box-link">
                            <a href="<?php echo base_url(); ?>login/member">
                                <i class="fa fa-user"></i>
                                <p>Ke Halaman Log In</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>