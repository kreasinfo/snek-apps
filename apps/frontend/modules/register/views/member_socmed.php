<script type="text/javascript">
jQuery(document).ready(function($) {
  $('.input-group.date').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        });
});

</script>
<section class="topblock mb-30">
  <div class="container">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <h3 class="mb-20"><?php echo $this->lang->line('register_as_member'); ?></h3>
        <?php if(isset($error_hash['error']) && count($error_hash['error']) > 0){ ?>
          <p class="alert alert-danger"><strong><?php echo $error_hash['error']; ?></strong></p>
        <?php } ?>
        <?php if(isset($success)){ ?>
            <p class="alert alert-success"><strong><?php echo $success; ?></strong></p>  
        <?php } ?>
        <form id="register" method="post" action="" >
                <div class="form-group">
                  Silahkan melengkapi beberapa data yang kami butuhkan untuk proses registrasi Anda.
                </div>
                <input type="hidden" name="fbid" value="<?php echo (isset($fbid) && $fbid != 0 && $fbid != '')?$fbid:''; ?>"/>
                <input type="hidden" name="twitterid" value="<?php echo (isset($twitterid) && $twitterid != 0 && $twitterid != '')?$twitterid:''; ?>"/>
                <div class="form-group">
                  <label for="exampleInputEmail1"><?php echo $this->lang->line('fullname'); ?></label>
                  <input type="text" name="fullname" class="form-control" id="exampleInputEmail1" placeholder="<?php echo $this->lang->line('fullname'); ?>" value="<?php echo isset($fullname)?$fullname:'';?>">
                  <em><?php echo isset($error_hash['fullname'])?$error_hash['fullname']:'';?></em>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1"><?php echo $this->lang->line('email'); ?></label>
                  <input type="text" name="email" class="form-control" id="exampleInputEmail1" placeholder="<?php echo $this->lang->line('email'); ?>" value="<?php echo isset($email)?$email:'';?>">
                  <em><?php echo isset($error_hash['email'])?$error_hash['email']:'';?></em>
                </div>
                <?php if(!(isset($fbid) && $fbid != 0 && $fbid != '') && !(isset($twitterid) && $twitterid != 0 && $twitterid != '')){ ?>
                  <div class="form-group">
                    <label for="exampleInputPassword1"><?php echo $this->lang->line('password'); ?></label>
                    <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="<?php echo $this->lang->line('password'); ?>" value="<?php echo isset($password)?$password:'';?>">
                    <em><?php echo isset($error_hash['password'])?$error_hash['password']:'';?></em>
                  </div>
                <?php } ?>
                <div class="form-group">
                  <label for="phonefield"><?php echo $this->lang->line('phone'); ?></label>
                  <input type="text" name="phone" class="form-control" id="phonefield" placeholder="<?php echo $this->lang->line('phone'); ?>" value="<?php echo isset($phone)?$phone:'';?>">
                  <em><?php echo isset($error_hash['phone'])?$error_hash['phone']:'';?></em>
                </div>
                <div class="form-group">
                  <label for="phonefield"><?php echo $this->lang->line('captcha'); ?></label><br>
                  <img id="img" src="<?php echo base_url(); ?>captcha/listing/<?php echo strtotime(date("Y-m-d H:i:s"))?>" style="vertical-align:middle;" />
                  <input name="captcha" type="text" class="form-control" style="width:50%;float:right;" >
                  <br>
                  <em style="float:right;"><?php echo isset($error_hash['captcha'])?$error_hash['captcha']:'';?></em>
                  <em style="float:right;"><?php echo isset($error_hash['error_captcha'])?$error_hash['error_captcha']:'';?></em>
                  <br>
                </div>
                <div class="formend text-right">
                  <input type="button" class="btn btn-default btn-sm redbtn" value="Cancel" />
                  <input type="submit" class="btn btn-default btn-sm" value="<?php echo $this->lang->line('daftar'); ?>" />
                </div>
            </form>
      </div>
      <div class="col-xs-4">
        &nbsp;
      </div>
    </div>
  </div>
</section>
