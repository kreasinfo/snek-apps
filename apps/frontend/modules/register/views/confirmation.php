<div class="container">
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li class="active">Konfirmasi</li>
    </ol>
</div>
<div class="border-bottom"></div>

<div class="container">
  <div class="row mt-100">
    <div class="col-sm-3">&nbsp;</div>
      <div class="col-sm-6">
        <div id="loginblock" class="box-border" style="text-align:center;">
          <?php if(isset($status) && $status == 'success'){ ?>
            <h3 class="mb-20"><?php echo $this->lang->line('label_success'); ?></h3>
            <p><?php echo $message; ?> <a href="<?php echo base_url(); ?>login/member/<?php echo replace_slash(current_url()); ?>">disini</a></p>
          <?php }else if(isset($status) && $status == 'error'){ ?>
            <h3 class="mb-20"><?php echo $this->lang->line('label_error'); ?></h3>
            <p><?php echo $message; ?></p>
          <?php } ?>
        </div>
      </div>
      <div class="col-sm-3">&nbsp;</div>
  </div>
</div>
<br>
<div class="mb-100"></div>