<?php
if ( session_status() == PHP_SESSION_NONE ) {
	session_start();
}
require_once __DIR__ . "/../../../libraries/facebook-php-sdk-v4-4.0-dev/autoload.php";
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\GraphUser;
use Facebook\GraphSessionInfo;

class Register extends CI_Controller {
	var $webconfig;
	var $pagename;
	var $configuration;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->lang->load('global', 'bahasa');
		$this->load->helper('phpmailer');
		$this->load->library('session');
		$this->load->sharedModel('LoginModel');
		$this->load->sharedModel('MembersModel');

		$this->load->sharedModel('ShopconfigurationModel');	
		$this->configuration = $this->ShopconfigurationModel->listData();
		$this->webconfig = $this->config->item('webconfig');
		$this->pagename = $this->lang->line('register');	
	}

	function index()
	{
		$tdata = array();
		
		$this->module_name = $this->lang->line('label_register_member');
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
		### FB LOGIN UNTU URL LOGIN ###
		FacebookSession::setDefaultApplication(fb_app_id, fb_app_secret);
		$helper = new FacebookRedirectLoginHelper("http://".APP_DOMAIN."/login/member_fblogin");
		$tdata['fb_login_url'] = $helper->getLoginUrl( array( 'scope' => fb_required_scope ) );
		$tdata['fb_login_url'] = isset($helper)?$helper->getLoginUrl( array( 'scope' => fb_required_scope ) ):'';
		#### END
		
		if($_POST){
			
			$validator = new FormValidator();
		    $validator->addValidation("fullname","req",$this->lang->line('error_empty_name_members'));
		    $validator->addValidation("phone","req",$this->lang->line('error_empty_phone_members'));
		    $validator->addValidation("address","req",$this->lang->line('error_empty_address_members'));
		    $validator->addValidation("email","req",$this->lang->line('error_empty_email_members'));
		    $validator->addValidation("email","email",$this->lang->line('error_wrong_email_members'));
			$validator->addValidation("password","req",$this->lang->line('error_empty_password_members'));
			$validator->addValidation("password","minlen=5",$this->lang->line('error_password_5_members'));
			$validator->addValidation("captcha","req",$this->lang->line('error_empty_captcha'));
			
		    if($validator->ValidateForm())
		    {
		    	if ($this->session->userdata('captcha') == $this->input->post('captcha')) {
			    	$name = $this->input->post('fullname');
					$email = $this->input->post('email');
					$password = $this->input->post('password');
			        $doInsert = $this->MembersModel->entriData(array(
															        'fullname'=>$this->input->post('fullname')
															        ,'phone'=>$this->input->post('phone')
															        ,'email'=>$this->input->post('email')
															        ,'password'=>$this->input->post('password')
															        ,'address'=>$this->input->post('address')
															  ));
					if($doInsert == 'empty'){
						$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
					}else if($doInsert == 'exist_email'){
						$tdata['error_hash'] = array('error' => $this->lang->line('error_duplicate_email'));
					}else if($doInsert == 'failed'){
						$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
					}else if($doInsert == 'not_same'){
						$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_password_not_same'));
					}else if($doInsert == 'success'){
						$tdata['success'] = $this->lang->line('msg_success_registration');
						$this->send_email_code_member($email, $name, md5($password.$email));
					}
						if($doInsert !='success'){
							$tdata['fullname']   = $this->input->post('fullname');
							$tdata['phone']      = $this->input->post('phone');
							$tdata['email']      = $this->input->post('email');
							$tdata['password']   = $this->input->post('password');
							$tdata['address']   = $this->input->post('address');
						}
						
					## LOAD DATA TEMPLATE ##

					$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
					$this->load->sharedView('register', $ldata);
				} else {
					$tdata['fullname']   = $this->input->post('fullname');
					$tdata['phone']      = $this->input->post('phone');
					$tdata['email']      = $this->input->post('email');
					$tdata['password']   = $this->input->post('password');
					$tdata['address']   = $this->input->post('address');

					$tdata['error_hash'] = array('error_captcha' => $this->lang->line('error_invalid_captcha'));
					## LOAD DATA TEMPLATE ##

					$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
					$this->load->sharedView('register', $ldata);
				}
		    }
		    else
		    {
		        $tdata['fullname']     = $this->input->post('fullname');
			    $tdata['phone'] = $this->input->post('phone');
			    $tdata['email'] = $this->input->post('email');
			    $tdata['password'] = $this->input->post('password');
			    $tdata['address'] = $this->input->post('address');
		    	$tdata['error_hash'] = $validator->GetErrors();

				## LOAD DATA TEMPLATE ##
		    	
				$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
				$this->load->sharedView('register', $ldata);
		    }
		}else{
			## LOAD DATA TEMPLATE ##
			
			## LOAD LAYOUT ##
			$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
			$this->load->sharedView('register', $ldata);
		}
		
	}

	function ukm()
	{
		$tdata = array();
		
		$this->module_name = $this->lang->line('label_register_member').' UKM';
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');

		if($_POST){
			
			$validator = new FormValidator();
		    $validator->addValidation("fullname","req",$this->lang->line('error_empty_name_members'));
		    $validator->addValidation("phone","req",$this->lang->line('error_empty_phone_members'));
		    $validator->addValidation("address","req",$this->lang->line('error_empty_address_members'));
		    $validator->addValidation("email","req",$this->lang->line('error_empty_email_members'));
		    $validator->addValidation("email","email",$this->lang->line('error_wrong_email_members'));
			$validator->addValidation("password","req",$this->lang->line('error_empty_password_members'));
			$validator->addValidation("password","minlen=5",$this->lang->line('error_password_5_members'));
			$validator->addValidation("captcha","req",$this->lang->line('error_empty_captcha'));
			
		    if($validator->ValidateForm())
		    {
		    	if ($this->session->userdata('captcha') == $this->input->post('captcha')) {
			    	$name = $this->input->post('fullname');
					$email = $this->input->post('email');
					$password = $this->input->post('password');
			        $doInsert = $this->MembersModel->entriDataUkm(array(
															        'fullname'=>$this->input->post('fullname')
															        ,'phone'=>$this->input->post('phone')
															        ,'email'=>$this->input->post('email')
															        ,'password'=>$this->input->post('password')
															        ,'address'=>$this->input->post('address')
															  ));
					if($doInsert == 'empty'){
						$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
					}else if($doInsert == 'exist_email'){
						$tdata['error_hash'] = array('error' => $this->lang->line('error_duplicate_email'));
					}else if($doInsert == 'failed'){
						$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
					}else if($doInsert == 'not_same'){
						$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_password_not_same'));
					}else if($doInsert == 'success'){
						$tdata['success'] = $this->lang->line('msg_success_registrationukm');
						$this->send_email_reg_ukm($email, $name);
					}
						if($doInsert !='success'){
							$tdata['fullname']   = $this->input->post('fullname');
							$tdata['phone']      = $this->input->post('phone');
							$tdata['email']      = $this->input->post('email');
							$tdata['password']   = $this->input->post('password');
							$tdata['address']   = $this->input->post('address');
						}
						
					## LOAD DATA TEMPLATE ##

					$ldata['content'] = $this->load->view($this->router->class.'/register_ukm',$tdata, true);
					$this->load->sharedView('register', $ldata);
				} else {
					$tdata['fullname']   = $this->input->post('fullname');
					$tdata['phone']      = $this->input->post('phone');
					$tdata['email']      = $this->input->post('email');
					$tdata['password']   = $this->input->post('password');
					$tdata['address']   = $this->input->post('address');

					$tdata['error_hash'] = array('error_captcha' => $this->lang->line('error_invalid_captcha'));
					## LOAD DATA TEMPLATE ##

					$ldata['content'] = $this->load->view($this->router->class.'/register_ukm',$tdata, true);
					$this->load->sharedView('register', $ldata);
				}
		    }
		    else
		    {
		        $tdata['fullname']     = $this->input->post('fullname');
			    $tdata['phone'] = $this->input->post('phone');
			    $tdata['email'] = $this->input->post('email');
			    $tdata['password'] = $this->input->post('password');
			    $tdata['address'] = $this->input->post('address');
		    	$tdata['error_hash'] = $validator->GetErrors();

				## LOAD DATA TEMPLATE ##
		    	
				$ldata['content'] = $this->load->view($this->router->class.'/register_ukm',$tdata, true);
				$this->load->sharedView('register', $ldata);
		    }
		}else{
			## LOAD DATA TEMPLATE ##
			
			## LOAD LAYOUT ##
			$ldata['content'] = $this->load->view($this->router->class.'/register_ukm',$tdata, true);
			$this->load->sharedView('register', $ldata);
		}
		
	}

	function confirmation($key = '')
	{
		$tdata = $fdata = array();
		$this->pagename = "Confirmation";
		
		$doValidate = $this->MembersModel->confirmation($key);
		if($doValidate == 'success'){
			$tdata['message'] = $this->lang->line('msg_success_confirmation');
			$tdata['status'] = 'success';
		}else{
			$tdata['message'] = $this->lang->line('msg_error_confirmation');
			$tdata['status'] = 'error';
		}		
		
		## LOAD DATA TEMPLATE ##
		$ldata['informationpage'] = $this->InformationpageModel->listData();
		
		## LOAD LAYOUT ##
		$ldata['content'] = $this->load->view($this->router->class.'/confirmation',$tdata, true);
		$this->load->sharedView('template', $ldata);
	}

	function send_email_code_member($email, $name, $code){
		$this->configuration = $this->ShopconfigurationModel->listData();
		$this->load->library('email');

		$this->email->initialize(array(
		  'protocol' => 'smtp',
		  'smtp_host' => HOST_EMAIL,
		  'smtp_user' => USER_EMAIL,
		  'smtp_pass' => PASS_EMAIL,
		  'smtp_port' => PORT_EMAIL,
		  'crlf' => "\r\n",
		  'newline' => "\r\n",
		  'mailtype'  => 'html', 
           'charset'   => 'iso-8859-1'
		));


		$this->email->from('noreply@snekbook.com', 'Snekbook Admin');
		$this->email->to($email);

		$this->email->subject($this->lang->line('email_members_register_verification_code'));
		$message = $this->load->view($this->router->class.'/email_code_body', array(
																					"code" => $code
																					,"name" => $name
																				), TRUE);
		$this->email->message($message);
		$this->email->send();
	}

	function send_email_reg_ukm($email, $name){
		$this->configuration = $this->ShopconfigurationModel->listData();
		$this->load->library('email');

		$this->email->initialize(array(
		  'protocol' => 'smtp',
		  'smtp_host' => HOST_EMAIL,
		  'smtp_user' => USER_EMAIL,
		  'smtp_pass' => PASS_EMAIL,
		  'smtp_port' => PORT_EMAIL,
		  'crlf' => "\r\n",
		  'newline' => "\r\n",
		  'mailtype'  => 'html', 
           'charset'   => 'iso-8859-1'
		));


		$this->email->from('noreply@snekbook.com', 'Snekbook Admin');
		$this->email->to($email);

		$this->email->subject($this->lang->line('email_members_register_ukm'));
		$message = $this->load->view($this->router->class.'/email_registrasi_ukm', array(
																					"name" => $name
																				), TRUE);
		$this->email->message($message);
		$this->email->send();
	}

	function member_fb($fbid = '')
	{
		if($fbid == 0){
			redirect($_SERVER['HTTP_REFERER']);
			die();
		}
		$tdata = array();
		$tdata['fbid'] = $fbid;
		
		$this->pagename = $this->lang->line('register_member');	
		$this->module_name = $this->lang->line('label_register_member');
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');

		### FB LOGIN UNTU URL LOGIN ###
		FacebookSession::setDefaultApplication(fb_app_id, fb_app_secret);
		$helper = new FacebookRedirectLoginHelper("http://".APP_DOMAIN."/login/member_fblogin/");
		$tdata['fb_login_url'] = $helper->getLoginUrl( array( 'scope' => fb_required_scope ) );
		$tdata['fb_login_url'] = isset($helper)?$helper->getLoginUrl( array( 'scope' => fb_required_scope ) ):'';
		#### END #######
		
		if($_POST){
			
			$validator = new FormValidator();
		    $validator->addValidation("fullname","req",$this->lang->line('error_empty_name_members'));
		    $validator->addValidation("phone","req",$this->lang->line('error_empty_phone_members'));
		    $validator->addValidation("fbid","req",$this->lang->line('error_empty_fbid_members'));
		    $validator->addValidation("email","req",$this->lang->line('error_empty_email_members'));
		    $validator->addValidation("email","email",$this->lang->line('error_wrong_email_members'));
			$validator->addValidation("captcha","req",$this->lang->line('error_empty_captcha'));
			
		    if($validator->ValidateForm())
		    {
		    	if ($this->session->userdata('captcha') == $this->input->post('captcha')) {
			    	$name = $this->input->post('fullname');
					$email = $this->input->post('email');
					$password = "socmedlogin";
			        $doInsert = $this->MembersModel->entriData(array(
															        'fullname'=>$this->input->post('fullname')
															        ,'phone'=>$this->input->post('phone')
															        ,'email'=>$this->input->post('email')
															        ,'password'=> "socmedlogin"
															        ,'fbid'=>$fbid
															  ));
					if($doInsert == 'empty'){
						$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
					}else if($doInsert == 'exist_email'){
						$tdata['error_hash'] = array('error' => $this->lang->line('error_duplicate_email'));
					}else if($doInsert == 'failed'){
						$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
					}else if($doInsert == 'not_same'){
						$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_password_not_same'));
					}else if($doInsert == 'success'){

						try
						{
						  // In case it comes from a redirect login helper
						  $session = $helper->getSessionFromRedirect();
						}
						catch( FacebookRequestException $ex )
						{
						  // When Facebook returns an error
						  echo $ex;
						}
						catch( Exception $ex )
						{
						  // When validation fails or other local issues
						  echo $ex;
						}

						// see if we have a session in $_Session[]
						if( isset($_SESSION['token']))
						{
						    // We have a token, is it valid?
						    $session = new FacebookSession($_SESSION['token']);
						    try
						    {
						        $session->Validate(fb_app_id, fb_app_secret);
						    }
						    catch( FacebookAuthorizationException $ex)
						    {
						        // Session is not valid any more, get a new one.
						        $session ='';
						    }
						}
						if ($session) {
							// sharing link on the user timeline
							$postRequest = new FacebookRequest($session, 'POST', '/me/feed', array('link' => 'http://www.arsiteki.com', 'description' => 'Arsiteki.com', 'message' => 'Buruan Gabung di Arsiteki - Arsitektur dan Desain Interior Indonesia'));
							$postResponse = $postRequest->execute();
							$posting = $postResponse->getGraphObject();
						}
						$tdata['success'] = $this->lang->line('msg_success_registration');
						if($this->send_email_code_member($email, $name, md5($password.$email))){
							redirect(base_url().$this->router->class.'/thank');
						}
					}
						if($doInsert !='success'){
							$tdata['fullname']   = $this->input->post('fullname');
							$tdata['phone']      = $this->input->post('phone');
							$tdata['email']      = $this->input->post('email');
							$tdata['password']   = $this->input->post('password');
						}
						
					## LOAD DATA TEMPLATE ##
					
					$tdata['fb_login_url'] = $helper->getLoginUrl( array( 'scope' => fb_required_scope ) );
					
					$ldata['content'] = $this->load->view($this->router->class.'/member_socmed',$tdata, true);
					$this->load->sharedView('template', $ldata);
				} else {
					$tdata['fullname']   = $this->input->post('fullname');
					$tdata['phone']      = $this->input->post('phone');
					$tdata['email']      = $this->input->post('email');
					$tdata['password']   = $this->input->post('password');

					$tdata['error_hash'] = array('error_captcha' => $this->lang->line('error_invalid_captcha'));
					## LOAD DATA TEMPLATE ##
					
					$tdata['fb_login_url'] = $helper->getLoginUrl( array( 'scope' => fb_required_scope ) );
					
					$ldata['content'] = $this->load->view($this->router->class.'/member_socmed',$tdata, true);
					$this->load->sharedView('template', $ldata);
				}
		    }
		    else
		    {
		        $tdata['fullname']     = $this->input->post('fullname');
			    $tdata['phone'] = $this->input->post('phone');
			    $tdata['email'] = $this->input->post('email');
			    $tdata['password'] = $this->input->post('password');
		    	$tdata['error_hash'] = $validator->GetErrors();

		    	## LOAD DATA TEMPLATE ##
				
				$tdata['fb_login_url'] = $helper->getLoginUrl( array( 'scope' => fb_required_scope ) );
		    	
				$ldata['content'] = $this->load->view($this->router->class.'/member_socmed',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{
			## LOAD DATA TEMPLATE ##
			
			$tdata['fb_login_url'] = $helper->getLoginUrl( array( 'scope' => fb_required_scope ) );
			
			## LOAD LAYOUT ##
			$ldata['content'] = $this->load->view($this->router->class.'/member_socmed',$tdata, true);
			$this->load->sharedView('template', $ldata);
		}
		
	}
}