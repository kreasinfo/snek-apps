<?php
class Notfoundpage extends CI_Controller {
	var $webconfig;
	var $pagename;
	var $configuration;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->lang->load('global', 'bahasa');

	
		$this->load->library('session');
		$this->webconfig = $this->config->item('webconfig');
		$this->pagename = '404 Not Found Page';
		$this->configuration = $this->ShopconfigurationModel->listData();	
	}

	function index()
	{
		$tdata = array();

		## LOAD LAYOUT ##
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		$ldata['informationpage'] = $this->InformationpageModel->listData();
		
		$this->load->sharedView('notfound', $ldata);
	}
	
}