

<div class="container">
    <div class="row margin-top-40">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="bg-white" style="min-height:400px;">
                <center><img src="<?php echo $this->webconfig['frontend_template']; ?>images/404-Error.png" class="img-responsive margin-top-40" width="180" /></center>
                <div class="ft-error">404 ERROR PAGE</div>
                <p class="des-error">Halaman yang anda cari tidak di temukan silahakn periksa kembali <br /> Klik Button dibawah untuk ke halaman utama</p>
                <center><a href="<?php echo base_url(); ?>" class="btn-error">HALAMAN UTAMA</a></center>
            </div>  
        </div>
    </div>
</div>