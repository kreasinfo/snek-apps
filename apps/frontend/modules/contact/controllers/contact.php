<?php
class Contact extends CI_Controller {
	var $webconfig;
	var $pagename;
	var $configuration;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('phpmailer');
		$this->load->library('session');
		$this->lang->load('global', 'bahasa');

		
		$this->webconfig = $this->config->item('webconfig');
		$this->pagename = $this->lang->line('contact_us');
		$this->configuration = $this->ShopconfigurationModel->listData();	
	}

	function index()
	{
		$tdata = array();
		
		## LOAD LAYOUT ##
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		$ldata['informationpage'] = $this->InformationpageModel->listData();
		
		$this->load->sharedView('template', $ldata);
	}

	function post_contact()
	{
		$tdata = array();
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
		if($_POST)
		{
			if ($this->session->userdata('captcha') == $this->input->post('captcha')) {
				$validator = new FormValidator();
			    $validator->addValidation("nama","req",'Silahkan mengisi nama Anda.');
			    $validator->addValidation("message","req",'Silahkan mengisi pesan Anda');
			    $validator->addValidation("subject","req",'Silahkan mengisi subject Anda');
			    $validator->addValidation("email","req",'Silahkan mengisi email Anda');
			    $validator->addValidation("email","email",'Silahkan mengisi email Anda dengan benar');
				
			    if($validator->ValidateForm())
			    {
			    	$ldata = array();
			    	$this->send_email_code_member($_POST['email'], $_POST['nama'], $_POST['message']);
			    	$tdata['success'] = $this->lang->line('msg_success_email');
			    	$tdata['nama']   = $this->input->post('nama');
					$tdata['message']      = $this->input->post('message');
					$tdata['subject']      = $this->input->post('subject');
					$tdata['email']   = $this->input->post('email');
					$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
					$this->load->sharedView('template', $ldata);
			    }else{
			    	$ldata = array();
			    	$tdata['nama']   = $this->input->post('nama');
					$tdata['message']      = $this->input->post('message');
					$tdata['subject']      = $this->input->post('subject');
					$tdata['email']   = $this->input->post('email');
					
			    	$tdata['error_hash'] = $validator->GetErrors();
					$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
					$this->load->sharedView('template', $ldata);
			    }
		    }else{
		    	$ldata = array();
		    	$tdata['error_hash'] = array('error_captcha' => $this->lang->line('error_invalid_captcha'));
				$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}
	}

	
	function send_email_code_member($email, $name, $message){
		send_email(array(
						'email_tujuan' => $this->configuration['email_contact']
						,'email_tujuan_text' => 'Admin Website'
						,'addreply_email' => $email
						,'subject' => 'Pesan Baru dari Form Kontak Website'
						,'setfrom_email' => SENDER_EMAIL
						,'setfrom_text' => $name
						,'body' => $this->load->view($this->router->class.'/email_code_body', 
																							array(
																								"message" => $message
																								,"name" => $name
																								,"email" => $email
																							), 
																							true)
						));
	}
}