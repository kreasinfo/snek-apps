<?php if(isset($error_hash) && count($error_hash) > 0){ ?>
    <?php foreach($error_hash as $inp_err){ ?>
        <script type="text/javascript">
        jQuery(document).ready(function($) {
            toastr.error("<?php echo $inp_err; ?>");
            });
        </script>
    <?php } ?>
<?php } ?>
<?php if(isset($success)){ ?>
    <script type="text/javascript">
    jQuery(document).ready(function($) {
        toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
        });
    </script>
<?php } ?>
<div class="container">
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a></li>
      <li class="active">Kontak</li>
    </ol>
</div>

<div class="bg-detail">
    <div class="container">
        <center><div class="head-detail">HUBUNGI KAMI</div></center>
    </div>
</div>

<div class="container"><!--- Container -->
    <div class="row margin-top-40">
        <div class="col-lg-8 col-lg-offset-2">
            <form id="contactform" method="post" action="<?php echo base_url().$this->router->class; ?>/post_contact">
            <div class="row"><!-- Row Form -->
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInput">Nama* :</label>
                        <input type="text" class="form-control" placeholder="Tulis Nama Disini" name="nama" value="<?php echo isset($nama)?$nama:'';?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInput">Email* :</label>
                        <input type="email" class="form-control" placeholder="Tulis Email Disini" name="email" value="<?php echo isset($email)?$email:'';?>">
                    </div>
                </div>
            </div><!-- End Row Form -->
            
            <div class="row"><!-- Row Form -->
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInput">Subjek* :</label>
                        <input type="text" class="form-control" placeholder="Tulis Subjek Disini" name="subject" value="<?php echo isset($subject)?$subject:'';?>">
                    </div>
                </div>
            </div><!-- End Row Form -->
            
            <div class="row"><!-- Row Form -->
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInput">Pesan* :</label>
                        <textarea class="form-control" placeholder="Tulis Pesan Disini" name="message"><?php echo isset($message)?$message:'';?></textarea>
                    </div>
                </div>
            </div><!-- End Row Form -->
            
            <div class="row"><!-- Row Form -->
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4"><img id="img" src="<?php echo base_url(); ?>captcha/" class="img-responsive captcha"/></div>
                            <div class="col-xs-8"><input type="text" class="form-control" name="captcha" placeholder="captcha" value="<?php echo isset($captcha)?$captcha:'';?>"/></div>
                        </div>
                    </div>
                </div>
            </div><!-- End Row Form -->
            
            <div class="row" style="margin-bottom: 40px;"><!-- Row Form -->
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12"><input type="submit" class="btn btn-kontak btn-block" value="KIRIM PESAN" /></div>
                        </div>
                    </div>
                </div>
            </div><!-- End Row Form -->
            </form>
        </div>
    </div>
</div><!--- Container -->