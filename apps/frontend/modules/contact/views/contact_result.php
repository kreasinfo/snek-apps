<?php if(isset($error_hash) && count($error_hash) > 0){ ?>
    <?php foreach($error_hash as $inp_err){ ?>
        <div class="nicdark_alerts nicdark_bg_red nicdark_radius nicdark_shadow">
            <p class="white nicdark_size_big">
                <i class="icon-cancel-circled-outline iconclose"></i>
                    <strong><?php echo $this->lang->line('error_notif'); ?>:</strong> <?php echo $inp_err; ?>
                </p>
            <i class="icon-warning-empty nicdark_iconbg right big yellow"></i>
        </div>
        <div class="nicdark_space20"></div>
    <?php } ?>
<?php } ?>

<?php if(isset($success)){ ?>
    <div class="nicdark_alerts nicdark_bg_green nicdark_radius nicdark_shadow">
        <p class="white nicdark_size_big">
            <i class="icon-cancel-circled-outline iconclose"></i>
                <strong><?php echo $this->lang->line('success_notif'); ?>:</strong> <?php echo $success; ?>
            </p>
        <i class="icon-warning-empty nicdark_iconbg right big yellow"></i>
    </div>
    <div class="nicdark_space20"></div>
<?php } ?>