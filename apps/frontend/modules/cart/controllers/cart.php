<?php

class Cart extends CI_Controller {
	var $webconfig;
	var $pagename;
	var $configuration;

	function __construct()

	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->lang->load('global', 'bahasa');
		$this->load->helper('phpmailer');
		$this->load->sharedModel('ProdukModel');
		$this->load->sharedModel('LoginModel');
		$this->load->sharedModel('SalesModel');
		$this->load->sharedModel('BankaccountModel');
		$this->paymentbanks = $this->BankaccountModel->listData();
		$this->load->library('session');
		$this->webconfig = $this->config->item('webconfig');
		$this->configuration = $this->ShopconfigurationModel->listData();

	}

	function email_konfirmasi(){

		$this->load->sharedModel('SalesModel');	

		# GET SESSION #

		$tdata['listdata'] = $this->SalesModel->listData(array('id' => $this->session->userdata('idsales')));

		$this->load->view($this->router->class.'/email_konfirmasi_order', $tdata);

	}

	function kasir(){	

		$this->load->sharedModel('MembersModel');

		if(!$this->LoginModel->isLoggedIn()){

			redirect(base_url());

		}

			

		$tdata = array();



		# LOAD MEMBER DATA #

		$tdata['members'] = $this->MembersModel->getUserdata(array(

												'id' => $this->session->userdata('memberid')

											));		

		

		# GET SESSION #

		if($this->session->userdata('cart') != ''){

			$tdata['listdata'] = $this->session->userdata('cart');

		}else{

			$tdata['listdata'] = array();

		}

		if(count($tdata['listdata']) == 0){

			redirect(base_url());

		}



		if($this->session->userdata('tipe_pengiriman') !=''){

			$tdata['tipe_pengiriman'] = $this->session->userdata('tipe_pengiriman');

			$tdata['tipe_pengiriman_text'] = $this->session->userdata('tipe_pengiriman_text');



			$tdata['pengiriman'] = $this->session->userdata('pengiriman');

			$tdata['pengiriman_text'] = $this->session->userdata('pengiriman_text');

		}



		if($this->session->userdata('custom_ongkir') !=''){

			$tdata['custom_ongkir'] = $this->session->userdata('custom_ongkir');

		}

		

		if($_POST){

			if($this->input->post('custom_ongkir') == '0'){

				require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');

				$validator = new FormValidator();

				$validator->addValidation("client_name","req","Silahkan mengisi nama pembeli");

				$validator->addValidation("client_postcode","req","Silahkan mengisi kode pos");

				$validator->addValidation("client_email","email","Silahkan mengisi alamat email yang benar");

				$validator->addValidation("client_address","req","Silahakan mengisi alamat pembeli");

				$validator->addValidation("client_phone","req","Silahkan mengisi no telepon atau handphone");

			}else{

				require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');

				$validator = new FormValidator();

				$validator->addValidation("products","req","Anda tidak memiliki produk");

				$validator->addValidation("size","req","Produk Anda tidak memiliki size");

				$validator->addValidation("quantity","numberwendy","Kuantitas produk harus lebih dari 0");

				$validator->addValidation("client_name","req","Silahkan mengisi nama pembeli");

				$validator->addValidation("client_postcode","req","Silahkan mengisi kode pos");

				$validator->addValidation("client_email","email","Silahkan mengisi alamat email yang benar");

				$validator->addValidation("client_address","req","Silahakan mengisi alamat pembeli");

				$validator->addValidation("client_phone","req","Silahkan mengisi no telepon atau handphone");

				$validator->addValidation("tipe_pengiriman","req","Silahkan memilih tipe pengiriman");

				$validator->addValidation("pengiriman","req","Silahkan memilih wilayah pengiriman");

				$validator->addValidation("post_name","req","Silahkan mengisi nama penerima");

				$validator->addValidation("post_postcode","req","Silahakan mengisi kode pos penerima");

				$validator->addValidation("post_email","req","Silahkan mengisi email penerima");

				$validator->addValidation("post_email","email","Silahkan mengisi alamat email yang benar");

				$validator->addValidation("post_address","req","Silahakn mengisi alamat penerima");

				$validator->addValidation("post_phone","req","Silahakn mengisi no telepon atau handphone penerima");

			}



			

			if($validator->ValidateForm())

			{

				$doUpdate = $this->SalesModel->entriData(array(

																'client_name'      => $this->input->post('client_name')

																,'client_address'  => $this->input->post('client_address')

																,'client_postcode' => $this->input->post('client_postcode')

																,'client_phone'    => $this->input->post('client_phone')

																,'client_email'    => $this->input->post('client_email')

																,'post_name'       => $this->input->post('post_name')

																,'post_address'    => $this->input->post('post_address')

																,'post_postcode'   => $this->input->post('post_postcode')

																,'post_phone'      => $this->input->post('post_phone')

																,'post_email'      => $this->input->post('post_email')

																,'tipe_pengiriman' => $this->input->post('tipe_pengiriman')

																,'city'    		   => $this->input->post('pengiriman')

																,'products'        => $this->input->post('products')

																,'quantity'        => $this->input->post('quantity')

																,'productprice'    => $this->input->post('productprice')

																,'productname'     => $this->input->post('productname')

																,'productberat'     => $this->input->post('berat')

																,'discount'     => $this->input->post('discount')

																,'userid'     		=> $this->input->post('userid')

																,'size'     		=> $this->input->post('size')

																,'path'     		=> $this->input->post('path')

																,'custom_ongkir'     		=> $this->input->post('custom_ongkir')

																,'gran_total'     		=> $this->input->post('gran_total')

															  ));

				

				if($doUpdate == 'empty'){

					$tdata['error_hash'] = array('error' => 'Silahkan periksa kembali isian anda.');

					

					## LOAD LAYOUT ##

					$ldata['content'] = $this->load->view($this->router->class.'/kasir',$tdata, true);

					$ldata['informationpage'] = $this->InformationpageModel->listData();

					$this->load->sharedView('template', $ldata);



				}else if($doUpdate == 'failed'){

					$tdata['error_hash'] = array('error' => 'Mohon maaf, telah terjadi kesalahan didalam sistem kami.');

					

					## LOAD LAYOUT ##

					$ldata['content'] = $this->load->view($this->router->class.'/kasir',$tdata, true);

					$ldata['informationpage'] = $this->InformationpageModel->listData();

					$this->load->sharedView('template', $ldata);



				}else if($doUpdate == 'success'){

					## KIRIM EMAIL ##

					$edata = array();

					$edata['listdata'] = $this->SalesModel->listData(array('id' => $this->session->userdata('idsales')));

					$this->session->unset_userdata('cart');

					$this->session->unset_userdata('tipe_pengiriman');

					$this->session->unset_userdata('tipe_pengiriman_text');

					$this->session->unset_userdata('pengiriman');

					$this->session->unset_userdata('pengiriman_text');

					$this->send_email_konfirmasi($edata, $this->input->post('client_email'), $this->input->post('client_name'));

					redirect(base_url().$this->router->class.'/konfirmasi/');

					

					

				} 	

			}

			else

			{					

				$tdata['error_hash'] = $validator->GetErrors();

				$tdata['client_name']     = $this->input->post('client_name');

				$tdata['client_address']     = $this->input->post('client_address');

				$tdata['client_postcode']     = $this->input->post('client_postcode');

				$tdata['client_phone']     = $this->input->post('client_phone');

				$tdata['client_email']     = $this->input->post('client_email');

				$tdata['post_name']     = $this->input->post('post_name');

				$tdata['post_address']     = $this->input->post('post_address');

				$tdata['tipe_pengiriman']     = $this->input->post('tipe_pengiriman');

				$tdata['tipe_pengiriman_text']     = $this->input->post('tipe_pengiriman_text');

				$tdata['pengiriman']     = $this->input->post('pengiriman');

				$tdata['pengiriman_text']     = $this->input->post('pengiriman_text');

				$tdata['post_postcode']     = $this->input->post('post_postcode');

				$tdata['post_phone']     = $this->input->post('post_phone');

				$tdata['post_email']     = $this->input->post('post_email');

				

				$tdata['error_hash'] = $validator->GetErrors();

				

				## LOAD LAYOUT ##

				$ldata['content'] = $this->load->view($this->router->class.'/kasir',$tdata, true);

				$ldata['informationpage'] = $this->InformationpageModel->listData();

				$this->load->sharedView('template', $ldata);

			}

		}else{

			## LOAD LAYOUT ##

			$ldata['content'] = $this->load->view($this->router->class.'/kasir',$tdata, true);

			$ldata['informationpage'] = $this->InformationpageModel->listData();

			$this->load->sharedView('template', $ldata);

		}

		

	}

	function konfirmasi(){

		$this->pagename = 'KONFIRMASI PEMESANAN';	



		# GET SESSION #

		$tdata = array();



		## LOAD LAYOUT ##

		$ldata['content'] = $this->load->view($this->router->class.'/thankyou',$tdata, true);

		$ldata['informationpage'] = $this->InformationpageModel->listData();

		$this->load->sharedView('template', $ldata);

	}

	function view(){

		$this->pagename = 'DAFTAR BELANJA';	



		# GET SESSION #

		$tdata = array();

		if($this->session->userdata('cart') != ''){

			$tdata['listdata'] = $this->session->userdata('cart');

		}else{

			$tdata['listdata'] = array();

		}

		if(count($tdata['listdata']) == 0){

			redirect(base_url());

		}

		

		if($this->session->userdata('tipe_pengiriman') !=''){

			$tdata['tipe_pengiriman'] = $this->session->userdata('tipe_pengiriman');

			$tdata['tipe_pengiriman_text'] = $this->session->userdata('tipe_pengiriman_text');



			$tdata['pengiriman'] = $this->session->userdata('pengiriman');

			$tdata['pengiriman_text'] = $this->session->userdata('pengiriman_text');

		}

		

		if($this->session->userdata('custom_ongkir') !=''){

			$tdata['customongkos'] = $this->session->userdata('custom_ongkir');

		}

		

		

		## LOAD LAYOUT ##

		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);

		$ldata['informationpage'] = $this->InformationpageModel->listData();

		$this->load->sharedView('template', $ldata);

	}

	function view_japri(){
		$this->pagename = 'DAFTAR BELANJA';	
		# GET SESSION #
		$tdata = array();
		if($this->session->userdata('cart') != ''){
			$tdata['listdata'] = $this->session->userdata('cart');
		}else{
			$tdata['listdata'] = array();
		}

		if(count($tdata['listdata']) == 0){
			redirect(base_url());
		}

		if($this->session->userdata('tipe_pengiriman') !=''){
			$tdata['tipe_pengiriman'] = $this->session->userdata('tipe_pengiriman');
			$tdata['tipe_pengiriman_text'] = $this->session->userdata('tipe_pengiriman_text');
			$tdata['pengiriman'] = $this->session->userdata('pengiriman');
			$tdata['pengiriman_text'] = $this->session->userdata('pengiriman_text');
		}

		if($this->session->userdata('custom_ongkir') !=''){
			$tdata['customongkos'] = $this->session->userdata('custom_ongkir');
		}

		## LOAD LAYOUT ##

		$ldata['content'] = $this->load->view($this->router->class.'/index_japri',$tdata, true);
		$ldata['informationpage'] = $this->InformationpageModel->listData();
		$this->load->sharedView('template', $ldata);
	}

	

	function addproduct()

	{		

		$pid = $this->input->post('productid');

		$qty = $this->input->post('quantity');

		$size = $this->input->post('size');

		

		$productlist = array();

		$productlistsesion = $this->session->userdata('cart');

		$product = $this->ProdukModel->getData(array('id' => $pid));

		if(count($product) > 0){

			if($product['stock'] < $qty){

				echo 'stock';

				return;

			}else{	

				if(is_array($productlistsesion)){

					if($this->product_exists($size)){

						echo 'exist';

						return;

					}

					$max = count($productlistsesion);

								

					$productlist[$max]['id']    =  $product['id'];

					$productlist[$max]['name']  =  $product['name'];

					$productlist[$max]['price'] =  $product['price'];

					$productlist[$max]['berat'] =  $product['berat'];

					$productlist[$max]['slug'] =  $product['slug'];

					$productlist[$max]['discount'] =  $product['discount'];

					$productlist[$max]['rasa'] =  $product['produk_ukuran'][0]['size'];

					$productlist[$max]['quantity'] =  $qty;

					$productlist[$max]['size'] =  $size;

					$productlist[$max]['status_produk'] =  $product['status_produk'];

					$productlist[$max]['img'] =  isset($product['images'][0]['path'])?$product['images'][0]['path']:'';

					$newproductlist['cart'] = array_merge($productlist, $productlistsesion);



					$outputArray = array(); 

					$keysArray = array(); 

					foreach ($newproductlist['cart'] as $innerArray) {

						if (!in_array($innerArray['status_produk'], $keysArray)) { 

							$keysArray[] = $innerArray['status_produk'];

							$outputArray[] = $innerArray; 

						}

					}

					$resultarray = count($outputArray);

					if($resultarray == '1'){

						$this->session->set_userdata($newproductlist);

					}else{

						echo 'notequal';

						return;

					}

				}

				else{				

					$productlist['cart'][0]['id']    =  $product['id'];

					$productlist['cart'][0]['name']  =  $product['name'];

					$productlist['cart'][0]['price'] =  $product['price'];

					$productlist['cart'][0]['berat'] =  $product['berat'];	

					$productlist['cart'][0]['slug'] =  $product['slug'];	

					$productlist['cart'][0]['discount'] =  $product['discount'];	

					$productlist['cart'][0]['rasa'] =   $product['produk_ukuran'][0]['size'];

					$productlist['cart'][0]['quantity'] =  $qty;

					$productlist['cart'][0]['size'] =  $size;

					$productlist['cart'][0]['status_produk'] =  $product['status_produk'];

					$productlist['cart'][0]['img'] =  isset($product['images'][0]['path'])?$product['images'][0]['path']:'';

					$newproductlist	= $productlist;

					$this->session->set_userdata($newproductlist);

				}

					

			}	

		}

		echo 'success';

	}

	function product_exists($size){

		$size = intval($size);

		$productlist = $this->session->userdata('cart');

		$max = count($productlist);

		$flag = 0;

		for($i = 0; $i < $max; $i++){

			if($size == $productlist[$i]['size']){

				$flag = 1;

				break;

			}

		}

		return $flag;

	}

	function remove_product(){

		$pid = $this->input->post('productid');

		$pid = intval($pid);		

		$productlist = $this->session->userdata('cart');	

		$max = count($productlist);

		for($i = 0; $i < $max; $i++){

			if($pid == $productlist[$i]['id']){						

				unset($productlist[$i]);

				break;

			}

		}

		

		$newproductlist['cart']	= array_values($productlist);

		$this->session->unset_userdata('cart');

		$this->session->set_userdata($newproductlist);

		

	}

	function update_qty(){

		$pid = $this->input->post('productid');

		$qty = $this->input->post('qty');



		$pid = intval($pid);		



		$productlist = $this->session->userdata('cart');	



		$max = count($productlist);

		for($i = 0; $i < $max; $i++){

			if($pid == $productlist[$i]['size']){					

				$productlist[$i]['quantity'] = $qty;

				break;

			}

		}

		

		$newproductlist['cart']	= array_values($productlist);

		$this->session->unset_userdata('cart');

		$this->session->set_userdata($newproductlist);

		

	}



	function countdaftarbelanja(){

		if($this->session->userdata('cart') != ''){

			echo count($this->session->userdata('cart'));

		}else{

			echo 0;

		}

	}



	function countdaftarbelanja_list(){

		if($this->session->userdata('cart') != ''){

			echo "<i class='fa fa-tags'></i> ".count($this->session->userdata('cart'))." Produk";

		}else{

			echo 0;

		}

	}

	function getShippingPrice(){

		$this->load->sharedModel('PengirimanModel');

		echo $this->PengirimanModel->getPrice($_POST['idpengiriman']);

	}



	function send_email_konfirmasi($data, $email, $name){

		$this->load->library('email');



		$this->email->initialize(array(

		  'protocol' => 'smtp',

		  'smtp_host' => HOST_EMAIL,

		  'smtp_user' => USER_EMAIL,

		  'smtp_pass' => PASS_EMAIL,

		  'smtp_port' => PORT_EMAIL,

		  'crlf' => "\r\n",

		  'newline' => "\r\n",

		  'mailtype'  => 'html', 

          'charset'   => 'iso-8859-1'

		));





		$this->email->from('noreply@snekbook.com', 'Snekbook Admin');

		$this->email->to($email);



		$this->email->subject($this->lang->line('email_order_confirmation'));

		$message = $this->load->view($this->router->class.'/email_konfirmasi_order',$data, TRUE);

		$this->email->message($message);

		$this->email->send();

	}



	function services(){

		$this->session->set_userdata('tipe_pengiriman',$_POST['id']);
		
		if($_POST['id'] == ''){

			$this->session->unset_userdata('tipe_pengiriman');

			$this->session->unset_userdata('tipe_pengiriman_text');

			$this->session->unset_userdata('pengiriman');

			$this->session->unset_userdata('pengiriman_text');

		}else{

			$this->session->set_userdata('tipe_pengiriman_text',$_POST['text']);

			$this->session->unset_userdata('custom_ongkir');

		}

	}



	function services_city(){

		$this->session->set_userdata('pengiriman',$_POST['pengiriman']);

		$this->session->set_userdata('pengiriman_text',$_POST['pengiriman_text']);

	}



	function customservices(){

		$this->session->set_userdata('custom_ongkir',$_POST['custom_price']);

		

		if($_POST['custom_price'] !=''){

			$this->session->unset_userdata('tipe_pengiriman');

			if($_POST['custom_price'] == '0'){

				$this->session->unset_userdata('tipe_pengiriman_text');

				$this->session->unset_userdata('pengiriman_text');

			}else{

				$this->session->set_userdata('tipe_pengiriman_text','Kirim ke tempat');

				$this->session->set_userdata('pengiriman_text','Kota Bandung');

			}

			

			$this->session->unset_userdata('pengiriman');

			

		}else{

			$this->session->unset_userdata('custom_ongkir');

		}

	}

	function choseorder(){
		$tdata = array();
		$this->load->view($this->router->class.'/choseorder',$tdata);
	}

	function chosecheckout(){
		$tdata = array();
		$this->load->view($this->router->class.'/chosecheckout',$tdata);
	}

}