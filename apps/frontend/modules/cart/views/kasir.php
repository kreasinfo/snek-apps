<script type="text/javascript">

jQuery(document).ready(function($) {

    $('#post_postcode').numeric();

    <?php if(isset($custom_ongkir) && $custom_ongkir !=''){?>

        ongkir();

    <?php }else{ ?>

        <?php if(isset($tipe_pengiriman)) { ?>

            // pengiriman('<?php echo $tipe_pengiriman; ?>');

            ongkir();

            $('.ambilditempat').hide();

        <?php } ?>

    <?php } ?>



    <?php if(isset($custom_ongkir) && $custom_ongkir == '0'){?>

        $('.datapengiriman').hide();

        $('.ambilditempat').show();

    <?php } ?>

    

    function pengiriman(id_tipe){

        if(id_tipe == ''){

            $('.pengiriman_block').html('&nbsp; &nbsp; &nbsp; <em>Silahkan pilih tipe pengiriman.</em>');

        }else{

            $('.pengiriman_block').html('');

            var html = '';

            var urlservices = '';

            html += '<input type="hidden" name="pengiriman" value="<?php echo isset($pengiriman)?$pengiriman:''; ?>" id="pengiriman" style="width:300px" class="input-xlarge" />';

            html += '<input type="hidden" name="pengiriman_text" value="<?php echo isset($pengiriman_text)?$pengiriman_text:''; ?>" id="pengiriman_text" />';

            $('.pengiriman_block').append(html);



            $("#pengiriman").select2({

                initSelection: function(element, callback) {

                    callback({id: '<?php echo isset($pengiriman)?$pengiriman:''; ?>', text: '<?php echo isset($pengiriman_text)?$pengiriman_text:''; ?>' });

                },

                ajax: {

                    url: '<?php echo base_url(); ?>webservices/pengiriman/'+id_tipe+'/',

                    dataType: 'json',

                    data: function (term, page) {

                        return {

                            q: term

                        };

                    },

                    results: function (data, page) {

                        return { results: data };

                    } ,

                    cache: false

                }

            }).on("change", function(e) {

              $('#pengiriman_text').val($('#s2id_pengiriman .select2-chosen').text());

              ongkir();

            });

        }

        

    }

    function ongkir(){

        var harga_pengiriman = 0;

        var berattotal = $(".berattotal_kasir").val();

        var pengiriman = $("#pengiriman").val();

        

        $.ajax({

            type: 'POST',

            url: "<?php echo base_url().$this->router->class; ?>/getShippingPrice/",

            data: {idpengiriman:pengiriman},

            success: function(response) {

                <?php if(isset($custom_ongkir) && $custom_ongkir !=''){?>

                    harga_pengiriman = <?php echo isset($custom_ongkir)?$custom_ongkir:'';?>;

                <?php }else{ ?>

                    harga_pengiriman = response;

                <?php } ?>



                var totalhrgaproduk = $("#totalhrgaproduk_kasir").val();

                var total_pengiriman = parseInt(harga_pengiriman) * parseInt(Math.ceil(berattotal/1000));

                

                var total_harga = parseInt(total_pengiriman) + parseInt(totalhrgaproduk);

                $("#total_pengiriman").text(total_pengiriman).formatCurrency({ decimalSymbol:',', digitGroupSymbol:'.', symbol:'IDR ' });

                $("#total_all").text(total_harga).formatCurrency({ digitGroupSymbol:'.', symbol:'IDR ' ,roundToDecimalPlace:'0' });

                $("#gran_total").val(total_harga);

                $("#total_pengirimansidemenu_kasir").text(total_pengiriman).formatCurrency({ digitGroupSymbol:'.', symbol:'IDR ' ,roundToDecimalPlace:'0' });

                if(harga_pengiriman !='0'){

                     $(".totalongkirdetail").html('Ongkir : ('+Math.ceil(berattotal)+' gr) '+parseInt(Math.ceil(berattotal/1000))+'Kg x '+parseInt(harga_pengiriman));

                }

            }

        })

    }

});

function submitkie(){

    document.pembelian.submit();

}

function copyfield(){

    $("#post_name").attr({value:$("#client_name").val()});

    $("#post_address").val($("#client_address").val());

    $("#post_postcode").attr({value:$("#client_postcode").val()});

    $("#post_phone").attr({value:$("#client_phone").val()});

    $("#post_email").attr({value:$("#client_email").val()});

}

</script>



<div class="container">

    <ol class="breadcrumb">

        <li><a href="<?php echo base_url(); ?>">Home</a></li>

        <li><a href="<?php echo base_url(); ?>cart/view">Keranjang Belanja</a></li>

        <li class="active">Order</li>

    </ol>

</div>



<div class="border-bottom"></div>

<?php //echo json_encode($this->session->all_userdata());?>

<form name="pembelian" method="post" action="">

<?php if(isset($error_hash) && count($error_hash) > 0){ ?>

    <?php foreach($error_hash as $inp_err){ ?>

        <script type="text/javascript">

        jQuery(document).ready(function($) {

            toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");

            });

        </script>

    <?php } ?>

<?php } ?>

<?php if(isset($success)){ ?>

    <script type="text/javascript">

    jQuery(document).ready(function($) {

        toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");

        });

    </script>

<?php } ?>

<input type="hidden" name="userid" id="userid" readonly="readonly" value='<?php echo $members['id']?>' size="30" class="inputText" />

<div class="container">

    <div class="row margin-top-30">

        <div class="col-md-6">

            <div class="ttl-side"><b>Data<span class="ft-red">&nbsp;Pembeli</span></b></div>

            <div class="border-bottom margin-bottom-10"></div>

            <div class="row margin-top-20">

                <div class="col-md-6">

                    <label>Nama</label>

                    <input class="form-control" name="client_name" id="client_name" type="text" readonly value="<?php echo $members['fullname']?>">

                </div>

                <div class="col-md-6">

                    <label>Email</label>

                    <input class="form-control" name="client_email" id="client_email" type="text" readonly value="<?php echo $members['email']?>">

                </div>

                <div class="col-md-6">

                    <label>Alamat</label>

                    <textarea class="form-control" name="client_address" id="client_address" readonly><?php echo isset($members['address'])?$members['address']:'';?></textarea>

                </div>

                <div class="col-md-6">

                    <label>Telepon</label>

                    <input class="form-control" name="client_phone" id="client_phone" type="text" readonly value="<?php echo $members['phone']?>">

                </div>

                <div class="col-md-6 mb-15">

                    <label>Kodepos</label>

                    <input class="form-control" name="client_postcode" id="client_postcode" type="text" readonly value="<?php echo $members['postcode']?>">

                </div>

                <div class="col-md-6 mb-15">

                    <a class="btn btn-login btn-block" href="javascript:void(0)" onclick="copyfield()">Salin Data Pengiriman</a>

                </div>

            </div>

        </div>

        <div class="col-md-6">

            <div class="ttl-side"><b>Data<span class="ft-red">&nbsp;Pengiriman</span></b></div>

            <div class="border-bottom margin-bottom-10"></div>

            <div class="row margin-top-20 datapengiriman">

                <div class="col-md-6">

                    <label>Nama</label>

                    <input class="form-control" name="post_name" type="text" value="<?php echo isset($post_name)?$post_name:'';?>" id="post_name">

                </div>

                <div class="col-md-6">

                    <label>Email</label>

                    <input class="form-control" name="post_email" type="text" value="<?php echo isset($post_email)?$post_email:'';?>" id="post_email">

                </div>

                <div class="col-md-6">

                    <label>Alamat</label>

                    <textarea name="post_address" class="form-control" id="post_address"> <?php echo isset($post_address)?$post_address:'';?></textarea>

                </div>

                <div class="col-md-6">

                    <label>Telepon</label>

                    <input class="form-control" name="post_phone" type="text" value="<?php echo isset($post_phone)?$post_phone:'';?>" id="post_phone">

                </div>

                <div class="col-md-6">

                    <label>Kodepos</label>

                    <input class="form-control" name="post_postcode" type="text" value="<?php echo isset($post_postcode)?$post_postcode:'';?>" id="post_postcode">

                </div>

                <div class="col-md-6">

                    <label>Jenis Pengiriman</label>

                    <input class="form-control" name="tipe_pengiriman_text" type="text" value="<?php echo $this->session->userdata('tipe_pengiriman_text');?>" id="post_postcode" readonly>

                    <input type="hidden" name="tipe_pengiriman" value="<?php echo $this->session->userdata('tipe_pengiriman');?>" />

                </div>

                <div class="col-md-6">

                    <label>Kota</label>

                    <input class="form-control" name="pengiriman_text" type="text" value="<?php echo $this->session->userdata('pengiriman_text');?>" id="post_postcode" readonly>

                    <input type="hidden" name="pengiriman" value="<?php echo $this->session->userdata('pengiriman');?>" />

                </div>

            </div>

            <div class="row margin-top-20 ambilditempat">

                <div class="col-md-12">

                    <label>Ambil di tempat</label><br>

                    Anda memilih untuk mengambil produk di toko kami berikut alamatnya :

                    <input type="hidden" value="<?php echo isset($custom_ongkir)?$custom_ongkir:'';?>" name="custom_ongkir"/>

                </div>

                <div class="col-md-12">

                    <div class="head-border margin-top-20">

                        <?php echo isset($this->configuration['address'])?$this->configuration['address']:'';?>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-md-12">

            <div class="border-bottom margin-top-10"></div>

        </div>

    </div>

</div>

<div class="container">

    <div class="head-border margin-top-20">PRODUK YANG DIBELI</div>

    <div class="box-border margin-top-10"><!-- barang -->

        <table class="table">

            <thead>

                <tr>

                    <th>Foto Produk</th>

                    <th>Nama Produk</th>

                    <th>Rasa</th>

                    <th>Harga</th>

                    <th><center>Berat (gr)</center></th>

                    <th><center>Jumlah</center></th>

                    <th>Sub Total</th>

                </tr>

            </thead>

            <tbody>

                <?php if(isset($listdata) && count($listdata) > 0){ ?>

                <?php foreach ($listdata as $key => $value) { ?>

                <tr>

                    <td><img src="<?php echo thumb_image($value['img'],'360x203','product'); ?>" class="img-responsive" width="120" /></td>

                    <td><?php echo $value['name']; ?></td>

                    <td><?php echo $value['rasa']; ?></td>

                    <td><?php echo rupiah($value['price']); ?></td>

                    <td><center><?php echo format_weight($value["berat"]);?></center></td>

                    <td><center><?php echo $value['quantity'];?></center></td>

                    <td><?php echo rupiah($value['quantity']*$value['price']); ?></td>

                </tr>

                <input type="hidden" name="quantity[]" id="quantity_<?php echo $value["id"];?>" value="<?php echo $value['quantity'];?>" />

                <input type="hidden" name="productprice[]" id="productprice_<?php echo $value["id"];?>" value="<?php echo $value["price"]- ($value['price'] * ($value['discount']/100));?>" />

                <input type="hidden" name="products[]" value="<?php echo $value["id"];?>"  />

                <input type="hidden" name="productname[]" value="<?php echo $value["name"];?>"  />

                <input type="hidden" name="discount[]" value="<?php echo $value["discount"];?>"  />

                <input type="hidden" name="path[]" value="<?php echo $value["img"];?>"  />

                <input type="hidden" name="berat[]" value="<?php echo $value["berat"];?>"  />

                <input type="hidden" name="size[]" value="<?php echo $value["size"];?>"  />

                <?php 

                    $totalhrgaproduk[] = ($value['price'] - ($value['price'] * ($value['discount']/100))) * $value['quantity']; 

                    $totalberat[] = $value['berat'] * $value['quantity']; 

                ?>

                <?php } ?>

                <?php } ?>

            </tbody>

        </table>

    </div><!-- End barang -->

    <div class="mb-15"></div>

</div>

<div class="container">

    <div class="row">

        <div class="col-lg-8"></div>

        <div class="col-lg-4">

            <div class="box-border">

                <div class="head-total">DETAIL ORDER</div>

                <div class="border-bottom margin-top-10"></div>

                <div class="row margin-top-10">

                    <div class="col-xs-8"><div class="pull-left">Total :</div></div> 

                    <div class="col-xs-4"><div class="pull-right totalharga_kasir" ><span class="ft-red"></span><?php echo rupiah(array_sum($totalhrgaproduk)); ?></div></div>

                    <input type="hidden" id="totalhrgaproduk_kasir" value="<?php echo array_sum($totalhrgaproduk); ?>"/>

                </div>

                

                <div class="row margin-top-10">

                    <div class="col-xs-8"><div class="pull-left totalongkirdetail">Ongkir :</div></div> 

                    <div class="col-xs-4">

                        <div class="pull-right" id="total_pengirimansidemenu_kasir"><span class="ft-red"></span>IDR 0</div>

                        <input type="hidden" class="berattotal_kasir" value="<?php echo array_sum($totalberat); ?>"/>

                        <input type="hidden" name="pengiriman" value="<?php echo isset($pengiriman)?$pengiriman:''; ?>" id="pengiriman" />

                        <input type="hidden" id="totalhargaongkir" value=""/>

                    </div>

                </div>

                

                <div class="border-bottom margin-top-10"></div>

                <div class="ft-total">

                    <div class="row" style="font-weight:bold;">

                        <div class="col-xs-6"><div class="pull-left">Grand Total :</div></div> 

                        <div class="col-xs-6"><div class="pull-right" id="total_all"><span class="ft-red">IDR.</span> 0</div>

                        <input type="hidden" id="gran_total" name="gran_total" value=""/>

                        </div>

                    </div>

                </div>

                <?php if($this->LoginModel->isLoggedIn()){ ?>

                    <a href="javascript:submitkie()" class="btn btn-login btn-block">Order Sekarang</a>

                    <!-- <a href="#" class="btn btn-login btn-block">Order Now</a> -->

                <?php }else{ ?>

                    <div class="alert">

                        <?php echo $this->lang->line('please_login_untuk_ke_kasir'); ?>

                    </div>

                <?php } ?>

            </div>

        </div>

    </div>

</div>

</form>

<div class="mb-15"></div>

<div class="margin-top-30"></div>