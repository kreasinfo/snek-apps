<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Konfirmasi Pemesanan</title>
</head>

<body>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#f7f7f7" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:1.4em; color:#333;">
  <tr>
    <td>
      <table width="640" border="0" cellpadding="0" cellspacing="0" style="margin:20px auto;">
          <tr style="background:#FFF;">
            <td style="text-align:right; padding:20px 0;border-left:1px solid #CCC; border-right:1px solid #CCC; border-top:1px solid #CCC;"><a href="<?php echo base_url(); ?>" target="_blank">
            <img src="<?php echo $this->webconfig['frontend_template']; ?>images/Logo.png" alt="Snekbook" style="border:none; float:center" width="170" /></a>
            <p style="float:right; font-size:16px; font-weight:bold; text-transform:uppercase; color:#fff; padding:10px 0; margin:0;">Invoice untuk Order ID #<?php echo isset($listdata['id'])?$listdata['id']:'';?></p>
            </td>
          </tr>
          <tr>
            <td style="background:#FFF; border-left:1px solid #ddd; border-right:1px solid #ddd; border-bottom:1px solid #ddd; padding:10px 20px;">
            <p style="border-bottom:1px solid #ddd;font-size:16px; font-weight:bold; text-transform:uppercase; color:#333; padding:10px 0; margin:0;">INVOICE ORDER ID #<?php echo isset($listdata['id'])?$listdata['id']:'';?></p>
              <p>Terima kasih, berikut ini adalah rincian pesanan Anda di Snekbook.com</p>
                <table width="100%" border="0" cellpadding="10" cellspacing="1" bgcolor="#ddd" style="margin-top:20px;">
                  <tr style="font-size:12px; font-weight:bold; text-transform:uppercase;">
                    <td colspan="2" bgcolor="#FFF">Detail Produk</td>
                    <td width="10%" align="center" bgcolor="#FFF">Diskon</td>
                    <td width="9%" align="center" bgcolor="#FFF">Qty</td>
                    <td width="18%" align="center" bgcolor="#FFF">Harga per Unit</td>
                    <td width="18%" align="right" bgcolor="#FFF">Sub Total</td>
                  </tr>

                  <?php if(isset($listdata['details']) && count($listdata['details']) > 0){ //print_r($listdata) ?>
                    <?php foreach ($listdata['details'] as $key => $value) { ?>
                       <tr>
                        <td width="11%" bgcolor="#FFF">
                          <img src="<?php echo thumb_image($value['path'],'360x203','product'); ?>" alt="image1" style="border:1px solid #ddd; width:97px;">
                        </td>
                        <td width="34%" bgcolor="#FFF">
                            <h4><?php echo $value['name']; ?></h4>
                            <p><span>Harga :</span> <?php echo $this->lang->line('currency').' '.number_format($value['price'], 0, ',', '.'); ?></p>
                            <p><span>Berat :</span> <?php echo $value['berat']; ?> gr</p>
                            <p><span>Rasa :</span> <?php echo sizeName($value['size']); ?></p>
                        </td>
                        <td align="center" bgcolor="#FFF">
                         <?php echo $value['discount']; ?> %
                        </td>
                        <td align="center" bgcolor="#FFF"><?php echo isset($value['quantity'])?$value['quantity']:'';?></td>
                        <td align="center" bgcolor="#FFF"><?php echo $this->lang->line('currency').' '.number_format($value['price'] - ($value['price'] * ($value['discount']/100)), 0, ',', '.'); ?></td>
                        <td align="right" bgcolor="#FFF">
                          <?php echo $this->lang->line('currency').' '.number_format(($value['price'] - ($value['price'] * ($value['discount']/100))) * $value['quantity'], 0, ',', '.'); ?>
                          <?php 
                              $totalhrgaproduk[] = ($value['price'] - ($value['price'] * ($value['discount']/100))) * $value['quantity']; 
                              $totalberat[] = $value['berat'] * $value['quantity']; 
                          ?>
                        </td>
                      </tr>
                    <?php } ?>
                  <?php } ?>

                </table>
                <table width="100%" border="0" cellpadding="7" cellspacing="0" style="background:#f7f7f7; border:1px solid #ddd; margin-top:15px;">
                  <tr>
                    <td width="46%">Total Berat</td>
                    <td colspan="2" align="right"><?php echo number_format(ceil(array_sum($totalberat)/1000), 0, ',', '.'); ?> Kg</td>
                  </tr>
                  <tr>
                    <td>Biaya Pengiriman</td>
                    <td width="33%">
                    <?php if($listdata['post_price'] != '0'){?>
                      <?php echo number_format(ceil(array_sum($totalberat)/1000), 0, ',', '.'); ?> Kg x Rp <?php echo number_format($listdata['post_price'], 0, ',', '.'); ?>
                    <?php } ?>
                    </td>
                    <td width="21%" align="right">
                      Rp <?php 
                      $hargapengiriman = $listdata['post_price']*ceil(array_sum($totalberat)/1000);
                      echo number_format($hargapengiriman, 0, ',', '.'); 
                      ?>
                    </td>
                  </tr>
                  <tr>
                    <td style="font-size:14px; font-weight:bold; border-top:1px solid #ddd;">GRAND TOTAL</td>
                    <td colspan="2" align="right" style="font-size:14px; font-weight:bold; border-top:1px solid #ddd; color:#f2498f;">
                      <?php echo $this->lang->line('currency').' '.number_format(array_sum($totalhrgaproduk) + $hargapengiriman, 0, ',', '.'); ?>
                    </td>
                  </tr>
                </table>
                <table>
                  <td valign="top">
                      <p>Snekbook.com hanya menerima pembayaran melalui nomor rekening dibawah ini</p>

                      <?php if($this->paymentbanks !='' && count($this->paymentbanks) > 0){ //print_r($this->paymentbanks); ?>
                        <?php foreach ($this->paymentbanks as $key => $value) {?>
                          <p><strong>
                          <?php echo isset($value['name'])?$value['name']:'';?> <?php echo (isset($value['account_cabang']) && $value['account_cabang'] != '')?"Cabang ".$value['account_cabang']:'';?><br/>
                          <?php echo isset($value['account_rek'])?$value['account_rek']:'';?><br/>
                          <?php echo isset($value['account_name'])?$value['account_name']:'';?>
                          </strong></p>
                        <?php } ?>
                      <?php }else{ ?>
                        [<em>Tidak terdapat data no Rekening, Silahkan menghubungi Admin untuk No Rekening Pembayaran.</em>]
                      <?php } ?>

                        <p>Konfirmasi pembayaran dapat dilakukan dengan mengakses halaman <a href="<?php echo base_url(); ?>member/order" style="color:#1abc9c">Order Belanja</a> pada profil anda.
                        Jika ada pertanyaan jangan ragu untuk menghubungi kami. Terima kasih.</p>
                       <br />
                        <p><strong>Admin Snekbook.com</strong><br /></p>
                        Jl. Hasanudin No.28 | Bandung - Jawa Barat<br />
                        INDONESIA <br />
                        +62812 2081 7720 | snekbook@gmail.com<br />
                    </td>
                </table>
            </td>
          </tr>
          <tr>
            <td height="40" style="font-size:11px; color:#666;">
              <a href="<?php echo base_url(); ?>" target="_blank" style="color:#333;text-decoration:none;"><strong>snekbook.com</strong></a><br/>
              &copy; <?php echo date('Y'); ?> <?php echo isset($this->configuration['site_title'])?$this->configuration['site_title']:'Site Name'?>. Call Center <strong><?php echo isset($this->configuration['phone'])?$this->configuration['phone']:'000 - 1234546'?></strong>
            </td>
          </tr>
        </table>
    </td>
  </tr>
</table>
</body>
</html>