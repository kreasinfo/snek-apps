<script type="text/javascript">

jQuery(document).ready(function($) {

    $("#tipe_pengiriman").select2({

        placeholder: "Pilih Tipe Pengiriman",

        allowClear: true,

        initSelection: function(element, callback) {

            callback({id: '<?php echo isset($tipe_pengiriman)?$tipe_pengiriman:''; ?>', text: '<?php echo isset($tipe_pengiriman_text)?$tipe_pengiriman_text:''; ?>' });

        },

        ajax: {

            url: '<?php echo base_url(); ?>webservices/tipe_pengiriman/',

            dataType: 'json',

            data: function (term, page) {

                return {

                    q: term

                };

            },

            results: function (data, page) {

                return { results: data };

            } ,

            cache: false

        }

    }).on("change", function(e) {

        var text = $('#s2id_tipe_pengiriman .select2-chosen').text();

        var id = $('#tipe_pengiriman').val();

        var arr = {id:id,text:text};

        

        if(id !='' && text !=''){

            $('#ongkirfree').prop('checked', false);

            $('#ongkirlimit').prop('checked', false);

        }

        $.ajax({

            type: 'POST',

            url: "<?php echo base_url().$this->router->class; ?>/services",

            data: arr,

            success: function(response) {

                // console.log(response);

            }

        });

        

        $('#tipe_pengiriman_text').val($('#s2id_tipe_pengiriman .select2-chosen').text());

        pengiriman($('#tipe_pengiriman').val());

    });



    <?php if(isset($tipe_pengiriman)) { ?>

        pengiriman('<?php echo $tipe_pengiriman; ?>');

        ongkir();

    <?php } ?>



    <?php if(isset($customongkos) && $customongkos != ''){?>

        custom_ongkir('<?php echo $customongkos;?>');

        $('#orders_ongkir').hide();

    <?php } ?>



    $('#ongkirfree').change(function() {

        /*if($(this).is(":checked")) {

            var returnVal = confirm("Are you sure?");

            $(this).attr("checked", returnVal);

        }

        $('#textbox1').val($(this).is(':checked'));*/

        $(this).is(":checked");

        if($(this).is(":checked")) {

            $('#ongkirlimit').prop('checked', false);

            $('#orders_ongkir').hide();



            $("#tipe_pengiriman").select2("val", "");

            var text = '';

            var id = '';

            var arr = {id:id,text:text};

            

            $.ajax({

                type: 'POST',

                url: "<?php echo base_url().$this->router->class; ?>/services",

                data: arr,

                success: function(response) {

                    pengiriman($('#tipe_pengiriman').val());

                }

            });

            

            $.ajax({

                type: 'POST',

                url: "<?php echo base_url().$this->router->class; ?>/customservices",

                data: {custom_price:$(this).val()},

                success: function(response) {

                    

                }

            });



            custom_ongkir(0);

        }else{

            $('#orders_ongkir').show();

            $("#tipe_pengiriman").select2("val", "");

            var text = '';

            var id = '';

            var arr = {id:id,text:text};

            

            $.ajax({

                type: 'POST',

                url: "<?php echo base_url().$this->router->class; ?>/services",

                data: arr,

                success: function(response) {

                    pengiriman($('#tipe_pengiriman').val());

                }

            });

            

            $.ajax({

                type: 'POST',

                url: "<?php echo base_url().$this->router->class; ?>/customservices",

                data: {custom_price:''},

                success: function(response) {

                    // console.log(response);

                }

            });

        }

       

    });

    

    $('#ongkirlimit').change(function() {

        $(this).is(":checked");

        if($(this).is(":checked")) {

            $('#ongkirfree').prop('checked', false);



            $("#tipe_pengiriman").select2("val", "");

            var text = '';

            var id = '';

            var arr = {id:id,text:text};

            

            $.ajax({

                type: 'POST',

                url: "<?php echo base_url().$this->router->class; ?>/services",

                data: arr,

                success: function(response) {

                    pengiriman($('#tipe_pengiriman').val());

                }

            });



            $.ajax({

                type: 'POST',

                url: "<?php echo base_url().$this->router->class; ?>/customservices",

                data: {custom_price:$(this).val()},

                success: function(response) {

                    // console.log(response);

                }

            });



            custom_ongkir('<?php echo isset($this->configuration['harga_kirim'])?$this->configuration['harga_kirim']:'';?>');

        }

    });



});

function pengiriman(id_tipe){
    if(id_tipe == ''){
        $('.pengiriman_block').html('&nbsp; &nbsp; &nbsp; <em>Silahkan pilih tipe pengiriman.</em>');
        var html = '';

        var urlservices = '';

        html += '<input type="hidden" name="pengiriman" value="<?php echo isset($pengiriman)?$pengiriman:''; ?>" id="pengiriman"  class="form-control" />';

        html += '<input type="hidden" name="pengiriman_text" value="<?php echo isset($pengiriman_text)?$pengiriman_text:''; ?>" id="pengiriman_text" />';

        $('.pengiriman_block').append(html);

    }else{

        

        $('.pengiriman_block').html('');

        var html = '';

        var urlservices = '';

        html += '<input type="hidden" name="pengiriman" value="<?php echo isset($pengiriman)?$pengiriman:''; ?>" id="pengiriman"  class="form-control" />';

        html += '<input type="hidden" name="pengiriman_text" value="<?php echo isset($pengiriman_text)?$pengiriman_text:''; ?>" id="pengiriman_text" />';

        $('.pengiriman_block').append(html);

        

        $("#pengiriman").select2({

            initSelection: function(element, callback) {

                callback({id: '<?php echo isset($pengiriman)?$pengiriman:''; ?>', text: '<?php echo isset($pengiriman_text)?$pengiriman_text:''; ?>' });

            },

            ajax: {

                url: '<?php echo base_url(); ?>webservices/pengiriman/'+id_tipe+'/',

                dataType: 'json',

                data: function (term, page) {

                    return {

                        q: term

                    };

                },

                results: function (data, page) {

                    return { results: data };

                } ,

                cache: false

            }

        }).on("change", function(e) {

          $('#pengiriman_text').val($('#s2id_pengiriman .select2-chosen').text());



            var pengiriman_text = $('#s2id_pengiriman .select2-chosen').text();

            var pengiriman = $('#pengiriman').val();

            var city = {pengiriman:pengiriman,pengiriman_text:pengiriman_text};

            $.ajax({

                type: 'POST',

                url: "<?php echo base_url().$this->router->class; ?>/services_city",

                data: city,

                success: function(response) {

                    // console.log(response);

                }

            });



          ongkir();

        });

    }

    

}



function ongkir(){

    var harga_pengiriman = 0;

    var berattotal = $("#berattotal").val();

    var pengiriman = $("#pengiriman").val();

    

    var ongkirfree = $('#ongkirfree').is(":checked");

    var ongkirlimit = $('#ongkirlimit').is(":checked");

    

    if(ongkirfree == false && ongkirlimit == false){

        jQuery.ajax({

            type: 'POST',

            url: "<?php echo base_url().$this->router->class; ?>/getShippingPrice/",

            data: {idpengiriman:pengiriman},

            success: function(response) {

                

                harga_pengiriman = response;

                var totalhrgaproduk = $("#totalhrgaproduk").val();

                var total_pengiriman = parseInt(harga_pengiriman) * parseInt(Math.ceil(berattotal/1000));

                var total_harga = parseInt(total_pengiriman) + parseInt(totalhrgaproduk);

                

                $("#total_pengiriman").text(total_pengiriman).formatCurrency({ digitGroupSymbol:'.', symbol:'IDR ' ,roundToDecimalPlace:'0' });

                $("#total_pengirimansidemenu").text(total_pengiriman).formatCurrency({ digitGroupSymbol:'.', symbol:'IDR ' ,roundToDecimalPlace:'0' });

                $("#total_all").text(total_harga).formatCurrency({ digitGroupSymbol:'.', symbol:'IDR ' ,roundToDecimalPlace:'0' });

                $("#harga_pengiriman").val(response);

                $("#totalhargaongkir").val(total_pengiriman);

                $("#hargakirim_perkilo").text(response).formatCurrency({ digitGroupSymbol:'.', symbol:'IDR ' ,roundToDecimalPlace:'0' });

            }

        });

    }else{

        

    }

}



function custom_ongkir(customongkir){

    var berattotal = $("#berattotal").val();

    var response = customongkir;

    harga_pengiriman = response;

    var totalhrgaproduk = $("#totalhrgaproduk").val();

    var total_pengiriman = parseInt(harga_pengiriman) * parseInt(Math.ceil(berattotal/1000));

    var total_harga = parseInt(total_pengiriman) + parseInt(totalhrgaproduk);

    

    $("#total_pengiriman").text(total_pengiriman).formatCurrency({ digitGroupSymbol:'.', symbol:'IDR ' ,roundToDecimalPlace:'0' });

    $("#total_pengirimansidemenu").text(total_pengiriman).formatCurrency({ digitGroupSymbol:'.', symbol:'IDR ' ,roundToDecimalPlace:'0' });

    $("#total_all").text(total_harga).formatCurrency({ digitGroupSymbol:'.', symbol:'IDR ' ,roundToDecimalPlace:'0' });

    $("#harga_pengiriman").val(response);

    $("#totalhargaongkir").val(total_pengiriman);

    $("#hargakirim_perkilo").text(response).formatCurrency({ digitGroupSymbol:'.', symbol:'IDR ' ,roundToDecimalPlace:'0' });

}



function submitkie(){

    

    var tipe_pengiriman = $('#tipe_pengiriman').val();

    var pengiriman = $('#pengiriman').val();

    var ongkirfree = $('#ongkirfree').is(":checked");

    var ongkirlimit = $('#ongkirlimit').is(":checked");



    if(ongkirfree == false && ongkirlimit == false){

        if(tipe_pengiriman == ''){

            toastr.info('Silahkan mengisi tipe pengiriman', "<?php echo $this->lang->line('error_notif'); ?>");

        }

        if(pengiriman == ''){

            toastr.info('Silahkan mengisi kota pengiriman', "<?php echo $this->lang->line('error_notif'); ?>");

        }

        if(tipe_pengiriman && pengiriman !=''){

            // window.location.href = "<?php echo base_url(); ?>cart/kasir/";
            $.fancybox.open({
                href: '<?php echo base_url(); ?>cart/chosecheckout/',
                type: 'ajax',
                padding: 15
            });
        }

    }else{

        // window.location.href = "<?php echo base_url(); ?>cart/kasir/";
         $.fancybox.open({
             href: '<?php echo base_url(); ?>cart/chosecheckout/',
             type: 'ajax',
             padding: 15
         });

    }

   

}

function format_hasil(jumlahtot){

    jumlahtot = jumlahtot.toString();

    var pattern = /(-?\d+)(\d{3})/;

    while (pattern.test(jumlahtot))

        jumlahtot = jumlahtot.replace(pattern, "$1.$2");

    return jumlahtot;

}

</script>

<div class="container">

    <ol class="breadcrumb">

        <li><a href="<?php echo base_url(); ?>">Home</a></li>

        <li class="active">Keranjang Belanja</li>

    </ol>

</div>

<div class="border-bottom"></div>

<?php //echo json_encode($this->session->all_userdata());?>

<div class="container">

    <div class="row margin-top-30">

        <div class="col-lg-9"><!-- Main Konten -->

            <div class="info-product pull-left"><i class="fa fa-shopping-cart"></i> Keranjang Belanja Kamu</div>

            <div class="info-product pull-left" id="daftarcart"></div>

            <div class="clear" ></div>

            <div class="box-grey margin-top-20" id="keranjang" style="display:none;">

                <form method="">

                    <table class="table">

                        <thead>

                            <tr>

                                <th width="16%">PRODUK</th>

                                <th width="26%"></th>

                                <th width="15%">RASA</th>

                                <th width="15%">HARGA</th>

                                <th width="10%">JUMLAH</th>

                                <th width="18%">SUB TOTAL</th>

                                <th width="8%"></th>

                            </tr>

                        </thead>

                        <tbody>

                            <tr>

                                <td colspan="6"><center>Tidak ada data</center></td>

                            </tr>

                        </tbody>

                    </table>

                </form>

            </div>

            <?php if(isset($listdata) && count($listdata) > 0){ ?>

            <?php foreach ($listdata as $key => $value) { ?>

            <div class="box-grey margin-top-20 isicart" id="<?php echo $value['id']; ?>">

                <form method="">

                    <table class="table">

                        <thead>

                            <tr>

                                <th width="16%">PRODUK</th>

                                <th width="26%"></th>

                                <th width="15%">RASA</th>

                                <th width="15%">HARGA</th>

                                <th width="10%">JUMLAH</th>

                                <th width="18%">SUB TOTAL</th>

                                <th width="8%">HAPUS</th>

                            </tr>

                        </thead>

                        <tbody>

                            <tr>

                                <td><img src="<?php echo thumb_image($value['img'],'360x203','product'); ?>" class="img-responsive" width="120" /></td>

                                <td>

                                    <a href="<?php echo base_url().'produk/read/'.$value['id'].'/'.$value['slug']; ?>" class="ttl-keranjang"><?php echo $value['name']; ?></a>

                                    <div class="clear"></div>

                                    <i style="font-size:11px;color:red;">

                                        <?php

                                        $data = '';

                                        if($value['status_produk'] !=''){

                                            if($value['status_produk'] == '0'){

                                                $data = '*Bandung Only';

                                            }else{

                                                $data = '';

                                            }

                                        }

                                        echo $data;

                                        ?>

                                    </i>

                                    <!-- <div class="info-product-s pull-left margin-top-10"><i class="fa fa-tags"></i> Toko Dodol Livina</div>

                                    <div class="clear"></div> -->

                                    <!-- <p class="ft-red">Tersisa 27 Barang Lagi</p> -->

                                    <input type="hidden" name="productberat[]" id="productberat_<?php echo $value["id"];?>" value="<?php echo $value["berat"];?>" />

                                    <p id="totalberat_<?php echo $value["id"];?>"><?php echo format_weight($value['quantity']*$value['berat']); ?> gram</p>

                                </td>

                                <td>

                                    <?php echo $value['rasa']; ?>

                                </td>

                                <td>

                                <input type="hidden" name="productprice[]" id="productprice_<?php echo $value["id"];?>" value="<?php echo $value["price"];?>" />

                                <?php echo rupiah($value['price']); ?>

                                </td>

                                <td>

                                    <select name="qty[]" style="width:50px;" id="quantity_<?php echo $value["id"];?>" data-id="<?php echo $value["size"];?>">

                                        <?php for($i = 1; $i <= sizeStock($value['size']); $i++){ ?>

                                          <option value="<?php echo $i; ?>" <?php if($i == $value['quantity']){ echo "selected='selected'"; } ?>><?php echo $i; ?></option>

                                        <?php } ?>

                                    </select>

                                </td>

                                <td id="totalthis_<?php echo $value["id"];?>"><?php echo rupiah($value['quantity']*$value['price']); ?></td>

                                <td><center><a href="javascript:removecart(<?php echo $value['id']; ?>)"  class="btn btn-danger" ><i class="fa fa-trash"></i></a></center></td>

                                <?php 

                                    $totalhrgaproduk[] = ($value['price'] - ($value['price'] * ($value['discount']/100))) * $value['quantity']; 

                                    $totalberat[] = $value['berat'] * $value['quantity']; 

                                ?>

                            </tr>

                        </tbody>

                    </table>

                </form>

            </div>

            <?php } ?>

            <?php }else{ ?>

           

            <?php } ?>

        </div><!--- End Main Konten -->

        <div class="mb-15"></div>

       

        <div class="col-lg-3"><!-- Side -->

            <div class="box-border">

                <div class="head-total">DETAIL ORDER</div>

                <div class="border-bottom margin-top-10"></div>

                <div class="row margin-top-10">

                    <div class="col-xs-5"><div class="pull-left">Total :</div></div> 

                    <div class="col-xs-7"><div class="pull-right totalharga" ><span class="ft-red"></span><?php echo rupiah(array_sum($totalhrgaproduk)); ?></div></div>

                    <input type="hidden" id="totalhrgaproduk" value="<?php echo array_sum($totalhrgaproduk); ?>"/>

                </div>

                

                <div class="row margin-top-10">

                    <div class="col-xs-5"><div class="pull-left">Ongkir :</div></div> 

                    <div class="col-xs-7">

                        <div class="pull-right" id="total_pengirimansidemenu"><span class="ft-red"></span>IDR 0</div>

                        <input type="hidden" id="totalhargaongkir" value=""/>

                    </div>

                </div>

                

                <div class="border-bottom margin-top-10"></div>

                <div class="ft-total">

                    <div class="row" style="font-weight:bold;">

                        <div class="col-xs-6"><div class="pull-left">Grand Total :</div></div> 

                        <div class="col-xs-6"><div class="pull-right" id="total_all"><span class="ft-red">IDR.</span> 0</div>

                        <input type="hidden" id="grantotal" value=""/>

                        </div>

                    </div>

                </div>

                <?php if($this->LoginModel->isLoggedIn()){ ?>

                    <a href="javascript:submitkie()"  class="btn btn-login btn-block" id="orders">ORDER SEKARANG</a>

                <?php }else{ ?>

                    <!-- <div class="alert"> -->

                        <a href="<?php echo base_url(); ?>login/member/<?php echo replace_slash(current_url()); ?>"  class="btn btn-login btn-block" id="orders">ORDER SEKARANG</a>

                    <!-- </div> -->

                <?php } ?>

            </div>

        </div><!--- End Side -->

    </div>

</div>



<div class="container">

    <div class="head-border margin-top-30">ESTIMASI ONGKOS KIRIM</div>

    <?php /* <div class="box-border margin-top-10" style="padding-bottom: 0px;">

        <div class="row">

            <div class="col-md-9 margin-bottom-10">

                <!-- <div class="col-md-12">

                    <div class="margin-bottom-10">Untuk anda yang berada di daerah kota bandung kami berikan pelayanan khusus untuk pembelian produk kami</div>

                </div> -->

                <div class="col-md-12 margin-bottom-10">

                    <div class="margin-bottom-10">

                        <label style="cursor:pointer;">

                            <input type="checkbox" name="bdgongkir" id="ongkirfree" value="0" <?php if(isset($customongkos) && $customongkos == '0'){ echo "checked";}?>>&nbsp;

                            Ambil di tempat ongkir free

                        </label>

                    </div>

                </div>

                <div class="col-md-12 margin-bottom-10">

                    <div class="margin-bottom-10"><i>*daerah di luar kota bandung tetap layanan pengiriman menggunakan kurir</i></div>

                </div>

            </div>

            <div class="col-md-3">

                <center>

                    <a href="#"><img width="60px" alt="ukm" src="<?php echo $this->webconfig['frontend_template']; ?>images/iconKota.png" ></a>

                </center>

                <div class="col-md-12 margin-bottom-10">

                    <center>

                    <span >LIHAT STORE</span>

                    </center>

                </div>

            </div>

            

            

        </div>

    </div>*/?>

    <div class="box-border margin-top-10" id="orders_ongkir"><!-- Ongkir -->

        <div class="row">

            <div class="col-md-3 margin-bottom-10">

                <div class="margin-bottom-10">PILIH <span class="ft-red">TIPE PENGIRIMAN</span> :</div>

                <input type="hidden" id="tipe_pengiriman" name="tipe_pengiriman" value="<?php echo isset($tipe_pengiriman)?$tipe_pengiriman:''; ?>" class="form-control" />

                <input type="hidden" id="tipe_pengiriman_text" name="tipe_pengiriman_text" value="<?php echo isset($tipe_pengiriman_text)?$tipe_pengiriman_text:''; ?>" class="form-control" />

            </div>

            <div class="col-md-3">

                <div class="margin-bottom-10">PILIH KOTA <span class="ft-red">KAMU</span> :</div>

                <div class="pengiriman_block"><em>Silahkan pilih tipe pengiriman.</em></div>

            </div>

            <div class="mb-15"></div>

            <div class="col-md-4">

                <div class="row">

                    <div class="col-xs-4">

                        <div class="margin-bottom-10">Harga/Kg</div>

                        <div id="hargakirim_perkilo"></div>

                        <input type="hidden" id="harga_pengiriman" value="0"/>

                    </div>

                    <div class="col-xs-3">

                        <div class="margin-bottom-10" >Berat<span class="ft-red">(gr)</span></div>

                        <input type="hidden" id="berattotal" value="<?php echo array_sum($totalberat); ?>"/>

                        <div class="berattotal"></div>

                    </div>

                    <div class="col-xs-5">

                        <div class="margin-bottom-10">Total<span class="ft-red"> Ongkir</span></div>

                        <div id="total_pengiriman"></div>

                        

                    </div>

                    

                </div>

            </div>

        </div>

    </div><!-- End Ongkir -->

</div>



<div class="box-grey margin-top-30">

    <div class="container margin-bottom-40">

        <div class="margin-bottom-10">GIMANA SIH ALUR <span class="ft-red">BELANJANYA?</span></div>

        <div class="border-bottom"></div>

        <div class="row margin-top-20">

            <div class="col-md-3">

                <div class="box-alur">

                    <center><img src="<?php echo $this->webconfig['frontend_template']; ?>images/alur1.png" class="img-responsive" /></center>

                    TENTUIN BARANG YANG KAMU MAU?

                    <p class="margin-top-10">Pilih barang yang tertera di display berdasarkan yang kamu mau.</p>

                </div>

            </div>

            

            <div class="col-md-3">

                <div class="box-alur">

                    <center><img src="<?php echo $this->webconfig['frontend_template']; ?>images/alur2.png" class="img-responsive" /></center>

                    LANJUTIN KE KERANJANG, TENTUIN JUMLAHNYA

                    <p class="margin-top-10">Setelah masuk ke menu keranjang belanja, data barang yang kamu mau udah di list, tentuin jumlahnya buat di bayar ke transaski selanjutnya.</p>

                </div>

            </div>

            

            <div class="col-md-3">

                <div class="box-alur">

                    <center><img src="<?php echo $this->webconfig['frontend_template']; ?>images/alur3.png" class="img-responsive" /></center>

                    MASUK KE TRANSAKSI, BAYAR SESUAI TOTAL 

                    <p class="margin-top-10">Setelah masuk ke menu keranjang belanja, data barang yang kamu mau udah di list, tentuin jumlahnya buat di bayar ke transaski selanjutnya.</p>

                </div>

            </div>

            

            <div class="col-md-3">

                <div class="box-alur">

                    <center><img src="<?php echo $this->webconfig['frontend_template']; ?>images/alur4.png" class="img-responsive" /></center>

                    KONFIRMASI DEH PEMBAYARAN KAMU, SELESAI

                    <p class="margin-top-10">Setelah kamu bayar taihanya, segera konfirmasi buat segera dikirm, detail status bisa dilihat di profil kamu.</p>

                </div>

            </div>

        </div>

    </div>

</div>