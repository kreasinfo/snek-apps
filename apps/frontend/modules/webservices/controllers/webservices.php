<?php
class Webservices extends CI_Controller{
	var $webconfig;
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->sharedModel('LoginModel');

		// if(!$this->LoginModel->isLoggedIn()){
		// 	redirect(base_url().'login');
		// }
	}
	function index(){
		
	}
	function tipe_pengiriman(){
		$this->load->sharedModel('Kategori_pengirimanModel');
		$get_data = array();
		parse_str($_SERVER['QUERY_STRING'], $get_data);
		$get_data = $this->security->xss_clean($get_data);

		$arr = $this->Kategori_pengirimanModel->filterSelect2($get_data['q']);
		header('Content-Type: application/json');
    	echo json_encode( $arr );
	}
	function pengiriman($id_tipe){
		$this->load->sharedModel('PengirimanModel');
		$get_data = array();
		parse_str($_SERVER['QUERY_STRING'], $get_data);
		$get_data = $this->security->xss_clean($get_data);
		
		$productlistsesion = $this->session->userdata('cart');
		if(is_array($productlistsesion)){
			$outputArray = array(); 
			$keysArray = array(); 
			foreach ($productlistsesion as $innerArray) {
				if (!in_array($innerArray['status_produk'], $keysArray)) { 
					$keysArray[] = $innerArray['status_produk'];
					$outputArray[] = $innerArray; 
				}
			}
			$resultarray = count($outputArray);
			if(isset($outputArray[0]['status_produk']) && $outputArray[0]['status_produk'] == '0'){
				$bdg = 'kota bandung';
			}else{
				$bdg = '';
			}
		}

		$arr = $this->PengirimanModel->filterSelect2($get_data['q'], $id_tipe, $bdg);
		header('Content-Type: application/json');
    	echo json_encode( $arr );
	}
}