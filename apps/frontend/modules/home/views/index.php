<script type="text/javascript">
$(document).ready(function() {
  $('.bxslider').bxSlider({
    mode: 'fade',
    captions: true
  });
});
function searchThis(){
    var frm = document.formsearch;
    var keyword = frm.keyword.value;
    var city = frm.city.value;
    if(keyword == ''){ keyword = '-'; }
    if(city == ''){ city = '-'; }
    
    location = '<?php echo base_url(); ?>produk/search/'+keyword.replace(/\s/g,'%20')+'/-/'+city.replace(/\s/g,'%20');
}
</script>
<div class="bg-form">   
    <div class="container"><!-- Container Form -->
      <div class="ft-head3 margin-form">SILAHKAN CARI PRODUK DISINI</div>
        <p class="form-des">Pencarian berdasarkan nama produk dan nama kota untuk hasil lebih maksimal</p>
      <div class="row margin-bottom-20">
          <div class="col-md-10 col-md-offset-1">
              <div class="form-bg">
                  <div class="row">
                      <form id="formsearch" name="formsearch" method="POST" action="<?php echo base_url();?>produk/search/" onsubmit="searchThis(); return false;" >
                        <div class="col-md-5"><input class="form-search" type="text" name="keyword" placeholder="Nama Produk" /></div>
                          <div class="col-md-5 no-padding">
                            <div class="formgroup">
                              <select id="city" name="city" class="form-search">
                                <option value="">-- Pilih Kota --</option>
                                  <?php if(isset($listlokasiukm) && $listlokasiukm !=''){?>
                                    <?php foreach ($listlokasiukm as $key => $value) {?>
                                      <option value="<?php echo $value['id'];?>" <?php if(isset($id_lokasiukm) && $id_lokasiukm == $value['id']) {echo "selected=selected";}?>><?php echo $value['name'];?></option>
                                    <?php }?>
                                  <?php } ?>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <input type="submit" class="btn btn-search btn-block" value="CARI" />
                          </div>
                      </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End Container Form -->    
</div>
<div class="container">
  <div class="ft-head4 margin-top-30"> Produk Yang <span class="ft-red">Lagi Ngetren</span></div>
    <p class="des-ngetren">Berikut adalah Beberapa Produk yang Kami Rekomendasikan untuk Anda</p>
    <ul class="bxslider">
      <li>
        <div class="row margin-top-30"><!-- Row Product Big -->
            <?php if(isset($listtopselling) && count($listtopselling) > 0){ ?>
            <?php foreach($listtopselling as $key => $listdata){ ?>
            <?php if($key <= 1){?>
            <div class="col-md-6">
                <div class="box-img" >
                    <a href="<?php echo base_url().'produk/read/'.(isset($listdata['id'])?$listdata['id']:'').'/'.(isset($listdata['slug'])?$listdata['slug']:''); ?>"><img src="<?php echo thumb_image($listdata['images']['path'],'555x311','product'); ?>" class="img-responsive" style="width: 100%;"/></a>
                      <div class="des-info" style="background-color: rgba(0, 0, 0, 0.4);">
                        <div class="col-md-12">
                        <a href="<?php echo base_url().'produk/read/'.(isset($listdata['id'])?$listdata['id']:'').'/'.(isset($listdata['slug'])?$listdata['slug']:''); ?>" class="ttl-produk"><?php echo $listdata['name']; ?></a>
                        </div>
                        <div class="col-md-6">
                           <?php echo isset($listdata['total']['totalquantity'])?$listdata['total']['totalquantity']:'0';?> Terjual
                          <!-- <img src="<?php echo $this->webconfig['frontend_template']; ?>images/rating.png" alt="Rating" /> -->
                          <p><?php echo rupiah($listdata['price']); ?></p>
                          
                        </div>
                        <div class="col-md-6">
                          <a href="<?php echo base_url().'produk/read/'.$listdata['id'].'/'.$listdata['slug']; ?>" class="btn-mau-s pull-right">&nbsp;Beli </a>
                         <!--  <div class="btn-terjual-s pull-left"><?php echo isset($listdata['total']['totalquantity'])?$listdata['total']['totalquantity']:'0';?> Terjual</div> -->
                        </div>
                       
                          <div class="clearfix"></div>
                      </div>
                  </div>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
        </div><!-- End Row Product Big -->
        
        <div class="row margin-top-30"><!-- Row Product Small -->
          <?php if(isset($listproduct) && count($listproduct) > 0){ ?>
          <?php foreach($listproduct as $key => $listdata){ ?>
          <?php if($key <= 2){?>
          <div class="col-md-4">
            <div class="box-img">
                <a href="<?php echo base_url().'produk/read/'.(isset($listdata['id'])?$listdata['id']:'').'/'.(isset($listdata['slug'])?$listdata['slug']:''); ?>"><img src="<?php echo thumb_image($listdata['images']['path'],'360x203','product'); ?>" class="img-responsive" style="width: 100%;"/></a>
              </div>
              <div class="box-S-product">
                <a href="<?php echo base_url().'produk/read/'.(isset($listdata['id'])?$listdata['id']:'').'/'.(isset($listdata['slug'])?$listdata['slug']:''); ?>" class="name-product pull-left"><?php echo $listdata['name']; ?></a>
                  <div class="price-home pull-right"><?php echo rupiah($listdata['price']); ?></div>
                  <div class="clearfix"></div>
              </div>
              <div class="box-other">
                <div class="row">
                    <div class="col-xs-4"><span class="ft-red"><?php echo isset($listdata['total']['totalquantity'])?$listdata['total']['totalquantity']:'0';?></span> Terjual<!-- <img src="<?php echo $this->webconfig['frontend_template']; ?>images/rating.png" alt="rating" /> --></div>
                      <div class="col-xs-4"></div>
                      <div class="col-xs-4"><a href="<?php echo base_url().'produk/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"  class="btn-mau-p">&nbsp;&nbsp;  Beli &nbsp;&nbsp;</a></div>
                  </div>
              </div>
          </div>
          <?php } ?>
          <?php } ?>
          <?php } ?>
        </div><!-- end Row Product Small -->
      </li>
      
      <li>
        <div class="row margin-top-30"><!-- Row Product Big -->
          <?php if(isset($listtopselling) && count($listtopselling) > 0){ ?>
          <?php foreach($listtopselling as $key => $listdata){ ?>
          <?php if($key >= 2 AND $key <= 3){?>
          <div class="col-md-6">
            <div class="box-img">
                <a href="<?php echo base_url().'produk/read/'.(isset($listdata['id'])?$listdata['id']:'').'/'.(isset($listdata['slug'])?$listdata['slug']:''); ?>"><img src="<?php echo thumb_image($listdata['images']['path'],'555x311','product'); ?>" class="img-responsive" style="width: 100%;"/></a>
                  <div class="des-info" style="background-color: rgba(0, 0, 0, 0.4);">
                    <div class="col-md-12">
                      <a href="<?php echo base_url().'produk/read/'.(isset($listdata['id'])?$listdata['id']:'').'/'.(isset($listdata['slug'])?$listdata['slug']:''); ?>" class="ttl-produk"><?php echo $listdata['name']; ?></a>
                    </div>
                    <div class="col-md-6">
                       
                       <?php echo isset($listdata['total']['totalquantity'])?$listdata['total']['totalquantity']:'0';?> Terjual
                      <!-- <img src="<?php echo $this->webconfig['frontend_template']; ?>images/rating.png" alt="Rating" /> -->
                      <p><?php echo rupiah($listdata['price']); ?></p>
                      
                    </div>
                    <div class="col-md-6">
                      <a href="<?php echo base_url().'produk/read/'.$listdata['id'].'/'.$listdata['slug']; ?>" class="btn-mau-s pull-right">&nbsp;Beli </a>
                     <!--  <div class="btn-terjual-s pull-left"><?php echo isset($listdata['total']['totalquantity'])?$listdata['total']['totalquantity']:'0';?> Terjual</div> -->
                    </div>
                       
                    <div class="clearfix"></div>
                  </div>
              </div>
          </div>
          <?php } ?>
          <?php } ?>
          <?php } ?> 
        </div><!-- End Row Product Big -->
        
        <div class="row margin-top-30"><!-- Row Product Small -->
          <?php if(isset($listproduct) && count($listproduct) > 0){ ?>
          <?php foreach($listproduct as $key => $listdata){ ?>
          <?php if($key >= 3 AND $key <=5){?>
          <div class="col-md-4">
            <div class="box-img">
                <a href="<?php echo base_url().'produk/read/'.(isset($listdata['id'])?$listdata['id']:'').'/'.(isset($listdata['slug'])?$listdata['slug']:''); ?>"><img src="<?php echo thumb_image($listdata['images']['path'],'360x203','product'); ?>" class="img-responsive" style="width: 100%;"/></a>
              </div>
              <div class="box-S-product">
                <a href="#" class="name-product pull-left"><?php echo $listdata['name']; ?></a>
                  <div class="price-home pull-right"><?php echo rupiah($listdata['price']); ?></div>
                  <div class="clearfix"></div>
              </div>
              <div class="box-other">
                <div class="row">
                    <div class="col-xs-4"><span class="ft-red"><?php echo isset($listdata['total']['totalquantity'])?$listdata['total']['totalquantity']:'0';?></span> Terjual<!-- <img src="<?php echo $this->webconfig['frontend_template']; ?>images/rating.png" alt="rating" /> --></div>
                      <div class="col-xs-4"></div>
                      <div class="col-xs-4"><a href="<?php echo base_url().'produk/read/'.$listdata['id'].'/'.$listdata['slug']; ?>" class="btn-mau-p">&nbsp;&nbsp;  Beli &nbsp;&nbsp;</a></div>
                  </div>
              </div>
          </div>
          <?php } ?>
          <?php } ?>
          <?php } ?>
        </div><!-- end Row Product Small -->
      </li>
    </ul>
</div>
<div class="container"><!-- Container Info -->
  <div class="row">
      <div class="col-lg-10 col-lg-offset-1">
          <div class="row">
                <div class="col-md-4">
                    <div class="box-info-ukm">
                        <center><img src="<?php echo $this->webconfig['frontend_template']; ?>images/iconUKM.png" alt="ukm" /></center>
                        <div class="big-info"><?php echo isset($sum_ukm)?$sum_ukm:'';?></div>
                        Jumlah UKM yang Tergabung dari Berbagai Daerah Di Indonesia
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-info-ukm">
                        <center><img src="<?php echo $this->webconfig['frontend_template']; ?>images/iconKota.png" alt="ukm" /></center>
                        <div class="big-info"><?php echo isset($sumkotaukm)?$sumkotaukm:'';?></div>
                        Kota yang terdaftar disini dari Berbagai Daerah di indonesia
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-info-ukm">
                        <center><img src="<?php echo $this->webconfig['frontend_template']; ?>images/iconProduct.png" alt="ukm" /></center>
                        <div class="big-info"><?php echo isset($sumproduk)?$sumproduk:'';?></div>
                        Jumlah Produk UKM yang Tergabung dari Berbagai Daerah di Indonesia
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- end Container Info -->
<div class="clear"></div>
<div class="margin-top-40">
  <center>
      <p><a href="<?php echo base_url().'register'; ?>" class="btn-daftar">DAFTAR SEKARANG</a></p>
      <p class="txt-daftar"></p>
  </center>
</div>

<div class="bg-partner margin-top-40"><!-- Partner -->
    <div class="container">
    <div class="head-partner">OUR <span class="ft-red">PARTNER</span></div>
        <ul id="flexiselDemo1"> 
            <li><img src="<?php echo $this->webconfig['frontend_template']; ?>images/logo-nyt.png" /></li>
            <li><img src="<?php echo $this->webconfig['frontend_template']; ?>images/logo-microsoft.png" /></li>    
            <li><img src="<?php echo $this->webconfig['frontend_template']; ?>images/logo-ebay.png" /></li>     
            <li><img src="<?php echo $this->webconfig['frontend_template']; ?>images/logo-hp.png" /></li> 
            <li><img src="<?php echo $this->webconfig['frontend_template']; ?>images/room23.png" /></li>
            <li><img src="<?php echo $this->webconfig['frontend_template']; ?>images/logo-youtube.png" /></li>
            <li><img src="<?php echo $this->webconfig['frontend_template']; ?>images/ayamdower.png" /></li>
            <li><img src="<?php echo $this->webconfig['frontend_template']; ?>images/dapurqta.png" /></li>                                     
        </ul>
    </div>
</div><!-- End Partner -->