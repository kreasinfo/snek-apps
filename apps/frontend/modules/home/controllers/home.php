<?php
class Home extends CI_Controller {
	var $webconfig;
	var $pagename;
	var $configuration;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('session');
		$this->lang->load('global', 'bahasa');
		$this->load->sharedModel('OrderModel');
		$this->load->sharedModel('ProdukModel');
		$this->load->sharedModel('LoginModel');
		$this->load->sharedModel('Lokasi_ukmModel');
		$this->load->sharedModel('UkmModel');
		$this->webconfig = $this->config->item('webconfig');
		$this->pagename = $this->lang->line('home');
		$this->configuration = $this->ShopconfigurationModel->listData();	
	}

	function index()
	{
		$tdata = array();

		$tdata['sumproduk'] = count($this->ProdukModel->listData());
		$tdata['sumkotaukm'] = count($this->Lokasi_ukmModel->listData());
		$tdata['sum_ukm'] = count($this->UkmModel->listData());
		
		$listorder = $this->OrderModel->listDataTopselling();
		
		if(count($listorder) > '2'){
			$imp = implode(', ', array_map(function ($entry) {
			  return $entry['product_id'];
			}, $listorder));
			
			$tdata['listtopselling'] = $this->ProdukModel->listData(array('topproduct' => $imp,'limit' => 8));
			$totals = array();
			foreach ($tdata['listtopselling'] as $key => $row)
			{
			    $totals[$key] = $row['total']['totalquantity'];
			}
			array_multisort($totals, SORT_DESC, $tdata['listtopselling']);

			$tdata['listproduct'] = $this->ProdukModel->listData(array('excludeid' => $imp, 'limit'=> 6));
		}else{
			$tdata['listtopselling'] = $this->ProdukModel->listData(array('limit'=> 4));
			$tdata['listproduct'] = $this->ProdukModel->listData(array(
																		'start'  => 4
																		,'limit'=>12,
																		));
		}
		
		$tdata['listlokasiukm'] = $this->Lokasi_ukmModel->listData();
		
		## LOAD LAYOUT ##
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		$ldata['informationpage'] = $this->InformationpageModel->listData();
		$this->load->sharedView('template', $ldata);
	}

	function notfound()
	{
		$tdata = array();
		
		## LOAD LAYOUT ##
		$ldata['content'] = $this->load->view($this->router->class.'/notfound',$tdata, true);
		$this->load->sharedView('template', $ldata);
	}
}