<?php
class Captcha extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');	
	}
	function index(){
		redirect(base_url().$this->router->class.'/listing');
	}
	function listing($id = 0){	
		$this->load->library('session');
		require_once(dirname(__FILE__).'/../../../libraries/kcaptcha.php');
		$captcha = new KCAPTCHA();
		$captcha->Generate();
		$ses_captcha = $captcha->getKeyString();
		
		$this->session->unset_userdata('captcha');	
		$this->session->set_userdata('captcha', $ses_captcha);
		
		$captcha->OutputPic();
	}
}
?>