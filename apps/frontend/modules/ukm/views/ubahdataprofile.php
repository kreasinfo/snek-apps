<script type="text/javascript">
jQuery(document).ready(function($) {
  $('.tglset').datetimepicker({
      timepicker:false,
      closeOnDateSelect:true,
      format:'d-m-Y'
  });

  var oFReader = null;
  var image = null;
  var globalResizedWidth = '<?php if($this->webconfig["width_resize"] != ''){ echo $this->webconfig["width_resize"]; }else{ echo 0; } ?>';
  var jcrop_api, globalWidth, globalHeight, globalConfDimension;

  $("#uploadImageProfile").change(function(){
      $(this).parent().find(".image_block").remove();
      $.when( createImageElementProfile(this) ).done( cropImageElementProfile(this) );
  });

  function createImageElementProfile(obj) {
      var html = '<div class="image_block">';
      html += '<input type="hidden" class="a1" name="a1" value="0" />';
      html += '<input type="hidden" class="b1" name="b1" value="0" />';
      html += '<input type="hidden" class="a2" name="a2" value="0" />';
      html += '<input type="hidden" class="b2" name="b2" value="0" />';
      html += '<br><img class="image_preview" style="width:'+globalResizedWidth+'px" />';
      html += '</div>';
      $(obj).after(html);
  }

  function cropImageElementProfile(obj) {
      var ext = getExtension($(obj).val());
      if(ext == "jpg" || ext == "jpeg" || ext == "png" || ext == "gif"){
          var dimensionconf = '<?php if(count($this->webconfig["image_profile"]) > 0){ echo $this->webconfig["image_profile"][0]; } ?>';
          var separator = dimensionconf.split("x");
          var dimheight = separator[1];
          var dimwidth = separator[0];
          if(dimwidth != 'undefined' || dimheight != 'undefined'){
              doLoadCroppingProfile(obj, dimwidth, dimheight);
          }
          return false;
      }else{
          alert('<?php echo $this->lang->line("msg_error_extension"); ?>');
          $('.image_block').remove();
          $("#uploadImage").val("");
          return false;
      }
  }

  function doLoadCroppingProfile(obj, dimwidth, dimheight) {
      if(oFReader !=null){
          oFReader = null;
      }
      
      var min_width = dimwidth;
      var min_height = dimheight;
      var objFile = obj.files[0];
      var max_foto_mb = '<?php echo $this->webconfig['max_foto_filesize']; ?>';
      var max_foto_byte = parseInt(max_foto_mb)*1048576; //convert MB to Byte
      
      if(objFile.size > max_foto_byte) {
          $(obj).parent().find(".image_block").remove();
          $(obj).val("");
          alert("<?php echo $this->lang->line('label_error_ukuran'); ?>");
          $('.image_block').remove();
          $("#uploadImage").val("");
      } else {
          // prepare HTML5 FileReader
          oFReader = new FileReader();
          image  = new Image();
          oFReader.readAsDataURL(objFile);
          
          oFReader.onload = function (_file) {
              image.src    = _file.target.result;
              image.onload = function() {
                      globalWidth = this.width;
                      globalHeight = this.height;
                      
                      $(obj).parent().find(".image_preview").attr("src", this.src);

                      if(globalWidth < min_width || globalHeight < min_height) {
                              $(obj).parent().find(".image_block").remove();
                              $(obj).val("");
                              alert("<?php echo $this->lang->line('label_error_dimensi'); ?>");
                      } else {
                          cropImageProfile(globalWidth, globalHeight, min_width, min_height, $(obj).parent().find(".image_preview"));
                      }
              };

              image.onerror= function() {
                  alert('Invalid file type: '+ objFile.type);
              };     
              
          }
      }
  }

  function cropImageProfile(width, height, minwidth, minheight, obj) {
      var resizedWidth = globalResizedWidth;
      var resizedHeight = (resizedWidth * height) / width;
      var resizedMinWidth = (minwidth * resizedWidth) / width;
      var resizedMinHeight = (minheight * resizedHeight) / height;
      
      if(minwidth != '' || minheight != ''){
          $(obj).Jcrop({
              setSelect: [ 0, 0, resizedMinWidth, resizedMinHeight ],
              minSize: [ resizedMinWidth, resizedMinHeight ],
              onSelect: updateCoordsProfle,
              allowSelect: false,
              bgFade: true,
              bgOpacity: 0.4,
              aspectRatio: minwidth / minheight
          },function(){
              jcrop_api = this;
          });
      }
  }

  function updateCoordsProfle(c){
      $('.a1').val(c.x);
      $('.b1').val(c.y);
      $('.a2').val(c.x2);
      $('.b2').val(c.y2);
      $('.w').val(c.w);
      $('.h').val(c.h);
  };

  function getExtension(filename) {
      return filename.split('.').pop().toLowerCase();
  }

});

</script>
<div class="container">
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li class="active">Profil Ukm</li>
    </ol>
</div>

<div class="border-bottom"></div>

<div class="container"><!--- Container -->
    <div class="row">
        <div class="col-lg-3">
            <?php echo isset($side_menu)?$side_menu:''; ?>
        </div>
        
        <div class="col-lg-9 border-left" style="padding-bottom:30px;">
            <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
                <?php foreach($error_hash as $inp_err){ ?>
                  <script type="text/javascript">
                  jQuery(document).ready(function($) {
                      toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                      });
                  </script>
                <?php } ?>
            <?php } ?>
            <?php if(isset($success)){ ?>
                <script type="text/javascript">
                jQuery(document).ready(function($) {
                  toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                  });
                </script>
            <?php } ?>
            <div class="head-border margin-top-20">EDIT PROFIL</div>
                <form method="post" action="" enctype="multipart/form-data">
                    <div class="row margin-top-10">
                        <div class="col-md-6">
                            <label>Nama Ukm :</label>
                            <input type="text" class="form-control" name="fullname" placeholder="Nama Ukm" value="<?php echo isset($ukmdata['name'])?$ukmdata['name']:'';?>"/>
                        </div>
                        <div class="col-md-6">
                            <label>Email :</label>
                            <input type="text" class="form-control" name="email" placeholder="<?php echo $this->lang->line('email'); ?>" value="<?php echo isset($ukmdata['email'])?$ukmdata['email']:'';?>" readonly/>
                        </div>
                    </div>
                    <div class="row margin-top-10">
                        <div class="col-md-6">
                            <label>No. Telpon :</label>
                            <input type="text" class="form-control" name="phone" placeholder="<?php echo $this->lang->line('phone'); ?>" value="<?php echo isset($ukmdata['phone'])?$ukmdata['phone']:'';?>" />
                        </div>
                    </div>
                    
                    <div class="row margin-top-10">
                        <div class="col-md-12">
                            <label>Alamat :</label>
                            <textarea name="address" class="form-control"><?php echo isset($ukmdata['address'])?$ukmdata['address']:'';?></textarea>
                        </div>
                    </div>
                    <div class="row margin-top-10">
                        <div class="col-md-12">
                            <label>Gambar</label>
                            <input class="text gbr" type="file" id="uploadImageProfile" name="pathprofile" value="<?php echo isset($logo_img)?$logo_img:''; ?>"><br class="clear">
                              <p>
                                  <em>
                                      <?php echo $this->lang->line('label_info_ukuran'); ?> 
                                      <?php echo $this->webconfig['max_foto_filesize']; ?> MB
                                      <?php echo $this->lang->line('label_info_dimensi'); ?> 
                                      <?php if(count($this->webconfig["image_profile"]) > 0){ echo $this->webconfig["image_profile"][0]; }else{ echo 0; } ?> px
                                  </em>
                              </p>
                              <!-- <div id="image">
                                  <?php if(isset($ukmdata['logo_img']) && $ukmdata['logo_img'] != '' && count($ukmdata['logo_img']) > 0){ ?>
                                    <img src="<?php echo thumb_image($ukmdata['logo_img'],'200x200', 'profile'); ?>" alt="thumbs" width="200" border="0" style="width:200px"/>
                                  <?php }else{?>
                                    <img src="<?php echo $this->webconfig['base_template']; ?>img/no_image_news.jpg" width="600" style="width:200px"/>
                                  <?php }?>
                              </div> -->
                        </div>
                    </div>
                    <div class="row margin-top-10">
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-mau-b" value="EDIT DATA">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>      
</div><!--- Container -->