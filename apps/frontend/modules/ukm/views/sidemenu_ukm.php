<div class="margin-top-20"></div>
<div class="img-profil">
    <img src="<?php echo isset($ukmdata['logo_img'])?thumb_image($ukmdata['logo_img'],'200x200', 'profile'):$this->webconfig['frontend_template'].'images/default.jpg'; ?>" class="img-responsive" />
</div>
<div class="margin-top-20"></div>
<a href="<?php echo base_url(); ?>ukm/dashboard/"  ><div <?php if($this->pagename == $this->lang->line('dashboard')){ echo 'class="side-profil activeprofil"'; }else{  echo 'class="side-profil"';}?>>Dashboard</div></a>

<a href="<?php echo base_url(); ?>ukm/updateprofile/" ><div <?php if($this->pagename == $this->lang->line('update_profile')){ echo 'class="side-profil activeprofil"'; }else{  echo 'class="side-profil"';}?> >Edit Profil</div></a>

<a href="<?php echo base_url(); ?>ukm/order/" ><div <?php if($this->pagename == $this->lang->line('orders')){ echo 'class="side-profil activeprofil"'; }else{  echo 'class="side-profil "';}?> >Daftar Order</div></a>

<a href="<?php echo base_url(); ?>ukm/changepassword/" ><div <?php if($this->pagename == $this->lang->line('change_password')){ echo 'class="side-profil activeprofil"'; }else{  echo 'class="side-profil"';}?> ><?php echo $this->lang->line('change_password'); ?></div></a>

<a href="<?php echo base_url(); ?>login/logoutUkm/" ><div <?php if($this->pagename == $this->lang->line('logout')){ echo 'class="side-profil activeprofil"'; }else{  echo 'class="side-profil"';}?>><?php echo $this->lang->line('logout'); ?></div></a>
<div class="margin-bottom-20"></div>