<script type="text/javascript">
  jQuery(document).ready(function($) {
    $("#listData").load('<?php echo base_url().$this->router->class; ?>/getAllDataDashboardOrder');
  });
  function searchThis(){
    var frm = document.searchform;
    var name = frm.name.value;
    if(name == ''){ name = '-'; }
    // jQuery("#listData").load("<?php echo base_url().$this->router->class; ?>/searchdata/"+name.replace(/\s/g,'%20'));
  }
</script>
<div class="container">
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li class="active">Profil Ukm</li>
    </ol>
</div>

<div class="border-bottom"></div>
<?php  //echo json_encode($this->session->all_userdata()); ?>
<div class="container"><!--- Container -->
    <div class="row">
        <div class="col-lg-3">
            <?php echo isset($side_menu)?$side_menu:''; ?>
        </div>
        
        <div class="col-lg-9 border-left" style="padding-bottom:30px;">
            <div class="head-border margin-top-20">PROFIL UKM</div>
            <div class="row margin-top-10">
                <div class="col-md-6">
                    <div class="box-item-ukm">
                        <label>Nama :</label>
                        <div class="ft-profil"><?php echo $ukmdata['name']; ?></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box-item-ukm">
                        <label>No. Telepon :</label>
                        <div class="ft-profil"><?php echo $ukmdata['phone']; ?></div>
                    </div>
                </div>
            </div>
            
            <div class="row margin-top-10 margin-bottom-30">
                <div class="col-md-6">
                    <div class="box-item-ukm">
                        <label>Email :</label>
                        <div class="ft-profil"><?php echo $ukmdata['email']; ?></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box-item-ukm">
                        <label>Alamat :</label>
                        <div class="ft-profil"><?php echo $ukmdata['address']; ?></div>
                    </div>
                </div>
            </div>
            <div class="border-bottom"></div>
            <div class="head-border margin-top-20">Status Order Terbaru</div>
            <div class="box-border margin-top-10">
                <div id="listData">
                    <center>
                        <img src='<?php echo $this->webconfig['back_base_template']; ?>img/loading.gif' align='middle' style="margin:5px; width:auto;" />
                    </center>
                </div>
                <!-- <table class="table table-responsive">
                    <thead>
                        <th>No. Order</th>
                        <th>Gambar</th>
                        <th>Nama Produk</th>
                        <th>Status</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>#12Nhdb7</td>
                            <td><img src="<?php echo $this->webconfig['frontend_template']; ?>images/ProductS-1.jpg" width="100" class="img-responsive"></td>
                            <td><a href="" class="name-product">Tahu Cimukulkuk Mondani...</a></td>
                            <td><div class="dikirim">DIKIRIM</div></td>
                        </tr>
                        <tr>
                            <td>#12Nhdb7</td>
                            <td><img src="<?php echo $this->webconfig['frontend_template']; ?>images/ProductS-1.jpg" width="100" class="img-responsive"></td>
                            <td><a href="" class="name-product">Tahu Cimukulkuk Mondani...</a></td>
                            <td><div class="diproses">DIPROSES</div></td>
                        </tr>
                        <tr>
                            <td>#12Nhdb7</td>
                            <td><img src="<?php echo $this->webconfig['frontend_template']; ?>images/ProductS-1.jpg" width="100" class="img-responsive"></td>
                            <td><a href="" class="name-product">Tahu Cimukulkuk Mondani...</a></td>
                            <td><div class="diterima">DITERIMA</div></td>
                        </tr>
                    </tbody>
                </table> -->
            </div>
        </div>
    </div>      
</div><!--- Container -->