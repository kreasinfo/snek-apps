<?php
class Ukm extends CI_Controller {
	var $webconfig;
	var $pagename;
	var $configuration;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('common');
		$this->load->helper('image');
		$this->load->library('pagination');
		$this->lang->load('global', 'bahasa');
		$this->load->helper('phpmailer');
		$this->load->helper('image');
		$this->load->library('image_moo');
		$this->load->sharedModel('LoginModel');
		$this->load->sharedModel('UkmModel');
		$this->load->sharedModel('OrderModel');
		$this->load->sharedModel('BankaccountModel');
		$this->webconfig = $this->config->item('webconfig');
		
		$this->configuration = $this->ShopconfigurationModel->listData();
		if(!$this->LoginModel->isLoggedInUkm()){
			redirect(base_url().'login/');
		}	
	}
	function dashboard(){
		
		$this->pagename = $this->lang->line('dashboard');	
		$tdata = array();
		$sdata = array();
		
		$tdata['ukmdata'] = $sdata['ukmdata'] = $this->UkmModel->detailUkm($this->session->userdata('ukmid'),$this->session->userdata('ukmemail'));
		
		## LOAD LAYOUT ##
		$tdata['side_menu'] = $this->load->view($this->router->class.'/sidemenu_ukm',$sdata, true);
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		## LOAD DATA TEMPLATE ##
		$ldata['informationpage'] = $this->InformationpageModel->listData();
		$this->load->sharedView('template', $ldata);
	}
	function updateprofile(){
		$this->pagename = $this->lang->line('update_profile');	
		$tdata = array();

		$tdata['ukmdata'] = $sdata['ukmdata'] = $this->UkmModel->detailukm($this->session->userdata('ukmid'),$this->session->userdata('ukmemail'));

		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');

		if($_POST){
			
			$validator = new FormValidator();
		    $validator->addValidation("fullname","req",$this->lang->line('error_empty_name_ukm'));
		    $validator->addValidation("phone","req",$this->lang->line('error_empty_phone_ukm'));
			
		    if($validator->ValidateForm())
		    {
		        $doInsert = $this->UkmModel->updateData(array(
														        'id'=>$this->session->userdata('ukmid')
														        ,'fullname'=>$this->input->post('fullname')
														        ,'birthday'=>$this->input->post('birthday')
														        ,'gender'=>$this->input->post('gender')
														        ,'phone'=>$this->input->post('phone')
														        ,'address'=>$this->input->post('address')
														        ,'postcode'=>$this->input->post('postcode')
														        ,'a1' => $this->input->post('a1')
																,'b1' => $this->input->post('b1')
																,'a2' => $this->input->post('a2')
																,'b2' => $this->input->post('b2')
																,'width_resize' => $this->webconfig["width_resize"]
														  ));
				if($doInsert == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doInsert == 'exist_email'){
					$tdata['error_hash'] = array('error' => $this->lang->line('error_duplicate_email'));
				}else if($doInsert == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doInsert == 'not_same'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_password_not_same'));
				}else if($doInsert == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_update_profile');
				}
					if($doInsert !='success'){
						$tdata['fullname']   = $this->input->post('fullname');
						$tdata['birthday']   = $this->input->post('birthday');
						$tdata['gender']     = $this->input->post('gender');
						$tdata['phone']      = $this->input->post('phone');
						$tdata['address']      = $this->input->post('address');
						$tdata['postcode']   = $this->input->post('postcode');
					}
				$tdata['ukmdata'] = $sdata['ukmdata'] = $this->UkmModel->detailukm($this->session->userdata('ukmid'),$this->session->userdata('ukmemail'));	
				
				## LOAD DATA TEMPLATE ##
				$ldata['informationpage'] = $this->InformationpageModel->listData();
				
				$tdata['side_menu'] = $this->load->view($this->router->class.'/sidemenu_ukm',$sdata, true);
				$ldata['content'] = $this->load->view($this->router->class.'/ubahdataprofile',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {
		        $tdata['fullname']     = $this->input->post('fullname');
		        $tdata['birthday']     = $this->input->post('birthday');
			    $tdata['gender'] = $this->input->post('gender');
			    $tdata['phone'] = $this->input->post('phone');
				$tdata['address']      = $this->input->post('address');
				$tdata['postcode']   = $this->input->post('postcode');
		    	$tdata['error_hash'] = $validator->GetErrors();

				## LOAD DATA TEMPLATE ##
				$ldata['informationpage'] = $this->InformationpageModel->listData();
		    	
		    	$tdata['side_menu'] = $this->load->view($this->router->class.'/sidemenu_ukm',$sdata, true);
				$ldata['content'] = $this->load->view($this->router->class.'/ubahdataprofile',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{

			## LOAD LAYOUT ##
			$tdata['side_menu'] = $this->load->view($this->router->class.'/sidemenu_ukm',$sdata, true);
			$ldata['content'] = $this->load->view($this->router->class.'/ubahdataprofile',$tdata, true);

			## LOAD DATA TEMPLATE ##
			$ldata['informationpage'] = $this->InformationpageModel->listData();
			
			$this->load->sharedView('template', $ldata);
		}
	}

	function getAllDataDashboardOrder($page = 0){
		
		$listdata['lists'] = $this->UkmModel->listDataOrderUkm(array(
																'id_ukm' => $this->session->userdata('ukmid'),
																'limit' => '10'
															));
		
		$this->load->view($this->router->class.'/dashboard_orderlist', $listdata);
	}

	function order(){
		$this->pagename = $this->lang->line('orders');	
		$tdata = array();
		$sdata = array();

		$tdata['ukmdata'] = $sdata['ukmdata'] = $this->UkmModel->detailukm($this->session->userdata('ukmid'),$this->session->userdata('ukmemail'));

		## LOAD LAYOUT ##
		$tdata['side_menu'] = $this->load->view($this->router->class.'/sidemenu_ukm',$sdata, true);
		$ldata['content'] = $this->load->view($this->router->class.'/order',$tdata, true);
		## LOAD DATA TEMPLATE ##
		$ldata['informationpage'] = $this->InformationpageModel->listData();
		$this->load->sharedView('template', $ldata);
	}

	function getAllDataProject($page = 0){
		
		$all_data = $this->UkmModel->listDataCount();
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 3;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li><a class="active">';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->UkmModel->listDataOrderUkm(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
																,'id_ukm' => $this->session->userdata('ukmid'),
															));
		
		$this->load->view($this->router->class.'/orderlist', $listdata);
	}

	function changepassword(){
		$this->pagename = $this->lang->line('change_password');	
		$tdata = array();

		$tdata['ukmdata'] = $sdata['ukmdata'] = $this->UkmModel->detailukm($this->session->userdata('ukmid'),$this->session->userdata('ukmemail'));

		if($_POST){
			
			require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
			$validator = new FormValidator();
			$validator->addValidation("password","req",$this->lang->line('error_empty_password_members'));
			$validator->addValidation("password","minlen=5",$this->lang->line('error_password_5_members'));
			$validator->addValidation("password_1","req",$this->lang->line('error_empty_repeat_password_members'));
		    $validator->addValidation("password_1","eqelmnt=password",$this->lang->line('error_empty_retype_password_true_users'));
			
		    if($validator->ValidateForm())
		    {
		        $doInsert = $this->UkmModel->updatePassword(array(
														        'id'=>$this->session->userdata('ukmid')
														        ,'password'=>$this->input->post('password')
														        ,'password_1'=>$this->input->post('password_1')
														  ));
				if($doInsert == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doInsert == 'exist_email'){
					$tdata['error_hash'] = array('error' => $this->lang->line('error_duplicate_email'));
				}else if($doInsert == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doInsert == 'not_same'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_password_not_same'));
				}else if($doInsert == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_update_password');
					redirect(base_url().'login/logoutUkm');
				}

				$tdata['side_menu'] = $this->load->view($this->router->class.'/sidemenu_ukm',$sdata, true);
				$ldata['content'] = $this->load->view($this->router->class.'/ubahpassword',$tdata, true);
				## LOAD DATA TEMPLATE ##
				$ldata['informationpage'] = $this->InformationpageModel->listData();
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {

		    	$tdata['error_hash'] = $validator->GetErrors();
		    	$tdata['side_menu'] = $this->load->view($this->router->class.'/sidemenu_ukm',$sdata, true);
				$ldata['content'] = $this->load->view($this->router->class.'/ubahpassword',$tdata, true);
				## LOAD DATA TEMPLATE ##
				$ldata['informationpage'] = $this->InformationpageModel->listData();
				$this->load->sharedView('template', $ldata);
		    }
		}else{

			## LOAD LAYOUT ##
			$tdata['side_menu'] = $this->load->view($this->router->class.'/sidemenu_ukm',$sdata, true);
			$ldata['content'] = $this->load->view($this->router->class.'/ubahpassword',$tdata, true);

			## LOAD DATA TEMPLATE ##
			$ldata['informationpage'] = $this->InformationpageModel->listData();
			$this->load->sharedView('template', $ldata);
		}
	}
	
}