<?php
class Member extends CI_Controller {
	var $webconfig;
	var $pagename;
	var $configuration;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('common');
		$this->load->helper('image');
		$this->load->library('pagination');
		$this->lang->load('global', 'bahasa');
		$this->load->helper('phpmailer');
		$this->load->helper('image');
		$this->load->library('image_moo');
		$this->load->sharedModel('LoginModel');
		$this->load->sharedModel('MembersModel');
		$this->load->sharedModel('OrderModel');
		$this->load->sharedModel('BankaccountModel');
		$this->webconfig = $this->config->item('webconfig');
		
		$this->configuration = $this->ShopconfigurationModel->listData();
		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login/');
		}	
	}
	function dashboard(){
		
		$this->pagename = $this->lang->line('dashboard');	
		$tdata = array();
		$sdata = array();
		
		$tdata['memberdata'] = $sdata['memberdata'] = $this->MembersModel->detailMember($this->session->userdata('memberid'),$this->session->userdata('memberemail'));
		
		## LOAD LAYOUT ##
		$tdata['side_menu'] = $this->load->view($this->router->class.'/sidemenu_member',$sdata, true);
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		## LOAD DATA TEMPLATE ##
		$ldata['informationpage'] = $this->InformationpageModel->listData();
		$this->load->sharedView('template', $ldata);
	}
	function updateprofile(){
		$this->pagename = $this->lang->line('update_profile');	
		$tdata = array();

		$tdata['memberdata'] = $sdata['memberdata'] = $this->MembersModel->detailMember($this->session->userdata('memberid'),$this->session->userdata('memberemail'));

		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');

		if($_POST){
			
			$validator = new FormValidator();
		    $validator->addValidation("fullname","req",$this->lang->line('error_empty_name_members'));
		    $validator->addValidation("birthday","req",$this->lang->line('error_empty_birthday_members'));
		    $validator->addValidation("gender","req",$this->lang->line('error_empty_gender_members'));
		    $validator->addValidation("phone","req",$this->lang->line('error_empty_phone_members'));
			
		    if($validator->ValidateForm())
		    {
		        $doInsert = $this->MembersModel->updateData(array(
														        'id'=>$this->session->userdata('memberid')
														        ,'fullname'=>$this->input->post('fullname')
														        ,'birthday'=>$this->input->post('birthday')
														        ,'gender'=>$this->input->post('gender')
														        ,'phone'=>$this->input->post('phone')
														        ,'address'=>$this->input->post('address')
														        ,'postcode'=>$this->input->post('postcode')
														        ,'a1' => $this->input->post('a1')
																,'b1' => $this->input->post('b1')
																,'a2' => $this->input->post('a2')
																,'b2' => $this->input->post('b2')
																,'width_resize' => $this->webconfig["width_resize"]
														  ));
				if($doInsert == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doInsert == 'exist_email'){
					$tdata['error_hash'] = array('error' => $this->lang->line('error_duplicate_email'));
				}else if($doInsert == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doInsert == 'not_same'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_password_not_same'));
				}else if($doInsert == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_update_profile');
				}
					if($doInsert !='success'){
						$tdata['fullname']   = $this->input->post('fullname');
						$tdata['birthday']   = $this->input->post('birthday');
						$tdata['gender']     = $this->input->post('gender');
						$tdata['phone']      = $this->input->post('phone');
						$tdata['address']      = $this->input->post('address');
						$tdata['postcode']   = $this->input->post('postcode');
					}
				$tdata['memberdata'] = $sdata['memberdata'] = $this->MembersModel->detailMember($this->session->userdata('memberid'),$this->session->userdata('memberemail'));	
				
				## LOAD DATA TEMPLATE ##
				$ldata['informationpage'] = $this->InformationpageModel->listData();
				
				$tdata['side_menu'] = $this->load->view($this->router->class.'/sidemenu_member',$sdata, true);
				$ldata['content'] = $this->load->view($this->router->class.'/ubahdataprofile',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {
		        $tdata['fullname']     = $this->input->post('fullname');
		        $tdata['birthday']     = $this->input->post('birthday');
			    $tdata['gender'] = $this->input->post('gender');
			    $tdata['phone'] = $this->input->post('phone');
				$tdata['address']      = $this->input->post('address');
				$tdata['postcode']   = $this->input->post('postcode');
		    	$tdata['error_hash'] = $validator->GetErrors();

				## LOAD DATA TEMPLATE ##
				$ldata['informationpage'] = $this->InformationpageModel->listData();
		    	
		    	$tdata['side_menu'] = $this->load->view($this->router->class.'/sidemenu_member',$sdata, true);
				$ldata['content'] = $this->load->view($this->router->class.'/ubahdataprofile',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{

			## LOAD LAYOUT ##
			$tdata['side_menu'] = $this->load->view($this->router->class.'/sidemenu_member',$sdata, true);
			$ldata['content'] = $this->load->view($this->router->class.'/ubahdataprofile',$tdata, true);

			## LOAD DATA TEMPLATE ##
			$ldata['informationpage'] = $this->InformationpageModel->listData();
			
			$this->load->sharedView('template', $ldata);
		}
	}
	
	function order(){
		$this->pagename = $this->lang->line('orders');	
		$tdata = array();
		$sdata = array();

		$tdata['memberdata'] = $sdata['memberdata'] = $this->MembersModel->detailMember($this->session->userdata('memberid'),$this->session->userdata('memberemail'));

		## LOAD LAYOUT ##
		$tdata['side_menu'] = $this->load->view($this->router->class.'/sidemenu_member',$sdata, true);
		$ldata['content'] = $this->load->view($this->router->class.'/order',$tdata, true);
		## LOAD DATA TEMPLATE ##
		$ldata['informationpage'] = $this->InformationpageModel->listData();
		
		
		$this->load->sharedView('template', $ldata);
	}

	function getAllDataDashboardOrder($page = 0){
		
		$listdata['lists'] = $this->OrderModel->listData(array(
																'statusorder' => '0'
															));
		
		$listdata['bank_lists'] = $this->BankaccountModel->listData();
		$this->load->view($this->router->class.'/dashboard_orderlist', $listdata);
	}

	function getAllDataProject($page = 0){
		
		$all_data = $this->OrderModel->listDataCount();
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 3;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li><a class="active">';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->OrderModel->listData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
															));
		
		$listdata['bank_lists'] = $this->BankaccountModel->listData();
		$this->load->view($this->router->class.'/orderlist', $listdata);
	}

	function viewproject($id = 0){
		$this->pagename = $this->lang->line('orders');
		if($id == 0){
			redirect(base_url().$this->router->class);
		}

		$tdata['lists'] = $this->OrderModel->listData(array('id'=>$id));
		$this->load->view($this->router->class.'/vieworder',$tdata);
	}

	function changepassword(){
		$this->pagename = $this->lang->line('change_password');	
		$tdata = array();

		$tdata['memberdata'] = $sdata['memberdata'] = $this->MembersModel->detailMember($this->session->userdata('memberid'),$this->session->userdata('memberemail'));

		if($_POST){
			
			require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
			$validator = new FormValidator();
			$validator->addValidation("password","req",$this->lang->line('error_empty_password_members'));
			$validator->addValidation("password","minlen=5",$this->lang->line('error_password_5_members'));
			$validator->addValidation("password_1","req",$this->lang->line('error_empty_repeat_password_members'));
		    $validator->addValidation("password_1","eqelmnt=password",$this->lang->line('error_empty_retype_password_true_users'));
			
		    if($validator->ValidateForm())
		    {
		        $doInsert = $this->MembersModel->updatePassword(array(
														        'id'=>$this->session->userdata('memberid')
														        ,'password'=>$this->input->post('password')
														        ,'password_1'=>$this->input->post('password_1')
														  ));
				if($doInsert == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doInsert == 'exist_email'){
					$tdata['error_hash'] = array('error' => $this->lang->line('error_duplicate_email'));
				}else if($doInsert == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doInsert == 'not_same'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_password_not_same'));
				}else if($doInsert == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_update_password');
					redirect(base_url().'login/logout');
				}

				$tdata['side_menu'] = $this->load->view($this->router->class.'/sidemenu_member',$sdata, true);
				$ldata['content'] = $this->load->view($this->router->class.'/ubahpassword',$tdata, true);
				## LOAD DATA TEMPLATE ##
				$ldata['informationpage'] = $this->InformationpageModel->listData();
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {

		    	$tdata['error_hash'] = $validator->GetErrors();
		    	$tdata['side_menu'] = $this->load->view($this->router->class.'/sidemenu_member',$sdata, true);
				$ldata['content'] = $this->load->view($this->router->class.'/ubahpassword',$tdata, true);
				## LOAD DATA TEMPLATE ##
				$ldata['informationpage'] = $this->InformationpageModel->listData();
				$this->load->sharedView('template', $ldata);
		    }
		}else{

			## LOAD LAYOUT ##
			$tdata['side_menu'] = $this->load->view($this->router->class.'/sidemenu_member',$sdata, true);
			$ldata['content'] = $this->load->view($this->router->class.'/ubahpassword',$tdata, true);

			## LOAD DATA TEMPLATE ##
			$ldata['informationpage'] = $this->InformationpageModel->listData();
			$this->load->sharedView('template', $ldata);
		}
	}

	function changeStatus() {
		$tdata = array();
		
		$doChange = $this->OrderModel->changeStatusOrder(array(
															'orderid' => $this->input->post('orderid')
															,'status' => $this->input->post('status')
															,'shipping_agen' => $this->input->post('agen')
															,'shipping_number' => $this->input->post('no')
															,'to_bank' => $this->input->post('to_bank')
															,'from_bank' => $this->input->post('from_bank')
															,'total_transfered' => $this->input->post('total_transfered')
															,'account_name_bank' => $this->input->post('account_name_bank')
														));
		
		if($doChange == 'empty'){
			$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
		}else if($doChange == 'failed'){
			$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
		}else if($doChange == 'success'){
			$tdata['success'] = $this->lang->line('msg_success_changestatus_order');
			if($this->input->post('status') != 4) {
				$orderDetail = $this->OrderModel->listData(array('id' => $this->input->post('orderid')));
				switch($this->input->post('status')) {
					case 8:
						$this->load->library('email');

						$this->email->initialize(array(
						  'protocol' => 'smtp',
						  'smtp_host' => HOST_EMAIL,
						  'smtp_user' => USER_EMAIL,
						  'smtp_pass' => PASS_EMAIL,
						  'smtp_port' => PORT_EMAIL,
						  'crlf' => "\r\n",
						  'newline' => "\r\n",
						  'mailtype'  => 'html', 
				           'charset'   => 'iso-8859-1'
						));


						$this->email->from('noreply@snekbook.com', 'Snekbook Admin');
						$this->email->to($this->configuration['email_contact']);

						$this->email->subject('Konfirmasi Pembayaran Snekbook');
						$message = $this->load->view($this->router->class.'/email_payment_confirmation', 
																							array(
																								"listdata" => isset($orderDetail)?$orderDetail:""
																							), TRUE);
						$this->email->message($message);
						$this->email->send();
					break;
				}
			}
		}

		$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode($tdata));
	}

	function connecttofb(){
		### FB LOGIN UNTU URL LOGIN ###
		FacebookSession::setDefaultApplication(fb_app_id, fb_app_secret);
		$helper = new FacebookRedirectLoginHelper("http://".APP_DOMAIN."/member/connecttofb/");

		try
		{
		  // In case it comes from a redirect login helper
		  $session = $helper->getSessionFromRedirect();
		}
		catch( FacebookRequestException $ex )
		{
		  // When Facebook returns an error
		  echo $ex;
		}
		catch( Exception $ex )
		{
		  // When validation fails or other local issues
		  echo $ex;
		}

		// see if we have a session in $_Session[]
		if( isset($_SESSION['token']))
		{
		    // We have a token, is it valid?
		    $session = new FacebookSession($_SESSION['token']);
		    try
		    {
		        $session->Validate(fb_app_id, fb_app_secret);
		    }
		    catch( FacebookAuthorizationException $ex)
		    {
		        // Session is not valid any more, get a new one.
		        $session ='';
		    }
		}


		### HASIL LOGIN ##
		if ( isset( $session ) && $session != ''  ){
			$_SESSION['token'] = $session->getToken();
			$user_profile = (new FacebookRequest($session, 'GET', '/me'))->execute()->getGraphObject(GraphUser::className());

			### CEK USER SUDAH TERDAFTAR SEBELUMNYA ATAU BELUM ##
			$data_profile = $user_profile->asArray();

			$cekfblogin = $this->LoginModel->checkFbLoginmember($data_profile['id']);

			if(count($cekfblogin) == 0){
				$doUpdateLogin = $this->LoginModel->updateFacebookMember($this->session->userdata('memberid'), $data_profile['id']);

				if($doUpdateLogin){
					redirect(base_url().$this->router->class.'/dashboard/success_fb_connect');
				}else{
					redirect(base_url().$this->router->class.'/dashboard/error_fb_connect');
				}
			}else{
				redirect(base_url().$this->router->class.'/dashboard/error_fb_connect_exist');
			}
		}
		#### END
	}
	
}