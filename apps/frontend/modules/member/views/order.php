<script type="text/javascript">
  jQuery(document).ready(function($) {
    $("#listData").load('<?php echo base_url().$this->router->class; ?>/getAllDataProject/');
  });
  function searchThis(){
    var frm = document.searchform;
    var name = frm.name.value;
    if(name == ''){ name = '-'; }
    jQuery("#listData").load("<?php echo base_url().$this->router->class; ?>/searchdata/"+name.replace(/\s/g,'%20'));
  }
</script>
<div class="container">
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li class="active">Order Belanja</li>
    </ol>
</div>

<div class="border-bottom"></div>

<div class="container"><!--- Container -->
    <div class="row">
        <div class="col-lg-3">
            <?php echo isset($side_menu)?$side_menu:''; ?>
        </div>
        
        <div class="col-lg-9 border-left" style="padding-bottom:30px;">
          <div class="box-border margin-top-20">
            <div class="row">
              <div class="col-md-6">
                <div class="head-border">ORDER BELANJA</div>
              </div>
              <div class="col-md-4">
                <input type="text" class="form-control" name="name" placeholder="<?php echo $this->lang->line('label_noinvoice_order'); ?>" style="width:100%; ">
              </div>
              <div class="col-md-2">
                <button class="btn btn-mau-b" type="button" onclick="searchThis()"><i class="fa fa-search"></i> <?php echo $this->lang->line('search'); ?></button>
              </div>
            </div>
          </div>
          <div id="listData">
            <center>
              <img src='<?php echo $this->webconfig['back_base_template']; ?>img/loading.gif' align='middle' style="margin:5px; width:auto;" />
            </center>
          </div>
        </div>
    </div>      
</div><!--- Container -->