<script type="text/javascript">
jQuery(document).ready(function($) {

});

</script>
<div class="container">
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li class="active">Profil</li>
    </ol>
</div>

<div class="border-bottom"></div>

<div class="container"><!--- Container -->
    <div class="row">
        <div class="col-lg-3">
            <?php echo isset($side_menu)?$side_menu:''; ?>
        </div>
        
        <div class="col-lg-9 border-left" style="padding-bottom:30px;">
            <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
                <?php foreach($error_hash as $inp_err){ ?>
                  <script type="text/javascript">
                  jQuery(document).ready(function($) {
                      toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                      });
                  </script>
                <?php } ?>
            <?php } ?>
            <?php if(isset($success)){ ?>
                <script type="text/javascript">
                jQuery(document).ready(function($) {
                  toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                  });
                </script>
            <?php } ?>
            <div class="head-border margin-top-20">UBAH PASSWORD KAMU</div>
                <form method="post" action="" enctype="multipart/form-data">
                    <div class="row margin-top-10">
                        <div class="col-md-6">
                            <label>Password Baru</label>
                            <input type="password" class="form-control" name="password" placeholder="" value="<?php echo isset($password)?$password:'';?>"/>
                        </div>
                    </div>
                    <div class="row margin-top-10">
                        <div class="col-md-6">
                            <label>Ulangi Password</label>
                            <input type="password" class="form-control" name="password_1" placeholder="" value="<?php echo isset($password_1)?$password_1:'';?>" />
                        </div>
                    </div>
                    <div class="row margin-top-10">
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-mau-b" value="SIMPAN">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>      
</div><!--- Container -->