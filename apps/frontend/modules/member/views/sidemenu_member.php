<div class="margin-top-20"></div>
<div class="img-profil">
    <img src="<?php echo isset($memberdata['logo_img'])?thumb_image($memberdata['logo_img'],'200x200', 'profile'):$this->webconfig['frontend_template'].'images/default.jpg'; ?>" class="img-responsive" />
</div>
<div class="margin-top-20"></div>
<a href="<?php echo base_url(); ?>member/dashboard/"  ><div <?php if($this->pagename == $this->lang->line('dashboard')){ echo 'class="side-profil activeprofil"'; }else{  echo 'class="side-profil"';}?>>Dashboard</div></a>

<a href="<?php echo base_url(); ?>member/updateprofile/" ><div <?php if($this->pagename == $this->lang->line('update_profile')){ echo 'class="side-profil activeprofil"'; }else{  echo 'class="side-profil"';}?> >Edit Profil</div></a>

<a href="<?php echo base_url(); ?>member/order/" ><div <?php if($this->pagename == $this->lang->line('orders')){ echo 'class="side-profil activeprofil"'; }else{  echo 'class="side-profil "';}?> >Order Belanja</div></a>

<a href="<?php echo base_url(); ?>member/changepassword/" ><div <?php if($this->pagename == $this->lang->line('change_password')){ echo 'class="side-profil activeprofil"'; }else{  echo 'class="side-profil"';}?> ><?php echo $this->lang->line('change_password'); ?></div></a>

<!--<a href="" ><div <?php if($this->pagename == $this->lang->line('dashboard')){ echo 'class="side-profil"'; }else{  echo 'class="side-profil"';}?>>Konfirmasi Barang</div></a> -->

<a href="<?php echo base_url(); ?>login/logout/" ><div <?php if($this->pagename == $this->lang->line('logout')){ echo 'class="side-profil activeprofil"'; }else{  echo 'class="side-profil"';}?>><?php echo $this->lang->line('logout'); ?></div></a>
<div class="margin-bottom-20"></div>