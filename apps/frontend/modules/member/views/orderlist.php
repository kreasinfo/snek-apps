
<style type="text/css">
	div.jqismooth { width: 400px; }
	.frm_label { display:block; width: 140px; text-align: left; float: left;}
	p { text-align: left; }
</style>
<script type="text/javascript">
jQuery(document).ready(function($) {
	$('#checkall').click(function () {
		$('#tabellistdata .check-all').attr('checked', this.checked);
	});
	$(".pagination a").click(function(e){
		// stop normal link click
		e.preventDefault(); 
		var linkUrl = jQuery(this).attr("href");
		if(linkUrl){
			jQuery('#listData').load(linkUrl.replace(/\s/g,'%20'));
		}
	});
	$('.fancybox').fancybox();
});
function changeStatus(status,orderid,gran_total) {
	var nextStatus = 8;
	var txt = "<div style='text-align: center;'><?php echo $this->lang->line('alert_payment_accepted'); ?> <label class='orid'>#"+orderid+"</label> <?php echo $this->lang->line('alert_payment_accepted2'); ?><br /><br />";
		txt += "<p><label class='frm_label'><?php echo $this->lang->line('label_to_bank'); ?></label><select name='to_bank'><option value=''>-- <?php echo $this->lang->line('label_to_bank'); ?> --</option><?php if(isset($bank_lists) && count($bank_lists) > 0) { foreach($bank_lists as $l) { echo '<option value='.$l['id'].'>'.$l['name'].' ('.$l['account_rek'].')</option>'; } } ?></select></p>";
		txt += "<p><label class='frm_label'><?php echo $this->lang->line('label_from_bank'); ?></label><input type='text' class='text txt_input' name='from_bank' id='from_bank' /></p>";
		txt += "<p><label class='frm_label'><?php echo $this->lang->line('label_total_transfered'); ?></label><input type='text' class='text numeric txt_input' name='total_transfered' id='total_transfered' value='"+gran_total+"' readonly/></p>";
		txt += "<p><label class='frm_label'><?php echo $this->lang->line('label_account_name_bank'); ?></label><input type='text' class='text txt_input' name='account_name_bank' id='account_name_bank' /></p>";
		txt += "<?php echo $this->lang->line('alert_payment_accepted3'); ?>";
		txt += "<input type='hidden' id='orderid' name='orderid' value='"+orderid+"' /> <input type='hidden' id='status' name='status' value='"+nextStatus+"' /></div>";
	jQuery.prompt(txt ,{
		buttons: {
			<?php echo $this->lang->line('ok'); ?>: true,
			<?php echo $this->lang->line('cancel'); ?>: false },
			prefix:'jqismooth',
			focus: 1,
			submit: function(e,v,m,f){
				if(v){
					var orderid = f.orderid;
					var status = f.status;
					var to_bank = f.to_bank;
					var from_bank = f.from_bank;
					var total_transfered = f.total_transfered;
					var account_name_bank = f.account_name_bank;
					if(to_bank && total_transfered && from_bank && account_name_bank !=''){
						$.ajax({
							type: 'POST',
							url: "<?php echo base_url().$this->router->class; ?>/changeStatus/",
							data: {'orderid':orderid, 'status':status, 'to_bank':to_bank, 'from_bank':from_bank, 'total_transfered':total_transfered, 'account_name_bank':account_name_bank },
							dataType: 'json',
							success: function(response) {
								console.log(response);
								if(typeof response.error_hash != 'undefined'){
									$.each(response.error_hash, function(a,b) {
										toastr.error("", response.error_hash.error);
									});

								}
								if(typeof response.success != 'undefined') {
									toastr.success("", response.success);
								}
								$("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
							}
						});
					}else{
						alert('Data ada yang belum terisi');
						return false;
					}
					
				}else{
					$.prompt.close();
				}
			}
		}
	);
	
	$('.numeric').numeric();
}

function format_hasil(jumlahtot){
    jumlahtot = jumlahtot.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(jumlahtot))
        jumlahtot = jumlahtot.replace(pattern, "$1.$2");
    return jumlahtot;
}
</script>

<form id="tabellistdata" method="post" action="<?php echo base_url().$this->router->class; ?>/postProcess" onsubmit="return false;">
	<div class="box-border margin-top-10">
	  	<table class="table table-responsive">
			<thead>
				<th>No</th>
				<th>Tanggal</th>
				<th>No. Invoice</th>
				<th>Status</th>
				<th>Opsi</th>
			</thead>
			<tbody>
			<?php if(isset($lists) && count($lists) > 0){ ?>
			<?php $i = $start_no; foreach ($lists as $list) { $i++; ?>
			<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo isset($list['datecreated'])?date_lang_reformat_long($list['datecreated']):'';?></td>
				<td>
					<b>#<?php echo $list['id']; ?></b>
					<a href="<?php echo base_url().$this->router->class.'/viewproject/'.$list['id']; ?>" class="fancybox fancybox.ajax">
                    	<button class="btn btn-primary btn-xs" type="button">
                			Detail Order
                		</button>
                	</a>
				</td>
				<td><?php echo statusOrder($list['status']); ?></td>
				<td>
					<?php if($list['status'] == 0) {?>
                    	<button class="btn btn-danger btn-xs" type="button" onclick="changeStatus('<?php echo isset($list['status'])?$list['status']:'';?>','<?php echo isset($list['id'])?$list['id']:''; ?>','<?php echo isset($list['gran_total'])?format_price($list['gran_total'],''):''; ?>')">
                			Konfirmasi Pembayaran
                		</button>
                	<?php } ?>
				</td>
			</tr>
			<?php } ?>
			<?php }else{ ?>
				<tr>
					<td colspan="5"><center>Tidak ada data</center></td>
				</tr>
			<?php } ?>
			</tbody>
	    </table>
	</div>
</form>