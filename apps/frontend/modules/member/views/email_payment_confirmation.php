<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PAYMENT CONFIRMATION</title>
</head>

<body>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#f7f7f7" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:1.4em; color:#333;">
  <tr>
    <td>
      <table width="640" border="0" cellpadding="0" cellspacing="0" style="margin:20px auto;">
        <tr style="background:#FFF;">
            <td style="text-align:center; padding:20px 0;border-left:1px solid #CCC; border-right:1px solid #CCC; border-top:1px solid #CCC;"><a href="<?php echo base_url(); ?>" target="_blank">
            <img src="<?php echo $this->webconfig['shop_template']; ?>images/Logo.png" alt="Snekbook" style="border:none;" /></a>
            </td>
        </tr>
          <tr>
            <td style="background:#FFF; border-left:1px solid #ddd; border-right:1px solid #ddd; border-bottom:1px solid #ddd; padding:10px 20px;">
            <p style="border-bottom:1px solid #ddd;font-size:16px; font-weight:bold; text-transform:uppercase; color:#333; padding:10px 0; margin:0;">Konfirmasi Pembayaran</p>
              <p>
                Dear, <strong>Admin</strong><br/>
                Customer Anda telah melakukan konfirmasi pembayaran untuk <strong>Order ID #<?php echo isset($listdata[0]['id'])?$listdata[0]['id']:'';?></strong>
              </p>
                <p>Silahkan melakukan pengecekan kembali di data order untuk melanjutkan proses pembelian customer tersebut</p>
                <p><strong>Admin Snekbook.com</strong><br /></p>
                        Jl. Hasanudin No.28 | Bandung - Jawa Barat<br />
                        INDONESIA <br />
                        +62812 2081 7720 | snekbook@gmail.com<br />
            </td>
          </tr>
          <tr>
            <td height="40" style="font-size:11px; color:#666;">
              <a href="<?php echo "http://".APP_DOMAIN."/"; ?>" target="_blank" style="color:#333;text-decoration:none;"><strong>snekbook.com</strong></a><br/>
              &copy; <?php echo date('Y'); ?> <?php echo isset($this->configuration['site_title'])?$this->configuration['site_title']:'Site Name'?>. Call Center <strong><?php echo isset($this->configuration['phone'])?$this->configuration['phone']:'000 - 1234546'?></strong>
            </td>
          </tr>
        </table>
    </td>
  </tr>
</table>
</body>
</html>