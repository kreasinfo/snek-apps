<script type="text/javascript">
  $(document).ready(function() {
    $('.pgwSlideshow').pgwSlideshow();
  });
function searchThis(){
  var frm = document.formsearch;
  var keyword = frm.keyword.value;
  var city = frm.city.value;
  if(keyword == ''){ keyword = '-'; }
  if(city == ''){ city = '-'; }
  location = '<?php echo base_url(); ?>produk/search/'+keyword.replace(/\s/g,'%20')+'/-/'+city.replace(/\s/g,'%20');
}
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/id_EN/all.js#xfbml=1&appId=178152138907771";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<div class="container">

    <ol class="breadcrumb">

      <li><a href="<?php echo base_url(); ?>">Home</a></li>

      <li><a href="<?php echo base_url(); ?>produk">produk</a></li>

      <li class="active">Detail Produk</li>

    </ol>

</div>

<div class="border-bottom"></div>

<?php /*echo json_encode($this->session->all_userdata());*/?>

<div class="container">

  <div class="row margin-top-30">

      <div class="col-lg-9"><!-- Main Konten -->

          <div class="info-product pull-left"><i class="fa fa-map-marker"></i>&nbsp;<?php echo isset($listdata['id_lokasi_ukm']['name'])?$listdata['id_lokasi_ukm']['name']:'';?></div>
            <!-- <div class="info-product pull-left"><i class="fa fa-tags"></i>&nbsp;<?php echo isset($listdata['id_ukm']['name'])?$listdata['id_ukm']['name']:'';?></div> -->

           <!--  <div class="info-product pull-left"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/rating.png" /></div> -->
           <div class="info-product pull-right hidesearch" style="padding: 0px;border: 0px;">
           <div class="col-md-12">
              <div class="form-bg" style="padding: 0px">
                  <div class="row">
                      <form id="formsearch" name="formsearch" method="POST" action="<?php echo base_url();?>produk/search/" onsubmit="searchThis(); return false;" >
                        <div class="col-md-4"><input class="form-control" type="text" name="keyword" placeholder="Nama Produk" style="height: 33px;" /></div>
                          <div class="col-md-5 no-padding">
                            <div class="formgroup">
                              <select id="city" name="city" class="form-control" style="height: 33px;">
                                <option value="">-- Pilih Kota --</option>
                                  <?php if(isset($listlokasiukm) && $listlokasiukm !=''){?>
                                    <?php foreach ($listlokasiukm as $key => $value) {?>
                                      <option value="<?php echo $value['id'];?>" <?php if(isset($id_lokasiukm) && $id_lokasiukm == $value['id']) {echo "selected=selected";}?>><?php echo $value['name'];?></option>
                                    <?php }?>
                                  <?php } ?>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <input type="submit" class="btn btn-danger btn-block" value="CARI" style="border-radius: 0px;" />
                          </div>
                      </form>
                    </div>
                </div>
            </div>
            </div>
            <div class="clear"></div>

            <div class="border-bottom margin-top-10"></div>

          <div class="row margin-top-20">

              <div class="col-md-8">

                    <ul class="pgwSlideshow">

                        <?php if (isset($listdata['images']) && count($listdata['images']) > 0) { ?>

                            <?php foreach ($listdata['images'] as $key => $value) { ?>

                                <li><img src="<?php echo thumb_image($value['path'],'555x311','product'); ?>"></li>

                            <?php } ?>

                        <?php } ?>

                    </ul>

                </div>

                

                <div class="col-md-4 no-padding-left">

                  <div class="ttl-detail-produk"><h3 class="nomargin"><?php echo clean_str($listdata['name']); ?></h3></div>

                  <i style="font-size:11px;color:red;">

                      <?php

                      $data = '';

                      if($listdata['status_produk'] !=''){

                          if($listdata['status_produk'] == '0'){

                              $data = '*Bandung Only';

                          }else{

                              $data = '';

                          }

                      }

                      echo $data;

                      ?>

                  </i>

                  <div class="ttl-detail-produk">DESKRIPSI PRODUK :</div>

                  <p class="produk-des">

                      <?php echo clean_str($listdata['description']); ?>

                    </p>
                    <div class="row margin-top-10">

                      <div class="col-xs-6">

                        <div class="ttl-detail-produk">HARGA PRODUK :</div>

                        <div class="item-produk"><?php echo rupiah($listdata['price']); ?></div>

                        </div>

                        <div class="col-xs-6">

                        <div class="ttl-detail-produk">BERAT PRODUK :</div>

                        <div class="item-produk"><?php echo number_format($listdata['berat'], 0, ',', '.'); ?> Gram</div>

                        </div>

                    </div>

                    <div class="row margin-top-10">

                      <div class="col-xs-6">

                        <div class="ttl-detail-produk">Pilih Rasa :</div>

                        <div class="item-produk">
                          <select class="select" name="ukuran" id="ukuranproduct" style="width:100px;">

                              <option value=""><?php echo $this->lang->line('label_pilih_rasa'); ?></option>

                              <?php foreach($listdata['produk_ukuran'] as $key => $value){ ?>

                                  <option value="<?php echo $value['id']; ?>"><?php echo $value['size']; ?></option>

                              <?php } ?>

                          </select>

                        </div>

                      </div>

                      <div class="col-xs-6">

                        <div id="quantityblock">

                        </div>

                        <div id="quantityblock_temp" style="display:none;">

                          <div class="ttl-detail-produk">JUMLAH :</div>

                          <div class="item-produk">

                            <select id="qty" name="stock" style="width:100px;">

                                <option value="">- Pilih -</option>

                            </select>

                          </div>

                        </div>

                      </div>

                    </div>

                    <div class="row margin-top-10">

                      <div class="col-xs-12">

                        <?php if(isset($listdata['stock']['jumlah_stock']) && $listdata['stock']['jumlah_stock'] > 0){ ?>

                        <div class="">

                        
                        <center>
                          <a href="javascript:addtocartbeli('<?php echo $listdata['id']; ?>')" class="" style="">
                            <div class="col-xs-6 btn btn-mau-b  margin-top-20" style="width: 48%;margin-right:1%;">
                             BELI
                            </div>
                          </a>
                        </center>
                        

                        
                          <center>
                            <a style="" href="javascript:addtocart('<?php echo $listdata['id']; ?>')" class="">
                              <div class="col-xs-6 btn btn-mau-b  margin-top-20" style="width: 47%;margin-left:4%;padding: 8px 10px;letter-spacing: 0px;">
                                KERANJANG
                              </div>
                            </a>
                          </center>
                        </div>

                        <?php }else{ ?>

                          <a href="#" class="cart big-button"><?php echo $this->lang->line('stock_empty'); ?></a>

                        <?php } ?>

                      </div>

                    </div>

                </div>

          </div>

            

            <div class="head-comment margin-top-30"><i class="fa fa-comment"></i>  PRODUK <span class="ft-red">TESTIMONI</span></div>

            <div class="border-bottom margin-bottom-20 margin-top-10"></div>

            <div class="fb-comments" data-href="<?php echo base_url() ?>produk/detail/<?php echo $listdata['id']; ?>/<?php echo $listdata['slug']; ?>" data-width="860" data-num-posts="5" data-colorscheme="light"></div>

        </div><!--- End Main Konten -->

        

        <div class="col-lg-3"><!-- Side -->

          <div class="box-side-product">

              <div class="ttl-side">PRODUK <span class="ft-red">SEJENIS</span></div>

                <div class="border-bottom margin-bottom-10"></div>



                <?php if(isset($listrelated) && count($listrelated) > 0){ ?>

                <?php foreach($listrelated as $key => $listdata){ ?>

                <?php if(isset($listdata['images']['path']) && $listdata['images']['path'] != ''){ ?>

                <div class="box-img margin-top-10">

                    <a href="<?php echo base_url().'produk/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><img src="<?php echo thumb_image($listdata['images']['path'],'360x203', 'product'); ?>" class="img-responsive" style="width: 100%;"/></a>

                    <div class="location"><i class="fa fa-map-marker ft-yellow"></i> &nbsp;<?php echo isset($listdata['id_lokasi_ukm']['name'])?$listdata['id_lokasi_ukm']['name']:'';?></div>

                </div>

                <?php } ?>

                <div class="row margin-top-10">

                  <div class="col-sm-12"><center><a href="<?php echo base_url().'produk/read/'.$listdata['id'].'/'.$listdata['slug']; ?>" class="name-product margin-bottom-10 margin-top-10"><?php echo clean_str($listdata['name']); ?></a></center></div>

                </div>

                <div class="row margin-top-10">

                    <div class="col-xs-6"><div class="price-home pull-left"><?php echo rupiah($listdata['price']); ?></div></div>

                    <div class="col-xs-6"><a href="<?php echo base_url().'produk/read/'.$listdata['id'].'/'.$listdata['slug']; ?>" class="btn-mau-p pull-right">Beli</a></div>

                </div>

                <div class="border-bottom margin-top-10"></div>

                <?php } ?>

                <?php } ?>



            </div>

        </div><!--- End Side -->

    </div>

</div>



<div class="bg-partner margin-top-40"><!-- Partner -->

    <div class="container">

    <div class="head-partner">OUR <span class="ft-red">PARTNER</span></div>

        <ul id="flexiselDemo1"> 

            <li><img src="<?php echo $this->webconfig['frontend_template']; ?>images/logo-nyt.png" /></li>

            <li><img src="<?php echo $this->webconfig['frontend_template']; ?>images/logo-microsoft.png" /></li>    

            <li><img src="<?php echo $this->webconfig['frontend_template']; ?>images/logo-ebay.png" /></li>     

            <li><img src="<?php echo $this->webconfig['frontend_template']; ?>images/logo-hp.png" /></li> 

            <li><img src="<?php echo $this->webconfig['frontend_template']; ?>images/room23.png" /></li>
            
            <li><img src="<?php echo $this->webconfig['frontend_template']; ?>images/logo-youtube.png" /></li>
       
        </ul>

    </div>

</div><!-- End Partner -->