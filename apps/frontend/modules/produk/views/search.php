<script type="text/javascript">
  jQuery(document).ready(function($) {
    
    $('#keyword').bind('keypress', function(e) {
        var code = e.keyCode || e.which;
        if(code == 13) { //Enter keycode
           
            var keyword = $(this).val();
            if(keyword ==''){
                keyword = '-';
            }
            var pathname = window.location.pathname;
            var exp = pathname.split("/");
            
            window.location.href = "<?php echo base_url().$this->router->class; ?>/search/"+keyword+"/"+exp[5].replace(/\s/g,'%20')+"/"+exp[6].replace(/\s/g,'%20');
        }
    });
    
    $("#price").change(function() {
        var keyword = $(this).val();
        if(keyword ==''){
            keyword = '-';
        }
        var pathname = window.location.pathname;
        var exp = pathname.split("/");
        
        window.location.href = "<?php echo base_url().$this->router->class; ?>/search/"+exp[4].replace(/\s/g,'%20')+"/"+keyword+"/"+exp[6].replace(/\s/g,'%20');
    });
    $("#kota").change(function() {
        var keyword = $(this).val();
        if(keyword ==''){
            keyword = '-';
        }
        var pathname = window.location.pathname;
        var exp = pathname.split("/");
        
        window.location.href = "<?php echo base_url().$this->router->class; ?>/search/"+exp[4].replace(/\s/g,'%20')+"/"+exp[5].replace(/\s/g,'%20')+"/"+keyword;
    });
  });
</script>
<div class="container">
    <div class="box-border margin-top-20">
        <div class="row">
            <div class="col-md-3"><h4>HASIL <span class="ft-red">PENCARIAN</span></h4></div>
            <div class="col-md-9">
            
            <div class="row">
                <div class="col-md-3">
                    <p class="margin-top-10">Filter Berdasarkan :</p>
                </div>
                <div class="col-md-3">
                    <input class="form-control" type="text" name="keyword" placeholder="Nama Produk" id="keyword" value="<?php echo isset($keyword)?$keyword:'';?>" />
                </div>
                <div class="col-md-3">
                    <div class="form-res">
                        <select id="price" class="form-control">
                            <option value="">-- Pilih --</option>
                            <option value="1" <?php if(isset($price) && $price == '1') {echo "selected=selected";}?>>Harga Terendah</option>
                            <option value="2" <?php if(isset($price) && $price == '2') {echo "selected=selected";}?>>Harga Tertinggi</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-res">
                    <select id="kota" class="form-control">
                        <option value="">-- Pilih Kota --</option>
                        <?php if(isset($listlokasiukm) && $listlokasiukm !=''){?>
                            <?php foreach ($listlokasiukm as $key => $value) {?>
                                <option value="<?php echo $value['id'];?>" <?php if(isset($id_lokasiukm) && $id_lokasiukm == $value['id']) {echo "selected=selected";}?>><?php echo $value['name'];?></option>
                            <?php }?>
                        <?php } ?>
                    </select>
                    </div>
                </div>
            </div>
            
            </div>
        </div>
    </div>
    
    <?php if(isset($lists) && count($lists) > 0){ ?>
    <?php foreach($lists as $listdata){ ?>
    <div class="box-grey margin-top-20"><!-- item -->
        <div class="row">
            <div class="col-md-5">
                <a href="<?php echo base_url().'produk/read/'.$listdata['id'].'/'.$listdata['slug']; ?>">
                <?php if(isset($listdata['images']['path']) && $listdata['images']['path'] != ''){ ?>
                    <img src="<?php echo thumb_image($listdata['images']['path'],'360x203', 'product'); ?>" class="img-responsive" style="width: 100%;">
                <?php } ?>
                </a>
            </div>
            <div class="col-md-7">
                <div class="row">
                    <div class="col-md-4">
                        <div class="bold-search">Nama Produk</div>
                        <a href="<?php echo base_url().'produk/read/'.$listdata['id'].'/'.$listdata['slug']; ?>" class="ttl-pro-search"><?php echo $listdata['name']; ?></a>
                    </div>
                    <div class="col-md-2">
                        <div class="bold-search">Harga</div>
                        <div class="ft-red"><?php echo rupiah($listdata['price']); ?></div>
                    </div>
                    <div class="col-md-3">
                        <div class="bold-search">Lokasi</div>
                        <i class="fa fa-map-marker"></i> <span class="ft-red">&nbsp;<?php echo isset($listdata['id_lokasi_ukm']['name'])?$listdata['id_lokasi_ukm']['name']:'';?></span>
                    </div>
                    <div class="col-md-3">
                        <div class="bold-search"> &nbsp;</div>
                        <span class="ft-red">&nbsp;<?php echo isset($listdata['total']['totalquantity'])?$listdata['total']['totalquantity']:'0';?></span> Terjual
                        <!-- <img src="<?php echo $this->webconfig['frontend_template']; ?>images/rating.png" class="img-responsive" /> -->
                    </div>
                </div>
                
                <div class="row margin-top-10">
                    <div class="col-md-12">
                    <div class="bold-search">Deskripsi</div>
                    <p class="des-search"><?php echo $listdata['description']; ?></p>
                    </div>
                </div>
                
               <a href="<?php echo base_url().'produk/read/'.$listdata['id'].'/'.$listdata['slug']; ?>" class="btn btn-mau-b">BELI<input type="hidden" id="qty" value="1"/></a>
            </div>
        </div>
    </div><!-- End item -->
    <?php } ?>
    <?php }else{ ?>
        <div class="box-grey margin-top-20"><!-- item -->
            <div class="row">
                <div class="col-md-12">
                    <center>Data tidak ditemukan</center>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<div class="container">
  <center>
    <nav>
      <ul class="pagination">
        <?php echo isset($pagination)?$pagination:''; ?>
      </ul>
    </nav>
    </center>
</div>