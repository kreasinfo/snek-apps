<script type="text/javascript">

	jQuery(document).ready(function($) {

		$(".pagination a").click(function(e){

			// stop normal link click

			e.preventDefault(); 

			var linkUrl = jQuery(this).attr("href");

			if(linkUrl){

				jQuery('#listData').load(linkUrl.replace(/\s/g,'%20'));

			}

		});

	});

	

</script>



<?php if(isset($listproduct) && count($listproduct) > 0){ ?>

  <?php $no = 0; foreach($listproduct as $key => $listdata){ $no++;?>

    <div class="col-lg-4 margin-top-30">

        <div class="box-img">

              <a href="<?php echo base_url().'produk/read/'.(isset($listdata['id'])?$listdata['id']:'').'/'.(isset($listdata['slug'])?$listdata['slug']:''); ?>"><img src="<?php echo thumb_image($listdata['images']['path'],'360x203','product'); ?>" class="img-responsive" style="width: 100%;"/></a>

              <div class="location"><i class="fa fa-map-marker ft-yellow"></i>&nbsp;<?php echo isset($listdata['id_lokasi_ukm']['name'])?$listdata['id_lokasi_ukm']['name']:'';?></div>

          </div>

          <div class="box-S-product">

              <a href="<?php echo base_url().'produk/read/'.(isset($listdata['id'])?$listdata['id']:'').'/'.(isset($listdata['slug'])?$listdata['slug']:''); ?>" class="name-product pull-left"><?php echo $listdata['name']; ?></a>

              <div class="price-home pull-right"><?php echo rupiah($listdata['price']); ?></div>

              <div class="clearfix"></div>

          </div>

          <div class="box-other">

              <div class="row">

                  <div class="col-xs-4"><span class="ft-red"><?php echo isset($listdata['total']['totalquantity'])?$listdata['total']['totalquantity']:'0';?></span> Terjual<!--<img src="<?php echo $this->webconfig['frontend_template']; ?>images/rating.png" alt="rating" />--></div>

                  <div class="col-xs-4"></div>

                  <div class="col-xs-4"><a href="<?php echo base_url().'produk/read/'.$listdata['id'].'/'.$listdata['slug']; ?>" class="btn-mau-p">&nbsp;&nbsp;&nbsp; Beli&nbsp;&nbsp;&nbsp;</a><input type="hidden" id="qty" value="1"/> </div>

              </div>

          </div>

    </div>

  <?php } ?>

<?php }else{ ?>
  <div class="col-lg-12 margin-top-30">
    <div class="box-other">
    <center>Tidak ada data.</center>
    </div>
  </div>
<?php } ?>


<div class="clearfix"></div>

<div class="container">

  <center>

    <nav>

      <ul class="pagination">

        <?php echo isset($pagination)?$pagination:""; ?>

      </ul>

    </nav>

    </center>

</div>

