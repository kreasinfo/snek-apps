<script type="text/javascript">
  jQuery(document).ready(function($) {
    $("#listData").load('<?php echo base_url().$this->router->class; ?>/getAllData/');

    $("#sortname").change(function() {
        $('#price').val('');
        var sortname = $(this).val();
        var price = $('#price').val();
        var city = $('#city').val();

        if (sortname == '') { sortname = '-'};
        if (price == '') { price = '-'};
        if (city == '') { city = '-'};
        
        jQuery("#listData").load("<?php echo base_url().$this->router->class; ?>/searchdata/"+sortname.replace(/\s/g,'%20')+"/"+price.replace(/\s/g,'%20')+"/"+city.replace(/\s/g,'%20'));
    });

    $("#price").change(function() {
        $('#sortname').val('');
        var sortname = $('#sortname').val();
        var price = $(this).val();
        var city = $('#city').val();

        if (sortname == '') { sortname = '-'};
        if (price == '') { price = '-'};
        if (city == '') { city = '-'};
        
        jQuery("#listData").load("<?php echo base_url().$this->router->class; ?>/searchdata/"+sortname.replace(/\s/g,'%20')+"/"+price.replace(/\s/g,'%20')+"/"+city.replace(/\s/g,'%20'));
    });

    $("#city").change(function() {
        var sortname = $('#sortname').val();
        var price = $('#price').val();
        var city = $(this).val();

        if (sortname == '') { sortname = '-'};
        if (price == '') { price = '-'};
        if (city == '') { city = '-'};
        
        jQuery("#listData").load("<?php echo base_url().$this->router->class; ?>/searchdata/"+sortname.replace(/\s/g,'%20')+"/"+price.replace(/\s/g,'%20')+"/"+city.replace(/\s/g,'%20'));
    });
  });
  function searchThis(){
    var frm = document.searchform;
    var name = frm.name.value;
    if(name == ''){ name = '-'; }
    jQuery("#listData").load("<?php echo base_url().$this->router->class; ?>/searchdata/"+name.replace(/\s/g,'%20'));
  }
</script>
<div class="container">
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a></li>
      <li class="active">Produk</li>
    </ol>
</div>

<div class="bg-detail">
  <div class="container">
      <center><div class="head-detail">LIST PRODUK</div></center>
    </div>
</div>

<div class="container"><!-- Container -->
  <div class="filter" style="margin: 30px 0 0;">
      <div class="row">
          <div class="col-md-2">
              <p class="margin-top-10">Filter Berdasarkan :</p>
            </div>
            <form method="" >
            <div class="col-md-3">
              <div class="form-res">
              <select id="sortname" class="form-control">
                <option value="">-- Pilih Nama --</option>
                <option value="1">Nama Ascending</option>
                <option value="2">Nama Descending</option>
              </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-res">
              <select id="price" class="form-control">\
                <option value="">-- Pilih Harga --</option>
                <option value="1">Harga Terendah</option>
                <option value="2">Harga Tertinggi</option>
              </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-res">
              <select id="city" class="form-control">
                <option value="">-- Pilih Kota --</option>
                <?php if(isset($listlokasiukm) && $listlokasiukm !=''){?>
                  <?php foreach ($listlokasiukm as $key => $value) {?>
                    <option value="<?php echo $value['id'];?>" <?php if(isset($id_lokasiukm) && $id_lokasiukm == $value['id']) {echo "selected=selected";}?>><?php echo $value['name'];?></option>
                  <?php }?>
                <?php } ?>
              </select>
              </div>
            </div>
            </form>
        </div>
    </div>
  <div class="row"> 
  <div id="listData"><center><img src='<?php echo $this->webconfig['back_base_template']; ?>img/loading.gif' align='middle' style="margin:5px;" /></center></div>
  </div>
</div><!-- Container -->
<!-- <div class="container">
  <center>
    <nav>
      <ul class="pagination">
         <li>
          <a href="#" aria-label="Previous">
            <i class="fa fa-chevron-left"></i>
          </a>
        </li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li>
          <a href="#" aria-label="Next">
            <i class="fa fa-chevron-right"></i>
          </a>
        </li>
      </ul>
    </nav>
    </center>
</div> -->