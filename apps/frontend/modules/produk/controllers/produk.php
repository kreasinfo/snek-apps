<?php
class Produk extends CI_Controller {
	var $webconfig;
	var $pagename;
	var $configuration;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('text');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->lang->load('global', 'bahasa');
		$this->load->sharedModel('OrderModel');
		$this->load->sharedModel('ProdukModel');
		$this->load->sharedModel('Lokasi_ukmModel');
		$this->webconfig = $this->config->item('webconfig');
		$this->pagename = $this->lang->line('product');
		$this->configuration = $this->ShopconfigurationModel->listData();	
	}

	function index($page = 0)
	{
		redirect(base_url().$this->router->class.'/listing');
	}

	function listing($page = 0){
		$tdata = array();
		$tdata['listlokasiukm'] = $this->Lokasi_ukmModel->listData();
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		$ldata['informationpage'] = $this->InformationpageModel->listData();
		$this->load->sharedView('template', $ldata);
	}
	function getAllData($page = 0){

		$all_data = $this->ProdukModel->listDataCount();
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data_product'];
        $cfg_pg ['uri_segment'] = 3;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '<i class="fa fa-chevron-right"></i>';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '<i class="fa fa-chevron-left"></i>';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><a href="#" aria-label="Next">';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['listproduct'] = $this->ProdukModel->listData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
															));
		
		$this->load->view($this->router->class.'/list', $listdata);
	}

	function searchdata($sortname = '-',$price = '-',$city = '-'){
		if($sortname == '-'){$sortname = '';}
		if($price == '-'){$price = '';}
		if($city == '-'){$city = '';}
		
		# PAGINATION #
		$all_data = $this->ProdukModel->filterDataProdukCount(array(
																'sortname'=>trim(urldecode($sortname))
																,'price'=>trim(urldecode($price))
																,'city'=>trim(urldecode($city))
																));
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/".$this->uri->segment(3)."/".$this->uri->segment(4)."/".$this->uri->segment(5)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data_product'];
        $cfg_pg ['uri_segment'] = 6;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '<i class="fa fa-chevron-right"></i>';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '<i class="fa fa-chevron-left"></i>';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><a href="#" aria-label="Next">';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['listproduct'] = $this->ProdukModel->filterDataProduk(array(
																			'sortname' => trim(urldecode($sortname))
																			,'price'=>trim(urldecode($price))
																			,'city'=>trim(urldecode($city))
																			,'start'  => $start_idx
																			,'limit' => $cfg_pg['per_page']
																		));
		$listData['price'] = $price;
		$listData['id_lokasiukm'] = $city;
		$this->load->view($this->router->class.'/list', $listdata);
	}

	function notfound()
	{
		$tdata = array();
		
		## LOAD LAYOUT ##
		$ldata['content'] = $this->load->view($this->router->class.'/notfound',$tdata, true);
		$this->load->sharedView('template', $ldata);
	}

	function read($id = 0, $slug = '')
	{
		$tdata = array();
		$tdata['listdata'] = $this->ProdukModel->getData(array('id' => $id, 'slug' => $slug));
		$tdata['listlokasiukm'] = $this->Lokasi_ukmModel->listData();
		$tdata['listrelated'] = $this->ProdukModel->listData(array(
																	'excludeid' => $id, 
																	'id_kategori' => isset($tdata['listdata']['id_kategori_produk']['id'])?$tdata['listdata']['id_kategori_produk']['id']:'',
																	'limit' => 3
																));
		
		if(count($tdata['listdata']) == 0){
			redirect(base_url().'notfoundpage');
		}
		
		## LOAD LAYOUT ##
		$ldata['content'] = $this->load->view($this->router->class.'/detail',$tdata, true);
		$ldata['informationpage'] = $this->InformationpageModel->listData();
		
		$this->load->sharedView('template', $ldata);
	}

	function load_quantitiy(){
		$id_data = $_POST['id'];
		$tdata['lists'] = $this->ProdukModel->getStockSize($id_data);
		
		$this->load->view($this->router->class.'/list_stock',$tdata);
	}

	function search($keyword = '',$price = '',$city = '')
	{
		$tdata = array();
		if($keyword == '-'){ $keyword = '';}
		if($city == '-'){ $city = '';}
		if($price == '-'){ $price = '';}
		
		$tdata['listlokasiukm'] = $this->Lokasi_ukmModel->listData();
		$tdata['keyword'] = isset($keyword)?$keyword:'';
		$tdata['id_lokasiukm'] = isset($city)?$city:'';
		$tdata['price'] = isset($price)?$price:'';
		
		$all_data = $this->ProdukModel->filterDataCount(array(
																'name' => trim(urldecode($keyword)),
																'city' => trim(urldecode($city)),
																'price' => trim(urldecode($price))
															));
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5).'/';

        $tdata["total_data"] = $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data_product'];
        $cfg_pg ['uri_segment'] = 6;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '<i class="fa fa-chevron-right"></i>';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '<i class="fa fa-chevron-left"></i>';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><a href="#" aria-label="Next">';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$tdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$tdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$tdata['lists'] = $this->ProdukModel->filterData(array(
															
															'name' => trim(urldecode($keyword))
															,'city' => trim(urldecode($city))
															,'price' => trim(urldecode($price))
															,'start'  => $start_idx
															,'limit' => $cfg_pg['per_page']
														));
		
		## LOAD LAYOUT ##
		$ldata['content'] = $this->load->view($this->router->class.'/search',$tdata, true);

		$ldata['informationpage'] = $this->InformationpageModel->listData();
		$this->load->sharedView('template', $ldata);
	}
}