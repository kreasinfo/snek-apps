<?php
class Email extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');	
		$this->lang->load('global', 'bahasa');
	}
	function index(){
		redirect(base_url().$this->router->class.'/listing');
	}
	function sendmail($id = 0){	
		$this->load->library('email');

		$this->email->initialize(array(
		  'protocol' => 'smtp',
		  'smtp_host' => HOST_EMAIL,
		  'smtp_user' => USER_EMAIL,
		  'smtp_pass' => PASS_EMAIL,
		  'smtp_port' => PORT_EMAIL,
		  'crlf' => "\r\n",
		  'newline' => "\r\n"
		));


		$this->email->from('moeh.noeryadie@gmail.com', 'Your Name');
		$this->email->to('moeh.noeryadie@gmail.com');
		$this->email->cc('moeh.noeryadie@gmail.com');
		$this->email->bcc('moeh.noeryadie@gmail.com');

		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');

		$this->email->send();

		echo $this->email->print_debugger();


	}
}
?>