
<div class="container">
    <div class="row margin-top-40">
        <div class="col-lg-4 col-lg-offset-4">
            <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
                <?php foreach($error_hash as $inp_err){ ?>
                    <script type="text/javascript">
                    jQuery(document).ready(function($) {
                        toastr.error("<?php echo $inp_err; ?>");
                        });
                    </script>
                <?php } ?>
            <?php } ?>
            <?php if(isset($success)){ ?>
                <script type="text/javascript">
                jQuery(document).ready(function($) {
                    toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                    });
                </script>
            <?php } ?>
            <div class="bg-white">
                <center><img src="<?php echo $this->webconfig['frontend_template']; ?>images/Logo.png" class="img-responsive" /></center>
                <p class="login-ttl">RESET <span class="ft-red">PASSWORD</span></p>
                <p class="login-des">Silahkan cek email Anda pada Inbox / Spam</p>
                <form id="form-login" role="form" method="post" >
                    <input type="text" class="form-control margin-bottom-10" name="email" placeholder="Email" />
                    <input type="submit" class="btn btn-login btn-block" value="KIRIM" /> <br />
                </form>
                <div class="border margin-bottom-30"></div>
                
                <div class="row">
                    <div class="col-xs-6">
                        <div class="box-link">
                            <a href="<?php echo base_url(); ?>">
                                <i class="fa fa-home"></i>
                                <p>Home</p>
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-xs-6">
                        <div class="box-link">
                            <a href="<?php echo base_url(); ?>login/member">
                                <i class="fa fa-user"></i>
                                <p>login</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>