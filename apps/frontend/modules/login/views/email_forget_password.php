<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $this->lang->line('email_members_forgot_password'); ?></title>
</head>

<body>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#ececec" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:1.4em; color:#333;">
  <tr>
    <td>
      <table width="640" border="0" cellpadding="0" cellspacing="0" style="margin:20px auto;">
          <tr style="background:#FFF;">
            <td style="text-align:center; padding:20px 0;border-left:1px solid #CCC; border-right:1px solid #CCC; border-top:1px solid #CCC;"><a href="<?php echo base_url(); ?>" target="_blank">
            <img src="<?php echo $this->webconfig['frontend_template']; ?>images/Logo.png" alt="snekbook" style="border:none;" /></a></td>
          </tr>
          <tr>
            <td style="background:#FFF; border-left:1px solid #CCC; border-right:1px solid #CCC; border-bottom:1px solid #CCC; padding:20px;">
                <h4 style="border-bottom:1px solid #ddd; padding-bottom:10px; margin:0;"><?php echo $this->lang->line('email_members_forgot_password'); ?></h4>
                <p>Dear <strong><?php echo $name; ?></strong></p>
                <p>Berikut ini adalah password baru Anda</p>
                <p>Email : <?php echo $email; ?></p>
                <p>Password : <?php echo $new_password; ?></p>
                <p>Demikian informasi yang dapat kami sampaikan, Terimakasih.</p>
               <!--  <p><a href="<?php echo base_url(); ?>login/member">Login</a><br/></p> -->
                <br />
                <p><strong>Admin Snekbook.com</strong><br /></p>
                        Jl. Hasanudin No.28 | Bandung - Jawa Barat<br />
                        INDONESIA <br />
                        +62812 2081 7720 | snekbook@gmail.com<br />
            </td>
          </tr>
          <tr>
            <td height="40">
              <a href="<?php echo base_url(); ?>" target="_blank" style="color:#333;text-decoration:none;"><strong>snekbook.com</strong></a><br/>
              &copy; <?php echo date('Y'); ?> <?php echo isset($this->configuration['site_title'])?$this->configuration['site_title']:'Site Name'?>. Call Center <strong><?php echo isset($this->configuration['phone'])?$this->configuration['phone']:'000 - 1234546'?></strong>
            </td>
          </tr>
        </table>
    </td>
  </tr>
</table>
</body>
</html>