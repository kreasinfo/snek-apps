<div class="container">
    <div class="row margin-top-40">
        <div class="col-lg-4 col-lg-offset-4">
            <?php if(isset($error_message) && count($error_message) > 0){ ?>
                <script type="text/javascript">
                jQuery(document).ready(function($) {
                    toastr.error("<?php echo $error_message; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                    });
                </script>
            <?php } ?>
            <?php if(isset($success)){ ?>
                <script type="text/javascript">
                jQuery(document).ready(function($) {
                    toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                    });
                </script>
            <?php } ?>
            <div class="bg-white">
                <center><img src="<?php echo $this->webconfig['frontend_template']; ?>images/Logo.png" class="img-responsive" /></center>
                <p class="login-ttl">LOG <span class="ft-red">IN UKM</span></p>
                <p class="login-des">Silahkan masukkan Email dan Password Anda</p>
                <form id="form-login" role="form" method="post" >
                    <input type="text" class="form-control margin-bottom-10" name="email" placeholder="Email" />
                    <input type="password" class="form-control margin-bottom-10" name="password" placeholder="Password" />
                    <input type="submit" class="btn btn-login btn-block" value="LOG IN" /> <br />
                    
                </form>
                <div class="border margin-bottom-30"></div>
                <!-- <a href="" class="btn-facebook margin-bottom-10">LOG IN VIA FACEBOOK</a>
                <a href="" class="btn-twitter margin-bottom-10">LOG IN VIA TWITTER</a> -->
                
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-link">
                            <a href="<?php echo base_url(); ?>">
                                <i class="fa fa-home"></i>
                                <p>Home</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>