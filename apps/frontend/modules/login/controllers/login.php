<?php
if ( session_status() == PHP_SESSION_NONE ) {
	session_start();
}
require_once __DIR__ . "/../../../libraries/facebook-php-sdk-v4-4.0-dev/autoload.php";
// require_once __DIR__ . "/../../../libraries/facebook-php-sdk-v4-5.0.0/src/Facebook/autoload.php";

// use Facebook\FacebookResponse;
// use Facebook\FacebookRequest;
// use Facebook\FacebookSession;   
// use Facebook\FacebookRedirectLoginHelper;
// use Facebook\GraphObject;
// use Facebook\GraphUser;
// use Facebook\GraphLocation;
// use Facebook\GraphSessionInfo;
// use Facebook\FacebookSDKException;
// use Facebook\FacebookRequestException;
// use Facebook\Entities\AccessToken;
// use Facebook\HttpClients\FacebookHttpable;
// use Facebook\HttpClients\FacebookStream;
// use Facebook\HttpClients\FacebookStreamHttpClient;
class Login extends CI_Controller {
	var $webconfig;
	var $pagename;
	var $configuration;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('phpmailer');
		$this->load->library('session');
		$this->lang->load('global', 'bahasa');
		$this->load->sharedModel('LoginModel');
		$this->load->sharedModel('MembersModel');
		$this->load->library('session');
		$this->webconfig = $this->config->item('webconfig');
		$this->pagename = $this->lang->line('login');	
	}

	function member($url_referer = '')
	{

		if($this->LoginModel->isLoggedIn()){
			redirect(base_url().'member/dashboard/');
		}
		$tdata = array();
		if($_POST){
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			$check = $this->LoginModel->doLogin($email,$password);
			
			switch ($check){
				case 'failed_input':
						$tdata['error_message'] = $this->lang->line('error_login_failed_input');
					break;
				case 'failed_login':
						$tdata['error_message'] = $this->lang->line('error_login_failed_login');
					break;
				case 'banded':
						$tdata['error_message'] = $this->lang->line('error_member_banded');
					break;
				case 'non_activation':
						$tdata['error_message'] = $this->lang->line('error_member_non_active');
					break;
				case 'success_login':
						$tdata['success'] = TRUE;
					break;
			}
			
			if(isset($tdata['success']) && $tdata['success'] == '1'){
				if($url_referer != ''){
					return redirect(replace_dash($url_referer));
				}else{
					return redirect(base_url().'member/dashboard');
				}
			}else{
				$ldata['content'] =  $this->load->view($this->router->class.'/index', $tdata, true);
				## LOAD DATA TEMPLATE ##
				$ldata['informationpage'] = $this->InformationpageModel->listData();

				$this->load->sharedView('login', $ldata);		
			}
		}else{
			## LOAD LAYOUT ##
			$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
			## LOAD DATA TEMPLATE ##
			$ldata['informationpage'] = $this->InformationpageModel->listData();
			
			$this->load->sharedView('login', $ldata);
		}
	}

	function ukm($url_referer = '')
	{

		if($this->LoginModel->isLoggedInUkm()){
			redirect(base_url().'ukm/dashboard/');
		}
		$tdata = array();
		if($_POST){
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			
			$check = $this->LoginModel->doLoginUkm($email,$password);
			
			switch ($check){
				case 'failed_input':
						$tdata['error_message'] = $this->lang->line('error_login_failed_input_ukm');
					break;
				case 'failed_login':
						$tdata['error_message'] = $this->lang->line('error_login_failed_login');
					break;
				case 'banded':
						$tdata['error_message'] = $this->lang->line('error_member_banded');
					break;
				case 'non_activation':
						$tdata['error_message'] = $this->lang->line('error_member_non_active');
					break;
				case 'success_login':
						$tdata['success'] = TRUE;
					break;
			}
			
			if(isset($tdata['success']) && $tdata['success'] == '1'){
				if($url_referer != ''){
					return redirect(replace_dash($url_referer));
				}else{
					return redirect(base_url().'ukm/dashboard');
				}
			}else{
				$ldata['content'] =  $this->load->view($this->router->class.'/indexukm', $tdata, true);
				## LOAD DATA TEMPLATE ##
				$ldata['informationpage'] = $this->InformationpageModel->listData();

				$this->load->sharedView('login', $ldata);		
			}
		}else{
			## LOAD LAYOUT ##
			$ldata['content'] = $this->load->view($this->router->class.'/indexukm',$tdata, true);
			## LOAD DATA TEMPLATE ##
			$ldata['informationpage'] = $this->InformationpageModel->listData();
			
			$this->load->sharedView('login', $ldata);
		}
	}

	function member_fblogin($variable = "")
	{
		// FacebookSession::setDefaultApplication(fb_app_id, fb_app_secret);
		// $helper = new FacebookRedirectLoginHelper("http://".APP_DOMAIN."/login/member_fblogin");
		// echo '<pre>';
		// print_r ($_SESSION);die();
		$fb = new Facebook\Facebook([
		  'app_id' => '{1540149669647348}',
		  'app_secret' => '{a3c620a19eda60b2179f8287c5e19607}',
		  'default_graph_version' => 'v2.5',
		]);
		$helper = $fb->getRedirectLoginHelper();
		$permissions = ['email', 'user_likes']; // optional
		$loginUrl = $helper->getLoginUrl('http://localhost/snek/login/member_fblogin', $permissions);
		echo '<pre>';
		print_r ($_SESSION['token']);die();
		try {
		  $accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}

		if (isset($accessToken)) {
		  // Logged in!
		  $_SESSION['facebook_access_token'] = (string) $accessToken;

		  // Now you can redirect to another page and use the
		  // access token from $_SESSION['facebook_access_token']
		}
		
		
		// 	$helper = new FacebookRedirectLoginHelper("http://".APP_DOMAIN."/login/member_fblogin",fb_app_id, fb_app_secret);
		// FacebookSession::setDefaultApplication(fb_app_id, fb_app_secret);
		echo '<pre>';
		print_r ($helper->getSessionFromRedirect());die();
		// try
		// {
		//   // In case it comes from a redirect login helper
		//   $session = $helper->getSessionFromRedirect();
		// }
		// catch( FacebookRequestException $ex )
		// {
		//   // When Facebook returns an error
		//   echo $ex;
		// }
		// catch( Exception $ex )
		// {
		//   // When validation fails or other local issues
		//   echo $ex;
		// }

		// // see if we have a session in $_Session[]
		// if( isset($_SESSION['token']))
		// {
		//     // We have a token, is it valid?
		//     $session = new FacebookSession($_SESSION['token']);
		//     try
		//     {
		//         $session->Validate(fb_app_id, fb_app_secret);
		//     }
		//     catch( FacebookAuthorizationException $ex)
		//     {
		//         // Session is not valid any more, get a new one.
		//         $session ='';
		//     }
		// }
		echo '<pre>';
		print_r (FacebookSession::setDefaultApplication(fb_app_id , fb_app_secret));die();
		
		### HASIL LOGIN ##
		if ( isset( $session ) && $session != ''  ){
			
			$_SESSION['token'] = $session->getToken();
			$user_profile = (new FacebookRequest($session, 'GET', '/me'))->execute()->getGraphObject(GraphUser::className());
			
			### CEK USER SUDAH TERDAFTAR SEBELUMNYA ATAU BELUM ##
			$_SESSION["fb_user_details"] = $data_profile = $user_profile->asArray();

			$data_profile = $user_profile->asArray();
			
			$cekfblogin = $this->LoginModel->checkFbLoginmember($data_profile['id']);
			
			if(count($cekfblogin) > 0){ // USER PERNAH LOGIN

				### DO LOGIN AND CREATE SESSION ####

				$check = $this->LoginModel->doLoginFBmember($cekfblogin['email'],$cekfblogin['password']);
				
				switch ($check){
					case 'failed_input':
							$tdata['error_message'] = $this->lang->line('error_login_failed_input');
						break;
					case 'failed_login':
							$tdata['error_message'] = $this->lang->line('error_login_failed_login');
						break;
					case 'banded':
							$tdata['error_message'] = $this->lang->line('error_member_banded');
						break;
					case 'non_activation':
							$tdata['error_message'] = $this->lang->line('error_member_non_active');
						break;
					case 'success_login':
							$tdata['success'] = TRUE;
						break;
				}

				if(isset($tdata['success']) && $tdata['success'] == '1'){
					if(isset($url_referer) && $url_referer != ''){
						return redirect(replace_dash($url_referer));
					}else{
						return redirect(base_url().'member/dashboard');
					}
				}else{
					$tdata['fb_login_url'] = $helper->getLoginUrl( array( 'scope' => fb_required_scope ) );
					$ldata['content'] =  $this->load->view($this->router->class.'/index', $tdata);
					## LOAD DATA TEMPLATE ##

					$this->load->sharedView('login', $ldata);			
				}
			}else{ // USER BELUM PERNAH LOGIN
				redirect(base_url().'register/member_fb/'.$data_profile['id'].'/');
			}
		}else{
			$tdata['error_message'] = $this->lang->line('error_login_failed_login');
			$tdata['fb_login_url'] = $helper->getLoginUrl( array( 'scope' => fb_required_scope ) );

			## LOAD LAYOUT ##
			$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
			## LOAD DATA TEMPLATE ##
			
			$this->load->sharedView('login', $ldata);		
		}
		#### END FB SYSTEM #####
	}
	
	function logout(){
		$this->LoginModel->doLogout();
		redirect(base_url());
	}
	
	function logoutUkm(){
		$this->LoginModel->doLogoutUkm();
		redirect(base_url());
	}
	
	function forgetpassword(){
		$tdata = array();
		$this->pagename = 'Lupa Password';
		if($_POST){
			$email = $this->input->post('email');
			if($email !=''){
				$checkExistEmail = $this->MembersModel->getUserdata(array('email' => $email));
				
				if(count($checkExistEmail) > 0){
					## random password ##
					$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
				    $new_password = '';
				    for ($i = 0; $i < 10; $i++) {
				        $new_password .= $characters[rand(0, strlen($characters) - 1)];
				    }

				    $doUpdate = $this->MembersModel->updatePassword(array(
															        'id' => $checkExistEmail['id']
															        ,'password' => $new_password
															  ));

				    if($doUpdate == 'empty'){
						$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
					}else if($doUpdate == 'failed'){
						$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
					}else if($doUpdate == 'success'){
						$tdata['success'] = 'Silahkan cek email, untuk melihat password baru Anda';
						$this->load->library('email');

						$this->email->initialize(array(
						  'protocol' => 'smtp',
						  'smtp_host' => HOST_EMAIL,
						  'smtp_user' => USER_EMAIL,
						  'smtp_pass' => PASS_EMAIL,
						  'smtp_port' => PORT_EMAIL,
						  'crlf' => "\r\n",
						  'newline' => "\r\n",
						  'mailtype'  => 'html', 
				           'charset'   => 'iso-8859-1'
						));


						$this->email->from('noreply@snekbook.com', 'Snekbook Admin');
						$this->email->to($email);

						$this->email->subject($this->lang->line('email_members_forgot_password'));
						$message = $this->load->view($this->router->class.'/email_forget_password', array(
																									'name'=> $checkExistEmail['fullname'],
																									'email'=> $checkExistEmail['email'],
																									'new_password' => $new_password,
																								), TRUE);
						$this->email->message($message);
						$this->email->send();
					}
				}else{
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_belumterdaftar'));
				}
			
					
				## LOAD DATA TEMPLATE ##
				$ldata['content'] = $this->load->view($this->router->class.'/forget',$tdata, true);
				$this->load->sharedView('login', $ldata);
			    
			}else{
				$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_email'));
				## LOAD DATA TEMPLATE ##
				$ldata['content'] = $this->load->view($this->router->class.'/forget',$tdata, true);
				$this->load->sharedView('login', $ldata);
			}
		}else{
			## LOAD DATA TEMPLATE ##
			$ldata['content'] = $this->load->view($this->router->class.'/forget',$tdata, true);
			$this->load->sharedView('login', $ldata);
		}
	}
	
}