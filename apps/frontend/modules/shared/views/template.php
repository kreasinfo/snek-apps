<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php 
            $meta_title = isset($this->configuration['site_title'])?stripslashes(htmlentities($this->configuration['site_title'])):'';
        ?>
        <title><?php echo ($this->pagename != "")?stripslashes(htmlentities($this->pagename)):$meta_title; ?> <?php echo isset($this->configuration['site_title'])?' | '.stripslashes(htmlentities($this->configuration['site_title'])):' | Site Title'?></title>
        <meta property="og:title" content="<?php echo ($this->pagename)?stripslashes(htmlentities($this->pagename)):''; ?> <?php echo isset($this->configuration['site_title'])?' | '.stripslashes(htmlentities($this->configuration['site_title'])):' | Site Title'?>">

        <?php if(isset($page_desc) && $page_desc != ''){ ?>
        <meta name="description" content="<?php echo $page_desc; ?>" />
        <meta property="og:description" content="<?php echo $page_desc; ?>">
        <?php }else{ ?>
        <meta name="description" content="<?php echo isset($this->configuration['meta_description'])?$this->configuration['meta_description']:'Description'?>" />
        <meta property="og:description" content="<?php echo isset($this->configuration['meta_description'])?$this->configuration['meta_description']:'Description'?>">
        <?php } ?>

        <?php if(isset($ogimg) && $ogimg != ''){ ?>
        <meta property="og:image" content="<?php echo $ogimg; ?>"/>
        <?php }else{ ?>
        <meta property="og:image" content="<?php echo $this->webconfig['frontend_template']; ?>img/logod.png"/>
        <?php } ?>

        <link rel="stylesheet" href="<?php echo $this->webconfig['frontend_template']; ?>css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo $this->webconfig['frontend_template']; ?>css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo $this->webconfig['frontend_template']; ?>css/main.css">
        <link rel="stylesheet" href="<?php echo $this->webconfig['frontend_template']; ?>css/jquery.bxslider.css">
        <link rel="stylesheet" href="<?php echo $this->webconfig['frontend_template']; ?>css/pgwslideshow.css">
        <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/toastr.min.css">
        <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/select2.css">
        <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/select2-bootstrap.css">
        <link rel="stylesheet" href="<?php echo $this->webconfig['frontend_template']; ?>css/jquery.datetimepicker.css">
        <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/jquery.Jcrop.css">
        <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/jquery.fancybox.css">
        <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/alert.css">
        <link rel="stylesheet" href="<?php echo $this->webconfig['frontend_template']; ?>css/custome.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/jquery-1.11.1.min.js"><\/script>')</script>
        <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/jquery-1.11.1.min.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/toastr/toastr.min.js"></script>
        <script src="<?php echo $this->webconfig['back_base_template']; ?>js/select2.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/jquery.datetimepicker.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/jquery.Jcrop.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/jquery.formatCurrency-1.4.0.min.js"></script>
        <script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery.fancybox.js"></script>
        <script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery-impromptu.js"></script>
        <script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery.numeric.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/custom.js"></script> 
        <script type="text/javascript">
          var base_url = "<?php echo base_url(); ?>";
          $('.fancybox').fancybox();
        </script>
         
    </head>
    <body>
      <div class="header" role="heading"><!-- Header -->
          <div class="container"><!-- container -->
                <nav class="navbar">
                    <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/Logo.png" class="img-responsive" /></a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <div class="navbar-nav navbar-right side-collapse in">
                            <nav role="navigation" class="navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                                    <li><a href="<?php echo base_url().'produk'; ?>">PRODUK UKM</a></li>
                                    <li><a href="<?php echo base_url().'contact'; ?>">KONTAK</a></li>
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> <span class="ft-red"> Account</span> <span class="caret"></span></a>
                                      <ul class="dropdown-menu">
                                        <?php if($this->LoginModel->isLoggedIn()){ ?>
                                            <li><a href="<?php echo base_url(); ?>member/dashboard">Profil</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="<?php echo base_url(); ?>member/updateprofile">Edit Profil</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="<?php echo base_url(); ?>member/order/">Order Belanja</a></li>
                                            <!-- <li><a href="#">Konfirmasi Pembayaran</a></li> -->
                                            <li role="separator" class="divider"></li>
                                            <li><a href="<?php echo base_url(); ?>login/logout/" class="logout">Logout</a></li>
                                        <?php }else{ ?>
                                            <li><a href="<?php echo base_url(); ?>login/member/<?php echo replace_slash(current_url()); ?>">Log In Pembeli</a></li>
                                            <li><a href="<?php echo base_url(); ?>register">Registrasi Pembeli</a></li>
                                            
                                            <li><a href="<?php echo base_url(); ?>login/ukm">Log In UKM</a></li>
                                            
                                            <li><a href="<?php echo base_url(); ?>register/ukm">Registrasi UKM</a></li>
                                            
                                        <?php } ?>
                                      </ul>
                                    </li>
                                    <li class="cart">
                                        <a href="<?php echo base_url(); ?>cart/choseorder" class="fancybox fancybox.ajax">
                                            <div>
                                                <i class="fa fa-shopping-cart pull-left"></i> 
                                                <div class="cart-no pull-left" id="daftarbelanja">0</div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- <li class="cart"><a href="<?php echo base_url(); ?>cart/view">
                                      <div>
                                          <i class="fa fa-shopping-cart pull-left"></i> 
                                            <div class="cart-no pull-left" id="daftarbelanja">0</div>
                                            <div class="clearfix"></div>
                                        </div>
                                      </a>
                                    </li> -->
                                    
                                </ul>
                            </nav>
                        </div>
                    </div>
                </nav>
            </div><!-- End Container -->        
      </div><!-- End Header -->
        
        <?php echo isset($content)?$content:''; ?>
        
        <div class="footer"><!-- Footer -->
          <div class="container">
              <div class="row">
                  <div class="col-md-3">
                      <img src="<?php echo $this->webconfig['frontend_template']; ?>images/Logo-bw.png" class="img-responsive margin-logo" />
                    </div>
                    <div class="col-md-2">
                      <div class="head-footer">NAVIGASI</div>
                        <ul class="nav-footer">
                          <li><a href="<?php echo base_url(); ?>">Home</a></li>
                            <li><a href="<?php echo base_url(); ?>produk">Produk</a></li>
                            <li><a href="<?php echo base_url(); ?>login/member/<?php echo replace_slash(current_url()); ?>">Login</a></li>
                            <li><a href="<?php echo base_url(); ?>contact">Kontak</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                      <div class="head-footer">SNEKBOOK</div>
                        <?php if(isset($informationpage) && count($informationpage) > 0){ ?>
                            <ul class="nav-footer">
                                <?php foreach($informationpage as $listdata){ ?>
                                    <li>
                                        <a href="<?php echo base_url(); ?>informationpage/read/<?php echo $listdata['id']; ?>/<?php echo $listdata['slug']; ?>">
                                            <?php echo $listdata['title']; ?>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </div>
                    <div class="col-md-2">
                      <div class="head-footer">SOSIAL MEDIA</div>
                        <ul class="nav-footer">
                          <li><a target = '_blank' href="<?php echo isset($this->configuration['fb_url'])?stripslashes(htmlentities($this->configuration['fb_url'])):''; ?>">Facebook</a></li>
                            <li><a target = '_blank' href="<?php echo isset($this->configuration['twitter_url'])?stripslashes(htmlentities($this->configuration['twitter_url'])):''; ?>">Twitter</a></li>
                            <li><a target = '_blank' href="<?php echo isset($this->configuration['instagram_url'])?stripslashes(htmlentities($this->configuration['instagram_url'])):''; ?>">Instagram</a></li>
                            <li><a target = '_blank' href="<?php echo isset($this->configuration['gplus_url'])?stripslashes(htmlentities($this->configuration['gplus_url'])):''; ?>">Gplus</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2">
                        <div class="head-footer">PENDAFTARAN</div>
                        <ul class="nav-footer">
                            <li>
                                <a href="<?php echo base_url(); ?>register/ukm">
                                    REGISTRASI UKM
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>login/ukm">
                                    LOGIN UKM
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>register">
                                    REGISTRASI PEMBELI
                                </a>
                            </li>
                            <li>
                            	<a href="<?php echo base_url(); ?>login/member/<?php echo replace_slash(current_url()); ?>">
                            	    LOG IN PEMBELI</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div><!-- End Footer -->
        <div class="copyright">Copryright © 2015. snekbook.com. allright reserved </div>
       
        
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/bootstrap.min.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/jquery.bxslider.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/pgwslideshow.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/jquery.easing.1.3.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/jquery.fitvids.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/jquery.flexisel.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/main.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/jquery.calculation.min.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
          ga('create', 'UA-72481346-1', 'auto');
          ga('send', 'pageview');
        </script> 
    </body>
</html>