<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php 
            $meta_title = isset($this->configuration['site_title'])?stripslashes(htmlentities($this->configuration['site_title'])):'';
        ?>
        <title><?php echo ($this->pagename != "")?stripslashes(htmlentities($this->pagename)):$meta_title; ?> <?php echo isset($this->configuration['site_title'])?' | '.stripslashes(htmlentities($this->configuration['site_title'])):' | Snekbook'?></title>
        <meta property="og:title" content="<?php echo ($this->pagename)?stripslashes(htmlentities($this->pagename)):''; ?> <?php echo isset($this->configuration['site_title'])?' | '.stripslashes(htmlentities($this->configuration['site_title'])):' | Snekbook'?>">

        <?php if(isset($page_desc) && $page_desc != ''){ ?>
        <meta name="description" content="<?php echo $page_desc; ?>" />
        <meta property="og:description" content="<?php echo $page_desc; ?>">
        <?php }else{ ?>
        <meta name="description" content="<?php echo isset($this->configuration['meta_description'])?$this->configuration['meta_description']:'Description'?>" />
        <meta property="og:description" content="<?php echo isset($this->configuration['meta_description'])?$this->configuration['meta_description']:'Description'?>">
        <?php } ?>

        <?php if(isset($ogimg) && $ogimg != ''){ ?>
        <meta property="og:image" content="<?php echo $ogimg; ?>"/>
        <?php }else{ ?>
        <meta property="og:image" content="<?php echo $this->webconfig['frontend_template']; ?>img/logod.png"/>
        <?php } ?>

        <link rel="stylesheet" href="<?php echo $this->webconfig['frontend_template']; ?>css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo $this->webconfig['frontend_template']; ?>css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo $this->webconfig['frontend_template']; ?>css/main.css">
        <link rel="stylesheet" href="<?php echo $this->webconfig['frontend_template']; ?>css/jquery.bxslider.css">
        <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/toastr.min.css">
        <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/jquery-1.11.1.min.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/toastr/toastr.min.js"></script>
    </head>
    <body class="bg">

        <?php echo isset($content)?$content:''; ?>
        
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/jquery-1.11.1.min.js"><\/script>')</script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/bootstrap.min.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/jquery.bxslider.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/jquery.easing.1.3.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/jquery.fitvids.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/jquery.flexisel.js"></script>
        <script src="<?php echo $this->webconfig['frontend_template']; ?>js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-72481346-1', 'auto');
            ga('send', 'pageview');
        </script> 
    </body>
</html>

