<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class SalesModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');	
		$this->maintablename = "orders";	
		$this->order_detail_table = "orders_detail";	
		$this->produk_ukuran_table = "produk_ukuran";	
		$this->pengiriman_table = "pengiriman";	
	}
	public function entriData($params=array()){
		$userid      = isset($params["userid"])?$params["userid"]:'';
		$client_name      = isset($params["client_name"])?$params["client_name"]:'';
		$client_address      = isset($params["client_address"])?$params["client_address"]:'';
		$client_postcode      = isset($params["client_postcode"])?$params["client_postcode"]:'';
		$client_phone      = isset($params["client_phone"])?$params["client_phone"]:'';
		$client_email      = isset($params["client_email"])?$params["client_email"]:'';
		$post_name      = isset($params["post_name"])?$params["post_name"]:'';
		$post_address      = isset($params["post_address"])?$params["post_address"]:'';
		$tipe_pengiriman      = isset($params["tipe_pengiriman"])?$params["tipe_pengiriman"]:'';
		$city      = isset($params["city"])?$params["city"]:'';
		$post_postcode      = isset($params["post_postcode"])?$params["post_postcode"]:'';
		$post_phone      = isset($params["post_phone"])?$params["post_phone"]:'';
		$post_email      = isset($params["post_email"])?$params["post_email"]:'';
		$products      = isset($params["products"])?$params["products"]:'';
		$quantity      = isset($params["quantity"])?$params["quantity"]:'';
		$size      = isset($params["size"])?$params["size"]:'';
		$productprice      = isset($params["productprice"])?$params["productprice"]:'';
		$productname      = isset($params["productname"])?$params["productname"]:'';
		$productberat      = isset($params["productberat"])?$params["productberat"]:'';
		$discount      = isset($params["discount"])?$params["discount"]:'';
		$path      = isset($params["path"])?$params["path"]:'';
		$custom_ongkir      = isset($params["custom_ongkir"])?$params["custom_ongkir"]:'';
		$gran_total      = isset($params["gran_total"])?$params["gran_total"]:'';
		
		if($products == '') $products = array();
		if($quantity == '') $quantity = array();
		if($size == '') $size = array();
		if($productprice == '') $productprice = array();
		if($productname == '') $productname = array();
		if($productberat == '') $productberat = array();
		if($discount == '') $discount = array();
		if($path == '') $path = array();
		
		if($custom_ongkir != '0'){
			if($client_name == '' 
				|| $client_address == '' 
				|| $client_postcode == '' 
				|| $client_phone == '' 
				|| $client_email == '' 
				|| $post_name == '' 
				|| $post_address == '' 
				|| $post_postcode == '' 
				|| $post_phone == '' 
				|| $post_email == '' 
				|| count($products) == 0 
				|| count($quantity) == 0 
				|| count($size) == 0 
				|| count($productprice) == 0 
				|| count($discount) == 0 
				|| count($productberat) == 0 
				|| count($path) == 0 
				|| count($productname) == 0) {
				return 'empty';
			}
		}
		
		$shipping = $this->__getShipping($city);
		if(count($shipping) > 0){
			$post_city = $shipping['id'];
			$post_price = $shipping['price'];
		}else{
			$post_city = '';
			$post_price = 0;
		}
		
		$data ['id'] = "Null";
		$data ['datecreated'] = date("Y-m-d H:i:s");
		$data ['userid'] = $this->db->escape_str($userid);
		$data ['client_name'] = $this->db->escape_str($client_name);
		$data ['client_address'] = $this->db->escape_str($client_address);
		$data ['client_postcode'] = $this->db->escape_str($client_postcode);
		$data ['client_email'] = $this->db->escape_str($client_email);
		$data ['client_phone'] = $this->db->escape_str($client_phone);
		$data ['gran_total'] = $this->db->escape_str($gran_total);

		$productlistsesion = $this->session->userdata('cart');
		if(is_array($productlistsesion)){
			$outputArray = array(); 
			$keysArray = array(); 
			foreach ($productlistsesion as $innerArray) {
				if (!in_array($innerArray['status_produk'], $keysArray)) { 
					$keysArray[] = $innerArray['status_produk'];
					$outputArray[] = $innerArray; 
				}
			}
			$resultarray = count($outputArray);
			if(isset($outputArray[0]['status_produk']) && $outputArray[0]['status_produk'] == '0'){
				$statusorder = '2';
			}else{
				$statusorder = '3';
			}
		}
		
		if($custom_ongkir == '0'){
			$data ['post_price'] = '0';
			$data ['status_order'] = '1';
		}else{
			$data ['post_price'] = $this->db->escape_str($post_price);
			$data ['status_order'] = $statusorder;
		}
		$data ['post_name'] = $this->db->escape_str($post_name);
		$data ['post_address'] = $this->db->escape_str($post_address);
		$data ['post_city'] = $this->db->escape_str($post_city);
		$data ['post_postcode'] = $this->db->escape_str($post_postcode);
		$data ['post_email'] = $this->db->escape_str($post_email);
		$data ['post_phone'] = $this->db->escape_str($post_phone);
		$data ['shipping_type'] = $this->db->escape_str($tipe_pengiriman);
		
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			$sales_id = $this->db->insert_id();
			$this->session->set_userdata('idsales', $sales_id);
			if(count($products) > 0 && count($quantity) > 0 && (count($products) == count($quantity))){	
				for($p = 0; $p < count($products); $p++){
					$this->db->query("
						UPDATE ".$this->produk_ukuran_table."
						SET 
							stock = stock - ".$quantity[$p]."
						WHERE 
							id = ".$size[$p]."
					");
					$idata['id'] = "Null";
					$idata['order_id'] = $sales_id;
					$idata['product_id'] = $products[$p];
					$idata['name'] = $productname[$p];
					$idata['price'] = $productprice[$p];
					$idata['berat'] = $productberat[$p];
					$idata['discount'] = $discount[$p];
					$idata['quantity'] = $quantity[$p];
					$idata['path'] = $path[$p];
					$idata['size'] = $size[$p];
					$InsertDetails = $this->db->insert($this->order_detail_table, $idata);
				}					
			}	
			$this->session->unset_userdata('cart');
			$this->session->unset_userdata('custom_ongkir');
			
			return 'success';
		}else{
			return 'failed';
		}
	}
	private function __getShipping($id){
		$q = $this->db->query("
			SELECT
				id
				,city
				,price
			FROM
				 ".$this->pengiriman_table."
			WHERE id = '".$id."'
		");
		$result = $q->first_row('array');
		return $result;
	}
	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$userid = isset($params["userid"])?$params["userid"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}
		if($userid != '') {
			$conditional = "WHERE userid = '".$userid."'";
		}
		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}
		
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		$result = $this->get_details($result);
		return $result;
	}
	private function get_details($details){
		if(count($details) > 0){
			$details['details'] = $this->__getDetailsSales($details['id']);
		}else{
			$details = array();
		}
		return $details;
	}
	public function __getDetailsSales($thisId){
		$q = $this->db->query("
			SELECT
				id
				,order_id
				,product_id
				,name
				,berat
				,price
				,discount
				,size
				,path
				,quantity
				,(price * quantity) as totalharga
			FROM
				".$this->order_detail_table."
			WHERE
				order_id = '".$thisId."'				
		");
		$result = $q->result_array();
		return $result;
	}
}