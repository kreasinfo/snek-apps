<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class OrderModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "orders";
		$this->pengiriman_table = "pengiriman";
		$this->kategori_pengiriman_table = "kategori_pengiriman";
		$this->fourthtablename = "orders_detail";
		$this->fivetablename = "produk_ukuran";
	}
	
	public function changeStatusOrder($params = array()) {
		$orderid = isset($params["orderid"])?$params["orderid"]:'';
		$status = isset($params["status"])?$params["status"]:'';
		$shipping_agen = isset($params["shipping_agen"])?$params["shipping_agen"]:'';
		$shipping_number = isset($params["shipping_number"])?$params["shipping_number"]:'';
		$to_bank = isset($params["to_bank"])?$params["to_bank"]:'';
		$from_bank = isset($params["from_bank"])?$params["from_bank"]:'';
		$total_transfered = isset($params["total_transfered"])?$params["total_transfered"]:'';
		$account_name_bank = isset($params["account_name_bank"])?$params["account_name_bank"]:'';

		if($orderid == '') {
			return 'empty';
		}
		if($status == '') {
			return 'empty';
		}
		if($status == 8) {
			if($to_bank == '' || $total_transfered == '' || $account_name_bank == '') {
				return 'empty';
			}
		}
		
		$conditional = "WHERE id = '".$orderid."'";
		$set_data = "SET datemodified=now(), status = '".$status."'";

		if($shipping_number != '') {
			$set_data .= ",shipping_no= '".$shipping_number."'";
		}
		if($to_bank != '') {
			$set_data .= ",transfered_bank_id= '".$to_bank."'";
		}
		if($from_bank != '') {
			$set_data .= ",client_bank_name= '".$from_bank."'";
		}
		if($total_transfered != '') {
			$set_data .= ",client_transfered= '".str_replace(".","",$total_transfered)."'";
		}
		if($account_name_bank != '') {
			$set_data .= ",client_account_name= '".$account_name_bank."'";
		}

		$doOrder = $this->db->query("UPDATE ".$this->maintablename."
								".$set_data."
								".$conditional."
								");
		if($doOrder) {
			if($status == 3) {
				/* Untuk saat ini update stock di lakukan saat user check out
				@$this->updateStock($orderid);
				*/
			}
			return 'success';
		} else {
			return 'failed';
		}
	}
	public function filterData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$rest  = "ORDER BY id DESC";
		$conditional = "WHERE id LIKE '%".$this->db->escape_str($name)."%'";
		$conditional .= "AND userid = ".$this->session->userdata('memberid');

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function filterDataCount($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';

		$rest  = "ORDER BY id DESC";
		$conditional = "WHERE id LIKE '%".$this->db->escape_str($name)."%'";
		$conditional .= "AND userid = ".$this->session->userdata('memberid');

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listData($params=array()){
		$statusorder = isset($params["statusorder"])?$params["statusorder"]:'';
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($statusorder != '') {
			$conditional .= "AND status = '".$statusorder."'";
		}

		$conditional .= "AND userid = ".$this->session->userdata('memberid');

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE 1 = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");

		$result = $q->result_array();
		$result = $this->__getShipping($result);
		$result = $this->__getOrderdetail($result);
		return $result;
	}

	private function __getOrderdetail($id){
        $countid = count($id);
        
        $i = 0;
        for ($i =0; $i < $countid; $i++) {
            $id[$i]["details"] = $this->__GetOrderdetailSQL($id[$i]["id"]);
        }
        return $id;
    }

    public function __GetOrderdetailSQL($id){
        $q = $this->db->query("
            SELECT
                *
            FROM
                ".$this->fourthtablename."
            WHERE
                order_id = '".$id."' 
            ORDER BY id DESC              
        ");
        $result = $q->result_array();
        return $result;
    }

	private function __getShipping($id_shipping){
        $countid = count($id_shipping);
        
        $i = 0;
        for ($i =0; $i < $countid; $i++) {
        	if($id_shipping[$i]["status_order"] == '1'){

        	}else{
        		$id_shipping[$i]["shipped"] = $this->__GetShippingSQL($id_shipping[$i]["post_city"]);
        	}
            
        }
        return $id_shipping;
    }
    
    private function __GetShippingSQL($id_shipping){
        $q = $this->db->query("
            SELECT
                *
            FROM
                ".$this->pengiriman_table."
            WHERE
                id = '".$id_shipping."'               
        ");
        $result = $q->first_row('array');
        $result = $this->__getCategory($result);
        return $result;
    }

    private function __getCategory($result){
    	if(isset($result) && count($result) > 0){
    		$result["category"] = $this->__GetCategorySQL($result["id_category"]);
    	}else{
    		$result["category"] = array();
    	}
        return $result;
    }

    private function __GetCategorySQL($id_category){
        $q = $this->db->query("
            SELECT
                *
            FROM
                ".$this->kategori_pengiriman_table."
            WHERE
                id = '".$id_category."'               
        ");
        $result = $q->first_row('array');
        return $result;
    }

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}
		$conditional .= "AND userid = ".$this->session->userdata('memberid');

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE 1 = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function reStock($orderid) {
		$orderDetail = $this->__getdetailOrderSQL($orderid);

		foreach ($orderDetail as $key_o => $val_o) {
			@$this->doRestockSQL($val_o['product_id'],$val_o['size'],$val_o['quantity']);
		}

		return true;
	}

	private function __getdetailOrderSQL($id){
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->fourthtablename."
			WHERE order_id = ".$id."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataTopselling($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY total DESC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}
		
		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				id,
				product_id,
				COUNT(product_id) as total,
				name,
				price,
				path
			FROM
				".$this->fourthtablename."
			WHERE 1=1 
			GROUP BY product_id
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		// $result = $this->__getProduk($result);
		return $result;
	}

}