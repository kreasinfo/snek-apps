<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ProdukModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "produk";
		$this->produk_kategori_table = "kategori_produk";
		$this->produk_ukuran_table = "produk_ukuran";
		$this->produk_img_table = "produk_img";
		$this->orders_detail_table = "orders_detail";
		$this->lokasi_ukm_table = "lokasi_ukm";
		$this->ukm_table = "ukm";
	}
	public function filterDataProduk($params=array()){
		$sortname = isset($params["sortname"])?$params["sortname"]:'';
		$price = isset($params["price"])?$params["price"]:'';
		$city = isset($params["city"])?$params["city"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$rest  = "";
		$conditional = '';

		if($city !=''){
			$conditional .= " AND id_lokasi_ukm = '".$this->db->escape_str($city)."'";
		}

		if($sortname != '' OR $price != '' ){
			if($sortname == '1'){
				$rest .= "ORDER BY name ASC";
			}elseif($sortname == '2'){
				$rest .= "ORDER BY name DESC";
			}
			if($price == '1'){
				$rest .= "ORDER BY price ASC";
			}elseif($price == '2'){
				$rest .= "ORDER BY price DESC";
			}
			
		}else{
			$rest .= "ORDER BY id ASC";
		}

		
		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE status = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		
		$result = $q->result_array();
		$result = $this->getLoopDetailListOne($result, 'images', $this->produk_img_table);
		$result = $this->getLoopDetail($result, 'id_kategori_produk', $this->produk_kategori_table);
		$result = $this->getLoopDetailLokasi($result, 'id_lokasi_ukm', $this->lokasi_ukm_table);
		$result = $this->getLoopDetailUkm($result, 'id_ukm', $this->ukm_table);
		$result = $this->getStockSum($result);
		return $result;
	}

	public function filterDataProdukCount($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$sortname = isset($params["sortname"])?$params["sortname"]:'';
		$city = isset($params["city"])?$params["city"]:'';
		$price = isset($params["price"])?$params["price"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		
		$rest  = "";
		$conditional = '';
		if($city !=''){
			$conditional .= " AND id_lokasi_ukm = '".$this->db->escape_str($city)."'";
		}
		if($sortname != '' OR $price != '' OR $city != ''){
			if($sortname == '1'){
				$rest .= "ORDER BY name ASC";
			}elseif($sortname == '2'){
				$rest .= "ORDER BY name DESC";
			}
			if($price == '1'){
				$rest .= "ORDER BY price ASC";
			}elseif($price == '2'){
				$rest .= "ORDER BY price DESC";
			}
			
		}else{
			$rest .= "ORDER BY id ASC";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE status = 1
			".$conditional."
			".$rest."
		");
		
		$result = $q->first_row('array');
		return $result;
	}
	public function filterData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$city = isset($params["city"])?$params["city"]:'';
		$price = isset($params["price"])?$params["price"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$rest  = "";
		$conditional = '';
		if($name !=''){
			$conditional .= "AND name LIKE '%".$this->db->escape_str($name)."%'";
		}

		if($city != ''){
			$conditional .= " AND id_lokasi_ukm = '".$this->db->escape_str($city)."'";
		}

		if($price != ''){
			if($price == '1'){
				$rest .= "ORDER BY price ASC";
			}elseif($price == '2'){
				$rest .= "ORDER BY price DESC";
			}
		}else{
			$rest .= "ORDER BY id ASC";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE status = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->getLoopDetailListOne($result, 'images', $this->produk_img_table);
		$result = $this->getLoopDetail($result, 'id_kategori_produk', $this->produk_kategori_table);
		$result = $this->getLoopDetailLokasi($result, 'id_lokasi_ukm', $this->lokasi_ukm_table);
		$result = $this->getLoopDetailUkm($result, 'id_ukm', $this->ukm_table);
		$result = $this->getStockSum($result);
		$result = $this->getTotalOrder($result, 'total', $this->orders_detail_table);
		return $result;
	}

	public function filterDataCount($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$city = isset($params["city"])?$params["city"]:'';
		$price = isset($params["price"])?$params["price"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		
		$rest  = "";
		$conditional = '';
		if($name !=''){
			$conditional .= "AND name LIKE '%".$this->db->escape_str($name)."%'";
		}

		if($city != ''){
			$conditional .= " AND id_lokasi_ukm = '".$this->db->escape_str($city)."'";
		}

		if($price != ''){
			if($price == '1'){
				$rest .= "ORDER BY price DESC";
			}elseif($price == '2'){
				$rest .= "ORDER BY price ASC";
			}
		}else{
			$rest .= "ORDER BY id ASC";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE status = 1
			".$conditional."
			".$rest."
		");
		
		$result = $q->first_row('array');
		return $result;
	}

	public function listData($params=array()){
		$id_kategori = isset($params["id_kategori"])?$params["id_kategori"]:'';
		$excludeidkategori = isset($params["excludeidkategori"])?$params["excludeidkategori"]:'';
		$excludeid = isset($params["excludeid"])?$params["excludeid"]:'';
		$topproduct = isset($params["topproduct"])?$params["topproduct"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		if($topproduct !=''){
			$rest = "-- ORDER BY id DESC";
		}else{
			$rest = "ORDER BY id DESC";
		}

		if($id_kategori != '') {
			$conditional = "AND id_kategori_produk = '".$id_kategori."'";
		}

		if($excludeidkategori != '') {
			$conditional .= "AND id_kategori_produk != '".$excludeidkategori."'";
		}

		if($excludeid != '') {
			$conditional .= "AND id NOT IN (".$excludeid.")";
		}

		if($topproduct != '') {
			$conditional .= "AND id IN (".$topproduct.")";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				id
				,slug
				,name
				,price
				,discount
				,id_kategori_produk
				,description
				,id_lokasi_ukm
				,id_ukm
			FROM
				".$this->maintablename."
			WHERE status = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->getLoopDetailListOne($result, 'images', $this->produk_img_table);
		$result = $this->getLoopDetail($result, 'id_kategori_produk', $this->produk_kategori_table);
		$result = $this->getLoopDetailLokasi($result, 'id_lokasi_ukm', $this->lokasi_ukm_table);
		$result = $this->getLoopDetailUkm($result, 'id_ukm', $this->ukm_table);
		$result = $this->getStockSum($result);
		$result = $this->getTotalOrder($result, 'total', $this->orders_detail_table);
		return $result;
	}

	public function listDataCount($params=array()){
		$id_kategori = isset($params["id_kategori"])?$params["id_kategori"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id_kategori != '') {
			$conditional = "AND id_kategori_produk = '".$id_kategori."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE status = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}



	public function getData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$slug = isset($params["slug"])?$params["slug"]:'';

		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional = "AND id = '".$id."'";
		}


		if($slug != '') {
			$conditional = "AND slug = '".$slug."'";
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE status = 1
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		$result = $this->getStockSumSingle($result);
		$result = $this->getLoopDetailSingle($result, 'id_kategori_produk', $this->produk_kategori_table);
		$result = $this->getLoopDetailListSingle($result, 'produk_ukuran', $this->produk_ukuran_table);
		$result = $this->getLoopDetailListSingle($result, 'images', $this->produk_img_table);
		$result = $this->getLoopDetailLokasiSingle($result, 'id_lokasi_ukm', $this->lokasi_ukm_table);
		$result = $this->getLoopDetailUkmSingle($result, 'id_ukm', $this->ukm_table);
		return $result;
	}

	public function listDataImages($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$id_product = isset($params["id_product"])?$params["id_product"]:'';
	
		$conditional = "";
	
		if($id != '') {
			$conditional .= "  AND id = '".$id."'";
		}
	
		if($id_product != '') {
			$conditional .= "  AND id_product = '".$id_product."'";
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->produk_img_table."
			WHERE (1=1)
			".$conditional."
		");
	
		$result = $q->result_array();
		return $result;
	}


	private function getStockSum($loopdata){
		if(count($loopdata) > 0){
			for($i = 0; $i < count($loopdata); $i++){
				$loopdata[$i]['stock'] = $this->getStockSumSQL($loopdata[$i]['id']);
			}
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}


	private function getStockSumSingle($loopdata){
		if(count($loopdata) > 0){
			$loopdata['stock'] = $this->getStockSumSQL($loopdata['id']);
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}


	private function getLoopDetailList($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			for($i = 0; $i < count($loopdata); $i++){
				$loopdata[$i][$fielddata] = $this->getSQLDetailList($loopdata[$i]['id'], $tablename);
			}
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}



	private function getLoopDetailListOne($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			for($i = 0; $i < count($loopdata); $i++){
				$loopdata[$i][$fielddata] = $this->getSQLDetailListOne($loopdata[$i]['id'], $tablename);
			}
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}


	private function getLoopDetailListSingle($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			if($fielddata == 'produk_ukuran'){
				$loopdata[$fielddata] = $this->getSQLDetailListUkuran($loopdata['id'], $tablename);
			}else{
				$loopdata[$fielddata] = $this->getSQLDetailList($loopdata['id'], $tablename);
			}
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}



	private function getLoopDetail($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			for($i = 0; $i < count($loopdata); $i++){
				$loopdata[$i][$fielddata] = $this->getSQLDetail($loopdata[$i][$fielddata], $tablename);
			}
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}

	private function getLoopDetailSingle($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			$loopdata[$fielddata] = $this->getSQLDetail($loopdata[$fielddata], $tablename);
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}

	public function getSQLDetailList($iddata, $tablename){
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$tablename."
			WHERE id_produk = '".$iddata."'
			ORDER BY id ASC
		");
		$result = $q->result_array();
		return $result;
	}

	public function getSQLDetailListUkuran($iddata, $tablename){
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$tablename."
			WHERE id_produk = '".$iddata."' AND stock > 0
			ORDER BY id ASC
		");
		$result = $q->result_array();
		return $result;
	}

	public function getSQLDetailListOne($iddata, $tablename){
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$tablename."
			WHERE id_produk = '".$iddata."'
			ORDER BY id ASC
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function getSQLDetail($iddata, $tablename){
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$tablename."
			WHERE id = '".$iddata."'
			ORDER BY id DESC
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function getStockSumSQL($iddata){
		$q = $this->db->query("
			SELECT
				SUM(stock) as jumlah_stock
			FROM
				".$this->produk_ukuran_table."
			WHERE id_produk = '".$iddata."'
			ORDER BY id DESC
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function getStockSize($iddata){
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->produk_ukuran_table."
			WHERE id = '".$iddata."'
			ORDER BY id DESC
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listDataPopular($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}
		
		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->fourthtablename."
			WHERE 1=1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->__getShipping($result);
		// $result = $this->__getOrderdetail($result);
		return $result;
	}

	private function getTotalOrder($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			for($i = 0; $i < count($loopdata); $i++){
				$loopdata[$i][$fielddata] = $this->getSQLTotalOrder($loopdata[$i]['id'], $tablename);
			}
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}

	public function getSQLTotalOrder($iddata, $tablename){
		$q = $this->db->query("
			SELECT
				id,
				product_id,
				COUNT(product_id) as total,
				sum(quantity) as totalquantity,
				name,
				price,
				quantity,
				path
			FROM
				".$tablename."
			WHERE product_id = '".$iddata."'
			GROUP BY product_id
		");
		$result = $q->first_row('array');
		return $result;
	}

	private function getLoopDetailLokasi($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			for($i = 0; $i < count($loopdata); $i++){
				$loopdata[$i][$fielddata] = $this->getSQLDetailLokasi($loopdata[$i][$fielddata], $tablename);
			}
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}

	public function getSQLDetailLokasi($iddata, $tablename){
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$tablename."
			WHERE id = '".$iddata."'
			ORDER BY id DESC
		");
		$result = $q->first_row('array');
		return $result;
	}

	private function getLoopDetailUkm($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			for($i = 0; $i < count($loopdata); $i++){
				$loopdata[$i][$fielddata] = $this->getSQLDetailUkm($loopdata[$i][$fielddata], $tablename);
			}
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}

	public function getSQLDetailUkm($iddata, $tablename){
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$tablename."
			WHERE id = '".$iddata."'
			ORDER BY id DESC
		");
		$result = $q->first_row('array');
		return $result;
	}

	private function getLoopDetailLokasiSingle($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			$loopdata[$fielddata] = $this->getSQLDetailLokasi($loopdata[$fielddata], $tablename);
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}

	private function getLoopDetailUkmSingle($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			$loopdata[$fielddata] = $this->getSQLDetailUkm($loopdata[$fielddata], $tablename);
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}
}