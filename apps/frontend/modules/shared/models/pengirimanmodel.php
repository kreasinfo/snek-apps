<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class PengirimanModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "pengiriman";
		$this->secondtablename = "kategori_pengiriman";
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->__getCategory($result);
		return $result;
	}

	private function __getCategory($id_category){
        $countid = count($id_category);
        
        $i = 0;
        for ($i =0; $i < $countid; $i++) {
            $id_category[$i]["category"] = $this->__GetCategorySQL($id_category[$i]["id_category"]);
        }
        return $id_category;
    }
    
    private function __GetCategorySQL($id_category){
        $q = $this->db->query("
            SELECT
                *
            FROM
                ".$this->secondtablename."
            WHERE
                id = '".$id_category."'               
        ");
        $result = $q->first_row('array');
        return $result;
    }
    
    public function getPrice($id_shipping){
        $q = $this->db->query("
            SELECT
                price
            FROM
                ".$this->maintablename."
            WHERE
                id = '".$id_shipping."'               
        ");
        $result = $q->first_row('array');
        if(count($result) > 0){
        	$hasil = $result['price'];
        }else{
        	$hasil = 0;
        }
        return $hasil;
    }


	public function filterSelect2($query = "", $id_tipe = "" ,$bdg = ''){
		$conditional = '';
		$limits = '';
		if($bdg !=''){
			$conditional .= " AND city LIKE '%".$bdg."%'";
			$limits .= "";
		}else{
			$limits .= "limit 0, 10";
		}
		$q = $this->db->query("
			SELECT
				id
				,city as text
			FROM
				".$this->maintablename."
			WHERE 
				city LIKE '%".$query."%'
				AND id_category = ".$id_tipe."
				".$conditional."
				".$limits."
		");
		
		$result = $q->result_array();
		return $result;
	}

}