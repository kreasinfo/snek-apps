<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class InformationpageModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "information_page";
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "AND id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				id
				,slug
				,title
			FROM
				".$this->maintablename."
			WHERE status  = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function detailData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$slug = isset($params["slug"])?$params["slug"]:'';

		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($slug != '') {
			$conditional .= "AND slug = '".$slug."'";
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE status = 1
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		return $result;
	}

}