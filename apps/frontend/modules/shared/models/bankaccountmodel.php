<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class BankAccountModel extends CI_Model{
	var $ci;
	function __construct() {	 
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "bank_account";	
	}
	public function listData(){
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			ORDER by name ASC
		");
		$result = $q->result_array();
		return $result;
	}
}