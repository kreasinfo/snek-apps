<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MembersModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "members";
		$this->ukm_table = "ukm";
	}
	public function entriData($params=array()){
		$fullname = isset($params["fullname"])?$params["fullname"]:'';
		$phone = isset($params["phone"])?$params["phone"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$password = isset($params["password"])?$params["password"]:'';
		$address = isset($params["address"])?$params["address"]:'';
		
		$exist_email = $this->__CheckEmailExist($email);
		
		if($exist_email > 0){return 'exist_email';}
		
		$data ['id'] = "Null";
		$data ['fullname'] = $this->db->escape_str($fullname);
		$data ['phone'] = $this->db->escape_str($phone);
		$data ['email'] = $this->db->escape_str($email);
		$data ['address'] = $this->db->escape_str($address);
		$data ['password'] = md5($password);
		$data ['date_created'] = date("Y-m-d H:i");
		$data ['status'] = 0;
		$data ['vernumber'] = md5($password.$email);
	
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_members')." dengan nama = ".htmlentities($fullname).""));
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function entriDataUkm($params=array()){
		$fullname = isset($params["fullname"])?$params["fullname"]:'';
		$phone = isset($params["phone"])?$params["phone"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$password = isset($params["password"])?$params["password"]:'';
		$address = isset($params["address"])?$params["address"]:'';
		
		$exist_email = $this->__CheckEmailExistUkm($email);
		
		if($exist_email > 0){return 'exist_email';}
		
		$data ['id'] = "Null";
		$data ['name'] = $this->db->escape_str($fullname);
		$data ['phone'] = $this->db->escape_str($phone);
		$data ['email'] = $this->db->escape_str($email);
		$data ['address'] = $this->db->escape_str($address);
		$data ['password'] = md5($password);
		$data ['datecreated'] = date("Y-m-d H:i");
	
		$doInsert = $this->db->insert($this->ukm_table, $data);
		if($doInsert){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_membersukm')." dengan nama = ".htmlentities($fullname).""));
			return 'success';
		}else{
			return 'failed';
		}
	}
	
	public function confirmation($vernumber = ''){
		$data = array('status' => 1);
		$this->db->where('vernumber', $vernumber);
		$doUpdate = $this->db->update($this->maintablename, $data); 
		if($doUpdate){
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function updateData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$fullname = isset($params["fullname"])?$params["fullname"]:'';
		$birthday = isset($params["birthday"])?$params["birthday"]:'';
		$address = isset($params["address"])?$params["address"]:'';
		$postcode = isset($params["postcode"])?$params["postcode"]:'';
		$gender = isset($params["gender"])?$params["gender"]:'';
		$phone = isset($params["phone"])?$params["phone"]:'';

		$reformat_birthday = date('Y-m-d',strtotime($birthday));

		$a1     = isset($params["a1"])?$params["a1"]:'';
        $b1     = isset($params["b1"])?$params["b1"]:'';
        $a2      = isset($params["a2"])?$params["a2"]:'';
        $b2      = isset($params["b2"])?$params["b2"]:'';
        $width_resize      = isset($params["width_resize"])?$params["width_resize"]:'';

        if ($_FILES['pathprofile']['size']<>0){
            $getData = $this->listDataUbahProfil(array('id' => $id));     
            if(count($getData) > 0) {
                $pfile = $this->webconfig['media-path-profile'].$getData[0]['logo_img'];
                if (file_exists($pfile)){
                    @unlink($pfile);
                }
                $delete_cropresize = $this->del_cropresize_profile($getData[0]['logo_img']);
            }
        }

        $upload_image_profile = $this->uploadImageProfile($width_resize, $a1, $b1, $a2, $b2);

        if(isset($upload_image_profile["error"])) {
            if($upload_image_profile["error"] == "failed_ext") {
                return "failed_ext";
            }
        }

        $path_profile = isset($upload_image_profile["path"])?$upload_image_profile["path"]:"";


		$sql_user = "fullname = '".$this->db->escape_str($fullname)."'
					,birthday = '".$this->db->escape_str($reformat_birthday)."'
					,address = '".$this->db->escape_str($address)."'
					,postcode = '".$this->db->escape_str($postcode)."'
					,gender = '".$this->db->escape_str($gender)."'
					,phone = '".$this->db->escape_str($phone)."'";
		
		if ($_FILES['pathprofile']['size']<>0) $sql_user .= ", logo_img = '".$this->db->escape_str($path_profile)."'";

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->pagename, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.", name = ".htmlentities($fullname).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	private function uploadImageProfile($width_resize, $a1, $b1, $a2, $b2) {
        $this->ci->load->helper('upload');
        if ($_FILES['pathprofile']['size']<>0){
            $data = array();

            $tmpName = $_FILES['pathprofile']['tmp_name']; 
            list($width, $height, $type, $attr) = getimagesize($tmpName);
            # Rasio Ukuran Sliders #
            $rasio = $width/$width_resize;
            $extension_thumb = strtoupper(end(explode('.', $_FILES['pathprofile']['name'])));
            if (!(($extension_thumb == 'JPEG') || ($extension_thumb == 'JPG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'GIF'))){
                $data['error'] = "failed_ext";
            }
            
            # UPLOAD #
            $pathdir = date("Y/m/d");
            $thepath = $this->webconfig['media-path-profile'].$pathdir."/";                       
            $thepath_create = $this->webconfig['media-path-profile'].$pathdir."/temp/";                       
            $exp = explode("/",$thepath_create);
            $way = '';
            foreach($exp as $n){
                $way .= $n.'/';
                @mkdir($way);       
            }
            
            $pos = strripos($_FILES['pathprofile']['name'], '.');
            if($pos === false){
                $ordinary_name = $_FILES['pathprofile']['name'];
            }else{
                $ordinary_name = substr($_FILES['pathprofile']['name'], 0, $pos);
            }

            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);
            
            $result = was_upload_file('pathprofile', $name_upload, false, $thepath_create."/", array(), array(), array());

            if ($result['status'] > 0){
                $data['path'] = "";
            }else{
                $path1 = $pathdir."/".$name_upload_c;
                $data['path'] = $path1;
                if(isset($rasio) && $rasio !=''){
                    $finalcrop_resize = $this->croping_resize_profile($result,$a1, $b1, $a2, $b2,$rasio,$thepath);
                }
            }
        }else{
            $data['error'] = "empty_file";
        }

        return $data;
    }

    private function croping_resize_profile($result,$a1, $b1, $a2, $b2,$rasio,$image_path) {
        
        # RESIZE and CROP #
        $thepath = $this->webconfig['media-path-profile-thumb'].date("Y/m/d")."/";                       
        $exp = explode("/",$thepath);
        $way ='';
        foreach($exp as $n){
            $way.=$n.'/';
            @mkdir($way);       
        }
        
        $file_name = $result['name'];
        $src_path = $image_path . 'temp/' . $file_name;
        
        $exp_file = explode('.', $file_name, 2);
        
        $des_path =  $image_path .$file_name;
        $aa1 = $a1 * $rasio;
        $bb1 = $b1 * $rasio;
        $aa2 = $a2 * $rasio;
        $bb2 = $b2 * $rasio;
        if($des_path !=''){
            $this->image_moo
                ->load($src_path)
                ->crop($aa1,$bb1,$aa2,$bb2)
                ->save($des_path);
        }

        ## RESIZE IMAGE ##
        foreach($this->webconfig['image_profile'] as $key => $value){
            $size_file = explode('x',$value);
            $file_final_name_resize = $exp_file[0].'_'.$value.'_thumb.'.$exp_file[1];
            $des_path_resize =  $thepath . $file_final_name_resize;
            $this->image_moo
                ->load($des_path)
                ->stretch($size_file[0],$size_file[1])
                ->save($des_path_resize);
        }

        /* Delete Temp File */
        if (file_exists($src_path)){
            @unlink($src_path);
        }
    }

    private function del_cropresize_profile($cfile) {
        if($cfile == "") return false;
        
        $explod_file = explode("/",$cfile);
        $filename = end($explod_file);
        $exp_file = explode(".",$filename);
        $exp_thumb_path = explode("/", $this->webconfig['media-path-profile-thumb'].$cfile, -1);
        $thumb_path = implode("/", $exp_thumb_path);
        
        foreach($this->webconfig['image_profile'] as $key => $value){
            $delete_resize = $thumb_path.'/'.$exp_file[0]."_".$value."_thumb.".$exp_file[1];
            if (file_exists($delete_resize)){
                @unlink($delete_resize);
            }
        }
    }

	public function updatePassword($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$password = isset($params["password"])?$params["password"]:'';

		if($password != ''){
			if(strlen($password) < 5){
				return 'min_5';
			}else{
				$sql_user = "password = '".$this->db->escape_str(md5($password))."' ";
			}

		}

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->pagename, 'details' => $this->lang->line('logs_modif_pass_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteData($id){
		if($id == 0) return 'failed';
		$doDelete = $this->db->query("
		DELETE FROM ".$this->maintablename."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function filterData($params=array()){
		$fullname = isset($params["fullname"])?$params["fullname"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$rest  = "ORDER BY id DESC";
		$conditional = "WHERE fullname LIKE '%".$this->db->escape_str($fullname)."%'";

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function filterDataCount($params=array()){
		$fullname = isset($params["fullname"])?$params["fullname"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';

		$rest  = "ORDER BY id DESC";
		$conditional = "WHERE fullname LIKE '%".$this->db->escape_str($fullname)."%'";

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function detailMember($id = 0, $email = ""){
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE id = ".$id." AND email = '".$this->db->escape_str($email)."'
		");
		$result = $q->first_row('array');
		return $result;
	}


	function checkname($nama){
		$q = $this->db->query("
			SELECT
				id
				,nama
			FROM
				".$this->maintablename."
			WHERE
				nama = '".$this->db->escape_str($nama)."'
		");
		$result = $q->first_row('array');
		return $result;
	}

	private function __CheckEmailExist($email){
		if($email !=''){
			$db = $this->db->query("
				SELECT
					count(id) as jumlah
				FROM
					".$this->maintablename."
				WHERE email LIKE '".$this->db->escape_str($email)."'
			")->result_array();

			$jumlah = $db[0]['jumlah'];
			return $jumlah;
		}else{
			return 'failed';
		}
	}

	private function __CheckEmailExistUkm($email){
		if($email !=''){
			$db = $this->db->query("
				SELECT
					count(id) as jumlah
				FROM
					".$this->ukm_table."
				WHERE email LIKE '".$this->db->escape_str($email)."'
			")->result_array();

			$jumlah = $db[0]['jumlah'];
			return $jumlah;
		}else{
			return 'failed';
		}
	}

	private function __CheckEmailExistEdit($id,$email){
		$conditional = "";
		if($id !='' AND $email !=''){
			$db = $this->db->query("
				SELECT
					count(id) as jumlah
				FROM
					".$this->maintablename."
				WHERE id NOT IN (".$id.")
				AND email LIKE '".$this->db->escape_str($email)."'
			")->result_array();

			$jumlah = $db[0]['jumlah'];
			return $jumlah;
		}
	}

	

	public function listDataUbahProfil($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		
		$conditional = "";
		
		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}
		
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
		");
		$result = $q->result_array();
		return $result;
	}

	public function getUserdata($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		
		$conditional = "";
		
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}
		
		if($email != '') {
			$conditional .= "AND email = '".$email."'";
		}
		
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE 1=1
			".$conditional."
		");
		$result = $q->first_row('array');
		return $result;
	}

}