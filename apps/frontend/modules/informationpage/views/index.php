<!-- Begin Main -->
<!-- <div role="main" class="main"> -->

  <!-- Begin page top -->
  <!-- <section class="page-top-lg">
    <div class="container">
      <div class="page-top-in">
        <h2><span><?PHP echo strtoupper($listdata['title']); ?></span></h2>
      </div>
    </div>
  </section> -->
  <!-- End page top -->
  <!-- <div class="container">
    <div class="row">
      <div class="col-md-3 animation">&nbsp;</div>
      <div class="col-md-6 animation">
        <h2><?PHP echo strtoupper($listdata['title']); ?></h2>
        <?PHP echo clean_str($listdata['body']); ?>
      </div>
      <div class="col-md-3 animation">&nbsp;</div>
    </div>  
</div> -->


<div class="container">
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li class="active"><?php echo strtoupper($listdata['title']); ?></li>
    </ol>
</div>
<div class="border-bottom"></div>
<div class="clearfix"></div>
<br>
<div class="container"><!--- Container -->
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <?php echo clean_str($listdata['body']); ?>
        </div>
    </div>
</div><!--- Container -->
<br>