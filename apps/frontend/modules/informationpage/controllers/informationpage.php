<?php
class Informationpage extends CI_Controller {
	var $webconfig;
	var $pagename;
	var $configuration;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('session');
		$this->lang->load('global', 'bahasa');

		$this->load->sharedModel('InformationpageModel');
		$this->load->sharedModel('LoginModel');
		
		$this->webconfig = $this->config->item('webconfig');
		$this->pagename = $this->lang->line('home');
		$this->configuration = $this->ShopconfigurationModel->listData();	
	}
	function index(){
		redirect(base_url().'notfoundpage');
	}

	function read($id = 0, $slug = '')
	{
		$tdata = array();
		$tdata['listdata'] = $this->InformationpageModel->detailData(array('id' => $id, 'slug' => $slug));

		if(count($tdata['listdata']) == 0){
			redirect(base_url().'notfoundpage');
		}
		
		$ldata['informationpage'] = $tdata['informationpage'] = $this->InformationpageModel->listData();
		## LOAD LAYOUT ##
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		$ldata['informationpage'] = $this->InformationpageModel->listData();
		$this->load->sharedView('template', $ldata);
	}
}