<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class LoginModel extends CI_Model {
	var $module, $CI;
	function __construct() {	 
		parent::__construct();
		$this->CI =& get_instance(); 
		$this->CI->load->library('session');		
		$this->module = "Authentification";
	}
	public function doLogin($email = '', $password = ''){
		if($email == '' || $password == ''){
			return 'failed_input';
		}
		
		$password = md5 ($password);
		$member = $this->checkLogin($email,$password);
		
		if(count($member) > 0){
			if($member['status'] == 0){
				return 'non_activation';
			}
			if($member['status'] == 1){
				$this->sessionCreate($member['id']);
				writeLog(array('module' => $this->module, 'details' => "Login to Application"));
				return 'success_login';
			}
		}else{
			return 'failed_login';
		}
	}
	public function doLoginUkm($email = '' ,$password = ''){
		if($email == '' || $password == ''){
			return 'failed_input';
		}
		
		$password = md5 ($password);
		$ukm = $this->checkLoginUkm($email,$password);
		
		if(count($ukm) > 0){
			if($ukm['status'] == 0){
				return 'non_activation';
			}
			if($ukm['status'] == 1){
				$this->sessionCreateUkm($ukm['id']);
				writeLog(array('module' => $this->module, 'details' => "Login Ukm to Application"));
				return 'success_login';
			}
		}else{
			return 'failed_login';
		}
	}
	public function doLogout(){
		writeLog(array('module' => $this->module, 'details' => "Member Logout from Application"));
		$this->session->unset_userdata('memberid');
		$this->session->unset_userdata('memberemail');
		$this->session->unset_userdata('membername');
	}
	public function doLogoutUkm(){
		writeLog(array('module' => $this->module, 'details' => "Ukm Logout from Application"));
		$this->session->unset_userdata('ukmid');
		$this->session->unset_userdata('ukmemail');
		$this->session->unset_userdata('ukmname');
	}
	public function isLoggedIn(){
		if($this->session->userdata('memberid') != ''
			&& $this->session->userdata('memberemail') != ''
			&& $this->session->userdata('membername') != ''
		){
			$validCheked = $this->__validSession($this->session->userdata('memberemail'),$this->session->userdata('memberid'));
			if($validCheked > 0){
				return true;
			}else{
				return false;
			}
		}else {
			return false;
		}
	}
	public function isLoggedInUkm(){
		if($this->session->userdata('ukmid') != ''
			&& $this->session->userdata('ukmemail') != ''
			&& $this->session->userdata('ukmname') != ''
		){
			$validCheked = $this->__validSessionUkm($this->session->userdata('ukmemail'),$this->session->userdata('ukmid'));
			if($validCheked > 0){
				return true;
			}else{
				return false;
			}
		}else {
			return false;
		}
	}
	private function sessionCreate($memberid){
		
		$q = $this->db->query("
			SELECT
				id
				,fullname
				,email
			FROM
				members
			WHERE
				id = '".$memberid."'		
		");
		$user = $q->first_row('array');
		# STORE SESSION #
		$this->session->set_userdata('memberid', $user['id']);
		$this->session->set_userdata('memberemail', $user['email']);
		$this->session->set_userdata('membername', $user['fullname']);
	}
	private function sessionCreateUkm($ukmid){
		
		$q = $this->db->query("
			SELECT
				id
				,name
				,email
			FROM
				ukm
			WHERE
				id = '".$ukmid."'		
		");
		$user = $q->first_row('array');
		# STORE SESSION #
		$this->session->set_userdata('ukmid', $user['id']);
		$this->session->set_userdata('ukmemail', $user['email']);
		$this->session->set_userdata('ukmname', $user['name']);
	}
	private function checkLogin($email = '', $password = ''){
		$q = $this->db->query("
			SELECT
				id
				,email
				,password
				,status
			FROM
				members
			WHERE
				email = '".$email."' AND password = '".$password."'
		");
		$result = $q->first_row('array');
		return $result;
	}
	private function checkLoginUkm($email = '' ,$password = ''){
		$q = $this->db->query("
			SELECT
				id
				,email
				,password
				,status
			FROM
				ukm
			WHERE
				email = '".$email."' AND password = '".$password."'
		");
		$result = $q->first_row('array');
		return $result;
	}
	private function __validSession($email, $memberid){
		$q = $this->db->query("
			SELECT
				id
				,email
			FROM
				members
			WHERE
				email = '".$email."'	
				AND	
				id = '".$memberid."'		
		");
		$result = $q->num_rows();
		return $result;
	}
	private function __validSessionUkm($ukmemail, $ukmid){
		$q = $this->db->query("
			SELECT
				id
				,email
			FROM
				ukm
			WHERE
				email = '".$ukmemail."'	
				AND	
				id = '".$ukmid."'		
		");
		$result = $q->num_rows();
		return $result;
	}

	
}
?>