<?php
class Pengiriman extends CI_Controller{
	var $webconfig;
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->sharedModel('PengirimanModel');
		$this->load->sharedModel('LoginModel');
		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login');
		}
		
		$this->webconfig = $this->config->item('webconfig');
		$this->module_name = $this->lang->line('pengiriman');
	}
	function index(){
		redirect(base_url().$this->router->class.'/listing');
	}
	function listing($page = 0){
		$tdata = array();
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		$this->load->sharedView('template', $ldata);
	}
	function getAllData($page = 0){
		
		$all_data = $this->PengirimanModel->listDataCount();
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 3;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><span>';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->PengirimanModel->listData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
															));
		
		$this->load->view($this->router->class.'/list', $listdata);
	}
	function searchdata($name = 'name',$id_kategori_pengiriman = 'id_kategori_pengiriman'){
		if($name == '-'){$name = '';}
		if($id_kategori_pengiriman == '-'){$id_kategori_pengiriman = '';}

		# PAGINATION #
		$all_data = $this->PengirimanModel->filterDataCount(array(
																'name'=>trim(urldecode($name))
																,'id_kategori_pengiriman'=>trim(urldecode($id_kategori_pengiriman))
															));
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/".$this->uri->segment(3)."/".$this->uri->segment(4)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 5;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><span>';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->PengirimanModel->filterData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
																,'name' => trim(urldecode($name))
																,'id_kategori_pengiriman' => trim(urldecode($id_kategori_pengiriman))
															));
		$this->load->view($this->router->class.'/list', $listdata);
	}
	function add(){
		$tdata = array();
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
		if($_POST)
		{
			$validator = new FormValidator();
		    $validator->addValidation("city","req",$this->lang->line('error_empty_city_pengiriman'));
		    $validator->addValidation("price","req",$this->lang->line('error_empty_price_pengiriman'));
		    $validator->addValidation("kategori_pengiriman","req",$this->lang->line('error_empty_id_category_pengiriman'));
		    
		    if($validator->ValidateForm())
		    {
		        $doInsert = $this->PengirimanModel->entriData(array(
														        'city'=>$this->input->post('city')
														        ,'price'=>$this->input->post('price')
														        ,'id_category'=>$this->input->post('kategori_pengiriman')
														  ));
				if($doInsert == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doInsert == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doInsert == 'exist_city'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_exist_city_category'));
				}else if($doInsert == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_entry');
				}
				$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {
		        $tdata['city']     = $this->input->post('city');
			    $tdata['price'] = $this->input->post('price');
			    $tdata['kategori_pengiriman'] = $this->input->post('kategori_pengiriman');
			    $tdata['kategori_pengiriman_text'] = $this->input->post('kategori_pengiriman_text');
		    	$tdata['error_hash'] = $validator->GetErrors();

				$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{
			$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
			$this->load->sharedView('template', $ldata);
		}
	}
	function modif($id = 0){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}

		$tdata['lists'] = $this->PengirimanModel->listData(array('id'=>$id));
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
		
		if($_POST)
		{
			$validator = new FormValidator();

		    $validator->addValidation("city","req",$this->lang->line('error_empty_city_pengiriman'));
		    $validator->addValidation("price","req",$this->lang->line('error_empty_price_pengiriman'));
		    $validator->addValidation("kategori_pengiriman","req",$this->lang->line('error_empty_id_category_pengiriman'));
			
		    if($validator->ValidateForm())
		    {
		        $doUpdate = $this->PengirimanModel->updateData(array(
														        'id' => $id
		        												,'city'=>$this->input->post('city')
														        ,'price'=>$this->input->post('price')
														        ,'id_category'=>$this->input->post('kategori_pengiriman')
														  ));

				if($doUpdate == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doUpdate == 'exist_city'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_exist_city_category'));
				}else if($doUpdate == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doUpdate == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_update');
					$tdata['lists'] = $this->PengirimanModel->listData(array('id'=>$id));
				}
				$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {
		        $tdata['city']     = $this->input->post('city');
			    $tdata['price'] = $this->input->post('price');
			    $tdata['kategori_pengiriman'] = $this->input->post('kategori_pengiriman');
			    $tdata['kategori_pengiriman_text'] = $this->input->post('kategori_pengiriman_text');
		    	$tdata['error_hash'] = $validator->GetErrors();

				$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{
			$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
			$this->load->sharedView('template', $ldata);
		}
	}
	public function deleteThis(){
		$doDelete = $this->__delete($this->input->post('dataid'));

		if($doDelete == 'failed'){
			echo "nodata";
		}else if($doDelete == 'success'){
			echo "success";
		}
	}
	public function postProcess(){
		$postvalue = $this->input->post('datacek');
		if($postvalue != ''){
			foreach($postvalue as $data){
				$this->__delete($data);
			}
			echo "success";
		}else{
			echo "nodata";
		}
	}
	private function __delete($id){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}
		$doDelete = $this->PengirimanModel->deleteData($id);
    	return $doDelete;
	}


}