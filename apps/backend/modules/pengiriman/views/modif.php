<script type="text/javascript">
jQuery(document).ready(function($) {
    $("#list_kategori_pengiriman").select2({
        placeholder: "<?php echo $this->lang->line('label_pilih_kategori'); ?>",
        allowClear: true,
        initSelection: function(element, callback) {
            callback({id: '<?php echo isset($lists[0]['category']['id'])?$lists[0]['category']['id']:''; ?>', text: '<?php echo isset($lists[0]['category']['name'])?$lists[0]['category']['name']:''; ?>' });
        },
        ajax: {
            url: '<?php echo base_url(); ?>webservices/kategori_pengiriman/',
            dataType: 'json',
            data: function (term, page) {
                return {
                    q: term
                };
            },
            results: function (data, page) {
                return { results: data };
            } ,
            cache: false
        }
    }).on("change", function(e) {
      $('#kategori_pengiriman_text').val($('.select2-chosen').text());
    });
    $('#price_an').autoNumeric('init', {aSign:'Rp ', mDec: '', aSep: '.', aDec: ','});
});
function getnumerik_price (){
    var getnumerik_price = jQuery('#price_an').autoNumeric('get');
    if(getnumerik_price == 0){
        jQuery('#price').val('');
    }else{
        jQuery('#price').val(getnumerik_price);
    }
}
</script>
<section class="main-content">
 
    <div class="content-wrap">
     
        <div class="wrapper">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo base_url('/'); ?>"><i class="ti-home mr5"></i><?php echo $this->lang->line('dashboard'); ?></a>
                </li>
                <li>
                    <a href="<?php echo base_url().$this->router->class; ?>"><?php echo ucfirst($this->module_name); ?></a>
                </li>
                <li class="active"><?php echo $this->lang->line('modif'); ?></li>
            </ol>

            <div class="panel">
                <header class="panel-heading">
                    <div class="row">
                        <div class="col-xs-10">
                            <h5 class="text-uppercase no-m"><?php echo strtoupper($this->module_name); ?></h5>
                            <small><?php echo $this->lang->line('kategory_teks'); ?></small>
                        </div>
                        <div class="col-xs-2 text-right">
                            <a class="btn btn-warning btn-xs" href="<?php echo base_url().$this->router->class; ?>"  title="<?php echo $this->lang->line('add'); ?>">
                                <i class="ti-plus"></i>
                                <?php echo $this->lang->line('list'); ?>
                            </a>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal" name="form" method="POST" action="" enctype="multipart/form-data">
                        <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
                            <?php foreach($error_hash as $inp_err){ ?>
                                <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                                    });
                                </script>
                            <?php } ?>
                        <?php } ?>
                        
                        <?php if(isset($success)){ ?>
                            <script type="text/javascript">
                            jQuery(document).ready(function($) {
                                toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                                });
                            </script>
                        <?php } ?>
                         <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('kategori_pengiriman'); ?> <span>*</span></label>
                            <div class="col-sm-5">
                                <input type="hidden" id="list_kategori_pengiriman" name="kategori_pengiriman" value="<?php echo isset($lists[0]['category']['id'])?$lists[0]['category']['id']:''; ?>" class="form-control" />
                                <input type="hidden" id="kategori_pengiriman_text" name="kategori_pengiriman_text" value="<?php echo isset($lists[0]['category']['name'])?$lists[0]['category']['name']:''; ?>" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('city'); ?> <span>*</span></label>
                            <div class="col-sm-5">
                                <input type="text" id="city" name="city" value='<?php echo isset($lists[0]['city'])?$lists[0]['city']:''; ?>' size="50" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('price'); ?> <span>*</span></label>
                            <div class="col-sm-2">
                                <input class="form-control" type="text" name="price_an" value='<?php echo isset($lists[0]['price'])?$lists[0]['price']:''; ?>' id="price_an" data-a-sign="Rp " size="20" onkeyup="getnumerik_price()" >
                                <input class="hidden" type="text" name="price" value='<?php echo isset($lists[0]['price'])?$lists[0]['price']:''; ?>' id="price" data-a-sign="Rp " size="20" >
                            </div>
                        </div>
                        <div class="form-group footertable">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-5 text-left">
                                <input class="btn btn-danger btn-sm mr5" type="submit" value="<?php echo $this->lang->line('navigation_kirim'); ?>">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
     
    </div>
 
    <a class="exit-offscreen"></a>
</section>