<p>
    Memasuki milenium ke tiga Indonesia dihadapkan pada tantangan  untuk menyiapkan masyarakat menuju era baru, yaitu globalisasi yang menyentuh semua aspek kehidupan.
</p>
<p>
    Dalam era global ini seakan dunia tanpa jarak.  Komunikasi dan transaksi ekonomi dari tingkat lokal hingga internasional dapat dilakukan sepanjang waktu.  Demikian pula nanti ketika perdagangan bebas sudah diberlakukan, tentu persaingan dagang dan tenaga kerja bersifat multi bangsa. Pada saat itu hanya bangsa yang unggullah yang akan mampu bersaing.
</p>
<p>Pendidikan merupakan modal dasar untuk menyiapkan insan yang berkualitas.</p>
<p>
    Menurut Undang-undang Sisdiknas Pendidikan adalah usaha sadar dan terencana untuk mewujudkan suasana belajar dan proses pembelajaran agar peserta didik secara aktif mengembangkan potensi dirinya untuk memiliki kekuatan spiritual keagamaan, pengendalian diri, kepribadian, kecerdasan, akhlak mulia, serta keterampilan yang diperlukan dirinya, masyarakat, bangsa dan negara.
</p>

<div class="container_img_4">
    <div class="container_item">
        <img alt="" src="<?php echo $this->webconfig['frontend_template']; ?>img/events/img2.jpg">
        <h5 class="nicdark_bg_yellow white">TITLE TEXT HERE</h5>
    </div>
    <div class="container_item">
        <img alt="" src="<?php echo $this->webconfig['frontend_template']; ?>img/events/img2.jpg">
        <h5 class="nicdark_bg_yellow white">TITLE TEXT HERE</h5>
    </div>
    <div class="container_item">
        <img alt="" src="<?php echo $this->webconfig['frontend_template']; ?>img/events/img2.jpg">
        <h5 class="nicdark_bg_yellow white">TITLE TEXT HERE</h5>
    </div>
    <div class="container_item">
        <img alt="" src="<?php echo $this->webconfig['frontend_template']; ?>img/events/img2.jpg">
        <h5 class="nicdark_bg_yellow white">TITLE TEXT HERE</h5>
    </div>
</div>

<p>
    Menurut UNESCO pendidikan hendaknya dibangun dengan empat pilar,  yaitu  learning to know, learning to do, learning to be, dan learning to live together.
</p>
<p>
    Sejak beberapa tahun terakhir ini masalah anak berkebutuhan khusus mulai merebak di Indonesia. Ini terlihat dengan mulai beredarnya informasi mengenai anak berkebutuhan khusus, dibukanya pusat-pusat terapi, terbentuknya yayasan-yayasan yang bergerak dibidang anak berkebutuhan khusus, seminar-seminar nasional yang membicarakan masalah anak berkebutuhan khusus dengan pakar dari dalam dan luar negeri, sampai adanya program pendidikan yang disosialisasikan oleh pemerintah mengenai program pendidikan inklusi.
</p>
<p>
Peningkatan masalah anak berkebutuhan khusus yang sangat pesat terjadi di seluruh dunia termasuk di Indonesia dalam kurun waktu 10 tahun terakhir ini. Melihat peningkatan prevalensi yang terus berlanjut, tidak tertutup kemungkinan bahwa saat ini anak berkebutuhan khusus sudah ada di setiap penjuru daerah di Indonesia. Yang menyedihkan adalah kurangnya lembaga pendidikan dan tenaga ahli yang paham mengenai anak berkebutuhan khusus ini, serta tidak tersebarnya pengetahuan mengenai penanganan anak  berkebutuhan khusus, sehingga selain kota-kota besar penanganan anak berkebutuhan khusus ini masih sangat minim bahkan seringkali terjadi salah diagnosa dan salah penanganan/terapi
</p>
<p>
Melalui penyebaran informasi, peningkatan pendidikan dan pengetahuan masyarakat umum, diharapkan penanganan anak berkebutuhan khusus menjadi terarah dan terpadu. Pada kenyataannya waktu adalah sangat berharga, karena semakin dini anak ditangani makin besar kemungkinan perubahan perilaku, emosi, motorik, dan lainnya ke arah lebih baik.
</p>
<p>
Berdasarkan pemikiran di atas perlunya melakukan berbagai upaya penanganan bagi anak berkebutuhan khusus. Dengan kondisi tersebut maka pendirian lembaga yang berkecimpung pada anak berkebutuhan khusus dengan beragam layanan yang lengkap sangat diperlukan.
</p>