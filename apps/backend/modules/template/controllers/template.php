<?php
class Template extends CI_Controller{
	var $webconfig;
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->sharedModel('LoginModel');
		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login');
		}
		
		$this->webconfig = $this->config->item('webconfig');
		$this->module_name = $this->lang->line('tentangkami');
	}
	function template_1(){
		$tdata = array();
		$this->load->view($this->router->class.'/template_1',$tdata);
	}
	function template_2(){
		$tdata = array();
		$this->load->view($this->router->class.'/template_2',$tdata);
	}
	function template_3(){
		$tdata = array();
		$this->load->view($this->router->class.'/template_3',$tdata);
	}
}