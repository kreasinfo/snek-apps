<script type="text/javascript">
jQuery(document).ready(function($) {
	$('#checkall').click(function () {
		$('#tabellistdata .check-all').attr('checked', this.checked);
	});
	$(".pagination a").click(function(e){
		// stop normal link click
		e.preventDefault(); 
		var linkUrl = jQuery(this).attr("href");
		if(linkUrl){
			jQuery('#listData').load(linkUrl.replace(/\s/g,'%20'));
		}
	});
});

function submitkie(){
	jQuery().ajaxStart(function($) {
		$('#loading').show();
		$('#result').hide();
	}).ajaxStop(function($) {
		$('#loading').hide();
		$('#result').fadeIn('slow');	
		$("#tabellistdata")[0].reset();
	});
	
	jQuery.ajax({
			type: 'POST',
			url: jQuery('#tabellistdata').attr('action'),
			data: jQuery('#tabellistdata').serialize(),
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				
				jQuery("#listData").load('<?php echo $_SERVER["REQUEST_URI"]; ?>');
			}
		})
		return false;
}
function deleteThis(code){
	var txt = "<?php echo $this->lang->line('alert_delete'); ?> <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
	jQuery.prompt(txt ,{  callback: doCondition, buttons: { <?php echo $this->lang->line('ok'); ?>: true, <?php echo $this->lang->line('cancel'); ?>: false },prefix:'jqismooth' });
}
function doCondition(v,m,f){
	  if(v){
	  	//alert(f.alertName);
		var posting = "dataid="+f.alertName;
		jQuery.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/deleteThis",
			data: posting,
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				jQuery("#listData").load('<?php echo $_SERVER["REQUEST_URI"]; ?>');
			}
		})
	  }else{
	  	jQuery.prompt.close();
	  }
}
function publishThis(code){
	var txt = "<?php echo $this->lang->line('alert_confirm'); ?>  <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
	$.prompt(txt ,{  callback: doPublish, buttons: { Ok: true, Cancel: false },prefix:'jqismooth' });
}
function doPublish(v,m,f){
	  if(v){
		var posting = "dataid="+f.alertName;
		
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/publish/",
			data: posting,
			success: function(response) {
				if(response == 'success'){
					toastr.success('<?php echo $this->lang->line('msg_success_confirm'); ?>','<?php echo $this->lang->line('success_notif'); ?>', 3000);
				}else{
					toastr.error('<?php echo $this->lang->line('msg_empty_confirm'); ?>', '<?php echo $this->lang->line('error_notif'); ?>',3000);
				}
				$("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	  }else{
	  	$.prompt.close();
	  }
}
function unpublishThis(code,page){
	var txt = "<?php echo $this->lang->line('alert_unconfirm'); ?>  <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
	$.prompt(txt ,{  callback: doUnpublish, buttons: { Ok: true, Cancel: false },prefix:'jqismooth' });
}
function doUnpublish(v,m,f){
	  if(v){
	  	var posting = "dataid="+f.alertName;
		
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/unpublish/",
			data: posting,
			success: function(response) {
				if(response == 'success'){
					toastr.success('<?php echo $this->lang->line('msg_success_unconfirm'); ?>','<?php echo $this->lang->line('success_notif'); ?>', 3000);
				}else{
					toastr.error('<?php echo $this->lang->line('msg_empty_unconfirm'); ?>','<?php echo $this->lang->line('error_notif'); ?>', 3000);
				}
				$("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	  }else{
	  	$.prompt.close();
	  }
}
</script>

<form id="tabellistdata" method="post" action="<?php echo base_url().$this->router->class; ?>/postProcess" onsubmit="return false;">
	<div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
            	<?php if(isset($lists) && count($lists) > 0) {?>
                    <table class="table table-striped no-m">
                        <thead>
                            <tr>
                                <th width="10" class="text-center">
                                	<input type="checkbox" id="checkall" />
                                </th>
                                <th width="10" class="text-center">
                                	<?php echo $this->lang->line('no'); ?>
                                </th>
                                <th width="250">
                                	<?php echo $this->lang->line('name_ukm'); ?>
                                </th>
								<th>
                                	<?php echo $this->lang->line('address'); ?>
                                </th>
								<th>
                                	<?php echo $this->lang->line('email'); ?>
                                </th>
								<th>
                                	<?php echo $this->lang->line('phone'); ?>
                                </th>
								<th width="150" class="text-center">
                                	<?php echo $this->lang->line('konfirmasi'); ?>
                                </th>
                                <th colspan="2" class="text-center" width="30">
                                	<?php echo $this->lang->line('option'); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php $i = $start_no; foreach ($lists as $list) { $i++; ?>
	                            <tr>
	                                <td class="text-center">
	                                	<input type="checkbox" name="datacek[]" class="check-all" value="<?php echo $list['id']; ?>" />
	                                </td>
	                                <td class="center_th_td">
	                                	<?php echo $i; ?>
	                                </td>
	                                <td>
	                                	<?php echo $list['name']; ?>
	                                </td>
	                                <td>
	                                	<?php echo $list['address']; ?>
	                                </td>
	                                <td>
	                                	<?php echo $list['email']; ?>
	                                </td>
	                                <td>
	                                	<?php echo $list['phone']; ?>
	                                </td>
	                                <td width='40' class="text-center">
	                                	<?php 
											switch($list['status']){
												case 0:
												?>
													<a href="javascript:void(0)" onclick="publishThis(<?php echo $list['id']; ?>)"  title="<?php echo $this->lang->line('navigation_publish'); ?>">
													<button class="btn btn-primary btn-xs" type="button">
														<i class="fa fa-arrow-up"></i> <?php echo $this->lang->line('navigation_unconfirm'); ?>
													</button>
													</a>
					                            <?php
													break;
												case 1:
												?>
													<a href="javascript:void(0)" onclick="unpublishThis(<?php echo $list['id']; ?>)" title="<?php echo $this->lang->line('navigation_unpublish'); ?>">
													<button class="btn btn-warning btn-xs" type="button">
														<i class="fa fa-arrow-down"></i> <?php echo $this->lang->line('navigation_confirm'); ?>
													</button>
													</a>
					                            <?php
													break;
											}
										?>
	                                </td>
	                                <td width='10' class="text-center">
	                                	<a href="<?php echo base_url('/ukm')."/modif/".$list['id']; ?>">
	                                	<button class="btn btn-danger btn-xs" type="button">
			                    			<i class="ti-pencil"></i>
			                    		</button>
							        	</a>  
	                                </td>
	                                <td width='10' class="text-center">
	                                	<button class="btn btn-danger btn-xs" type="button" onclick="deleteThis(<?php echo $list['id']; ?>)">
			                    			<i class="ti-trash"></i>
			                    		</button> 
	                                </td>
	                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <div class="row footertable">
                    	<div class="col-xs-1">
                    		<button class="btn btn-color btn-xs" type="button" onclick="submitkie()">
                    			<i class="ti-trash"></i>
                				<?php echo $this->lang->line('delete'); ?>
                    		</button>
                    	</div>
                    	<div class="col-xs-11 text-right">
                    		<ul class="pagination pagination-sm"><?php echo isset($pagination)?$pagination:""; ?></ul>
                    	</div>
                    </div>
                <?php } else { ?>
                	<p><center><?php echo $this->lang->line('no_data'); ?></center></p>
                <?php } ?>
            </div>
        </div>
    </div>
</form>