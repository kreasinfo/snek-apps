<?php
class Webservices extends CI_Controller{
	var $webconfig;
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->sharedModel('LoginModel');

		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login');
		}
	}
	function index(){
		
	}
	function kategori_pengiriman(){
		$this->load->sharedModel('Kategori_pengirimanModel');
		$get_data = array();
		parse_str($_SERVER['QUERY_STRING'], $get_data);
		$get_data = $this->security->xss_clean($get_data);

		$arr = $this->Kategori_pengirimanModel->filterSelect2($get_data['q']);
		header('Content-Type: application/json');
    	echo json_encode( $arr );
	}
        
        function lokasi_ukm(){
		$this->load->sharedModel('Lokasi_ukmModel');
		$get_data = array();
		parse_str($_SERVER['QUERY_STRING'], $get_data);
		$get_data = $this->security->xss_clean($get_data);

		$arr = $this->Lokasi_ukmModel->filterSelect2($get_data['q']);
		header('Content-Type: application/json');
    	echo json_encode( $arr );
	}
        
	function kategori_produk(){
		$this->load->sharedModel('Kategori_produkModel');
		$get_data = array();
		parse_str($_SERVER['QUERY_STRING'], $get_data);
		$get_data = $this->security->xss_clean($get_data);

		$arr = $this->Kategori_produkModel->filterSelect2($get_data['q']);
		header('Content-Type: application/json');
    	echo json_encode( $arr );
	}
}