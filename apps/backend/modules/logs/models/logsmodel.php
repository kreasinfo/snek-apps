<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class LogsModel extends CI_Model{
	function __construct() {	 
		parent::__construct();
		$ci = & get_instance();
		$ci->load->library('session');	
		$this->maintablename = "logs";
		$this->secondtablename = "users";
	}
	public function deleteData($id){
		if($id == 0) return 'failed';
		$doDelete = $this->db->query("
			DELETE FROM ".$this->maintablename."
			WHERE 
				id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_logs')." id = ".$id.""));		
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function filterData($params=array()){
		$ip = isset($params["ip"])?$params["ip"]:'';
		$module = isset($params["module"])?$params["module"]:'';
		$details = isset($params["details"])?$params["details"]:'';
		$day = isset($params["day"])?$params["day"]:'';
		$month = isset($params["month"])?$params["month"]:'';
		$year = isset($params["year"])?$params["year"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		
		$exclude = "";
		$offsetData  = "";
		$datecondition_0  = "";
		$datecondition_1  = "";
		$datecondition_2  = "";
		$conditional = " AND ip_address LIKE '%".$ip."%' AND module LIKE '%".$this->db->escape_str($module)."%' AND details LIKE '%".$this->db->escape_str($details)."%'";

		if($this->session->userdata('role') != 0) {
			if($this->__getProgrammers() != '' ){
				$exclude = "AND username NOT IN (".$this->__getProgrammers().")";
			}
		}
		
		if($day != 0){
			$datecondition_0 = "AND DAY(date_created) = '".$day."'";
		}
		if($month != 0){
			$datecondition_1 = "AND MONTH(date_created) = '".$month."'";
		}
		if($year != 0){
			$datecondition_2 = "AND YEAR(date_created) = '".$year."'";
		}
		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}
	
		$q = $this->db->query("
			SELECT
				id
				,date_created
				,ip_address
				,username
				,module
				,details
			FROM
				".$this->maintablename."
			WHERE 1=1
			".$exclude."
			".$conditional."
			".$datecondition_0."
			".$datecondition_1."
			".$datecondition_2."
			ORDER BY date_created DESC
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function filterDataCount($params=array()){
		$ip = isset($params["ip"])?$params["ip"]:'';
		$module = isset($params["module"])?$params["module"]:'';
		$details = isset($params["details"])?$params["details"]:'';
		$day = isset($params["day"])?$params["day"]:'';
		$month = isset($params["month"])?$params["month"]:'';
		$year = isset($params["year"])?$params["year"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		
		$exclude = "";
		$offsetData  = "";
		$datecondition_0  = "";
		$datecondition_1  = "";
		$datecondition_2  = "";
		$conditional = " AND ip_address LIKE '%".$ip."%' AND module LIKE '%".$this->db->escape_str($module)."%' AND details LIKE '%".$this->db->escape_str($details)."%'";

		if($this->session->userdata('role') != 0) {
			if($this->__getProgrammers() != '' ){
				$exclude = "AND username NOT IN (".$this->__getProgrammers().")";
			}
		}
		
		if($day != 0){
			$datecondition_0 = "AND DAY(date_created) = '".$day."'";
		}
		if($month != 0){
			$datecondition_1 = "AND MONTH(date_created) = '".$month."'";
		}
		if($year != 0){
			$datecondition_2 = "AND YEAR(date_created) = '".$year."'";
		}
			
		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE 1=1
			".$exclude."
			".$conditional."
			".$datecondition_0."
			".$datecondition_1."
			".$datecondition_2."
			ORDER BY date_created DESC
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$exclude = "";
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY date_created DESC";
		if($this->session->userdata('role') != 0) {
			if($this->__getProgrammers() != '' ){
				$exclude = "WHERE username NOT IN (".$this->__getProgrammers().")";
			}
		}

		if($id != '') {
			$conditional = "AND id = '".$id."'";
		}
		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}
		
		$q = $this->db->query("
			SELECT
				id
				,date_created
				,ip_address
				,username
				,module
				,details
			FROM
				".$this->maintablename."
			".$exclude."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}
	
	public function listDataCount($params=array()){
		$exclude = "";
		if($this->session->userdata('role') != 0) {
			if($this->__getProgrammers() != '' ){
				$exclude = "WHERE username NOT IN (".$this->__getProgrammers().")";
			}
		}
		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
				".$exclude."
			
		");
		$result = $q->first_row('array');
		return $result;
	}

	function __getProgrammers() {
	  $this->db->where("role", "0");
	  $query = $this->db->get($this->secondtablename);
	  $result = $query->result_array();
		if(count($result) > 0) {
			$data = "";
			foreach($result as $key=>$row) {
				if($key != 0) {
					$data .= ",";
				}
				$data .= "'".$row["username"]."'";
			}
		}else{
			$data = '';
		}
	  return $data;
	 }
}