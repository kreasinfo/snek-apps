<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#listData").load('<?php echo base_url('/logs'); ?>/getAllData/');
	});
	function searchThis(){
		jQuery('#loading').show();
		var frm = document.searchform;
		var ip = frm.ip.value;
		var module = frm.module.value;
		var details = frm.details.value;
		var day = frm.day.value;
		var month = frm.month.value;
		var year = frm.year.value;

        if(ip      == ''){ ip      = '-'; }
        if(module  == ''){ module  = '-'; }
        if(details == ''){ details = '-'; }
        
		jQuery("#listData").load('<?php echo base_url('/logs'); ?>/searchdata/'+ip.replace(/\s/g,'%20')+'/'+module.replace(/\s/g,'%20')+'/'+details.replace(/\s/g,'%20')+'/'+day.replace(/\s/g,'%20')+'/'+month.replace(/\s/g,'%20')+'/'+year.replace(/\s/g,'%20'));
		jQuery('#loading').hide();
	}
</script>
<section class="main-content">
 
    <div class="content-wrap">
     
        <div class="wrapper">
            <ol class="breadcrumb">
	            <li>
	            	<a href="<?php echo base_url('/'); ?>"><i class="ti-home mr5"></i><?php echo $this->lang->line('dashboard'); ?></a>
	            </li>
	            <li class="active"><?php echo ucfirst($this->module_name); ?></li>
            </ol>

            <div class="panel">
            	<header class="panel-heading">
        			<div class="row">
						<div class="col-xs-12">
							<h5 class="text-uppercase no-m"><?php echo strtoupper($this->module_name); ?></h5>
		            		<small><?php echo $this->lang->line('logs_teks'); ?></small>
						</div>
					</div>
        		</header>
            	<div class="panel-body">
		        	<div class="row widget bg-primary">
		        		<div class="col-xs-12 widget-body">
							<form id="searchform" name="searchform" method="POST" action="<?php echo base_url().$this->router->class; ?>/searchdata/" onsubmit="searchThis(); return false;">
								<div class="col-xs-1">
									<select name="day" class="form-control">                     
										<option value="0" <?php if(isset($day) && $day == '0'){ echo "selected=selected";}?>><?php echo $this->lang->line('date'); ?></option>
										<?php for($i = 1; $i <= 31; $i++){ ?>
											<option value="<?php echo $i; ?>" <?php if(isset($day) && $day == $i){ echo "selected=selected";}?>><?php echo $i; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="col-xs-2">
									<select name="month" class="form-control">
										<option value="0" <?php if(isset($month) && $month == '0'){ echo "selected=selected";}?>><?php echo $this->lang->line('month'); ?></option>
										<option value="01" <?php if(isset($month) && $month == '01'){ echo "selected=selected";}?>>Januari</option>
										<option value="02" <?php if(isset($month) && $month == '02'){ echo "selected=selected";}?>>Februari</option>
										<option value="03" <?php if(isset($month) && $month == '03'){ echo "selected=selected";}?>>Maret</option>
										<option value="04" <?php if(isset($month) && $month == '04'){ echo "selected=selected";}?>>April</option>
										<option value="05" <?php if(isset($month) && $month == '05'){ echo "selected=selected";}?>>Mei</option>
										<option value="06" <?php if(isset($month) && $month == '06'){ echo "selected=selected";}?>>Juni</option>
										<option value="07" <?php if(isset($month) && $month == '07'){ echo "selected=selected";}?>>Juli</option>
										<option value="08" <?php if(isset($month) && $month == '08'){ echo "selected=selected";}?>>Agustus</option>
										<option value="09" <?php if(isset($month) && $month == '09'){ echo "selected=selected";}?>>September</option>
										<option value="10" <?php if(isset($month) && $month == '10'){ echo "selected=selected";}?>>Oktober</option>
										<option value="11" <?php if(isset($month) && $month == '11'){ echo "selected=selected";}?>>November</option>
										<option value="12" <?php if(isset($month) && $month == '12'){ echo "selected=selected";}?>>Desember</option>
									</select>
								</div>
								<div class="col-xs-2">
									<select name="year" class="form-control">                        
										<option value="0" <?php if(isset($year) && $year == '0'){ echo "selected=selected";}?>><?php echo $this->lang->line('year'); ?></option>
										<?php for ($i = date("Y"); $i >= 2009; $i--) { ?>
										<option value="<?php echo $i; ?>" <?php if(isset($year) && $year == $i){ echo "selected=selected";}?>><?php echo $i; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="col-xs-2">
									<input type="text" name="ip" placeholder="IP address" class="form-control" />
								</div>
								<div class="col-xs-2">
									<input type="text" name="module" placeholder="Modules" class="form-control" />
								</div>
								<div class="col-xs-2">
									<input type="text" name="details" placeholder="Details" class="form-control" />
								</div>
								<div class="col-xs-1">
									<button class="btn btn-color" type="button" onclick="searchThis()"><i class="ti-search"></i> <?php echo $this->lang->line('search'); ?></button>
								</div>
							</form>
						</div>
					</div>
		    		<div id="listData"><center><img src='<?php echo $this->webconfig['back_base_template']; ?>img/loading.gif' align='middle' style="margin:5px;" /></center></div>
    			</div>
    		</div>
        </div>
     
    </div>
 
	<a class="exit-offscreen"></a>
</section>