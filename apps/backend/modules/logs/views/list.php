<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('#checkall').click(function () {
		jQuery('#tabellistdata .check-all').attr('checked', this.checked);
	});
	jQuery(".pagination a").click(function(e){
		// stop normal link click
		e.preventDefault(); 
		var linkUrl = jQuery(this).attr("href");
		if(linkUrl){
			jQuery('#listData').load(linkUrl.replace(/\s/g,'%20'));
		}
	  });		
					
});
function submitkie(){
	jQuery().ajaxStart(function() {
		jQuery('#loading').show();
		jQuery('#result').hide();
	}).ajaxStop(function() {
		jQuery('#loading').hide();
		jQuery('#result').fadeIn('slow');	
		jQuery("#tabellistdata")[0].reset();
	});
	
	jQuery.ajax({
			type: 'POST',
			url: jQuery('#tabellistdata').attr('action'),
			data: jQuery('#tabellistdata').serialize(),
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				
				jQuery("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
		return false;
}
function deleteThis(code){
	var txt = "<?php echo $this->lang->line('alert_delete'); ?> <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
	jQuery.prompt(txt ,{  callback: doCondition, buttons: { <?php echo $this->lang->line('ok'); ?>: true, <?php echo $this->lang->line('cancel'); ?>: false },prefix:'jqismooth' });
}
function doCondition(v,m,f){
	  if(v){
	  	//alert(f.alertName);
		var posting = "dataid="+f.alertName;
		jQuery.ajax({
			type: 'post',
			url: "<?php echo base_url('/logs'); ?>/deleteThis",
			data: posting,
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				jQuery("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	  }else{
	  	jQueryjQuery.prompt.close();
	  }
}

</script>
<form id="tabellistdata" method="post" action="<?php echo base_url().$this->router->class; ?>/postProcess" onsubmit="return false;">
	<div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
            	<?php if(isset($lists) && count($lists) > 0) {?>
                    <table class="table table-striped no-m">
                        <thead>
                            <tr>
                                <th width="10" class="center_th_td">
                                	<input type="checkbox" id="checkall" />
                                </th>
                                <th width="10" class="center_th_td">
                                	<?php echo $this->lang->line('no'); ?>
                                </th>
                                <th width="200">
                                	<?php echo $this->lang->line('date'); ?>
                                </th>
                                <th width="200">
                                	<?php echo $this->lang->line('ip'); ?>
                                </th>
                                <th width="200">
                                	<?php echo $this->lang->line('username'); ?>
                                </th>
                                <th width="200">
                                	<?php echo $this->lang->line('module'); ?>
                                </th>
                                <th width="200">
                                	<?php echo $this->lang->line('detail'); ?>
                                </th>
                                <th class="text-center" width="30">
                                	<?php echo $this->lang->line('option'); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php $i = $start_no; foreach ($lists as $list) { $i++; ?>
	                            <tr>
	                                <td class="center_th_td">
	                                	<input type="checkbox" name="datacek[]" class="check-all" value="<?php echo $list['id']; ?>" />
	                                </td>
	                                <td class="center_th_td">
	                                	<?php echo $i; ?>
	                                </td>
	                                <td>
	                                	<?php echo date_lang_reformat_long($list['date_created']); ?>
	                                </td>
	                                <td>
	                                	<?php echo clean_str($list['ip_address']); ?>
	                                </td>
	                                <td>
	                                	<?php echo clean_str($list['username']); ?>
	                                </td>
	                                <td>
	                                	<?php echo clean_str($list['module']); ?>
	                                </td>
	                                <td>
	                                	<?php echo clean_str($list['details']); ?>
	                                </td>
	                                <td width='80' class="text-center">
	                                	<button class="btn btn-danger btn-xs" type="button" onclick="deleteThis(<?php echo $list['id']; ?>)">
			                    			<i class="ti-trash"></i>
			                    		</button>
	                                </td>
	                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <div class="row footertable">
                    	<div class="col-xs-1">
                    		<button class="btn btn-color btn-xs" type="button" onclick="submitkie()">
                    			<i class="ti-trash"></i>
                				<?php echo $this->lang->line('delete'); ?>
                    		</button>
                    	</div>
                    	<div class="col-xs-11 text-right">
                    		<ul class="pagination pagination-sm"><?php echo isset($pagination)?$pagination:""; ?></ul>
                    	</div>
                    </div>
                <?php } else { ?>
                	<p><center><?php echo $this->lang->line('no_data'); ?></center></p>
                <?php } ?>
            </div>
        </div>
    </div>
</form>