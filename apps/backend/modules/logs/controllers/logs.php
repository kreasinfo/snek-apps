<?php
class Logs extends CI_Controller{
	var $webconfig;
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('LogsModel');
		$this->load->library('pagination');

		$this->load->sharedModel('LoginModel');
		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login');
		}
		if($this->session->userdata('role') != 0 && $this->session->userdata('role') != 1){
			redirect(base_url().'dashboard');
		}
		$this->webconfig = $this->config->item('webconfig');
		$this->module_name = $this->lang->line('logs');

	}
	function index(){
		redirect(base_url().$this->router->class.'/listing');
	}
	function listing($page = 0){
		$tdata = array();
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		$this->load->sharedView('template', $ldata);
	}
	function getAllData($page = 0){
		# PAGINATION #
		$count_all_data = $this->LogsModel->listDataCount();
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($count_all_data['jumlah'])?$count_all_data['jumlah']:0;
		$cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 3;

		$cfg_pg['first_link'] = '<span>&laquo;</span>';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '<span>&raquo;</span>';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '<span>&gt;</span>';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '<span>&lt;</span>';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li><b>';
		$cfg_pg['cur_tag_close'] = '<b></li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><span>';
		$cfg_pg['cur_tag_close'] = '</span></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->LogsModel->listData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
															));
		$this->load->view($this->router->class.'/list', $listdata);
	}
	function searchdata($ip = 'IP', $module = 'Modul', $details = 'Keterangan', $day = 0, $month = 0, $year = 0){
		if($ip == '-'){$ip = '';}
		if($module == '-'){$module = '';}
		if($details == '-'){$details = '';}

		$tdata['day'] = $day;
		$tdata['month'] = $month;
		$tdata['year'] = $year;

		$webconfig = $this->config->item('webconfig');
		$listdata['base_template'] = $ldata['base_template'] = $tdata['base_template'] = $webconfig['base_template'];
		$listdata['media_embed'] = $ldata['media_embed'] = $tdata['media_embed'] = $webconfig['media-server-embeded'];
		$listdata['base_site'] = $ldata['base_site'] = $tdata['base_site'] = $fdata['base_site'] =  $webconfig['base_site'];
		# PAGINATION #
			$count_all_data = $this->LogsModel->filterDataCount(
														array(
															'ip' => urldecode($ip)
															,'module' => urldecode($module)
															,'details' => urldecode($details)
															,'day' => urldecode($day)
															,'month' => urldecode($month)
															,'year' => urldecode($year)
														)
													);
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/".$this->uri->segment(3)."/".$this->uri->segment(4)."/".$this->uri->segment(5)."/".$this->uri->segment(6)."/".$this->uri->segment(7)."/".$this->uri->segment(8)."/";
        $cfg_pg ['total_rows'] = isset($count_all_data['jumlah'])?$count_all_data['jumlah']:0;
		$cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 9;

		$cfg_pg['first_link'] = '<span>&laquo;</span>';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '<span>&raquo;</span>';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '<span>&gt;</span>';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '<span>&lt;</span>';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li><b>';
		$cfg_pg['cur_tag_close'] = '<b></li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><span>';
		$cfg_pg['cur_tag_close'] = '</span></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->LogsModel->filterData(array(
																'ip' => urldecode($ip)
																,'module' => urldecode($module)
																,'details' => urldecode($details)
																,'day' => urldecode($day)
																,'month' => urldecode($month)
																,'year' => urldecode($year)
																,'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
															));
		$this->load->view($this->router->class.'/list', $listdata);
	}
	public function deleteThis(){
		$doDelete = $this->__delete($this->input->post('dataid'));

		if($doDelete == 'failed'){
			echo "nodata";
		}else if($doDelete == 'success'){
			echo "success";
		}
	}
	public function postProcess(){
		$postvalue = $this->input->post('datacek');
		if($postvalue != ''){
			foreach($postvalue as $data){
				$this->__delete($data);
			}
			echo "success";
		}else{
			echo "nodata";
		}
	}
	private function __delete($id){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}
		$doDelete = $this->LogsModel->deleteData($id);
    	return $doDelete;
	}
}