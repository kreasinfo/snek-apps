<?php

class Login extends CI_Controller {
	var $webconfig;
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');	
		$this->load->library('session');
		$this->load->sharedModel('LoginModel');	
		$this->webconfig = $this->config->item('webconfig');
		//$this->output->cache(1);		
	}
	
	function index()
	{			
		if($this->LoginModel->isLoggedIn()){
			redirect(base_url().'dashboard');
		}
		## LOAD TEMPLATE ##
		$tdata = array();
		
		## LOAD LAYOUT ##	
		$webconfig = $this->config->item('webconfig');
		
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
			
		$this->load->sharedView('login', $ldata);
	}
	function postProcess(){
		## LOAD LAYOUT ##	
		$webconfig = $this->config->item('webconfig');
		$listdata['base_template'] = $ldata['base_template'] = $tdata['base_template'] = $webconfig['base_template'];	
		$ldata['content'] = $this->load->view('login/index',$tdata, true);
		
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$check = $this->LoginModel->doLogin($username, $password);
		switch ($check){
			case 'failed_input':
					$tdata['error_message'] = $this->lang->line('error_login_failed_input');
				break;
			case 'failed_login':
					$tdata['error_message'] = $this->lang->line('error_login_failed_login');
				break;
			case 'success_login':
					$tdata['success'] = TRUE;
				break;
		}
		$this->load->view($this->router->class."/result", $tdata);
	}
	function logout(){
		$this->LoginModel->doLogout();
		redirect(base_url());
	}
}