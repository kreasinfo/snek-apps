<?php if(isset($error_message)){ ?>
    <div class="alert alert-danger">
        <strong><?php echo $this->lang->line('error_notif'); ?></strong>
        <br><?php echo $error_message; ?>
    </div>
<?php } ?>
<?php if (isset($success)){echo "redirect";} ?>