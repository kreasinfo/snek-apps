<div class="cover" style="background-image: url(<?php echo $this->webconfig['back_base_template']; ?>img/cover3.jpg)"></div>
<div class="overlay bg-info" style="background-color: rgba(255, 255, 255, 0.15);"></div>
<div class="center-wrapper">
    <div class="center-content"> 
    	<div class="text-center text-white"> 
    		<!-- <div class="locked-avatar"> <img class="img-circle" src="<?php echo $this->webconfig['back_base_template']; ?>img/avatar.jpg" alt="user"> 
    		</div> --> 
    		<span class="center-block mt25"><h3>Login Form</h3></span> <!-- <small>Frontend Web Developer</small>  -->
    	</div>

    	<div class="center-block lockcode mg-t">
    	<div class="message"></div> 
    		<form role="form" name="loginform" id="loginform" method="post" action="<?php echo base_url(); ?>login/postProcess" onsubmit="submitkie(); return false;"> 
    			<div class="mb15 mt25"> 
    				<input type="text" class="form-control input-lg lock-input" id="password" placeholder="Username" name="username">
    			</div>  
    			<div class="mb15 "> 
    				<input type="password" class="form-control input-lg lock-input" id="password" placeholder="Password" name="password"> 
    			</div>
    			<button class="btn btn-danger btn-lg btn-block" type="submit">Login</button>
    		</form> 
    	</div> 
    	<div class="text-center text-white small mt25"> 
    		<!-- <a href="signin.html"><i class="ti-power-off mr5"></i>Signin with different account.</a>  -->
    		Copyright &copy; <?php echo date('Y'); ?> <?php echo $this->lang->line('app_logo'); ?>
    	</div> 
   	</div>
</div>

<script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/jquery-1.11.1.min.js"></script>
<script type="text/javascript">

function submitkie(){
	$().ajaxStart(function() {
		$('#loading').show();
		$('#result').hide();
	}).ajaxStop(function() {
		$('#loading').hide();
		$('#result').fadeIn('slow');	
		$("#loginform")[0].reset();
	});
	
	$.ajax({
			type: 'POST',
			url: $('#loginform').attr('action'),
			data: $('#loginform').serialize(),
			success: function(data) {
				// $('.message').show();
				if(data == 'redirect'){
					top.location.href = '<?php echo base_url(); ?>dashboard';
				}else{
					$('.message').html(data);
					$('.message').slideDown(600,function () {$(this).fadeTo(350, 100)} );
				}		
			}
		})
		
		return false;
}
</script>
