<?php
class Members extends CI_Controller{
	var $webconfig;
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->helper('image');
		$this->load->library('image_moo');
		$this->load->sharedModel('MembersModel');
		$this->load->sharedModel('LoginModel');
		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login');
		}
		
		$this->webconfig = $this->config->item('webconfig');
		$this->module_name = $this->lang->line('members');
	}
	function index(){
		redirect(base_url().$this->router->class.'/listing');
	}
	function listing($page = 0){
		$tdata = array();
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		$this->load->sharedView('template', $ldata);
	}
	function getAllData($page = 0){
		
		$all_data = $this->MembersModel->listDataCount();
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 3;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><span>';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->MembersModel->listData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
															));
		
		$this->load->view($this->router->class.'/list', $listdata);
	}
	function searchdata($name = 'name'){
		if($name == '-'){$name = '';}

		# PAGINATION #
		$all_data = $this->MembersModel->filterDataCount(array('fullname'=>trim(urldecode($name))));
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/".$this->uri->segment(3)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 4;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><span>';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->MembersModel->filterData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
																,'fullname' => trim(urldecode($name))
															));
		$this->load->view($this->router->class.'/list', $listdata);
	}
	function add(){
		$tdata = array();
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
		if($_POST)
		{
			$validator = new FormValidator();
		    $validator->addValidation("fullname","req",$this->lang->line('error_empty_name_members'));
		    $validator->addValidation("birthday","req",$this->lang->line('error_empty_birthday_members'));
		    $validator->addValidation("address","req",$this->lang->line('error_empty_address_members'));
		    $validator->addValidation("postcode","req",$this->lang->line('error_empty_postcode_members'));
		    $validator->addValidation("gender","req",$this->lang->line('error_empty_gender_members'));
		    $validator->addValidation("phone","req",$this->lang->line('error_empty_phone_members'));
		    $validator->addValidation("email","req",$this->lang->line('error_empty_email_members'));
		    $validator->addValidation("email","email",$this->lang->line('error_wrong_email_members'));
			$validator->addValidation("password","req",$this->lang->line('error_empty_password_members'));
			$validator->addValidation("password","minlen=5",$this->lang->line('error_password_5_members'));
			$validator->addValidation("status","req",$this->lang->line('error_empty_status_members'));
			
		    if($validator->ValidateForm())
		    {
		        $doInsert = $this->MembersModel->entriData(array(
														        'fullname'=>$this->input->post('fullname')
														        ,'birthday'=>$this->input->post('birthday')
														        ,'address'=>$this->input->post('address')
														        ,'postcode'=>$this->input->post('postcode')
														        ,'gender'=>$this->input->post('gender')
														        ,'phone'=>$this->input->post('phone')
														        ,'email'=>$this->input->post('email')
														        ,'password'=>$this->input->post('password')
														        ,'status'=>$this->input->post('status')
														        ,'a1' => $this->input->post('a1')
																,'b1' => $this->input->post('b1')
																,'a2' => $this->input->post('a2')
																,'b2' => $this->input->post('b2')
																,'width_resize' => $this->webconfig["width_resize"]
														  ));
				if($doInsert == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doInsert == 'exist_email'){
					$tdata['error_hash'] = array('error' => $this->lang->line('error_duplicate_email'));
				}else if($doInsert == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doInsert == 'failed_ext'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_extension'));
				}else if($doInsert == 'failed_dimension'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_dimension'));
				}else if($doInsert == 'empty_file'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_file'));
				}else if($doInsert == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_entry');
				}
					if($doInsert !='success'){
						$tdata['fullname']     = $this->input->post('fullname');
						$tdata['birthday'] = $this->input->post('birthday');
						$tdata['address'] = $this->input->post('address');
						$tdata['postcode'] = $this->input->post('postcode');
						$tdata['gender'] = $this->input->post('gender');
						$tdata['phone'] = $this->input->post('phone');
						$tdata['email'] = $this->input->post('email');
						$tdata['password'] = $this->input->post('password');
						$tdata['status'] = $this->input->post('status');
					}
				$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {
		        $tdata['fullname']     = $this->input->post('fullname');
			    $tdata['birthday'] = $this->input->post('birthday');
			    $tdata['address'] = $this->input->post('address');
			    $tdata['postcode'] = $this->input->post('postcode');
			    $tdata['gender'] = $this->input->post('gender');
			    $tdata['phone'] = $this->input->post('phone');
			    $tdata['email'] = $this->input->post('email');
			    $tdata['password'] = $this->input->post('password');
			    $tdata['status'] = $this->input->post('status');
		    	$tdata['error_hash'] = $validator->GetErrors();

				$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{
			$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
			$this->load->sharedView('template', $ldata);
		}
	}
	function modif($id = 0){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}

		$tdata['lists'] = $this->MembersModel->listData(array('id'=>$id));
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
		if($_POST)
		{
			$validator = new FormValidator();

		    $validator->addValidation("fullname","req",$this->lang->line('error_empty_name_members'));
		    $validator->addValidation("birthday","req",$this->lang->line('error_empty_birthday_members'));
		    $validator->addValidation("address","req",$this->lang->line('error_empty_address_members'));
		    $validator->addValidation("postcode","req",$this->lang->line('error_empty_postcode_members'));
		    $validator->addValidation("gender","req",$this->lang->line('error_empty_gender_members'));
		    $validator->addValidation("phone","req",$this->lang->line('error_empty_phone_members'));
		    $validator->addValidation("email","req",$this->lang->line('error_empty_email_members'));
		    $validator->addValidation("status","req",$this->lang->line('error_empty_status_members'));
			$validator->addValidation("email","email",$this->lang->line('error_wrong_email_members'));
			
		    if($validator->ValidateForm())
		    {
		        $doUpdate = $this->MembersModel->updateData(array(
														        'id' => $id
		        												,'fullname'=>$this->input->post('fullname')
														        ,'birthday'=>$this->input->post('birthday')
														        ,'address'=>$this->input->post('address')
														        ,'postcode'=>$this->input->post('postcode')
														        ,'gender'=>$this->input->post('gender')
														        ,'phone'=>$this->input->post('phone')
														        ,'email'=>$this->input->post('email')
														        ,'password'=>$this->input->post('password')
														        ,'status'=>$this->input->post('status')
														        ,'a1' => $this->input->post('a1')
																,'b1' => $this->input->post('b1')
																,'a2' => $this->input->post('a2')
																,'b2' => $this->input->post('b2')
																,'width_resize' => $this->webconfig["width_resize"]
														  ));

				if($doUpdate == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doUpdate == 'exist_email'){
					$tdata['error_hash'] = array('error' => $this->lang->line('error_duplicate_email'));
				}else if($doUpdate == 'min_5'){
					$tdata['error_hash'] = array('error' => $this->lang->line('error_empty_password_5_users'));
				}else if($doUpdate == 'failed_ext'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_extension'));
				}else if($doUpdate == 'failed_dimension'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_dimension'));
				}else if($doUpdate == 'empty_file'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_file'));
				}else if($doUpdate == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doUpdate == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_update');
					$tdata['lists'] = $this->MembersModel->listData(array('id'=>$id));
				}

					if($doUpdate !='success'){
						$tdata['fullname']     = $this->input->post('fullname');
						$tdata['birthday'] = $this->input->post('birthday');
						$tdata['address'] = $this->input->post('address');
						$tdata['postcode'] = $this->input->post('postcode');
						$tdata['gender'] = $this->input->post('gender');
						$tdata['phone'] = $this->input->post('phone');
						$tdata['email'] = $this->input->post('email');
						$tdata['status'] = $this->input->post('status');
					}

				$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {
		        $tdata['fullname']     = $this->input->post('fullname');
			    $tdata['birthday'] = $this->input->post('birthday');
			    $tdata['address'] = $this->input->post('address');
			    $tdata['postcode'] = $this->input->post('postcode');
			    $tdata['gender'] = $this->input->post('gender');
			    $tdata['phone'] = $this->input->post('phone');
			    $tdata['email'] = $this->input->post('email');
			    $tdata['status'] = $this->input->post('status');
		    	$tdata['error_hash'] = $validator->GetErrors();

				$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{
			$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
			$this->load->sharedView('template', $ldata);
		}
	}
	public function deleteThis(){
		$doDelete = $this->__delete($this->input->post('dataid'));

		if($doDelete == 'failed'){
			echo "nodata";
		}else if($doDelete == 'success'){
			echo "success";
		}
	}
	public function postProcess(){
		$postvalue = $this->input->post('datacek');
		if($postvalue != ''){
			foreach($postvalue as $data){
				$this->__delete($data);
			}
			echo "success";
		}else{
			echo "nodata";
		}
	}
	private function __delete($id){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}
		$doDelete = $this->MembersModel->deleteData($id);
    	return $doDelete;
	}


}