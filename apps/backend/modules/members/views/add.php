<script type="text/javascript">
    jQuery(document).ready(function($) {
		jQuery(".datetimepicker").datepicker({
				autoclose: true,
		});
		jQuery(".postcode").numeric();

        var oFReader = null;
        var image = null;
        var globalResizedWidth = '<?php if($this->webconfig["width_resize"] != ''){ echo $this->webconfig["width_resize"]; }else{ echo 0; } ?>';
        var jcrop_api, globalWidth, globalHeight, globalConfDimension;

        $("#uploadImageProfile").change(function(){
            $(this).parent().find(".image_block").remove();
            $.when( createImageElementProfile(this) ).done( cropImageElementProfile(this) );
        });

        function createImageElementProfile(obj) {
            var html = '<div class="image_block">';
            html += '<input type="hidden" class="a1" name="a1" value="0" />';
            html += '<input type="hidden" class="b1" name="b1" value="0" />';
            html += '<input type="hidden" class="a2" name="a2" value="0" />';
            html += '<input type="hidden" class="b2" name="b2" value="0" />';
            html += '<br><img class="image_preview" style="width:'+globalResizedWidth+'px" />';
            html += '</div>';
            $(obj).after(html);
        }

        function cropImageElementProfile(obj) {
            var ext = getExtension($(obj).val());
            if(ext == "jpg" || ext == "jpeg" || ext == "png" || ext == "gif"){
                var dimensionconf = '<?php if(count($this->webconfig["image_profile"]) > 0){ echo $this->webconfig["image_profile"][0]; } ?>';
                var separator = dimensionconf.split("x");
                var dimheight = separator[1];
                var dimwidth = separator[0];
                if(dimwidth != 'undefined' || dimheight != 'undefined'){
                    doLoadCroppingProfile(obj, dimwidth, dimheight);
                }
                return false;
            }else{
                alert('<?php echo $this->lang->line("msg_error_extension"); ?>');
                $('.image_block').remove();
                $("#uploadImage").val("");
                return false;
            }
        }

        function doLoadCroppingProfile(obj, dimwidth, dimheight) {
            if(oFReader !=null){
                oFReader = null;
            }
            
            var min_width = dimwidth;
            var min_height = dimheight;
            var objFile = obj.files[0];
            var max_foto_mb = '<?php echo $this->webconfig['max_foto_filesize']; ?>';
            var max_foto_byte = parseInt(max_foto_mb)*1048576; //convert MB to Byte
            
            if(objFile.size > max_foto_byte) {
                $(obj).parent().find(".image_block").remove();
                $(obj).val("");
                alert("<?php echo $this->lang->line('label_error_ukuran'); ?>");
                $('.image_block').remove();
                $("#uploadImage").val("");
            } else {
                // prepare HTML5 FileReader
                oFReader = new FileReader();
                image  = new Image();
                oFReader.readAsDataURL(objFile);
                
                oFReader.onload = function (_file) {
                    image.src    = _file.target.result;
                    image.onload = function() {
                            globalWidth = this.width;
                            globalHeight = this.height;
                            
                            $(obj).parent().find(".image_preview").attr("src", this.src);

                            if(globalWidth < min_width || globalHeight < min_height) {
                                    $(obj).parent().find(".image_block").remove();
                                    $(obj).val("");
                                    alert("<?php echo $this->lang->line('label_error_dimensi'); ?>");
                            } else {
                                cropImageProfile(globalWidth, globalHeight, min_width, min_height, $(obj).parent().find(".image_preview"));
                            }
                    };

                    image.onerror= function() {
                        alert('Invalid file type: '+ objFile.type);
                    };     
                    
                }
            }
        }

        function cropImageProfile(width, height, minwidth, minheight, obj) {
            var resizedWidth = globalResizedWidth;
            var resizedHeight = (resizedWidth * height) / width;
            var resizedMinWidth = (minwidth * resizedWidth) / width;
            var resizedMinHeight = (minheight * resizedHeight) / height;
            
            if(minwidth != '' || minheight != ''){
                $(obj).Jcrop({
                    setSelect: [ 0, 0, resizedMinWidth, resizedMinHeight ],
                    minSize: [ resizedMinWidth, resizedMinHeight ],
                    onSelect: updateCoordsProfle,
                    allowSelect: false,
                    bgFade: true,
                    bgOpacity: 0.4,
                    aspectRatio: minwidth / minheight
                },function(){
                    jcrop_api = this;
                });
            }
        }

        function updateCoordsProfle(c){
            $('.a1').val(c.x);
            $('.b1').val(c.y);
            $('.a2').val(c.x2);
            $('.b2').val(c.y2);
            $('.w').val(c.w);
            $('.h').val(c.h);
        };

        function getExtension(filename) {
            return filename.split('.').pop().toLowerCase();
        }

    });
</script>
<section class="main-content">
 
    <div class="content-wrap">
     
        <div class="wrapper">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo base_url('/'); ?>"><i class="ti-home mr5"></i><?php echo $this->lang->line('dashboard'); ?></a>
                </li>
                <li>
                    <a href="<?php echo base_url().$this->router->class; ?>"><?php echo ucfirst($this->module_name); ?></a>
                </li>
                <li class="active"><?php echo $this->lang->line('add'); ?></li>
            </ol>

            <div class="panel">
                <header class="panel-heading">
                    <div class="row">
                        <div class="col-xs-10">
                            <h5 class="text-uppercase no-m"><?php echo strtoupper($this->module_name); ?></h5>
                            <small><?php echo $this->lang->line('members_teks'); ?></small>
                        </div>
                        <div class="col-xs-2 text-right">
                            <a class="btn btn-warning btn-xs" href="<?php echo base_url().$this->router->class; ?>"  title="<?php echo $this->lang->line('add'); ?>">
                                <i class="ti-plus"></i>
                                <?php echo $this->lang->line('list'); ?>
                            </a>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal" name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>/add" enctype="multipart/form-data">
                        <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
                            <?php foreach($error_hash as $inp_err){ ?>
                                <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                                    });
                                </script>
                            <?php } ?>
                        <?php } ?>
                        
                        <?php if(isset($success)){ ?>
                            <script type="text/javascript">
                            jQuery(document).ready(function($) {
                                toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                                });
                            </script>
                        <?php } ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('name'); ?> <span>*</span></label>
                            <div class="col-sm-5">
                                <input type="text" name="fullname" value='<?php echo isset($fullname)?$fullname:''; ?>' size="50" class="form-control">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('birthday'); ?> <span>*</span></label>
                            <div class="col-sm-2">
								<input type="text" name="birthday" value='<?php echo isset($birthday)?$birthday:''; ?>' data-date-format="mm/dd/yyyy" class="form-control datetimepicker" readonly  />
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('address'); ?> <span>*</span></label>
                            <div class="col-sm-5">
                                <textarea type="text" name="address" size="50" class="form-control"><?php echo isset($address)?$address:''; ?></textarea>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('postcode'); ?> <span>*</span></label>
                            <div class="col-sm-2">
                                <input type="text" name="postcode" value='<?php echo isset($postcode)?$postcode:''; ?>' size="50" class="form-control postcode">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('gender'); ?> <span>*</span></label>
							<div class="col-sm-5 radio">
								<label>
									<input type="radio" name="gender" value="1" <?php if(isset($gender) && $gender == 1){ echo "checked=checked"; }?> />
									<?php echo $this->lang->line('laki_laki'); ?>
								</label>
								 &nbsp
                                <label>
                                    <input type="radio" name="gender" value="0" <?php if(isset($gender) && $gender == 0){ echo "checked=checked"; }?> />
                                    <?php echo $this->lang->line('wanita'); ?>
                                </label>
							</div>
                        </div>
						<div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('phone'); ?> <span>*</span></label>
                            <div class="col-sm-3">
                                <input type="text" name="phone" value='<?php echo isset($phone)?$phone:''; ?>' size="50" class="form-control">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('email'); ?> <span>*</span></label>
                            <div class="col-sm-4">
                                <input type="text" name="email" value='<?php echo isset($email)?$email:''; ?>' size="50" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('password'); ?> <span>*</span></label>
                            <div class="col-sm-3">
                                <input type="password" class="form-control" name="password" value='<?php echo isset($password)?$password:''; ?>'>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('status'); ?> <span>*</span></label>
							<div class="col-sm-4 radio">
								<label>
                                    <input type="radio" name="status" value="1" <?php if(isset($status) && $status == 1){ echo "checked=checked"; }?> />
                                    <?php echo $this->lang->line('aktif'); ?>
                                </label>
                                &nbsp
                                <label>
                                    <input type="radio" name="status" value="0" <?php if(!isset($status)){ echo "checked=checked"; } ?> <?php if(isset($status) && $status == 0){ echo "checked=checked"; }?> />
                                    <?php echo $this->lang->line('nonaktif'); ?> 
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('profil'); ?> <span>*</span></label>
                            <div class="col-sm-10">
                                <input class="text gbr" type="file" id="uploadImageProfile" name="pathprofile" value="<?php echo isset($path)?$path:''; ?>"><br class="clear">
                                <p>
                                    <em>
                                        <?php echo $this->lang->line('label_info_ukuran'); ?> 
                                        <?php echo $this->webconfig['max_foto_filesize']; ?> MB
                                        <?php echo $this->lang->line('label_info_dimensi'); ?> 
                                        <?php if(count($this->webconfig["image_profile"]) > 0){ echo $this->webconfig["image_profile"][0]; }else{ echo 0; } ?> px
                                    </em>
                                </p>
                            </div>
                        </div>
                        <div class="form-group footertable">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-5 text-left">
                                <input class="btn btn-danger btn-sm mr5" type="submit" value="<?php echo $this->lang->line('navigation_kirim'); ?>">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
     
    </div>
 
    <a class="exit-offscreen"></a>
</section>