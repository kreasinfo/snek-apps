<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class UsersModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "users";
	}
	public function entriData($params=array()){
		$fullname      = isset($params["fullname"])?$params["fullname"]:'';
		$username    = isset($params["username"])?$params["username"]:'';
		$password  = isset($params["password"])?$params["password"]:'';
		$password_1  = isset($params["password_1"])?$params["password_1"]:'';
		$role      = isset($params["role"])?$params["role"]:'';
		if($fullname == '' || $username == '' || $password == '' || $role == '') {
			return 'empty';
		}else{
			$check = $this->__checkUserId($username);
			if($check > 0){
				return 'exist';
			}
		}


		$data ['id'] = "Null";
		$data ['fullname'] = $this->db->escape_str($fullname);
		$data ['username'] = $this->db->escape_str($username);
		$data ['password'] = md5($password);
		$data ['role'] = $role;
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_users')." dengan nama = ".htmlentities($fullname).""));
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function updateData($params=array()){
		$id   = isset($params["id"])?$params["id"]:'';
		$fullname      = isset($params["fullname"])?$params["fullname"]:'';
		$username    = isset($params["username"])?$params["username"]:'';
		$password  = isset($params["password"])?$params["password"]:'';
		$role      = isset($params["role"])?$params["role"]:'';

		$thisUserPass = $this->__getUserPassword(array(
														'id' => $id
													));
		$oldPass   = $thisUserPass['password'];
		$oldUserid = $thisUserPass['username'];
		if($fullname == '' || $username == '' || $role == '') {
			return 'empty';
		}else{
			if($username != $oldUserid){
			$check = $this->__checkUserId($username);
				if($check > 0){
					return 'exist';
				}
			}
		}
		if($password != ''){
			if(strlen($password) < 5){
				return 'min_5';
			}else{
				$sql_user = "fullname = '".$this->db->escape_str($fullname)."', username = '".$this->db->escape_str($username)."', role = '".$this->db->escape_str($role)."', password = '".$this->db->escape_str(md5($password))."'";
			}

		}else{
			$sql_user = "fullname = '".$this->db->escape_str($fullname)."', username = '".$this->db->escape_str($username)."', role = '".$this->db->escape_str($role)."'";
		}

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_users')." id = ".$id.", name = ".htmlentities($fullname).""));
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function deleteData($id){
		if($id == 0) return 'failed';
		$doDelete = $this->db->query("
		DELETE FROM ".$this->maintablename."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_users')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function filterData($params=array()){
		$fullname = isset($params["fullname"])?$params["fullname"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$rest  = "ORDER BY id DESC";
		$conditional = "WHERE fullname LIKE '%".$this->db->escape_str($fullname)."%'";
		if($this->session->userdata('role') != 0){
			if($this->session->userdata('role') == 1){
				$rest = "AND role != '0'";
			}else if($this->session->userdata('role') == 2){
				$rest = "AND role != '0'  AND role != '1'";
			}else if($this->session->userdata('role') == 3){
				$rest = "AND role != '0'  AND role != '1'  AND role != '2'";
			}
		}
		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function filterDataCount($params=array()){
		$fullname = isset($params["fullname"])?$params["fullname"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';

		$rest  = "ORDER BY id DESC";
		$conditional = "WHERE fullname LIKE '%".$this->db->escape_str($fullname)."%'";
		if($this->session->userdata('role') != 0){
			if($this->session->userdata('role') == 1){
				$rest = "AND role != '0'";
			}else if($this->session->userdata('role') == 2){
				$rest = "AND role != '0'  AND role != '1'";
			}else if($this->session->userdata('role') == 3){
				$rest = "AND role != '0'  AND role != '1'  AND role != '2'";
			}
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$username = isset($params["username"])?$params["username"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
			if($this->session->userdata('role') != 0){
				if($this->session->userdata('role') == 1){
					$rest = "AND role != '0'";
				}else if($this->session->userdata('role') == 2){
					$rest = "AND role != '0'  AND role != '1'";
				}else if($this->session->userdata('role') == 3){
					$rest = "AND role != '0'  AND role != '1'  AND role != '2'";
				}
			}
		}else{
			if($username != '') {
				$conditional = "WHERE username = '".$this->db->escape_str($username)."'";
				if($this->session->userdata('role') != 0){
					if($this->session->userdata('role') == 1){
						$rest = "AND role != '0'";
					}else if($this->session->userdata('role') == 2){
						$rest = "AND role != '0'  AND role != '1'";
					}else if($this->session->userdata('role') == 3){
						$rest = "AND role != '0'  AND role != '1'  AND role != '2'";
					}
				}
			}else{
				if($this->session->userdata('role') != 0){
					if($this->session->userdata('role') == 1){
						$rest = "WHERE role != '0'";
					}else if($this->session->userdata('role') == 2){
						$rest = "WHERE role != '0'  AND role != '1'";
					}else if($this->session->userdata('role') == 3){
						$rest = "WHERE role != '0'  AND role != '1'  AND role != '2'";
					}
				}
			}
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$username = isset($params["username"])?$params["username"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
			if($this->session->userdata('role') != 0){
				if($this->session->userdata('role') == 1){
					$rest = "AND role != '0'";
				}else if($this->session->userdata('role') == 2){
					$rest = "AND role != '0'  AND role != '1'";
				}else if($this->session->userdata('role') == 3){
					$rest = "AND role != '0'  AND role != '1'  AND role != '2'";
				}
			}
		}else{
			if($username != '') {
				$conditional = "WHERE username = '".$this->db->escape_str($username)."'";
				if($this->session->userdata('role') != 0){
					if($this->session->userdata('role') == 1){
						$rest = "AND role != '0'";
					}else if($this->session->userdata('role') == 2){
						$rest = "AND role != '0'  AND role != '1'";
					}else if($this->session->userdata('role') == 3){
						$rest = "AND role != '0'  AND role != '1'  AND role != '2'";
					}
				}
			}else{
				if($this->session->userdata('role') != 0){
					if($this->session->userdata('role') == 1){
						$rest = "WHERE role != '0'";
					}else if($this->session->userdata('role') == 2){
						$rest = "WHERE role != '0'  AND role != '1'";
					}else if($this->session->userdata('role') == 3){
						$rest = "WHERE role != '0'  AND role != '1'  AND role != '2'";
					}
				}
			}
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	private function __getUserPassword($params = array()){
		$id     = isset($params["id"])?$params["id"]:'';
		$username = isset($params["username"])?$params["username"]:'';
		$conditional = "";

		if($id != '') $conditional = "WHERE id = '".$id."'";
		if($username != '') $conditional = "WHERE username = '".$this->db->escape_str($username)."'";


		$q = $this->db->query("
			SELECT
				id
				,username
				,password
				,role
			FROM
				".$this->maintablename."
			".$conditional."
		");
		$result = $q->first_row('array');
		return $result;
	}
	private function __checkUserId($username){
		$q = $this->db->query("
			SELECT
				id
				,username
			FROM
				".$this->maintablename."
			WHERE
				username = '".$this->db->escape_str($username)."'
		");
		$result = $q->num_rows();
		return $result;
	}

	function CheckUname($username){
		$q = $this->db->query("
			SELECT
				id
				,username
			FROM
				".$this->maintablename."
			WHERE
				username = '".$this->db->escape_str($username)."'
		");
		$result = $q->num_rows();
		return $result;
	}

}