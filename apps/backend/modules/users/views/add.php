<script type="text/javascript">
jQuery(document).ready(function($) {
    $(".username").change(function () {
        $.ajax({
            type: 'post',
            url: "<?php echo base_url() ?>users/checkUsername/",
            dataType: "json",
            data: $(this).serialize(),
            success: function(data) {
                    if(data != 0){
                       alert("<?php echo $this->lang->line('error_duplicate_username'); ?>");
                       $('.username').val('');
                    }
                }
        });
    });
});
</script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        jQuery("#listData").load('<?php echo base_url().$this->router->class; ?>/getAllData/');
    });
    function searchThis(){
        var frm = document.searchform;
        var name = frm.name.value;
        if(name == ''){ name = '-'; }
        jQuery("#listData").load("<?php echo base_url().$this->router->class; ?>/searchdata/"+name.replace(/\s/g,'%20'));
    }
</script>
<section class="main-content">
 
    <div class="content-wrap">
     
        <div class="wrapper">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo base_url('/'); ?>"><i class="ti-home mr5"></i><?php echo $this->lang->line('dashboard'); ?></a>
                </li>
                <li>
                    <a href="<?php echo base_url().$this->router->class; ?>"><?php echo ucfirst($this->module_name); ?></a>
                </li>
                <li class="active"><?php echo $this->lang->line('add'); ?></li>
            </ol>

            <div class="panel">
                <header class="panel-heading">
                    <div class="row">
                        <div class="col-xs-10">
                            <h5 class="text-uppercase no-m"><?php echo strtoupper($this->module_name); ?></h5>
                            <small><?php echo $this->lang->line('users_teks'); ?></small>
                        </div>
                        <div class="col-xs-2 text-right">
                            <a class="btn btn-warning btn-xs" href="<?php echo base_url().$this->router->class; ?>"  title="<?php echo $this->lang->line('add'); ?>">
                                <i class="ti-plus"></i>
                                <?php echo $this->lang->line('list'); ?>
                            </a>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal" name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>/add" enctype="multipart/form-data">
                        <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
                            <?php foreach($error_hash as $inp_err){ ?>
                                <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                                    });
                                </script>
                            <?php } ?>
                        <?php } ?>
                        
                        <?php if(isset($success)){ ?>
                            <script type="text/javascript">
                            jQuery(document).ready(function($) {
                                toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                                });
                            </script>
                        <?php } ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('name'); ?> <span>*</span></label>
                            <div class="col-sm-5">
                                <input type="text" name="fullname" value='<?php echo isset($fullname)?$fullname:''; ?>' size="50" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('username'); ?> <span>*</span></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control username"  name="username" value='<?php echo isset($username)?$username:''; ?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('password'); ?> <span>*</span></label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" name="password" value='<?php echo isset($password)?$password:''; ?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('retype_password'); ?> <span>*</span></label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" name="password_1" value='<?php echo isset($password_1)?$password_1:''; ?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('group'); ?> <span>*</span></label>
                            <div class="col-sm-3">
                                <select name="role" class="form-control">
                                    <?php if($this->session->userdata('role') == 0){ ?>
                                        <option value="0" <?php if(isset($role) && $role == 0) echo "selected='selected'"; ?>>Programmer</option>
                                    <?php } ?>
                                    <?php if($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 ){ ?>
                                        <option value="1" <?php if(isset($role) && $role == 1) echo "selected='selected'"; ?>>Administrator</option>
                                    <?php } ?>
                                    <?php if($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 ){ ?>
                                        <option value="2" <?php if(isset($role) && $role == 2) echo "selected='selected'"; ?>>Super User</option>
                                    <?php } ?>
                                    <?php if($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 || $this->session->userdata('role') == 2 || $this->session->userdata('role') == 3 ) { ?>
                                        <option value="3" <?php if(isset($role) && $role == 3) echo "selected='selected'"; ?>>User</option>     
                                    <?php } ?>      
                                </select>
                            </div>
                        </div>
                        <div class="form-group footertable">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-5 text-left">
                                <input class="btn btn-danger btn-sm mr5" type="submit" value="<?php echo $this->lang->line('navigation_kirim'); ?>">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
     
    </div>
 
    <a class="exit-offscreen"></a>
</section>