<?php
class Users extends CI_Controller{
	var $webconfig;
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('pagination');

		$this->load->model('UsersModel');
		$this->load->sharedModel('LoginModel');
		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login');
		}
		if($this->session->userdata('role') != 0 && $this->session->userdata('role') != 1  && $this->session->userdata('role') != 2){
			redirect(base_url().'dashboard');
		}
		$this->webconfig = $this->config->item('webconfig');
		$this->module_name = $this->lang->line('users');
	}
	function index(){
		redirect(base_url().$this->router->class.'/listing');
	}
	function listing($page = 0){
		$tdata = array();
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		$this->load->sharedView('template', $ldata);
	}
	function getAllData($page = 0){
		if($this->session->userdata('role') == 3){
		$all_data = $this->UsersModel->listDataCount(array('userid'=>$this->session->userdata('userid')));
		}else{
		$all_data = $this->UsersModel->listDataCount();
		}
		
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 3;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><span>';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		if($this->session->userdata('role') == 3){
		$listdata['lists'] = $this->UsersModel->listData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
																,'username' => $this->session->userdata('username')
															));
		}else{
		$listdata['lists'] = $this->UsersModel->listData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
															));
		}
		
		$this->load->view($this->router->class.'/list', $listdata);
	}
	function searchdata($name = 'name'){
		if($name == '-'){$name = '';}

		# PAGINATION #
		$all_data = $this->UsersModel->filterDataCount(array('fullname'=>trim(urldecode($name))));
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/".$this->uri->segment(3)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 4;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><span>';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->UsersModel->filterData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
																,'fullname' => trim(urldecode($name))
															));
		$this->load->view($this->router->class.'/list', $listdata);
	}
	function add(){
		$tdata = array();
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
		if($_POST)
		{
			$validator = new FormValidator();
		    $validator->addValidation("fullname","req",$this->lang->line('error_empty_name_users'));
		    $validator->addValidation("username","req",$this->lang->line('error_empty_username_users'));
			$validator->addValidation("username","alnum",$this->lang->line('error_username_alfanumeric_users'));
		    $validator->addValidation("password","req",$this->lang->line('error_empty_password_users'));
			$validator->addValidation("password","minlen=5",$this->lang->line('error_empty_password_5_users'));
		    $validator->addValidation("password_1","req",$this->lang->line('error_empty_retype_password_users'));
		    $validator->addValidation("password_1","eqelmnt=password",$this->lang->line('error_empty_retype_password_true_users'));
		    if($validator->ValidateForm())
		    {
		        $doInsert = $this->UsersModel->entriData(array(
														        'fullname'=>$this->input->post('fullname')
														        ,'username'=>$this->input->post('username')
														        ,'password'=>$this->input->post('password')
														        ,'role'=>$this->input->post('role')
														  ));
				if($doInsert == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doInsert == 'exist'){
					$tdata['error_hash'] = array('error' => $this->lang->line('error_duplicate_username'));
				}else if($doInsert == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doInsert == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_entry');
				}
				$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {
		        $tdata['fullname']     = $this->input->post('fullname');
			    $tdata['username'] = $this->input->post('username');
			    $tdata['role'] = $this->input->post('role');
		    	$tdata['error_hash'] = $validator->GetErrors();

				$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{
			$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
			$this->load->sharedView('template', $ldata);
		}
	}
	function modif($id = 0){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}

		$tdata['lists'] = $this->UsersModel->listData(array('id'=>$id));
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
		if($_POST)
		{
			$validator = new FormValidator();

		    $validator->addValidation("fullname","req",$this->lang->line('error_empty_name_users'));
		    $validator->addValidation("username","req",$this->lang->line('error_empty_username_users'));
		    $validator->addValidation("password_1","eqelmnt=password",$this->lang->line('error_empty_retype_password_true_users'));
		    if($validator->ValidateForm())
		    {
		        $doUpdate = $this->UsersModel->updateData(array(
														        'id' => $id
		        												,'fullname'=>$this->input->post('fullname')
														        ,'username'=>$this->input->post('username')
														        ,'password'=>$this->input->post('password')
														        ,'role'=>$this->input->post('role')
														  ));

				if($doUpdate == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doUpdate == 'exist'){
					$tdata['error_hash'] = array('error' => $this->lang->line('error_duplicate_username'));
				}else if($doUpdate == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doUpdate == 'min_5'){
					$tdata['fullname']     = $this->input->post('fullname');
			    	$tdata['username'] = $this->input->post('username');
			   		$tdata['role'] = $this->input->post('role');
					$tdata['error_hash'] = array('error' => $this->lang->line('error_empty_password_5_users'));
				}else if($doUpdate == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_update');
					$tdata['lists'] = $this->UsersModel->listData(array('id'=>$id));
				}
				$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {
		        $tdata['fullname']     = $this->input->post('fullname');
			    $tdata['username'] = $this->input->post('username');
			    $tdata['password'] = $this->input->post('password');
			    $tdata['password_1'] = $this->input->post('password_1');
			    $tdata['role'] = $this->input->post('role');
		    	$tdata['error_hash'] = $validator->GetErrors();

				$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{
			$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
			$this->load->sharedView('template', $ldata);
		}
	}
	public function deleteThis(){
		$doDelete = $this->__delete($this->input->post('dataid'));

		if($doDelete == 'failed'){
			echo "nodata";
		}else if($doDelete == 'success'){
			echo "success";
		}
	}
	public function postProcess(){
		$postvalue = $this->input->post('datacek');
		if($postvalue != ''){
			foreach($postvalue as $data){
				$this->__delete($data);
			}
			echo "success";
		}else{
			echo "nodata";
		}
	}
	private function __delete($id){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}
		$doDelete = $this->UsersModel->deleteData($id);
    	return $doDelete;
	}

	public function checkUsername() {
  		$username = $this->input->post("username");
		$listdata = $this->UsersModel->CheckUname($username);

		$this->output
	    ->set_content_type('application/json')
	    ->set_output(json_encode($listdata));
	}


}