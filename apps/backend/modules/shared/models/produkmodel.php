<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ProdukModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "produk";
		$this->produk_kategori_table = "kategori_produk";
		$this->produk_ukuran_table = "produk_ukuran";
		$this->produk_img_table = "produk_img";
		$this->ukm_table = "ukm";
	}
	public function entriData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$slug = isset($params["slug"])?$params["slug"]:'';
		$description = isset($params["description"])?$params["description"]:'';
		$body = isset($params["body"])?$params["body"]:'';
		$price = isset($params["price"])?$params["price"]:'';
		$berat = isset($params["berat"])?$params["berat"]:'';
		$discount = isset($params["discount"])?$params["discount"]:'';
		$seo_title = isset($params["seo_title"])?$params["seo_title"]:'';
		$seo_description = isset($params["seo_description"])?$params["seo_description"]:'';
		$id_kategori_produk = isset($params["id_kategori_produk"])?$params["id_kategori_produk"]:'';
		$status = isset($params["status"])?$params["status"]:'';
        $size = isset($params["size"]) ? $params["size"] : '';
        $tags = isset($params["tags"]) ? $params["tags"] : '';
        $stock = isset($params["stock"]) ? $params["stock"] : '';
		$description_img = isset($params["description_img"])?$params["description_img"]:'';
		$x1     = isset($params["x1"])?$params["x1"]:'';
        $y1     = isset($params["y1"])?$params["y1"]:'';
        $x2      = isset($params["x2"])?$params["x2"]:'';
        $y2      = isset($params["y2"])?$params["y2"]:'';
        $width_resize      = isset($params["width_resize"])?$params["width_resize"]:'';
        $id_ukm = isset($params["id_ukm"]) ? $params["id_ukm"] : '';
        $id_lokasi_ukm = isset($params["id_lokasi_ukm"]) ? $params["id_lokasi_ukm"] : '';
        $status_produk = isset($params["status_produk"]) ? $params["status_produk"] : '';

        $upload_image = $this->uploadImage($width_resize, $x1, $y1, $x2, $y2);
       	
        if(isset($upload_image["error"])) {
            if($upload_image["error"] == "failed_ext") {
                return "failed_ext";
            }
            if($upload_image["error"] == "empty_file") {
                return "empty_file";
            }
        }

        if ($size == '')
            $size = array();
        if ($stock == '')
            $stock = array();

        if ($description_img == '')
            $description_img = array();

	
		if ($_FILES) {
			foreach ($_FILES as $value) {
				$path[] = $value;
			}
		}


		// if (isset($description_img) && $description_img !='') {
		// 	foreach ($description_img as $value) {
		// 		if ($value == '') {
		// 			return 'empty_description_img';
		// 		}
		// 	}
		// }

		$data ['id'] = "Null";
		$data ['datecreated'] = date('Y-m-d H:i:s');
		$data ['name'] = $this->db->escape_str($name);
		$data ['slug'] = $this->db->escape_str($slug);
		$data ['description'] = $this->db->escape_str($description);
		$data ['body'] = $this->db->escape_str($body);
		$data ['price'] = $this->db->escape_str(str_replace('Rp ', '', str_replace('.', '', $price)));
		$data ['berat'] = $this->db->escape_str($berat);
		$data ['tags'] = $this->db->escape_str($tags);
		$data ['discount'] = $this->db->escape_str($discount);
		$data ['seo_title'] = $this->db->escape_str($seo_title);
		$data ['seo_description'] = $this->db->escape_str($seo_description);
		$data ['id_kategori_produk'] = $this->db->escape_str($id_kategori_produk);
		$data ['userid'] = $this->db->escape_str($this->session->userdata('id_user'));
		$data ['status'] = $this->db->escape_str($status);
		$data ['id_ukm'] = $this->db->escape_str($id_ukm);
		$data ['id_lokasi_ukm'] = $this->db->escape_str($id_lokasi_ukm);
		$data ['status_produk'] = $this->db->escape_str($status_produk);
		
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			$id_produk = $this->db->insert_id();	

            if (count($size) > 0 || count($stock) > 0) {
                for ($si = 0; $si < count($size); $si++) {
                    $sdata['id'] = "Null";
                    $sdata['id_produk'] = $id_produk;
                    $sdata['size'] = $size[$si];
                    $sdata['stock'] = $stock[$si];
                    $InsertSize = $this->db->insert($this->produk_ukuran_table, $sdata);
                }
            }

            if(count($_FILES['path']) > 0){
				$imgdata['id_produk'] = $id_produk;
				//$imgdata['description'] = $this->db->escape_str($description_img);
				$imgdata['path'] = isset($upload_image["path"])?$upload_image["path"]:"";
				$this->db->insert($this->produk_img_table, $imgdata);
            }
			
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_produk')." dengan nama = ".htmlentities($name).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function updateData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$name = isset($params["name"])?$params["name"]:'';
		$slug = isset($params["slug"])?$params["slug"]:'';
		$description = isset($params["description"])?$params["description"]:'';
		$body = isset($params["body"])?$params["body"]:'';
		$price = isset($params["price"])?$params["price"]:'';
		$berat = isset($params["berat"])?$params["berat"]:'';
		$discount = isset($params["discount"])?$params["discount"]:'';
		$seo_title = isset($params["seo_title"])?$params["seo_title"]:'';
		$seo_description = isset($params["seo_description"])?$params["seo_description"]:'';
		$id_kategori_produk = isset($params["id_kategori_produk"])?$params["id_kategori_produk"]:'';
		$status = isset($params["status"])?$params["status"]:'';
        $size = isset($params["size"]) ? $params["size"] : '';
        $tags = isset($params["tags"]) ? $params["tags"] : '';
        $stock = isset($params["stock"]) ? $params["stock"] : '';
        $sizeidold = isset($params["sizeidold"]) ? $params["sizeidold"] : '';
        $sizeold = isset($params["sizeold"]) ? $params["sizeold"] : '';
        $stockold = isset($params["stockold"]) ? $params["stockold"] : '';
        $id_ukm = isset($params["id_ukm"]) ? $params["id_ukm"] : '';
        $id_lokasi_ukm = isset($params["id_lokasi_ukm"]) ? $params["id_lokasi_ukm"] : '';
        $status_produk = isset($params["status_produk"]) ? $params["status_produk"] : '';

        if ($size == '')
            $size = array();
        if ($stock == '')
            $stock = array();
        if ($sizeidold == '')
            $sizeidold = array();
        if ($sizeold == '')
            $sizeold = array();
        if ($stockold == '')
            $stockold = array();
		
		$sql_user = "
					name = '".$this->db->escape_str($name)."'
					,datemodified = '".date('Y-m-d H:i:s')."'
					,description = '".$this->db->escape_str($description)."'
					,body = '".$this->db->escape_str($body)."'
					,price = '".$this->db->escape_str(str_replace('Rp ', '', str_replace('.', '', $price)))."'
					,berat = '".$this->db->escape_str($berat)."'
					,discount = '".$this->db->escape_str($discount)."'
					,seo_title = '".$this->db->escape_str($seo_title)."'
					,seo_description = '".$this->db->escape_str($seo_description)."'
					,id_kategori_produk = '".$this->db->escape_str($id_kategori_produk)."'
					,status = '".$this->db->escape_str($status)."'
					,slug = '".$this->db->escape_str($slug)."'
					,tags = '".$this->db->escape_str($tags)."'
					,id_ukm = '".$this->db->escape_str($id_ukm)."'
					,id_lokasi_ukm = '".$this->db->escape_str($id_lokasi_ukm)."'
					,status_produk = '".$this->db->escape_str($status_produk)."'
					";
		
		

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){

            if (count($size) > 0 || count($stock) > 0) {
                for ($si = 0; $si < count($size); $si++) {
                    $sdata['id'] = "Null";
                    $sdata['id_produk'] = $id;
                    $sdata['size'] = $size[$si];
                    $sdata['stock'] = $stock[$si];
                    $InsertSize = $this->db->insert($this->produk_ukuran_table, $sdata);
                }
            }

            if (count($sizeold) > 0 || count($stockold) > 0 || count($sizeidold) > 0) {
                for ($soi = 0; $soi < count($sizeold); $soi++) {
                    $doUpdateStock = $this->db->query("
									UPDATE " . $this->produk_ukuran_table . "
									SET 
										size = '" . $sizeold[$soi] . "'
										,stock = '" . $stockold[$soi] . "'
									WHERE 
										id = " . $sizeidold[$soi] . "
									");
                }
            }
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_produk')." id = ".$id.", name = ".htmlentities($name).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function updateImg($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$x1     = isset($params["x1"])?$params["x1"]:'';
        $y1     = isset($params["y1"])?$params["y1"]:'';
        $x2      = isset($params["x2"])?$params["x2"]:'';
        $y2      = isset($params["y2"])?$params["y2"]:'';
        $width_resize      = isset($params["width_resize"])?$params["width_resize"]:'';
       	
        $upload_image = $this->uploadImage($width_resize, $x1, $y1, $x2, $y2);

        if(isset($upload_image["error"])) {
            if($upload_image["error"] == "failed_ext") {
                return "failed_ext";
            }
            if($upload_image["error"] == "empty_file") {
                return "empty_file";
            }
        }

        if ($_FILES) {
			foreach ($_FILES as $value) {
				$path[] = $value;
			}
		}

		$imgdata['id_produk'] = $id;
		//$imgdata['description'] = $this->db->escape_str($description_img);
		$imgdata['path'] = isset($upload_image["path"])?$upload_image["path"]:"";
		

		$doInsert = $this->db->insert($this->produk_img_table, $imgdata);
		if($doInsert){
			
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_imgproduk')." dengan id = ".htmlentities($id).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteData($id){
		if($id == 0) return 'failed';

		## DELETE IMG PRODUK FISIK ###
		$getData = $this->listDataImages(array('id_produk' => $id));
		if(isset($getData) && count($getData) > 0):
			foreach ($getData as $key => $value) {
				$pfile = $this->webconfig['media-path-images-original'].$value['path'];
				if (file_exists($pfile)){
					@unlink($pfile);
				}
			}
			
		endif;

		## DELETE IMG PRODUK ##
		$this->db->query("
		DELETE FROM ".$this->produk_img_table."
		WHERE
			id_produk = ".$id."
		");

		## DELETE SIZE PRODUK ##
		$this->db->query("
		DELETE FROM ".$this->produk_ukuran_table."
		WHERE
			id_produk = ".$id."
		");
		
		$doDelete = $this->db->query("
		DELETE FROM ".$this->maintablename."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_produk')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function filterData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$id_kategori_produk = isset($params["id_kategori_produk"])?$params["id_kategori_produk"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$rest  = "ORDER BY id DESC";
		$conditional = "WHERE name LIKE '%".$this->db->escape_str($name)."%'";

		if($id_kategori_produk != ''){
			$conditional .= "AND id_kategori_produk = '".$this->db->escape_str($id_kategori_produk)."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->getLoopDetail($result, 'id_kategori_produk', $this->produk_kategori_table);
		$result = $this->getLoopDetailUkm($result, 'id_ukm', $this->ukm_table);
		return $result;
	}

	public function filterDataCount($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$id_kategori_produk = isset($params["id_kategori_produk"])?$params["id_kategori_produk"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';

		$rest  = "ORDER BY id DESC";
		$conditional = "WHERE name LIKE '%".$this->db->escape_str($name)."%'";

		if($id_kategori_produk != ''){
			$conditional .= "AND id_kategori_produk = '".$this->db->escape_str($id_kategori_produk)."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->getLoopDetail($result, 'id_kategori_produk', $this->produk_kategori_table);
		$result = $this->getLoopDetailUkm($result, 'id_ukm', $this->ukm_table);
		return $result;
	}

	public function getData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->getLoopDetail($result, 'kategori_produk', $this->produk_kategori_table);
		$result = $this->getLoopDetailList($result, 'produk_ukuran', $this->produk_ukuran_table);
		$result = $this->getLoopDetailList($result, 'images', $this->produk_img_table);
		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	

    public function deleteSize($id) {
        if ($id == 0)
            return 'failed';

        $doDelete = $this->db->query("
			DELETE FROM " . $this->produk_ukuran_table . "
			WHERE 
				id = " . $id . "
		");
        if ($doDelete) {
            return 'success';
        } else {
            return 'failed';
        }
    }



	public function deleteImages($id){
		if($id == 0) return 'failed';
		$getData = $this->listDataImages(array('id' => $id));
		
		if(count($getData) > 0):
			$this->del_cropresize($getData[0]['path']);
			$pfile = $this->webconfig['media-path-product'].$getData[0]['path'];
			if (file_exists($pfile)){
				@unlink($pfile);
			}
		endif;
		$doDelete = $this->db->query("
		DELETE FROM ".$this->produk_img_table."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_img_product')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}



    public function doPublish($id){
		if($id == 0) return 'failed';
		
		$doPublish = $this->db->query("
			UPDATE ".$this->maintablename."
			SET status = '1'
			WHERE 
				id = ".$id."
		");
		if($doPublish){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_publish_produk')." id = ".$id.""));		
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function doUnublish($id){
		if($id == 0) return 'failed';
		
		$doUnublish = $this->db->query("
			UPDATE ".$this->maintablename."
			SET status = '0'
			WHERE 
				id = ".$id."
		");
		if($doUnublish){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_unpublish_produk')." id = ".$id.""));		
			return 'success';
		}else{
			return 'failed';
		}
	}
	

	public function listDataImages($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$id_produk = isset($params["id_produk"])?$params["id_produk"]:'';
	
		$conditional = "";
	
		if($id != '') {
			$conditional .= "  AND id = '".$id."'";
		}
	
		if($id_produk != '') {
			$conditional .= "  AND id_produk = '".$id_produk."'";
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->produk_img_table."
			WHERE (1=1)
			".$conditional."
		");
	
		$result = $q->result_array();
		return $result;
	}


	private function getLoopDetailList($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			for($i = 0; $i < count($loopdata); $i++){
				$loopdata[$i][$fielddata] = $this->getSQLDetailList($loopdata[$i]['id'], $tablename);
			}
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}



	private function getLoopDetail($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			for($i = 0; $i < count($loopdata); $i++){
				$loopdata[$i][$fielddata] = $this->getSQLDetail($loopdata[$i]['id_kategori_produk'], $tablename);
			}
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}
	private function getLoopDetailUkm($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			for($i = 0; $i < count($loopdata); $i++){
				$loopdata[$i][$fielddata] = $this->getSQLDetailUkm($loopdata[$i]['id_ukm'], $tablename);
			}
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}


	private function getLoopDetailSingle($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			$loopdata[$fielddata] = $this->getSQLDetail($loopdata[$fielddata], $tablename);
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}

	public function getSQLDetailList($iddata, $tablename){
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$tablename."
			WHERE id_produk = '".$iddata."'
			ORDER BY id ASC
		");
		$result = $q->result_array();
		return $result;
	}

	public function getSQLDetail($iddata, $tablename){
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$tablename."
			WHERE id = '".$iddata."'
			ORDER BY id DESC
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function getSQLDetailUkm($iddata, $tablename){
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$tablename."
			WHERE id = '".$iddata."'
			ORDER BY id DESC
		");
		$result = $q->first_row('array');
		return $result;
	}
	
    private function uploadImage($width_resize, $x1, $y1, $x2, $y2) {
        $this->ci->load->helper('upload');
        if ($_FILES['path']['size']<>0){
            $data = array();

            $tmpName = $_FILES['path']['tmp_name']; 
            list($width, $height, $type, $attr) = getimagesize($tmpName);
            # Rasio Ukuran Shopsliders #
            $rasio = $width/$width_resize;
            $extension_thumb = strtoupper(end(explode('.', $_FILES['path']['name'])));
            if (!(($extension_thumb == 'JPEG') || ($extension_thumb == 'JPG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'GIF'))){
                $data['error'] = "failed_ext";
            }
            
            # UPLOAD #
            $pathdir = date("Y/m/d");
            $thepath = $this->webconfig['media-path-product'].$pathdir."/";                       
            $thepath_create = $this->webconfig['media-path-product'].$pathdir."/temp/";                       
            $exp = explode("/",$thepath_create);
            $way = '';
            foreach($exp as $n){
                $way .= $n.'/';
                @mkdir($way);       
            }
            
            $pos = strripos($_FILES['path']['name'], '.');
            if($pos === false){
                $ordinary_name = $_FILES['path']['name'];
            }else{
                $ordinary_name = substr($_FILES['path']['name'], 0, $pos);
            }

            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);
            
            $result = was_upload_file('path', $name_upload, false, $thepath_create."/", array(), array(), array());

            if ($result['status'] > 0){
                $data['path'] = "";
            }else{
                $path1 = $pathdir."/".$name_upload_c;
                $data['path'] = $path1;
                if(isset($rasio) && $rasio !=''){
                    $finalcrop_resize = $this->croping_resize($result,$x1,$y1,$x2,$y2,$rasio,$thepath);
                }
            }
        }else{
            $data['error'] = "empty_file";
        }

        return $data;
    }
    private function croping_resize($result,$x1,$y1,$x2,$y2,$rasio,$image_path) {
        
        # RESIZE and CROP #
        $thepath = $this->webconfig['media-path-product-thumb'].date("Y/m/d")."/";                       
        $exp = explode("/",$thepath);
        $way ='';
        foreach($exp as $n){
            $way.=$n.'/';
            @mkdir($way);       
        }
        
        $file_name = $result['name'];
        $src_path = $image_path . 'temp/' . $file_name;
        
        $exp_file = explode('.', $file_name, 2);
        
        $des_path =  $image_path .$file_name;
        $xx1 = $x1 * $rasio;
        $yy1 = $y1 * $rasio;
        $xx2 = $x2 * $rasio;
        $yy2 = $y2 * $rasio;
        if($des_path !=''){
            $this->image_moo
                ->load($src_path)
                ->crop($xx1,$yy1,$xx2,$yy2)
                ->save($des_path);
        }

        ## RESIZE IMAGE ##
        foreach($this->webconfig['image_product'] as $key => $value){
            $size_file = explode('x',$value);
            $file_final_name_resize = $exp_file[0].'_'.$value.'_thumb.'.$exp_file[1];
            $des_path_resize =  $thepath . $file_final_name_resize;
            $this->image_moo
                ->load($des_path)
                ->stretch($size_file[0],$size_file[1])
                ->save($des_path_resize);
        }

        /* Delete Temp File */
        if (file_exists($src_path)){
            @unlink($src_path);
        }
    }
    private function del_cropresize($cfile) {
        if($cfile == "") return false;
        
        $explod_file = explode("/",$cfile);
        $filename = end($explod_file);
        $exp_file = explode(".",$filename);
        $exp_thumb_path = explode("/", $this->webconfig['media-path-product-thumb'].$cfile, -1);
        $thumb_path = implode("/", $exp_thumb_path);
        
        foreach($this->webconfig['image_product'] as $key => $value){
            $delete_resize = $thumb_path.'/'.$exp_file[0]."_".$value."_thumb.".$exp_file[1];
            if (file_exists($delete_resize)){
                @unlink($delete_resize);
            }
        }
    }
}