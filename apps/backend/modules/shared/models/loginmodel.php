<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class LoginModel extends CI_Model {
	var $module, $CI;
	function __construct() {	 
		parent::__construct();
		$this->CI =& get_instance(); 
		$this->CI->load->library('session');		
		$this->module = "Authentification";
	}
	public function doLogin($username = '', $password = ''){
		if($username == '' || $password == ''){
			return 'failed_input';
		}
		$existUser = $this->existUser($username);
		
		$password = md5 ($password);
		$user = $this->checkLogin($username,$password);
		if(count($user) > 0){
			$this->sessionCreate($user['id']);
			writeLog(array('module' => $this->module, 'details' => "Login to Application"));
			return 'success_login';
		}else{
			return 'failed_login';
		}
	}
	public function doLogout(){
		writeLog(array('module' => $this->module, 'details' => "Logout from Application"));
		$this->session->sess_destroy();
	}
	
	public function isLoggedIn(){
		if($this->session->userdata('userid') != ''
			&& $this->session->userdata('name') != ''
			&& $this->session->userdata('role') != ''
			&& $this->session->userdata('id_user') != ''
		){
			$validCheked = $this->__validSession($this->session->userdata('id_user'),$this->session->userdata('name'),$this->session->userdata('userid'),$this->session->userdata('role'));
			if($validCheked > 0){
				return true;
			}else{
				return false;
			}
		}else {
			return false;
		}
	}
	private function sessionCreate($iduser){
		$q = $this->db->query("
			SELECT
				id
				,fullname
				,password
				,username
				,role
			FROM
				users
			WHERE
				ID = '".$iduser."'		
		");
		$user = $q->first_row('array');
		# STORE SESSION #
		$this->session->set_userdata('id_user', $user['id']);
		$this->session->set_userdata('userid', $user['username']);
		$this->session->set_userdata('name', $user['fullname']);
		$this->session->set_userdata('role', $user['role']);
	}	
	private function existUser($username = ''){
		$q = $this->db->query("
			SELECT
				username
			FROM
				users
			WHERE
				username = '".$username."'		
		");
		$result = $q->num_rows();
		return $result;
	}
	private function checkLogin($username = '', $password = ''){
		$q = $this->db->query("
			SELECT
				id
				,username
				,password
			FROM
				users
			WHERE
				username = '".$username."' AND password = '".$password."'
		");
		$result = $q->first_row('array');
		return $result;
	}
	private function __validSession($id_user, $username, $userid, $role){
		$q = $this->db->query("
			SELECT
				username
				,fullname
				,role
				,id
			FROM
				users
			WHERE
				fullname = '".$username."'	
				AND	
				username = '".$userid."'		
				AND
				role = '".$role."'				
				AND
				id = '".$id_user."'		
		");
		$result = $q->num_rows();
		return $result;
	}
}
?>