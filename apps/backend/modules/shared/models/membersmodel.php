<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MembersModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "members";
	}
	public function entriData($params=array()){
		$fullname = isset($params["fullname"])?$params["fullname"]:'';
		$birthday = isset($params["birthday"])?$params["birthday"]:'';
		$address = isset($params["address"])?$params["address"]:'';
		$postcode = isset($params["postcode"])?$params["postcode"]:'';
		$gender = isset($params["gender"])?$params["gender"]:'';
		$phone = isset($params["phone"])?$params["phone"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$password = isset($params["password"])?$params["password"]:'';
		$status = isset($params["status"])?$params["status"]:'';

		$a1     = isset($params["a1"])?$params["a1"]:'';
        $b1     = isset($params["b1"])?$params["b1"]:'';
        $a2      = isset($params["a2"])?$params["a2"]:'';
        $b2      = isset($params["b2"])?$params["b2"]:'';
        $width_resize      = isset($params["width_resize"])?$params["width_resize"]:'';

        $upload_image_profile = $this->uploadImageProfile($width_resize, $a1, $b1, $a2, $b2);

        if(isset($upload_image_profile["error"])) {
            if($upload_image_profile["error"] == "failed_ext") {
                return "failed_ext";
            }
            if($upload_image_profile["error"] == "empty_file") {
                return "empty_file";
            }
        }
		
		$exist_email = $this->__CheckNamaExist($email);
	
		if($exist_email > 0){return 'exist_email';}

		$data ['id'] = "Null";
		$data ['fullname'] = $this->db->escape_str($fullname);
		$data ['birthday'] = date("Y-m-d", strtotime($birthday)); 
		$data ['address'] = $this->db->escape_str($address);
		$data ['postcode'] = $this->db->escape_str($postcode);
		$data ['gender'] = $this->db->escape_str($gender);
		$data ['phone'] = $this->db->escape_str($phone);
		$data ['email'] = $this->db->escape_str($email);
		$data ['password'] = md5($password);
		$data ['date_created'] = date("Y-m-d H:i");
		$data ['status'] = $status;
		$data ['logo_img'] = isset($upload_image_profile["path"])?$upload_image_profile["path"]:"";
		
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_members')." dengan nama = ".htmlentities($fullname).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	private function uploadImageProfile($width_resize, $a1, $b1, $a2, $b2) {
        $this->ci->load->helper('upload');
        if ($_FILES['pathprofile']['size']<>0){
            $data = array();

            $tmpName = $_FILES['pathprofile']['tmp_name']; 
            list($width, $height, $type, $attr) = getimagesize($tmpName);
            # Rasio Ukuran Sliders #
            $rasio = $width/$width_resize;
            $extension_thumb = strtoupper(end(explode('.', $_FILES['pathprofile']['name'])));
            if (!(($extension_thumb == 'JPEG') || ($extension_thumb == 'JPG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'GIF'))){
                $data['error'] = "failed_ext";
            }
            
            # UPLOAD #
            $pathdir = date("Y/m/d");
            $thepath = $this->webconfig['media-path-profile'].$pathdir."/";                       
            $thepath_create = $this->webconfig['media-path-profile'].$pathdir."/temp/";                       
            $exp = explode("/",$thepath_create);
            $way = '';
            foreach($exp as $n){
                $way .= $n.'/';
                @mkdir($way);       
            }
            
            $pos = strripos($_FILES['pathprofile']['name'], '.');
            if($pos === false){
                $ordinary_name = $_FILES['pathprofile']['name'];
            }else{
                $ordinary_name = substr($_FILES['pathprofile']['name'], 0, $pos);
            }

            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);
            
            $result = was_upload_file('pathprofile', $name_upload, false, $thepath_create."/", array(), array(), array());

            if ($result['status'] > 0){
                $data['path'] = "";
            }else{
                $path1 = $pathdir."/".$name_upload_c;
                $data['path'] = $path1;
                if(isset($rasio) && $rasio !=''){
                    $finalcrop_resize = $this->croping_resize_profile($result,$a1, $b1, $a2, $b2,$rasio,$thepath);
                }
            }
        }else{
            $data['error'] = "empty_file";
        }

        return $data;
    }

    private function croping_resize_profile($result,$a1, $b1, $a2, $b2,$rasio,$image_path) {
        
        # RESIZE and CROP #
        $thepath = $this->webconfig['media-path-profile-thumb'].date("Y/m/d")."/";                       
        $exp = explode("/",$thepath);
        $way ='';
        foreach($exp as $n){
            $way.=$n.'/';
            @mkdir($way);       
        }
        
        $file_name = $result['name'];
        $src_path = $image_path . 'temp/' . $file_name;
        
        $exp_file = explode('.', $file_name, 2);
        
        $des_path =  $image_path .$file_name;
        $aa1 = $a1 * $rasio;
        $bb1 = $b1 * $rasio;
        $aa2 = $a2 * $rasio;
        $bb2 = $b2 * $rasio;
        if($des_path !=''){
            $this->image_moo
                ->load($src_path)
                ->crop($aa1,$bb1,$aa2,$bb2)
                ->save($des_path);
        }

        ## RESIZE IMAGE ##
        foreach($this->webconfig['image_profile'] as $key => $value){
            $size_file = explode('x',$value);
            $file_final_name_resize = $exp_file[0].'_'.$value.'_thumb.'.$exp_file[1];
            $des_path_resize =  $thepath . $file_final_name_resize;
            $this->image_moo
                ->load($des_path)
                ->stretch($size_file[0],$size_file[1])
                ->save($des_path_resize);
        }

        /* Delete Temp File */
        if (file_exists($src_path)){
            @unlink($src_path);
        }
    }

    private function del_cropresize_profile($cfile) {
        if($cfile == "") return false;
        
        $explod_file = explode("/",$cfile);
        $filename = end($explod_file);
        $exp_file = explode(".",$filename);
        $exp_thumb_path = explode("/", $this->webconfig['media-path-profile-thumb'].$cfile, -1);
        $thumb_path = implode("/", $exp_thumb_path);
        
        foreach($this->webconfig['image_profile'] as $key => $value){
            $delete_resize = $thumb_path.'/'.$exp_file[0]."_".$value."_thumb.".$exp_file[1];
            if (file_exists($delete_resize)){
                @unlink($delete_resize);
            }
        }
    }

	public function updateData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$fullname = isset($params["fullname"])?$params["fullname"]:'';
		$birthday = isset($params["birthday"])?$params["birthday"]:'';
		$address = isset($params["address"])?$params["address"]:'';
		$postcode = isset($params["postcode"])?$params["postcode"]:'';
		$gender = isset($params["gender"])?$params["gender"]:'';
		$phone = isset($params["phone"])?$params["phone"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$password = isset($params["password"])?$params["password"]:'';
		$status = isset($params["status"])?$params["status"]:'';

		$exist_email = $this->__CheckEmailExistEdit($id,$email);
		
		if($exist_email > 0){return 'exist_email';}

		$a1     = isset($params["a1"])?$params["a1"]:'';
        $b1     = isset($params["b1"])?$params["b1"]:'';
        $a2      = isset($params["a2"])?$params["a2"]:'';
        $b2      = isset($params["b2"])?$params["b2"]:'';
        $width_resize      = isset($params["width_resize"])?$params["width_resize"]:'';

        if ($_FILES['pathprofile']['size']<>0){
            $getData = $this->listData(array('id' => $id));     
            if(count($getData) > 0) {
                $pfile = $this->webconfig['media-path-profile'].$getData[0]['logo_img'];
                if (file_exists($pfile)){
                    @unlink($pfile);
                }
                $delete_cropresize = $this->del_cropresize_profile($getData[0]['logo_img']);
            }
        }

        $upload_image_profile = $this->uploadImageProfile($width_resize, $a1, $b1, $a2, $b2);

        if(isset($upload_image_profile["error"])) {
            if($upload_image_profile["error"] == "failed_ext") {
                return "failed_ext";
            }
        }

        $path_profile = isset($upload_image_profile["path"])?$upload_image_profile["path"]:"";

		if($password != ''){
			if(strlen($password) < 5){
				return 'min_5';
			}else{
				$sql_user = "fullname = '".$this->db->escape_str($fullname)."',birthday = '".date("Y-m-d", strtotime($birthday))."',address = '".$this->db->escape_str($address)."',postcode = '".$this->db->escape_str($postcode)."',gender = '".$this->db->escape_str($gender)."',phone = '".$this->db->escape_str($phone)."',email = '".$this->db->escape_str($email)."',status = '".$this->db->escape_str($status)."', password = '".$this->db->escape_str(md5($password))."' ";
			}

		}else{
			$sql_user = "fullname = '".$this->db->escape_str($fullname)."',birthday = '".date("Y-m-d", strtotime($birthday))."',address = '".$this->db->escape_str($address)."',postcode = '".$this->db->escape_str($postcode)."',gender = '".$this->db->escape_str($gender)."',phone = '".$this->db->escape_str($phone)."',email = '".$this->db->escape_str($email)."',status = '".$this->db->escape_str($status)."' ";
		}
		
		if ($_FILES['pathprofile']['size']<>0) $sql_user .= ", logo_img = '".$this->db->escape_str($path_profile)."'";

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.", name = ".htmlentities($fullname).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteData($id){
		if($id == 0) return 'failed';

		# DELETE MEMBER PROJECT # 
		$this->db->where('id',$id);
		$queproject = $this->db->get($this->maintablename);
		$resultproject = $queproject->result_array();
		if(isset($resultproject) && count($resultproject) > 0){
			foreach ($resultproject as $key => $value) {
				@unlink($this->webconfig['media-path-profile'].$value['logo_img']);
				$exp_img = explode(".", $value['logo_img']);
				foreach ($this->webconfig['image_profile'] as $key => $image_data) {
					@unlink($this->webconfig['media-path-profile-thumb'].$exp_img[0]."_".$image_data."_thumb.".$exp_img[1]);
				}
			}
		}

		$doDelete = $this->db->query("
		DELETE FROM ".$this->maintablename."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function filterData($params=array()){
		$fullname = isset($params["fullname"])?$params["fullname"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$rest  = "ORDER BY id DESC";
		$conditional = "WHERE fullname LIKE '%".$this->db->escape_str($fullname)."%'";

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function filterDataCount($params=array()){
		$fullname = isset($params["fullname"])?$params["fullname"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';

		$rest  = "ORDER BY id DESC";
		$conditional = "WHERE fullname LIKE '%".$this->db->escape_str($fullname)."%'";

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}


	function checkname($nama){
		$q = $this->db->query("
			SELECT
				id
				,nama
			FROM
				".$this->maintablename."
			WHERE
				nama = '".$this->db->escape_str($nama)."'
		");
		$result = $q->first_row('array');
		return $result;
	}

	private function __CheckNamaExist($email){
		if($email !=''){
			$db = $this->db->query("
				SELECT
					count(id) as jumlah
				FROM
					".$this->maintablename."
				WHERE email LIKE '".$this->db->escape_str($email)."'
			")->result_array();

			$jumlah = $db[0]['jumlah'];
			return $jumlah;
		}else{
			return 'failed';
		}
	}

	private function __CheckEmailExistEdit($id,$email){
		$conditional = "";
		if($id !='' AND $email !=''){
			$db = $this->db->query("
				SELECT
					count(id) as jumlah
				FROM
					".$this->maintablename."
				WHERE id NOT IN (".$id.")
				AND email LIKE '".$this->db->escape_str($email)."'
			")->result_array();

			$jumlah = $db[0]['jumlah'];
			return $jumlah;
		}
	}

}