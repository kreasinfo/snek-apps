<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class PengirimanModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "pengiriman";
		$this->secondtablename = "kategori_pengiriman";
	}
	public function entriData($params=array()){
		$city = isset($params["city"])?$params["city"]:'';
		$price = isset($params["price"])?$params["price"]:'';
		$id_category = isset($params["id_category"])?$params["id_category"]:'';

		# CHECK IF EXIST #
		$this->db->where('id_category',$id_category);
		$this->db->where('city',$city);
		$query = $this->db->get('pengiriman');
	 	if ($query->num_rows() > 0){
			return 'exist_city';
		}

		$data ['id'] = "Null";
		$data ['city'] = $this->db->escape_str($city);
		$data ['price'] = $this->db->escape_str($price);
		$data ['id_category'] = $this->db->escape_str($id_category);
		
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_pengiriman')." dengan kota = ".htmlentities($city).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function updateData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$city = isset($params["city"])?$params["city"]:'';
		$price = isset($params["price"])?$params["price"]:'';
		$id_category = isset($params["id_category"])?$params["id_category"]:'';

		# CHECK IF EXIST #
		$this->db->where('id_category',$id_category);
		$this->db->where('city',$city);
		$this->db->where_not_in('id', $id);
		$query = $this->db->get('pengiriman');
	 	if ($query->num_rows() > 0){
			return 'exist_city';
		}
		
		$sql_user = "
					city = '".$this->db->escape_str($city)."'
					,price = '".$this->db->escape_str($price)."'
					,id_category = '".$this->db->escape_str($id_category)."'
					";
		
		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_pengiriman')." id = ".$id.", kota = ".htmlentities($city).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteData($id){
		if($id == 0) return 'failed';
		$doDelete = $this->db->query("
		DELETE FROM ".$this->maintablename."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_pengiriman')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function filterData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$id_kategori_pengiriman = isset($params["id_kategori_pengiriman"])?$params["id_kategori_pengiriman"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$rest  = "ORDER BY id DESC";
		$conditional = "WHERE city LIKE '%".$this->db->escape_str($name)."%'";

		if($id_kategori_pengiriman != ''){
			$conditional .= "AND id_category = '".$this->db->escape_str($id_kategori_pengiriman)."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->__getCategory($result);
		return $result;
	}

	public function filterDataCount($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$id_kategori_pengiriman = isset($params["id_kategori_pengiriman"])?$params["id_kategori_pengiriman"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';

		$rest  = "ORDER BY id DESC";
		$conditional = "WHERE city LIKE '%".$this->db->escape_str($name)."%'";

		if($id_kategori_pengiriman != ''){
			$conditional .= "AND id_category = '".$this->db->escape_str($id_kategori_pengiriman)."'";
		}


		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->__getCategory($result);
		return $result;
	}

	private function __getCategory($id_category){
        $countid = count($id_category);
        
        $i = 0;
        for ($i =0; $i < $countid; $i++) {
            $id_category[$i]["category"] = $this->__GetCategorySQL($id_category[$i]["id_category"]);
        }
        return $id_category;
    }
    
    private function __GetCategorySQL($id_category){
        $q = $this->db->query("
            SELECT
                *
            FROM
                ".$this->secondtablename."
            WHERE
                id = '".$id_category."'               
        ");
        $result = $q->first_row('array');
        return $result;
    }

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

}