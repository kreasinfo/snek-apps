<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Orderproduct_ukmModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "orders_detail";
		$this->secondtablename = "orders";
		$this->thirdtablename = "orders_detail";
		$this->fourthtablename = "produk";
		$this->fivethtablename = "produk_ukuran";
	}

	public function filterData($params=array()){
		$id_ukm = isset($params["id_ukm"])?$params["id_ukm"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$rest  = "ORDER BY a.id DESC";
		$conditional = '';
		if($id_ukm !=''){
			$conditional .= "AND b.id_ukm = '".$this->db->escape_str($id_ukm)."'";
		}
		
		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				a.*,b.id_ukm
			FROM
				".$this->maintablename." a
			LEFT JOIN ".$this->fourthtablename." b ON(a.product_id = b.id)
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->__getProduct($result);
		$result = $this->__getProductDetail($result);
		$result = $this->__getOrder($result);
		return $result;
	}

	public function filterDataCount($params=array()){
		$id_ukm = isset($params["id_ukm"])?$params["id_ukm"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';

		$rest  = "ORDER BY a.id DESC";
		$conditional = '';
		if($id_ukm !=''){
			$conditional .= "AND b.id_ukm = '".$this->db->escape_str($id_ukm)."'";
		}

		$q = $this->db->query("
			SELECT
				count(a.id) as jumlah
			FROM
				".$this->maintablename." a
			LEFT JOIN ".$this->fourthtablename." b ON(a.product_id = b.id)
			WHERE (1=1)
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->__getProduct($result);
		$result = $this->__getProductDetail($result);
		$result = $this->__getOrder($result);
		
		return $result;
	}

	private function __getProduct($id){
        $countid = count($id);
        
        $i = 0;
        for ($i =0; $i < $countid; $i++) {
            $id[$i]["product"] = $this->__GetProductSQL($id[$i]["product_id"]);
        }
        return $id;
    }

	private function __getProductDetail($id){
        $countid = count($id);
        
        $i = 0;
        for ($i =0; $i < $countid; $i++) {
            $id[$i]["productdetail"] = $this->__GetProductDetailSQL($id[$i]["size"]);
        }
        return $id;
    }

	private function __getOrder($id){
        $countid = count($id);
        
        $i = 0;
        for ($i =0; $i < $countid; $i++) {
            $id[$i]["orders"] = $this->__GetOrderSQL($id[$i]["order_id"]);
        }
        return $id;
    }

    public function __GetProductSQL($product_id){
        $q = $this->db->query("
            SELECT
                id,name
            FROM
                ".$this->fourthtablename."
            WHERE
                id = '".$product_id."' 
            ORDER BY id DESC              
        ");
        $result = $q->first_row('array');
        return $result;
    }

    public function __GetProductDetailSQL($size){
        $q = $this->db->query("
            SELECT
                id,size
            FROM
                ".$this->fivethtablename."
            WHERE
                id = '".$size."' 
            ORDER BY id DESC              
        ");
        $result = $q->first_row('array');
        return $result;
    }

    public function __GetOrderSQL($order_id){
        $q = $this->db->query("
            SELECT
                id,datecreated
            FROM
                ".$this->secondtablename."
            WHERE
                id = '".$order_id."' 
            ORDER BY id DESC              
        ");
        $result = $q->first_row('array');
        return $result;
    }

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function ubahDataBayar($id){
		if($id == 0) return 'failed';
		
		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			status = 1 ,date_trx = now()
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_bayar')." id = ".$id.", status = sudah bayar"));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function ubahDataBelumBayar($id){
		if($id == 0) return 'failed';
		
		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			status = 0 ,datemodified = now()
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_belumbayar')." id = ".$id.", status = belum bayar"));
			return 'success';
		}else{
			return 'failed';
		}
	}

}