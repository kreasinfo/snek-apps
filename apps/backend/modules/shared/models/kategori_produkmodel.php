<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kategori_produkModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "kategori_produk";
	}
	public function entriData($params=array()){
		$id_parent = isset($params["id_parent"])?$params["id_parent"]:'';
		$name = isset($params["name"])?$params["name"]:'';
		$slug = isset($params["slug"])?$params["slug"]:'';
		$description = isset($params["description"])?$params["description"]:'';

		$data ['id'] = "Null";
		$data ['id_parent'] = $this->db->escape_str($id_parent);
		$data ['name'] = $this->db->escape_str($name);
		$data ['slug'] = $this->db->escape_str($slug);
		$data ['description'] = $this->db->escape_str($description);
		
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_kategori_produk')." dengan nama = ".htmlentities($name).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function updateData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$id_parent = isset($params["id_parent"])?$params["id_parent"]:'';
		$name = isset($params["name"])?$params["name"]:'';
		$slug = isset($params["slug"])?$params["slug"]:'';
		$description = isset($params["description"])?$params["description"]:'';
		
		$sql_user = "
					name = '".$this->db->escape_str($name)."'
					,id_parent = '".$this->db->escape_str($id_parent)."'
					,description = '".$this->db->escape_str($description)."'
					,slug = '".$this->db->escape_str($slug)."'
					";
		
		

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_kategori_produk')." id = ".$id.", name = ".htmlentities($name).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteData($id){
		if($id == 0) return 'failed';

		$delete_cat_project = $this->DeleteKategori_produkProject($id);
		
		$doDelete = $this->db->query("
		DELETE FROM ".$this->maintablename."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_kategori_produk')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function filterData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$rest  = "ORDER BY id DESC";
		$conditional = "WHERE name LIKE '%".$this->db->escape_str($name)."%'";

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->__getParent($result);
		return $result;
	}

	public function filterDataCount($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';

		$rest  = "ORDER BY id DESC";
		$conditional = "WHERE name LIKE '%".$this->db->escape_str($name)."%'";

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->__getParent($result);
		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function DeleteKategori_produkProject($id){
		
		$arrayid = array($id);
		$que = $this->db->query("
			SELECT
				id,id_kategori_produk
			FROM
				".$this->secondtablename."
		");
		$result = $que->result_array();

		foreach ($result as $key => $value) {
			$exp = explode(',', $value['id_kategori_produk']);
			$arrmerge = array_diff($exp,$arrayid);
			$arrimplode = implode(',',$arrmerge);
			$this->db->query("
				UPDATE ".$this->secondtablename."
				SET
					id_kategori_produk = '".$arrimplode."'
				WHERE
					id = ".$value['id']."
				");
		}
		
		
	}

	public function filterSelect2($query = ""){
		$q = $this->db->query("
			SELECT
				id
				,name as text
			FROM
				".$this->maintablename."
			WHERE name LIKE '%".$query."%'
			limit 0, 10
		");
		$result = $q->result_array();
		return $result;
	}

	public function parentcategory($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$conditional ='';
		if ($id !="") {
			$conditional .= " AND id NOT IN (".$id.") ";
		}
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename." 
			WHERE id_parent = 0
			".$conditional."
		");
		$result = $q->result_array();
		$result = $this->__getChild($result);
		
		return $result;
	}
	private function __getChild($result) {
		
		if(count($result) > 0) {
			foreach ($result as $key => $value) {
				$result[$key]['child'] = $this->__getChildSQL($value['id']);
				$result[$key]['child'] = $this->__getChild($result[$key]['child']);
			}
		}
		return $result;
	}
	private function __getChildSQL($id = '') {
		$conditional = '';
		if($id != 0) {
			$conditional .= "WHERE id_parent = ".$id." ";
			
		}
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
		");

		$result = $q->result_array();
		return $result;
	}

	private function __getParent($id){
		$countid = count($id);
		
		$i = 0;
		for ($i =0; $i < $countid; $i++) {
			$id[$i]["parent"] = $this->__GetParentSQL($id[$i]["id_parent"]);
		}
		return $id;
	}
	private function __GetParentSQL($id_parent){
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE
				id = '".$id_parent."'				
		");
		$result = $q->first_row('array');
		return $result;
	}

}