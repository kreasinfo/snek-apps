<!doctype html>
<html class="signin no-js" lang="">
<head>
 
    <meta charset="utf-8">
    <meta name="description" content="Flat, Clean, Responsive, application admin template built with bootstrap 3">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title><?php echo $this->lang->line('login_system'); ?> | <?php echo $this->lang->line('app_logo'); ?></title>
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/animate.min.css">
     
     
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/skins/palette.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/fonts/font.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/main.css">
     
     
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
     
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/modernizr.js"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/modernizr.js"></script>
</head>
<body class="bg-primary">
    <?php echo isset($content)?$content:$this->lang->line('no_content'); ?>
</body>
</html>