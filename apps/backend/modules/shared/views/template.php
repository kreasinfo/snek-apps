<!doctype html>
<html class="no-js" lang="">
<head>
     
    <meta charset="utf-8">
    <meta name="description" content="Flat, Clean, Responsive, application admin template built with bootstrap 3">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
     
    <title><?php echo $this->module_name; ?> | <?php echo $this->lang->line('app_logo'); ?></title>

    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/animate.min.css">
     
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/select2.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/select2-bootstrap.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/skins/palette.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/fonts/font.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/main.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/alert.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/toastr.min.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/jquery.Jcrop.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>plugins/datepicker/datepicker.css">
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/jquery.fancybox.css">
    
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>css/custome.css">
     
     
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/modernizr.js"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/jquery-1.11.1.min.js"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery.numeric.js"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>js/select2.js"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/tinymce/tinymce.min.js"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery-impromptu.js"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery.limit-1.2.source.js"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery.fancybox.js"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery.autoNumeric.js"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery.number.js"></script>
</head>
 
<body>
<div class="app">
 
    <header class="header header-fixed navbar">
        <div class="brand">
         
            <a href="javascript:;" class="ti-menu off-left visible-xs" data-toggle="offscreen" data-move="ltr"></a>
             
             
            <a href="<?php echo base_url(); ?>" class="navbar-brand">
                <img src="<?php echo $this->webconfig['back_base_template']; ?>img/logo.png" alt="">
                <span class="heading-font">
                    <?php echo $this->lang->line('app_logo'); ?>
                </span>
            </a>
         
        </div>
        <ul class="nav navbar-nav">
            <li class="hidden-xs">
                <a href="javascript:;" class="toggle-sidebar">
                	<i class="ti-menu"></i>
                </a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown hidden-xs">
                <a href="javascript:;" data-toggle="dropdown">
                	<i class="ti-more-alt"></i>
                </a>
                <ul class="dropdown-menu animated zoomIn">
                    <li class="dropdown-header">Quick Links</li>
                    <li>
                    	<a href="javascript:;">Start New Campaign</a>
                    </li>
                    <li>
                    	<a href="javascript:;">Review Campaigns</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                    	<a href="javascript:;">Settings</a>
                    </li>
                    <li>
                    	<a href="javascript:;">Wish List</a>
                    </li>
                    <li>
                    	<a href="javascript:;">Purchases History</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                    	<a href="javascript:;">Activity Log</a>
                    </li>
                    <li>
                    	<a href="javascript:;">Settings</a>
                    </li>
                    <li>
                    	<a href="javascript:;">System Reports</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                    	<a href="javascript:;">Help</a>
                    </li>
                    <li>
                    	<a href="javascript:;">Report a Problem</a>
                    </li>
                </ul>
            </li>
            <li class="notifications dropdown">
                <a href="javascript:;" data-toggle="dropdown">
                    <i class="ti-bell"></i>
                    <div class="badge badge-top bg-danger animated flash">
                    	<span>3</span>
                    </div>
                </a>
                <div class="dropdown-menu animated fadeInLeft">
                    <div class="panel panel-default no-m">
                        <div class="panel-heading small"><b>Notifications</b>
                        </div>
                        <ul class="list-group">
                        <li class="list-group-item">
                        <a href="javascript:;">
                        <span class="pull-left mt5 mr15">
                        <img src="<?php echo $this->webconfig['back_base_template']; ?>img/face4.jpg" class="avatar avatar-sm img-circle" alt="">
                        </span>
                        <div class="m-body">
                        <div class="">
                        <small><b>CRYSTAL BROWN</b></small>
                        <span class="label label-danger pull-right">ASSIGN AGENT</span>
                        </div>
                        <span>Opened a support query</span>
                        <span class="time small">2 mins ago</span>
                        </div>
                        </a>
                        </li>
                        <li class="list-group-item">
                        <a href="javascript:;">
                        <div class="pull-left mt5 mr15">
                        <div class="circle-icon bg-danger">
                        <i class="ti-download"></i>
                        </div>
                        </div>
                        <div class="m-body">
                        <span>Upload Progress</span>
                        <div class="progress progress-xs mt5 mb5">
                        <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                        </div>
                        </div>
                        <span class="time small">Submited 23 mins ago</span>
                        </div>
                        </a>
                        </li>
                        <li class="list-group-item">
                        <a href="javascript:;">
                        <span class="pull-left mt5 mr15">
                        <img src="<?php echo $this->webconfig['back_base_template']; ?>img/face3.jpg" class="avatar avatar-sm img-circle" alt="">
                        </span>
                        <div class="m-body">
                        <em>Status Update:</em>
                        <span>All servers now online</span>
                        <span class="time small">5 days ago</span>
                        </div>
                        </a>
                        </li>
                        </ul>
                        <div class="panel-footer">
                            <a href="javascript:;">See all notifications</a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="off-right">
                <a href="javascript:;" data-toggle="dropdown">
                	<img src="<?php echo $this->webconfig['back_base_template']; ?>img/avatar.jpg" class="header-avatar img-circle" alt="user" title="user">
                	<span class="hidden-xs ml10">
                        <?php echo $this->session->userdata('name') != '' ? $this->session->userdata('name') : $this->lang->line('empty_name'); ?> 
                        (<?php echo $this->session->userdata('userid') != '' ? $this->session->userdata('userid') : $this->lang->line('empty_userid'); ?>)
                        | <?php
                          if($this->session->userdata('role') != ''):
                            switch($this->session->userdata('role')){
                              case 0:
                                echo "Programmer";
                                break;
                              case 1:
                                echo "Administrator";
                                break;
                              case 2:
                                echo "Superuser";
                                break;
                              case 3:
                                echo "User";
                                break;
                            }
                          endif;
                          ?>
                    </span>
                	<i class="ti-angle-down ti-caret hidden-xs"></i>
                </a>
                <ul class="dropdown-menu animated fadeInRight">
                    <li>
                        <a href="<?php echo base_url(); ?>shopconfiguration"><?php echo $this->lang->line('shopconfiguration'); ?></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>login/logout/"><?php echo $this->lang->line('label_logout'); ?></a>
                    </li>
                </ul>
            </li>
        </ul>
    </header>
     
    <section class="layout">
     
        <aside class="sidebar offscreen-left">
         
            <nav class="main-navigation" data-height="auto" data-size="6px" data-distance="0" data-rail-visible="true" data-wheel-step="10">
                <ul class="nav">
                 
                    <li <?php if($this->module_name == $this->lang->line('dashboard')){ echo 'class="active"';} ?>>
                        <a href="<?php echo base_url(); ?>">
                            <i class="fa fa-tachometer"></i>
                            <span><?php echo $this->lang->line('dashboard'); ?></span>
                        </a>
                    </li>
                    
                    <?php /*<li <?php if($this->module_name == $this->lang->line('blog')){ echo 'class="active"';} ?>>
                        <a href="<?php echo base_url(); ?>blog">
                            <i class="fa fa-edit"></i>
                            <span><?php echo $this->lang->line('blog'); ?></span>
                        </a>
                    </li>*/ ?>
                     
                    <li <?php if(
                                $this->module_name == $this->lang->line('kategori_produk')
                                || $this->module_name == $this->lang->line('kategori_pengiriman')
                                || $this->module_name == $this->lang->line('lokasi_ukm')
                                || $this->module_name == $this->lang->line('produk')
                                || $this->module_name == $this->lang->line('pengiriman')
                                || $this->module_name == $this->lang->line('shopsliders')
                                || $this->module_name == $this->lang->line('promoimg')
                                || $this->module_name == $this->lang->line('informationpage')
                                || $this->module_name == $this->lang->line('customerservicepage')
                                || $this->module_name == $this->lang->line('order')
                                || $this->module_name == $this->lang->line('ukm')
                                ){ echo 'class="open"';} ?>>
                        <a href="javascript:void(0)">
                            <i class="toggle-accordion"></i>
                            <i class="fa fa-shopping-cart"></i>
                            <span><?php echo $this->lang->line('ecommerce'); ?></span>
                        </a>
                        <ul class="sub-menu">
                            <li <?php if($this->module_name == $this->lang->line('order')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>order">
                                    <span><?php echo $this->lang->line('order'); ?></span>
                                </a>
                            </li>
                            <li <?php if($this->module_name == $this->lang->line('kategori_produk')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>kategori_produk">
                                    <span><?php echo $this->lang->line('kategori_produk'); ?></span>
                                </a>
                            </li>
                            <li <?php if($this->module_name == $this->lang->line('produk')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>produk">
                                    <span><?php echo $this->lang->line('produk'); ?></span>
                                </a>
                            </li>
                            <li <?php if($this->module_name == $this->lang->line('lokasi_ukm')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>lokasi_ukm">
                                    <span><?php echo $this->lang->line('lokasi_ukm'); ?></span>
                                </a>
                            </li>
                            <?php /*<li <?php if($this->module_name == $this->lang->line('promoimg')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>promoimg">
                                    <span><?php echo $this->lang->line('promoimg'); ?></span>
                                </a>
                            </li>
                            <li <?php if($this->module_name == $this->lang->line('shopsliders')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>shopsliders">
                                    <span><?php echo $this->lang->line('shopsliders'); ?></span>
                                </a>
                            </li>*/ ?>
                            <li <?php if($this->module_name == $this->lang->line('informationpage')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>informationpage">
                                    <span><?php echo $this->lang->line('informationpage'); ?></span>
                                </a>
                            </li>
                            <li <?php if($this->module_name == $this->lang->line('ukm')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>ukm">
                                    <span><?php echo $this->lang->line('ukm'); ?></span>
                                </a>
                            </li>
                            <?php /* <li <?php if($this->module_name == $this->lang->line('customerservicepage')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>customerservicepage">
                                    <span><?php echo $this->lang->line('customerservicepage'); ?></span>
                                </a>
                            </li> */?>
                        </ul>
                    </li>

                    <li <?php
                            if($this->module_name == $this->lang->line('pengiriman')){ echo 'class="open"';} 
                            if($this->module_name == $this->lang->line('kategori_pengiriman')){ echo 'class="open"';} 
                    ?>>
                        <a href="javascript:void(0)">
                            <i class="toggle-accordion"></i>
                            <i class="ti-world"></i>
                            <span><?php echo $this->lang->line('pengiriman'); ?></span>
                        </a>
                        <ul class="sub-menu">
                            <li <?php if($this->module_name == $this->lang->line('pengiriman')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>pengiriman">
                                    <span><?php echo $this->lang->line('pengiriman'); ?></span>
                                </a>
                            </li>
                            <li <?php if($this->module_name == $this->lang->line('kategori_pengiriman')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>kategori_pengiriman">
                                    <span><?php echo $this->lang->line('kategori_pengiriman'); ?></span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li <?php
                            if($this->module_name == $this->lang->line('orderproduct_ukm')){ echo 'class="open"';} 
                    ?>>
                        <a href="javascript:void(0)">
                            <i class="toggle-accordion"></i>
                            <i class="ti-calendar"></i>
                            <span><?php echo $this->lang->line('laporan'); ?></span>
                        </a>
                        <ul class="sub-menu">
                            <li <?php if($this->module_name == $this->lang->line('orderproduct_ukm')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>orderproduct_ukm">
                                    <span><?php echo $this->lang->line('orderproduct_ukm'); ?></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                     
                    <li <?php if(
                            $this->module_name == $this->lang->line('users')
                            || $this->module_name == $this->lang->line('members')
                            ){ echo 'class="open"';} ?>>
                        <a href="javascript:void(0)">
                            <i class="toggle-accordion"></i>
                            <i class="fa fa-users"></i>
                            <span><?php echo $this->lang->line('peoples'); ?></span>
                        </a>
                        <ul class="sub-menu">
                            <li <?php if($this->module_name == $this->lang->line('users')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>users">
                                    <span><?php echo $this->lang->line('users'); ?></span>
                                </a>
                            </li>
                            <li <?php if($this->module_name == $this->lang->line('members')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>members">
                                    <span><?php echo $this->lang->line('members'); ?></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                     
                    <li <?php if(
                            $this->module_name == $this->lang->line('logs')
                            || $this->module_name == $this->lang->line('bank')
                            || $this->module_name == $this->lang->line('shopconfiguration')
                            ){ echo 'class="open"';} ?>>
                        <a href="javascript:void(0)">
                            <i class="toggle-accordion"></i>
                            <i class="fa fa-gear"></i>
                            <span><?php echo $this->lang->line('utility'); ?></span>
                        </a>
                        <ul class="sub-menu">
                            <li <?php if($this->module_name == $this->lang->line('logs')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>logs">
                                    <span><?php echo $this->lang->line('logs'); ?></span>
                                </a>
                            </li>
                            <li <?php if($this->module_name == $this->lang->line('bank')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>bank">
                                    <span><?php echo $this->lang->line('bank'); ?></span>
                                </a>
                            </li>
                            <li <?php if($this->module_name == $this->lang->line('shopconfiguration')){ echo 'class="active"';} ?>>
                                <a href="<?php echo base_url(); ?>shopconfiguration">
                                    <span><?php echo $this->lang->line('shopconfiguration'); ?></span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li <?php if($this->module_name == $this->lang->line('look_web')){ echo 'class="active"';} ?>>
                         <a href="<?php echo 'http://'.APP_DOMAIN; ?>" target="_blank">
                            <i class="ti-home"></i>
                            <span><?php echo $this->lang->line('look_web'); ?></span>
                        </a>
                    </li>
                 
                </ul>
            </nav>
        </aside>
         
         
        <?php echo isset($content)?$content:''; ?>
     
    </section>
</div>
<script src="<?php echo $this->webconfig['back_base_template']; ?>bootstrap/js/bootstrap.js"></script>
<script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/jquery.slimscroll.min.js"></script>
<script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/jquery.easing.min.js"></script>
<script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/appear/jquery.appear.js"></script>
<script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/jquery.placeholder.js"></script>
<script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/fastclick.js"></script>

<script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/toastr/toastr.min.js"></script>
<script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/icheck/icheck.js"></script>

<script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery.alert.js"></script>
<script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery.slug.js"></script>
<script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery.Jcrop.min.js"></script>
 
 
 
 
<script src="<?php echo $this->webconfig['back_base_template']; ?>js/offscreen.js"></script>
<script src="<?php echo $this->webconfig['back_base_template']; ?>js/main.js"></script>
 
 
 
</body>
</html>