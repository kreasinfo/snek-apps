<section class="main-content">
         
    <div class="content-wrap">
     
        <div class="wrapper">
            <div class="row">
                <div class="col-md-4">
                    <section class="panel no-b">
                    <div class="panel-body">
                    <a href="javascript:;" class="show text-center">
                    <img src="<?php echo $this->webconfig['back_base_template']; ?>img/avatar.jpg" class="avatar avatar-lg img-circle" alt="">
                    </a>
                    <div class="show mt15 mb15 text-center">
                    <div class="h5"><b>Gerald Morris</b>
                    </div>
                    <p class="show text-muted">San Francisco, CA</p>
                    </div>
                    </div>
                    <div class="panel-footer no-p no-b">
                    <div class="row no-m">
                    <div class="col-xs-4 bg-primary p10 text-center brbl">
                    <a href="javascript:;">
                    <span class="ti-heart show mb5"></span>
                    782
                    </a>
                    </div>
                    <div class="col-xs-4 bg-info p10 text-center">
                    <a href="javascript:;">
                    <span class="ti-support show mb5"></span>
                    8,234
                    </a>
                    </div>
                    <div class="col-xs-4 bg-primary p10 text-center brbr">
                    <a href="javascript:;">
                    <span class="ti-mouse show mb5"></span>
                    290,847
                    </a>
                    </div>
                    </div>
                    </div>
                    </section>
                    <section class="widget">
                    <div class="cover rounded" style="background-image: url(<?php echo $this->webconfig['back_base_template']; ?>img/DeathtoStock_Wired5.jpg)"></div>
                    <div class="overlay rounded bg-color"></div>
                    <div class="widget-body">
                    <div class="text-white">
                    <br>
                    <p>Shaw, those twelve beige hooks are joined if I patch a young, gooey mouth.
                    </p>
                    <div class="mb10">
                    <a href="javascript:;" class="text-muted mr15">
                    <i class="ti-comment  mr5"></i>3523
                    </a>
                    <a href="javascript:;">
                    <i class="ti-heart  text-danger mr5"></i>12K
                    </a>
                    </div>
                    <ul class="text-white list-style-none">
                    <span class="fa fa-star text-danger"></span>
                    <span class="fa fa-star text-danger"></span>
                    <span class="fa fa-star text-danger"></span>
                    <span class="fa fa-star-o"></span>
                    <span class="fa fa-star-o"></span>
                    </ul>
                    <a href="javascript:;" class="show small">getbootstrap.com</a>
                    </div>
                    </div>
                    </section>
                    <section class="panel position-relative">
                    <div class="panel-body">
                    <a href="javascript:;" class="pull-left mr15">
                    <img src="<?php echo $this->webconfig['back_base_template']; ?>img/avatar.jpg" class="avatar avatar-sm img-circle" alt="">
                    </a>
                    <div class="overflow-hidden">
                    <p class="no-m">
                    <b>Gerald Morris</b>
                    <i class="fa fa-star toggle-active ml5 mr5 active"></i>
                    </p>
                    <small>Apple</small>
                    </div>
                    <div class="mt10 mb10">
                    <a class="btn btn-social btn-xs btn-facebook mr5"><i class="fa fa-facebook"></i>Facebook </a>
                    <a class="btn btn-social btn-xs btn-twitter mr5"><i class="fa fa-twitter"></i>Twitter </a>
                    <a class="btn btn-social btn-xs btn-github mr5"><i class="fa fa-github"></i>Github </a>
                    </div>
                    <p class="small">
                    <i class="fa fa-circle text-primary mr5"></i>Hope you can help. How can I get the sidebar widgets to appear on post pages? At the moment they only appear on the homepage.
                    </p>
                    </div>
                    </section>
                    <section class="panel panel-primary">
                    <header class="panel-heading">
                    <div class="h5">
                    <i class="fa fa-facebook-square mr5"></i>
                    <b>Facebook</b>
                    </div>
                    </header>
                    <footer class="panel-footer text-center">
                    <div class="row">
                    <div class="col-xs-4">
                    <div class="small show text-uppercase text-muted">Friends</div>
                    <div class="h4 no-m"><b>5434</b>
                    </div>
                    </div>
                    <div class="col-xs-4">
                    <div class="small show text-uppercase text-muted">Photos</div>
                    <div class="h4 no-m"><b>894</b>
                    </div>
                    </div>
                    <div class="col-xs-4">
                    <div class="small show text-uppercase text-muted">Messages</div>
                    <div class="h4 no-m"><b>08</b>
                    </div>
                    </div>
                    </div>
                    </footer>
                    </section>
                </div>
                <div class="col-md-4">
                    <section class="panel panel-primary no-b">
                    <header class="panel-heading clearfix brtl brtr">
                    <a href="javascript:;" class="pull-left mr15">
                    <img src="<?php echo $this->webconfig['back_base_template']; ?>img/avatar.jpg" class="avatar avatar-md img-circle bordered" alt="">
                    </a>
                    <div class="overflow-hidden">
                    <a href="javascript:;" class="h4 show no-m pt10">Gerald Morris</a>
                    <small>Human Resources Manager</small>
                    </div>
                    </header>
                    <div class="list-group">
                    <a href="javascript:;" class="list-group-item">
                    <span class="badge bg-primary pull-right">65</span>
                    <i class="ti-comment mr10 text-muted"></i>Messages
                    </a>
                    <a href="javascript:;" class="list-group-item">
                    <span class="badge bg-info pull-right">89</span>
                    <i class="ti-calendar mr10 text-muted"></i>Recent Activity
                    </a>
                    <a href="javascript:;" class="list-group-item">
                    <span class="badge bg-danger pull-right">8</span>
                    <i class="ti-bell mr10 text-muted"></i>Notifications
                    </a>
                    <a href="javascript:;" class="list-group-item">
                    <span class="badge bg-default pull-right">28</span>
                    <i class="ti-user mr10 text-muted"></i>Friend Invites
                    </a>
                    </div>
                    </section>
                    <section class="widget bg-danger">
                    <div class="cover rounded" style="background-image: url(<?php echo $this->webconfig['back_base_template']; ?>img/DeathtoStock_Wired5.jpg)"></div>
                    <div class="overlay rounded bg-danger"></div>
                    <div class="widget-header text-center">
                    <a href="javascript:;"><i class="ti-plus text-white pull-right"></i></a>
                    <a href="javascript:;"><i class="ti-menu text-white pull-left"></i></a>
                    <span>Quote</span>
                    </div>
                    <div class="widget-body">
                    <blockquote class="no-p no-m">
                    <p>This is dummy copy. It's Greek to you. Unless, of course, you're Greek, in which case, it really makes no sense. Why, you can't even read it! It is strictly for mock-ups. You may mock it up as strictly as you wish.</p>
                    <small class="text-white">Steve Jobs, CEO Apple</small>
                    </blockquote>
                    </div>
                    <footer class="widget-footer">
                    <p class="small text-right">
                    <a href="javascript:;" class="text-white mr10">Reply</a>
                    <a href="javascript:;" class="text-white"><i class="ti-heart mr5"></i>Like</a>
                    </p>
                    </footer>
                    </section>
                    <section class="widget bg-primary">
                    <div class="widget-body">
                    <a href="javascript:;" class="pull-left mr15">
                    <img src="<?php echo $this->webconfig['back_base_template']; ?>img/avatar.jpg" class="avatar avatar-md img-circle bordered" alt="">
                    </a>
                    <div class="overflow-hidden">
                    <div>
                    <b>Gerald Morris</b>
                    </div>
                    <small class="show">San Francisco, CA</small>
                    <small class="show">Interactive UX Developer</small>
                    </div>
                    </div>
                    </section>
                    <section class="panel panel-success bg-success">
                    <div class="panel-body">
                    <a href="javascript:;" class="pull-left mr15">
                    <img src="<?php echo $this->webconfig['back_base_template']; ?>img/avatar.jpg" class="avatar avatar-sm img-circle" alt="">
                    </a>
                    <div class="overflow-hidden">
                    <b>Gerald Morris</b>
                    <small class="pull-right">Yesterday, 13:48</small>
                    <div class="show small">San Francisco, CA</div>
                    <div class="mt15"></div>
                    <blockquote class="small no-m no-p">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Frater et T. Sed ad bona praeterita redeamus.</blockquote>
                    </div>
                    </div>
                    </section>
                </div>
                <div class="col-md-4">
                    <section class="panel panel-default  post-comments">
                    <div class="panel-heading">
                    Discussion
                    </div>
                    <div class="media p15">
                    <a class="pull-left" href="javascript:;">
                    <img class="media-object avatar avatar-sm" src="<?php echo $this->webconfig['back_base_template']; ?>img/avatar.jpg" alt="">
                    </a>
                    <div class="comment">
                    <div class="comment-author h6 no-m">
                    <a href="javascript:;"><b>Jane Doe</b></a>
                    </div>
                    <div class="comment-meta small">MAY 2014, 19:20</div>
                    <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Frater et T. Sed ad bona praeterita redeamus.
                    </p>
                    <p class="small">
                    <a href="javascript:;" class="text-muted mr10"><i class="ti-comment mr5"></i>Comment</a>
                    <a href="javascript:;" class="text-muted mr10"><i class="ti-share mr5"></i>Share</a>
                    <a href="javascript:;" class="mr10 text-danger"><i class="ti-heart mr5"></i>Like</a>
                    <a href="javascript:;" class="text-muted mr10"><i class="ti-more mr5"></i>More</a>
                    <i class="ti-bookmark text-warning" data-toggle="tooltip" data-original-title="View tags"></i>
                    </p>
                    <hr>
                    <div class="media">
                    <a class="pull-left" href="javascript:;">
                    <img class="media-object avatar avatar-sm" src="<?php echo $this->webconfig['back_base_template']; ?>img/face3.jpg" alt="">
                    </a>
                    <div class="comment">
                    <div class="comment-author h6 no-m">
                    <a href="javascript:;"><b>John Doe</b></a>
                    </div>
                    <div class="comment-meta small">MAY 2014, 22:04</div>
                    <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Frater et T. Sed ad bona praeterita redeamus.
                    </p>
                    <p class="small">
                    <a href="javascript:;" class="text-muted mr10"><i class="ti-comment mr5"></i>Comment</a>
                    <a href="javascript:;" class="text-muted mr10"><i class="ti-share mr5"></i>Share</a>
                    <a href="javascript:;" class="mr10 text-danger"><i class="ti-heart mr5"></i>Like</a>
                    <a href="javascript:;" class="text-muted mr10"><i class="ti-more mr5"></i>More</a>
                    </p>
                    </div>
                    </div>
                     
                    <hr>
                    <div class="media">
                    <a class="pull-left" href="javascript:;">
                    <img class="media-object avatar avatar-sm" src="<?php echo $this->webconfig['back_base_template']; ?>img/face4.jpg" alt="">
                    </a>
                    <div class="comment">
                    <div class="comment-author h6 no-m">
                    <a href="javascript:;"><b>Jane Doe</b></a>
                    </div>
                    <div class="comment-meta small">10 JUNE 2014, 07:28</div>
                    <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Frater et T. Sed ad bona praeterita redeamus.
                    </p>
                    <p class="small">
                    <a href="javascript:;" class="text-muted mr10"><i class="ti-comment mr5"></i>Comment</a>
                    <a href="javascript:;" class="text-muted mr10"><i class="ti-share mr5"></i>Share</a>
                    <a href="javascript:;" class="mr10 text-danger"><i class="ti-heart mr5"></i>Like</a>
                    <a href="javascript:;" class="text-muted mr10"><i class="ti-more mr5"></i>More</a>
                    </p>
                    </div>
                    <hr>
                     
                    </div>
                    </div>
                    <div class="media">
                    <a class="pull-left" href="javascript:;">
                    <img class="media-object avatar avatar-sm" src="<?php echo $this->webconfig['back_base_template']; ?>img/face5.jpg" alt="">
                    </a>
                    <div class="comment">
                    <div class="comment-author h6 no-m">
                    <a href="javascript:;"><b>Jane Doe</b></a>
                    </div>
                    <div class="comment-meta small">18 JUNE 2014, 19:20</div>
                    <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Frater et T. Sed ad bona praeterita redeamus.
                    </p>
                    <p class="small">
                    <a href="javascript:;" class="text-muted mr10"><i class="ti-comment mr5"></i>Comment</a>
                    <a href="javascript:;" class="text-muted mr10"><i class="ti-share mr5"></i>Share</a>
                    <a href="javascript:;" class="mr10 text-danger"><i class="ti-heart mr5"></i>Like</a>
                    <a href="javascript:;" class="text-muted mr10"><i class="ti-more mr5"></i>More</a>
                    </p>
                    </div>
                    </div>
                    </div>
                    <div class="panel-footer">
                    <form role="form" class="form-horizontal">
                    <div class="form-group no-m">
                    <div class="input-group">
                    <input type="text" class="form-control input-sm no-border" placeholder="Join the conversation">
                    <span class="input-group-btn">
                    <button class="btn btn-default btn-sm" type="button">POST</button>
                    </span>
                    </div>
                    </div>
                    </form>
                    </div>
                    </section>
                </div>
            </div>
        </div>
     
    </div>
     
    <a class="exit-offscreen"></a>
</section>