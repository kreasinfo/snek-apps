<?php
class Dashboard extends CI_Controller {
	var $webconfig;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->lang->load('global', 'bahasa');
		$this->load->sharedModel('LoginModel');
		
		$this->load->library('session');
		$this->webconfig = $this->config->item('webconfig');
		$this->module_name = $this->lang->line('dashboard');		

		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login');
		}
	}

	function index()
	{
		$tdata = array();

		## LOAD LAYOUT ##
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		$this->load->sharedView('template', $ldata);
	}
}