<!DOCTYPE html>
<html>
<head>
   
</head>
<div style="min-width:700px;max-width:700px">
	<section class="panel position-relative">
		<div class="panel-body">
			<h2><?php echo isset($lists[0]['name'])?$lists[0]['name']:''; ?></h2>
			<hr>
			<p class="small">
				<?php echo isset($lists[0]['description'])?stripcslashes($lists[0]['description']):''; ?>
			</p>
			<hr>
			<p class="text-left">
				<?php if(isset($lists[0]['images']) && $lists[0]['images'] != '' && count($lists[0]['images']) > 0){ ?>
                    <?php foreach ($lists[0]['images'] as $key => $list) { ?>
                    
                        <?php if(isset($list['path']) && $list['path'] != '' && count($list['path']) > 0){ ?>
                            
                                <img width="85"  src="<?php echo thumb_image($list['path'],'360x203', 'product'); ?>" alt="images" border="0" />
                            
                        <?php }else{ ?>
                            <img src="<?php echo $this->webconfig['base_template']; ?>img/no_image_news.jpg" width="600"/>
                        <?php } ?>
                       
                    <?php } ?>
                    <br class="clear"/>
                <?php } ?>
			</p>
			<hr>
			<p class="small">
				<?php echo isset($lists[0]['body'])?stripcslashes($lists[0]['body']):''; ?>
			</p>
		</div>
	</section>
</div>
</html>

	