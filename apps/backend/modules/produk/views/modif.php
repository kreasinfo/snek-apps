<script type="text/javascript">
jQuery(document).ready(function($) {
    $("#tagSelect").select2({
        tags:[]
    });
    // $('input#seo_title').limit('<?php echo $this->webconfig["limit_seo_title"];?>','#charsLeft');
    // $('textarea#seo_description').limit('<?php echo $this->webconfig["limit_seo_description"];?>','#charsLeft_textarea');
    $('#price').autoNumeric('init', {aSign:'Rp ', mDec: '', aSep: '.', aDec: ','});
    $('#discount').autoNumeric('init', {aSign:'', mDec: '', aSep: '.', aDec: ','});
    $('.angka').numeric();
    

    $("#list_kategori_produk").select2({
        placeholder: "<?php echo $this->lang->line('label_pilih_kategori'); ?>",
        allowClear: true,
        initSelection: function(element, callback) {
            callback({id: '<?php echo isset($lists[0]['id_kategori_produk']['id'])?$lists[0]['id_kategori_produk']['id']:''; ?>', text: '<?php echo isset($lists[0]['id_kategori_produk']['name'])?$lists[0]['id_kategori_produk']['name']:''; ?>' });
        },
        ajax: {
            url: '<?php echo base_url(); ?>webservices/kategori_produk/',
            dataType: 'json',
            data: function (term, page) {
                return {
                    q: term
                };
            },
            results: function (data, page) {
                return { results: data };
            } ,
            cache: false
        }
    }).on("change", function(e) {
      $('#kategori_produk_text').val($('.select2-chosen').text());
    });
    tinymce.init({
        selector: "textarea#body",
        theme: "modern",
        height: 400,
        plugins: [
             "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
             "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
             "save table contextmenu directionality emoticons template paste textcolor jbimages"
       ],
       toolbar: "template pagebreak | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages | print preview fullpage | forecolor backcolor emoticons",
       style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}}
        ],

        

      content_css : "<?php echo $this->webconfig['frontend_template']; ?>css/nicdark_style.css?ver=0,<?php echo $this->webconfig['frontend_template']; ?>css/nicdark_responsive.css?ver=0,<?php echo $this->webconfig['frontend_template']; ?>css/custome_back.css?ver=0",
      templates : [
                {
                    title: 'Template 1', 
                    description: 'Template halaman dengan list gambar yang menarik.', 
                    url: '<?php echo base_url(); ?>template/template_1/'
                },
                {
                    title: 'Template 2', 
                    description: 'Template halaman dengan satu gambar di tengah.', 
                    url: '<?php echo base_url(); ?>template/template_2/'
                },
                {
                    title: 'Template 3', 
                    description: 'Template halaman dengan list gambar di samping.', 
                    url: '<?php echo base_url(); ?>template/template_3/'
                },
        ],
        template_popup_width:900,
        
        relative_urls : false,
     });
});


    function delimg(id){
        var isi = {id:id};
        
        var images = $('.abcd').length;
        if(images > 1){
            $.prompt('<?php echo $this->lang->line('alert_delete_img');?>' ,{ 
                prefix:'jqismooth',
                buttons: { "Ok": true,"Tidak": false},
                submit: function(e,v,m,f){
                    if(e == true){
                        $.ajax({
                            type: "POST",
                            url: '<?php echo base_url().$this->router->class; ?>/delete_img',
                            data: isi,
                            success: function(data){
                                console.log(data);
                                if(data == 'success'){
                                    $('#abcd'+id).remove();
                                    $('#xyz'+id).remove();
                                }
                            }
                        });
                    }
                    $.prompt.close();
                } 
            });
        }else{
            $.prompt('<?php echo $this->lang->line('alert_info_del_img');?>' ,{ 
                prefix:'jqismooth',
                buttons: { "Ok": true}
            });
        }
    }
</script>
<section class="main-content">
 
    <div class="content-wrap">
     
        <div class="wrapper">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo base_url('/'); ?>"><i class="ti-home mr5"></i><?php echo $this->lang->line('dashboard'); ?></a>
                </li>
                <li>
                    <a href="<?php echo base_url().$this->router->class; ?>"><?php echo ucfirst($this->module_name); ?></a>
                </li>
                <li class="active"><?php echo $this->lang->line('modif'); ?></li>
            </ol>

            <div class="panel">
                <header class="panel-heading">
                    <div class="row">
                        <div class="col-xs-10">
                            <h5 class="text-uppercase no-m"><?php echo strtoupper($this->module_name); ?></h5>
                            <small><?php echo $this->lang->line('kategory_teks'); ?></small>
                        </div>
                        <div class="col-xs-2 text-right">
                            <a class="btn btn-warning btn-xs" href="<?php echo base_url().$this->router->class; ?>"  title="<?php echo $this->lang->line('add'); ?>">
                                <i class="ti-plus"></i>
                                <?php echo $this->lang->line('list'); ?>
                            </a>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal" name="form" method="POST" action="" enctype="multipart/form-data" onsubmit="return validate()">
                        <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
                            <?php foreach($error_hash as $inp_err){ ?>
                                <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                                    });
                                </script>
                            <?php } ?>
                        <?php } ?>
                        
                        <?php if(isset($success)){ ?>
                            <script type="text/javascript">
                            jQuery(document).ready(function($) {
                                toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                                });
                            </script>
                        <?php } ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('kategori_produk'); ?> <span>*</span></label>
                            <div class="col-sm-5">
                                <!-- <input type="hidden" id="list_kategori_produk" name="id_kategori_produk" value="<?php echo isset($lists[0]['id_kategori_produk']['id'])?$lists[0]['id_kategori_produk']['id']:''; ?>" class="form-control" />
                                <input type="hidden" id="kategori_produk_text" name="id_kategori_produk_text" value="<?php echo isset($lists[0]['id_kategori_produk']['name'])?$lists[0]['id_kategori_produk']['name']:''; ?>" class="form-control" /> -->
                                <select name="id_kategori_produk" class="form-control" id="id_kategori_produk" style="padding: 3px;">
                                    <option value="">-- <?php echo $this->lang->line('pilih_kategori'); ?> --</option>
                                    <?php foreach ($categories as $key => $value){ ?>
                                        <option <?php echo (isset($lists[0]['id_kategori_produk']) && $lists[0]['id_kategori_produk'] == $value['id'])?"selected='selected'":""; ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                        <?php echo (isset($value['child']) && count($value['child']) > 0)?getChannelChild($value['child'], isset($lists[0]['id_kategori_produk'])?$lists[0]['id_kategori_produk']:'',$value['name']):""; ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('name'); ?> <span>*</span></label>
                            <div class="col-sm-5">
                                <input type="text" id="name" name="name" value='<?php echo isset($lists[0]['name'])?$lists[0]['name']:''; ?>' size="50" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('ukm'); ?> <span>*</span></label>
                            <div class="col-sm-5">
                                <select name="id_ukm" class="form-control" id="id_ukm" style="padding: 3px;">
                                    <option value="">-- <?php echo $this->lang->line('pilih_ukm'); ?> --</option>
                                    <?php foreach ($ukm as $key => $value){ ?>
                                        <option <?php echo (isset($lists[0]['id_ukm']) && $lists[0]['id_ukm'] == $value['id'])?"selected='selected'":""; ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('lokasi_ukm'); ?> <span>*</span></label>
                            <div class="col-sm-5">
                                <select name="id_lokasi_ukm" class="form-control" id="id_lokasi_ukm" style="padding: 3px;">
                                    <option value="">-- <?php echo $this->lang->line('pilih_lokasi_ukm'); ?> --</option>
                                    <?php foreach ($lokasiukm as $key => $value){ ?>
                                        <option <?php echo (isset($lists[0]['id_lokasi_ukm']) && $lists[0]['id_lokasi_ukm'] == $value['id'])?"selected='selected'":""; ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('slug'); ?> <span>*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="slug" value='<?php echo isset($lists[0]['slug'])?$lists[0]['slug']:''; ?>' readonly="readonly" class="form-control ArticleSlug">
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $("#name").slug();
                                });
                            </script>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('description'); ?> </label>
                            <div class="col-sm-8">
                                <textarea type="text" name="description" class="form-control"><?php echo isset($lists[0]['description'])?$lists[0]['description']:''; ?></textarea>
                            </div>
                        </div>
                        <?php /* ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('seo_title'); ?> </label>
                            <div class="col-sm-5">
                                <input type="text" id="seo_title" name="seo_title" value='<?php echo isset($lists[0]['seo_title'])?$lists[0]['seo_title']:''; ?>' class="form-control">
                                <?php echo $this->lang->line('you_have'); ?> <b><span id="charsLeft"></span></b> <?php echo $this->lang->line('char_left'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('seo_description'); ?> </label>
                            <div class="col-sm-8">
                                <textarea type="text" id="seo_description" name="seo_description" class="form-control"><?php echo isset($lists[0]['seo_description'])?stripcslashes($lists[0]['seo_description']):''; ?></textarea>
                                <?php echo $this->lang->line('you_have'); ?> <b><span id="charsLeft_textarea"></span></b> <?php echo $this->lang->line('char_left'); ?>
                            </div>
                        </div>
                        <?php */ ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('body'); ?> <span>*</span></label>
                            <div class="col-sm-10">
                                <textarea type="text" id="body" name="body" class="form-control"><?php echo isset($lists[0]['body'])?stripcslashes($lists[0]['body']):''; ?></textarea>
                                 <div id="image_tinymce">
                                    <?php
                                        if(isset($image_tinymce) && count($image_tinymce) > 0){
                                            foreach ($image_tinymce as $key => $value) {
                                            ?>
                                                <input type="hidden" name="image_tinymce[]" value='<?php echo $value; ?>' >
                                            <?php
                                            }
                                        }else{
                                            ?>
                                                <input type="hidden" name="image_tinymce[]" value='' >
                                            <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('price'); ?> <span>*</span></label>
                            <div class="col-sm-2">
                                <input class="form-control" type="text" name="price" value='<?php echo isset($lists[0]['price'])?$lists[0]['price']:''; ?>' id="price" data-a-sign="Rp " size="20" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('berat'); ?> <span>*</span></label>
                            <div class="col-sm-2">
                                <input class="form-control angka" type="text" name="berat" value='<?php echo isset($lists[0]['berat'])?$lists[0]['berat']:''; ?>' size="20" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('discount'); ?></label>
                            <div class="col-sm-2">
                                <input class="form-control" type="text" name="discount" value='<?php echo isset($lists[0]['discount'])?$lists[0]['discount']:''; ?>' id="discount" size="20" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('size'); ?> <span>*</span></label>
                            <div class="col-sm-10">
                                <div id="loadingimages" style="display:none; text-align:center; margin:10px 5px 10px 5px;"><img src="<?php echo $this->webconfig['images']; ?>loading.gif" alt="loading..." /></div>
                                <script type="text/javascript">
                                    function deleteData(obj){
                                        $(obj).parent().parent().remove();
                                    }
                                    
                                    function deleteSizeById(obj,iddata){
                                        $('#loadingimages').show();
                                        var info = 'id=' + iddata;
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url().$this->router->class; ?>/deletesize/",
                                            data: info,
                                            success: function(response){
                                                if(response == 'success'){
                                                    $(obj).parent().parent().remove();
                                                }
                                                $('#loadingimages').hide();
                                            }
                                        }); 
                                    }
                                    function tambahUkuran(){
                                        var blockhtml = '';
                                        blockhtml += '<tr>';
                                            blockhtml += '<td>';
                                                blockhtml += '<input class="form-control" type="text" name="size[]" size="50">';
                                            blockhtml += '</td>';
                                            blockhtml += '<td>';
                                                blockhtml += '<input class="form-control" type="text" name="stock[]" size="10">';
                                            blockhtml += '</td>';
                                            blockhtml += '<td>';
                                                 blockhtml += '<a class="btn btn-danger btn-xs" onclick="deleteData(this)" href="javascript:void(0)">';
                                                     blockhtml += '<i class="ti-trash"></i>';
                                                     blockhtml += '<?php echo $this->lang->line('delete');?>';
                                                 blockhtml += '</a>';
                                            blockhtml += '</td>';
                                        blockhtml += '</tr>';
                                        $('#tableukuran tbody tr:last').after(blockhtml);
                                    }
                                    function validate(){
                                        var error_size_msg = 0;
                                        var error_stock_msg = 0;
                                        if($('#tableukuran tbody tr').length > 0){
                                            $.each($('#tableukuran tbody tr input[type="text"][name="size[]"]'), function(i,data) {
                                                if(data.value == ''){
                                                    alert('Silahkan mengisi data size.');
                                                    error_size_msg = parseInt(error_size_msg) + 1;
                                                    return false;
                                                }
                                            });
                                            $.each($('#tableukuran tbody tr input[type="text"][name="stock[]"]'), function(i,data) {
                                                if(data.value == ''){
                                                    alert('Silahkan mengisi data stock.');
                                                    error_stock_msg = parseInt(error_stock_msg) + 1;
                                                    return false;
                                                }
                                            });
                                        }
                                        if(error_size_msg > 0 || error_stock_msg > 0){
                                            return false;
                                        }else{
                                            return true;
                                        }
                                    }
                                </script>
                                <table class="table_list" id="tableukuran">
                                    <thead>
                                        <tr>
                                            <td>
                                                <?php echo $this->lang->line('rasa'); ?>
                                            </td>
                                            <td width="100">
                                                <?php echo $this->lang->line('stock'); ?>
                                            </td>
                                            <td width="100">
                                                <?php echo $this->lang->line('option'); ?>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(isset($lists[0]['produk_ukuran']) && (count($lists[0]['produk_ukuran']) > 0)) { ?>
                                            <?php $si = 0; foreach($lists[0]['produk_ukuran'] as $listsize){ $si++; ?>
                                                <input type="hidden" name="sizeidold[]" value='<?php echo isset($listsize['id'])?$listsize['id']:''; ?>'>
                                                <?php if($si == 0){ ?>
                                                    <tr>
                                                        <td>
                                                            <input class="form-control" type="text" name="sizeold[]" value='<?php echo isset($listsize['size'])?$listsize['size']:''; ?>' size="50">
                                                        </td>
                                                        <td>
                                                            <input class="form-control angka" type="text" name="stockold[]" value='<?php echo isset($listsize['stock'])?$listsize['stock']:''; ?>' size="10">
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                <?php }else{ ?>
                                                    <tr>
                                                        <td>
                                                            <input class="form-control" type="text" name="sizeold[]" value='<?php echo isset($listsize['size'])?$listsize['size']:''; ?>' size="50">
                                                        </td>
                                                        <td>
                                                            <input class="form-control angka" type="text" name="stockold[]" value='<?php echo isset($listsize['stock'])?$listsize['stock']:''; ?>' size="10">
                                                        </td>
                                                        <td>
                                                            <a class="btn btn-danger btn-xs" onclick="deleteSizeById(this,<?php echo isset($listsize['id'])?$listsize['id']:''; ?>)" href="javascript:void(0)">
                                                                <i class="ti-trash"></i>
                                                                <?php echo $this->lang->line('delete');?>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php }else{ ?>
                                            <tr>
                                                <td>
                                                    <input class="form-control" type="text" name="size[]" size="50">
                                                </td>
                                                <td>
                                                    <input class="form-control angka" type="text" name="stock[]" size="10">
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <br/>
                                <a class="btn btn-warning btn-xs" onclick="tambahUkuran()" href="javascript:void(0)">
                                    <i class="ti-plus"></i>
                                    <?php echo $this->lang->line('add');?>
                                </a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('tags'); ?> </label>
                            <div class="col-sm-8">
                                <input class="form-control" type="text" id="tagSelect" name="tags" value="<?php echo isset($lists[0]['tags'])?$lists[0]['tags']:''; ?>" />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('status'); ?> <span>*</span></label>
                            <div class="col-sm-4 radio">
                                <label>
                                    <input type="radio" name="status" value="1" <?php if(isset($lists[0]['status']) && $lists[0]['status'] == 1){ echo "checked=checked"; }?> />
                                    <?php echo $this->lang->line('aktif'); ?>
                                </label>
                                &nbsp
                                <label>
                                    <input type="radio" name="status" value="0" <?php if(!isset($lists[0]['status'])){ echo "checked=checked"; } ?> <?php if(isset($lists[0]['status']) && $lists[0]['status'] == 0){ echo "checked=checked"; }?> />
                                    <?php echo $this->lang->line('nonaktif'); ?> 
                                </label>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('status_produk'); ?> <span>*</span></label>
                            <div class="col-sm-4 radio">
                                <label>
                                    <input type="radio" name="status_produk" value="1" <?php if(isset($lists[0]['status_produk']) && $lists[0]['status_produk'] == 1){ echo "checked=checked"; }?> />
                                    <?php echo $this->lang->line('dalam_luar_kota'); ?>
                                </label>
                                &nbsp
                                <label>
                                    <input type="radio" name="status_produk" value="0" <?php if(!isset($lists[0]['status_produk'])){ echo "checked=checked"; } ?> <?php if(isset($lists[0]['status_produk']) && $lists[0]['status_produk'] == 0){ echo "checked=checked"; }?> />
                                    <?php echo $this->lang->line('dalam_kota'); ?>
                                </label>
                            </div>
                        </div>
                        <div class="form-group footertable">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-5 text-left">
                                <input class="btn btn-danger btn-sm mr5" type="submit" value="<?php echo $this->lang->line('navigation_kirim'); ?>">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
     
    </div>
 
    <a class="exit-offscreen"></a>
</section>