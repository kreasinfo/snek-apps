<script type="text/javascript">
jQuery(document).ready(function($) {
    $(".hidelog").hide();
    $(".images").hide();

    /* CROP IMG */
    var oFReader = null;
    var image = null;
    var globalResizedWidth = '<?php if($this->webconfig["width_resize"] != ''){ echo $this->webconfig["width_resize"]; }else{ echo 0; } ?>';
    var jcrop_api, globalWidth, globalHeight, globalConfDimension;

    $("#uploadImage").val("");

    $("#uploadImage").change(function(){
        $(this).parent().find(".image_block").remove();
        $.when( createImageElement(this) ).done( cropImageElement(this) );
    });
    
    function createImageElement(obj) {
        var html = '<div class="image_block">';
        html += '<input type="hidden" class="x1" name="x1" value="0" />';
        html += '<input type="hidden" class="y1" name="y1" value="0" />';
        html += '<input type="hidden" class="x2" name="x2" value="0" />';
        html += '<input type="hidden" class="y2" name="y2" value="0" />';
        html += '<br><img class="image_preview" style="width:'+globalResizedWidth+'px" />';
        html += '</div>';
        $(obj).after(html);
    }

    function cropImageElement(obj) {
        var ext = getExtension($(obj).val());
        if(ext == "jpg" || ext == "jpeg" || ext == "png" || ext == "gif"){
            var dimensionconf = '<?php if(count($this->webconfig["image_product"]) > 0){ echo $this->webconfig["image_product"][0]; } ?>';
            var separator = dimensionconf.split("x");
            var dimheight = separator[1];
            var dimwidth = separator[0];
            if(dimwidth != 'undefined' || dimheight != 'undefined'){
                doLoadCropping(obj, dimwidth, dimheight);
            }
            return false;
        }else{
            alert('<?php echo $this->lang->line("msg_error_extension"); ?>');
            $('.image_block').remove();
            $("#uploadImage").val("");
            return false;
        }
    }

    function doLoadCropping(obj, dimwidth, dimheight) {
        if(oFReader !=null){
            oFReader = null;
        }
        
        var min_width = dimwidth;
        var min_height = dimheight;
        var objFile = obj.files[0];
        var max_foto_mb = '<?php echo $this->webconfig['max_foto_filesize']; ?>';
        var max_foto_byte = parseInt(max_foto_mb)*1048576; //convert MB to Byte
        
        if(objFile.size > max_foto_byte) {
            $(obj).parent().find(".image_block").remove();
            $(obj).val("");
            alert("<?php echo $this->lang->line('label_error_ukuran'); ?>");
            $('.image_block').remove();
            $("#uploadImage").val("");
        } else {
            // prepare HTML5 FileReader
            oFReader = new FileReader();
            image  = new Image();
            oFReader.readAsDataURL(objFile);
            
            oFReader.onload = function (_file) {
                image.src    = _file.target.result;
                image.onload = function() {
                        globalWidth = this.width;
                        globalHeight = this.height;
                        
                        $(obj).parent().find(".image_preview").attr("src", this.src);

                        if(globalWidth < min_width || globalHeight < min_height) {
                                $(obj).parent().find(".image_block").remove();
                                $(obj).val("");
                                alert("<?php echo $this->lang->line('label_error_dimensi'); ?>");
                        } else {
                            cropImage(globalWidth, globalHeight, min_width, min_height, $(obj).parent().find(".image_preview"));
                        }
                };

                image.onerror= function() {
                    alert('Invalid file type: '+ objFile.type);
                };     
                
            }
        }
    }

    function cropImage(width, height, minwidth, minheight, obj) {
        var resizedWidth = globalResizedWidth;
        var resizedHeight = (resizedWidth * height) / width;
        var resizedMinWidth = (minwidth * resizedWidth) / width;
        var resizedMinHeight = (minheight * resizedHeight) / height;
        
        if(minwidth != '' || minheight != ''){
            $(obj).Jcrop({
                setSelect: [ 0, 0, resizedMinWidth, resizedMinHeight ],
                minSize: [ resizedMinWidth, resizedMinHeight ],
                onSelect: updateCoords,
                allowSelect: false,
                bgFade: true,
                bgOpacity: 0.4,
                aspectRatio: minwidth / minheight
            },function(){
                jcrop_api = this;
            });
        }
    }
    function updateCoords(c){
        $('.x1').val(c.x);
        $('.y1').val(c.y);
        $('.x2').val(c.x2);
        $('.y2').val(c.y2);
        $('.w').val(c.w);
        $('.h').val(c.h);
    };
    function getExtension(filename) {
        return filename.split('.').pop().toLowerCase();
    }
    /* END CROP IMG  */
    
});

function delimg(id){
    var isi = {id:id};
    var images = $('.cal').length;
    if(images > 1){
        $.prompt('<?php echo $this->lang->line('alert_delete_img');?>' ,{ 
            prefix:'jqismooth',
            buttons: { "Ok": true,"Tidak": false},
            submit: function(e,v,m,f){
                if(e == true){
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url().$this->router->class; ?>/delete_img',
                        data: isi,
                        success: function(data){
                            console.log(data);
                            if(data == 'success'){
                                $('.gambar'+id).remove();
                                toastr.success("<?php echo $this->lang->line('success_delete_imgproduk'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                            }
                        }
                    });
                }
                $.prompt.close();
            } 
        });
    }else{
        $.prompt('<?php echo $this->lang->line('alert_info_del_img');?>' ,{ 
            prefix:'jqismooth',
            buttons: { "Ok": true}
        });
    }
}

function showlog() {
    $(".images").show();
    $(".showlog").hide();
    $(".hidelog").show();
}
function hidelog() {
    $(".images").hide();
    $(".showlog").show();
    $(".hidelog").hide();
}
</script>
<section class="main-content">
 
    <div class="content-wrap">
     
        <div class="wrapper">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo base_url('/'); ?>"><i class="ti-home mr5"></i><?php echo $this->lang->line('dashboard'); ?></a>
                </li>
                <li>
                    <a href="<?php echo base_url().$this->router->class; ?>"><?php echo ucfirst($this->module_name); ?></a>
                </li>
                <li class="active"><?php echo $this->lang->line('modif'); ?></li>
            </ol>

            <div class="panel">
                <header class="panel-heading">
                    <div class="row">
                        <div class="col-xs-10">
                            <h5 class="text-uppercase no-m"><?php echo strtoupper($this->module_name); ?></h5>
                            <small><?php echo $this->lang->line('kategory_teks'); ?></small>
                        </div>
                        <div class="col-xs-2 text-right">
                            <a class="btn btn-warning btn-xs" href="<?php echo base_url().$this->router->class; ?>"  title="<?php echo $this->lang->line('add'); ?>">
                                <i class="ti-plus"></i>
                                <?php echo $this->lang->line('list'); ?>
                            </a>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal" name="form" method="POST" action="" enctype="multipart/form-data" onsubmit="return validate()">
                        <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
                            <?php foreach($error_hash as $inp_err){ ?>
                                <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                                    });
                                </script>
                            <?php } ?>
                        <?php } ?>
                        
                        <?php if(isset($success)){ ?>
                            <script type="text/javascript">
                            jQuery(document).ready(function($) {
                                toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                                });
                            </script>
                        <?php } ?>
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('nama_product'); ?> <span>*</span></label>
                            <div class="col-sm-4">
                                <input readonly type="text" value='<?php echo isset($lists[0]['name'])?$lists[0]['name']:''; ?>' size="50" class="form-control">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-xs-8 text-left">
                                <a href="javascript:void(0);" onclick="showlog();" class="btn btn-primary showlog"><?php echo $this->lang->line('tambah_gambar'); ?></a>
                                <a href="javascript:void(0);" onclick="hidelog();" class="btn btn-primary hidelog"><?php echo $this->lang->line('sembunyikan_gambar'); ?></a>
                            </div>

                            <div class="col-sm-2 text-right">
                                <input class="btn btn-danger btn-bg mr5" type="submit" value="<?php echo $this->lang->line('label_button_save'); ?>">
                            </div>
                        </div>

                        <div class="images">
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $this->lang->line('upload_image'); ?> <span>*</span></label>
                                <div class="col-sm-8">
                                <input class="text gbr" type="file" id="uploadImage" name="path" value="<?php echo isset($path)?$path:''; ?>"><br class="clear">
                                    <p>
                                        <em>
                                            <?php echo $this->lang->line('label_info_ukuran'); ?> 
                                            <?php echo $this->webconfig['max_foto_filesize']; ?> MB
                                            <?php echo $this->lang->line('label_info_dimensi'); ?> 
                                            <?php if(count($this->webconfig["image_product"]) > 0){ echo $this->webconfig["image_product"][0]; }else{ echo 0; } ?> px
                                        </em>
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('gambar'); ?> </label>
                            <div class="col-md-10"> 
                                <?php if(isset($lists[0]['images']) && $lists[0]['images'] != '' && count($lists[0]['images']) > 0){ ?>
                                    <?php foreach ($lists[0]['images'] as $key => $list) { ?>
                                        <?php if(isset($list['path']) && $list['path'] != ''){ ?>
                                            <img width="120" src="<?php echo thumb_image($list['path'],'360x203', 'product'); ?>" class="img-thumbnail margin-10 cal gambar<?php echo $list['id'];?>">
                                            <img id="listimgs" class="gambar<?php echo $list['id'];?>" src="<?php echo $this->webconfig['back_base_template']; ?>img/x.png" alt="delete" onclick="delimg('<?php echo $list['id'];?>');">
                                        <?php }else{ ?>
                                            <img src="<?php echo $this->webconfig['base_template']; ?>img/no_image_news.jpg" width="600"/>
                                        <?php } ?> 
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
     
    </div>
 
    <a class="exit-offscreen"></a>
</section>