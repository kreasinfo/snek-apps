<script type="text/javascript">
	jQuery(document).ready(function($) {
		jQuery("#listData").load('<?php echo base_url().$this->router->class; ?>/getAllData/');
		$("#list_kategori_produk").select2({
            placeholder: "<?php echo $this->lang->line('label_pilih_kategori'); ?>",
            allowClear: true,
            ajax: {
                url: '<?php echo base_url(); ?>webservices/kategori_produk/',
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term
                    };
                },
                results: function (data, page) {
                    return { results: data };
                } ,
                cache: false
            }
        });
	});
	function searchThis(){
		var frm = document.searchform;
		var name = frm.name.value;
		var id_kategori_produk = frm.id_kategori_produk.value;
		if(name == ''){ name = '-'; }
		if(id_kategori_produk == ''){ id_kategori_produk = '-'; }
		jQuery("#listData").load("<?php echo base_url().$this->router->class; ?>/searchdata/"+name.replace(/\s/g,'%20')+"/"+id_kategori_produk.replace(/\s/g,'%20')+"/");
	}
</script>
<section class="main-content">
 
    <div class="content-wrap">
     
        <div class="wrapper">
            <ol class="breadcrumb">
	            <li>
	            	<a href="<?php echo base_url('/'); ?>"><i class="ti-home mr5"></i><?php echo $this->lang->line('dashboard'); ?></a>
	            </li>
	            <li class="active"><?php echo ucfirst($this->module_name); ?></li>
            </ol>

            <div class="panel">
            	<header class="panel-heading">
        			<div class="row">
						<div class="col-xs-10">
							<h5 class="text-uppercase no-m"><?php echo strtoupper($this->module_name); ?></h5>
		            		<small><?php echo $this->lang->line('produk_teks'); ?></small>
						</div>
						<div class="col-xs-2 text-right">
							<a class="btn btn-warning btn-xs" href="<?php echo base_url().$this->router->class; ?>/add"  title="<?php echo $this->lang->line('add'); ?>">
								<i class="ti-plus"></i>
								<?php echo $this->lang->line('add'); ?>
							</a>
						</div>
					</div>
        		</header>
            	<div class="panel-body">
		        	<div class="row widget bg-primary">
		        		<div class="col-xs-12 widget-body">
							<form id="searchform" name="searchform" method="POST" action="<?php echo base_url().$this->router->class; ?>/searchdata/" onsubmit="searchThis(); return false;">
								<div class="col-xs-3">
									<input type="hidden" id="list_kategori_produk" name="id_kategori_produk" class="form-input full-width" />
								</div>
								<div class="col-xs-3">
									<input type="text" class="form-control" name="name" placeholder="<?php echo $this->lang->line('name'); ?>">
								</div>
								<div class="col-xs-6">
									<button class="btn btn-color" type="button" onclick="searchThis()"><i class="ti-search"></i> <?php echo $this->lang->line('search'); ?></button>
								</div>
							</form>
						</div>
					</div>
		    		<div id="listData"><center><img src='<?php echo $this->webconfig['back_base_template']; ?>img/loading.gif' align='middle' style="margin:5px;" /></center></div>
    			</div>
    		</div>
        </div>
     
    </div>
 
	<a class="exit-offscreen"></a>
</section>