<?php
class Produk extends CI_Controller{
	var $webconfig;
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('image');
		$this->load->helper('common');
		$this->load->library('image_moo');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->sharedModel('Kategori_produkModel');
		$this->load->sharedModel('ProdukModel');
		$this->load->sharedModel('Lokasi_ukmModel');
		$this->load->sharedModel('UkmModel');
		$this->load->sharedModel('LoginModel');
		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login');
		}
		
		$this->webconfig = $this->config->item('webconfig');
		$this->module_name = $this->lang->line('produk');
	}
	function index(){
		redirect(base_url().$this->router->class.'/listing');
	}
	function listing($page = 0){
		$tdata = array();
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		$this->load->sharedView('template', $ldata);
	}
	function getAllData($page = 0){
		
		$all_data = $this->ProdukModel->listDataCount();
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 3;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><span>';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->ProdukModel->listData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
															));
		
		$this->load->view($this->router->class.'/list', $listdata);
	}
	function searchdata($name = 'name', $id_kategori_produk = '-'){
		if($name == '-'){$name = '';}
		if($id_kategori_produk == '-'){$id_kategori_produk = '';}

		# PAGINATION #
		$all_data = $this->ProdukModel->filterDataCount(array(
														'name'=>trim(urldecode($name))
														,'id_kategori_produk'=>trim(urldecode($id_kategori_produk))
														));
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/".$this->uri->segment(3)."/".$this->uri->segment(4)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 5;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><span>';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->ProdukModel->filterData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
																,'name' => trim(urldecode($name))
																,'id_kategori_produk'=>trim(urldecode($id_kategori_produk))
															));
		$this->load->view($this->router->class.'/list', $listdata);
	}
	function add(){
		$tdata = array();
		$tdata['categories'] = $this->Kategori_produkModel->parentcategory();
		$tdata['lokasiukm'] = $this->Lokasi_ukmModel->listData();
		$tdata['ukm'] = $this->UkmModel->listData();
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
		if($_POST)
		{
			
			$validator = new FormValidator();
		    $validator->addValidation("name","req",$this->lang->line('error_empty_name_produk'));
		    $validator->addValidation("slug","req",$this->lang->line('error_empty_slug_produk'));
		    $validator->addValidation("description","req",$this->lang->line('error_empty_description_produk'));
		    $validator->addValidation("body","req",$this->lang->line('error_empty_body_produk'));
		    $validator->addValidation("price","req",$this->lang->line('error_empty_price_produk'));
		    $validator->addValidation("berat","req",$this->lang->line('error_empty_berat_produk'));
		    $validator->addValidation("id_kategori_produk","req",$this->lang->line('error_empty_id_kategori_produk_produk'));
		    $validator->addValidation("id_ukm","req",$this->lang->line('error_empty_id_ukm_produk'));
		    $validator->addValidation("id_lokasi_ukm","req",$this->lang->line('error_empty_id_lokasi_ukm_produk'));
			
		    if($validator->ValidateForm())
		    {
		        $doInsert = $this->ProdukModel->entriData(array(
														        'name'=>$this->input->post('name')
														        ,'slug'=>$this->input->post('slug')
														        ,'description'=>$this->input->post('description')
														        ,'body'=>$this->input->post('body')
														        ,'price'=>$this->input->post('price')
														        ,'status'=>$this->input->post('status')
														        ,'berat'=>$this->input->post('berat')
														        ,'discount'=>$this->input->post('discount')
														        ,'seo_title'=>$this->input->post('seo_title')
														        ,'seo_description'=>$this->input->post('seo_description')
														        ,'id_kategori_produk'=>$this->input->post('id_kategori_produk')
														        ,'stock'=>$this->input->post('stock')
														        ,'size'=>$this->input->post('size')
														        ,'tags'=>$this->input->post('tags')
														        ,'description_img'=>$this->input->post('description_img')
														        ,'x1' => $this->input->post('x1')
																,'y1' => $this->input->post('y1')
																,'x2' => $this->input->post('x2')
																,'y2' => $this->input->post('y2')
																,'width_resize' => $this->webconfig["width_resize"]
																,'id_ukm' => $this->input->post('id_ukm')
																,'id_lokasi_ukm' => $this->input->post('id_lokasi_ukm')
																,'status_produk' => $this->input->post('status_produk')
														  ));
				if($doInsert == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doInsert == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doInsert == 'failed_ext'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_extension'));
				}else if($doInsert == 'empty_file'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_file'));
				}else if($doInsert == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_entry');
				}
				$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {
		        $tdata['name']     = $this->input->post('name');
			    $tdata['slug'] = $this->input->post('slug');
			    $tdata['description'] = $this->input->post('description');
			    $tdata['body'] = $this->input->post('body');
			    $tdata['price'] = $this->input->post('price');
			    $tdata['berat'] = $this->input->post('berat');
			    $tdata['discount'] = $this->input->post('discount');
			    $tdata['status'] = $this->input->post('status');
			    $tdata['seo_title'] = $this->input->post('seo_title');
			    $tdata['seo_description'] = $this->input->post('seo_description');
			    $tdata['id_kategori_produk'] = $this->input->post('id_kategori_produk');
			    $tdata['id_kategori_produk_text'] = $this->input->post('id_kategori_produk_text');
                $tdata['size'] = $this->input->post('size');
                $tdata['tags'] = $this->input->post('tags');
                $tdata['stock'] = $this->input->post('stock');
                $tdata['description_img'] = $this->input->post('description_img');
                $tdata['id_ukm'] = $this->input->post('id_ukm');
                $tdata['id_lokasi_ukm'] = $this->input->post('id_lokasi_ukm');
                $tdata['status_produk'] = $this->input->post('status_produk');
		    	$tdata['error_hash'] = $validator->GetErrors();

				$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{
			$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
			$this->load->sharedView('template', $ldata);
		}
	}
	function modif($id = 0){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}
		$tdata = array();
		$tdata['lists'] = $this->ProdukModel->getData(array('id'=>$id));
		$tdata['categories'] = $this->Kategori_produkModel->parentcategory();
		$tdata['lokasiukm'] = $this->Lokasi_ukmModel->listData();
		$tdata['ukm'] = $this->UkmModel->listData();
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
		if($_POST)
		{
			$validator = new FormValidator();

		    $validator->addValidation("name","req",$this->lang->line('error_empty_name_produk'));
		    $validator->addValidation("slug","req",$this->lang->line('error_empty_slug_produk'));
		    $validator->addValidation("description","req",$this->lang->line('error_empty_description_produk'));
		    $validator->addValidation("body","req",$this->lang->line('error_empty_body_produk'));
		    $validator->addValidation("price","req",$this->lang->line('error_empty_price_produk'));
		    $validator->addValidation("berat","req",$this->lang->line('error_empty_berat_produk'));
		    $validator->addValidation("id_kategori_produk","req",$this->lang->line('error_empty_id_kategori_produk_produk'));
		    $validator->addValidation("id_ukm","req",$this->lang->line('error_empty_id_ukm_produk'));
		    $validator->addValidation("id_lokasi_ukm","req",$this->lang->line('error_empty_id_lokasi_ukm_produk'));
			
		    if($validator->ValidateForm())
		    {
		        $doUpdate = $this->ProdukModel->updateData(array(
														        'id' => $id
		        												,'name'=>$this->input->post('name')
														        ,'slug'=>$this->input->post('slug')
														        ,'description'=>$this->input->post('description')
														        ,'body'=>$this->input->post('body')
														        ,'price'=>$this->input->post('price')
														        ,'berat'=>$this->input->post('berat')
														        ,'status'=>$this->input->post('status')
														        ,'discount'=>$this->input->post('discount')
														        ,'seo_title'=>$this->input->post('seo_title')
														        ,'seo_description'=>$this->input->post('seo_description')
														        ,'id_kategori_produk'=>$this->input->post('id_kategori_produk')
														        ,'stock'=>$this->input->post('stock')
														        ,'size'=>$this->input->post('size')
														        ,'tags'=>$this->input->post('tags')
											                    , 'sizeidold' => $this->input->post('sizeidold')
											                    , 'sizeold' => $this->input->post('sizeold')
											                    , 'stockold' => $this->input->post('stockold')
											                    ,'id_ukm' => $this->input->post('id_ukm')
																,'id_lokasi_ukm' => $this->input->post('id_lokasi_ukm')
																,'status_produk' => $this->input->post('status_produk')
														  ));

				if($doUpdate == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doUpdate == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doUpdate == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_update');
					$tdata['lists'] = $this->ProdukModel->getData(array('id'=>$id));
				}
				$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {
		        $tdata['name']     = $this->input->post('name');
			    $tdata['slug'] = $this->input->post('slug');
			    $tdata['description'] = $this->input->post('description');
			    $tdata['body'] = $this->input->post('body');
			    $tdata['price'] = $this->input->post('price');
			    $tdata['berat'] = $this->input->post('berat');
			    $tdata['discount'] = $this->input->post('discount');
			    $tdata['seo_title'] = $this->input->post('seo_title');
			    $tdata['seo_description'] = $this->input->post('seo_description');
			    $tdata['id_kategori_produk'] = $this->input->post('id_kategori_produk');
                $tdata['size'] = $this->input->post('size');
                $tdata['tags'] = $this->input->post('tags');
                $tdata['stock'] = $this->input->post('stock');
			    $tdata['status'] = $this->input->post('status');
			    $tdata['id_ukm'] = $this->input->post('id_ukm');
                $tdata['id_lokasi_ukm'] = $this->input->post('id_lokasi_ukm');
                $tdata['status_produk'] = $this->input->post('status_produk');
		    	$tdata['error_hash'] = $validator->GetErrors();

				$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{
			$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
			$this->load->sharedView('template', $ldata);
		}
	}
	function view($id = 0){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}
		$tdata['lists'] = $this->ProdukModel->getData(array('id'=>$id));
		$this->load->view($this->router->class.'/view',$tdata);
	}
    public function deletesize() {
        $doDelete = $this->ProdukModel->deleteSize($_POST['id']);
        echo $doDelete;
    }
	public function delete_img(){
		if($_POST){
			$id = $this->input->post('id');
			$doDelete = $this->ProdukModel->deleteImages($id);

    		echo $doDelete;
		}
		
	}
	public function deleteThis(){
		$doDelete = $this->__delete($this->input->post('dataid'));

		if($doDelete == 'failed'){
			echo "nodata";
		}else if($doDelete == 'success'){
			echo "success";
		}
	}
	public function postProcess(){
		$postvalue = $this->input->post('datacek');
		if($postvalue != ''){
			foreach($postvalue as $data){
				$this->__delete($data);
			}
			echo "success";
		}else{
			echo "nodata";
		}
	}
	public function publish($id = 0){
		$doThis = $this->__doPublish($this->input->post('dataid'));
		
		if($doThis == 'failed'){
			echo "nodata";
		}else if($doThis == 'success'){
			echo "success";   	
		}
	}
	public function unpublish($id = 0){
		$doThis = $this->__doUnpublish($this->input->post('dataid'));
		
		if($doThis == 'failed'){
			echo "nodata";
		}else if($doThis == 'success'){
			echo "success";   	
		}
	}
	private function __delete($id){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}
		$doDelete = $this->ProdukModel->deleteData($id);
    	return $doDelete;
	}
	private function __doPublish($kodeid){
		if($kodeid == 0){
			redirect(base_url().$this->router->class."/");
		}
		$doPublish = $this->ProdukModel->doPublish($kodeid);
		return $doPublish;
	}
	private function __doUnpublish($kodeid){
		if($kodeid == 0){
			redirect(base_url().$this->router->class."/");
		}
		$doUnublish = $this->ProdukModel->doUnublish($kodeid);
		return $doUnublish;
	}

	function addimage($id = 0){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}

		$tdata['lists'] = $this->ProdukModel->getData(array('id'=>$id));
		
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
		if($_POST)
		{
			
			$validator = new FormValidator();
			
		    if($validator->ValidateForm())
		    {
		        $doUpdate = $this->ProdukModel->updateImg(array(
														        'id' => $id
		        												,'x1' => $this->input->post('x1')
																,'y1' => $this->input->post('y1')
																,'x2' => $this->input->post('x2')
																,'y2' => $this->input->post('y2')
																,'width_resize' => $this->webconfig["width_resize"]
														  ));

				if($doUpdate == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doUpdate == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doUpdate == 'failed_ext'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_extension'));
				}else if($doUpdate == 'empty_file'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_file'));
				}else if($doUpdate == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_update');
					$tdata['lists'] = $this->ProdukModel->getData(array('id'=>$id));
				}
				$ldata['content'] = $this->load->view($this->router->class.'/addimage',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {
		        
		    	$tdata['error_hash'] = $validator->GetErrors();

				$ldata['content'] = $this->load->view($this->router->class.'/addimage',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{
			$ldata['content'] = $this->load->view($this->router->class.'/addimage',$tdata, true);
			$this->load->sharedView('template', $ldata);
		}
	}
}