<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shipment Confrimation</title>
</head>

<body>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#f7f7f7" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:1.4em; color:#333;">
  <tr>
    <td>
      <table width="640" border="0" cellpadding="0" cellspacing="0" style="margin:20px auto;">
        <tr style="background:#FFF;">
            <td style="text-align:center; padding:20px 0;border-left:1px solid #CCC; border-right:1px solid #CCC; border-top:1px solid #CCC;"><a href="<?php echo base_url(); ?>" target="_blank">
            <img src="<?php echo $this->webconfig['shop_template']; ?>images/logod.png" alt="Snekbook" style="border:none;" /></a>
            </td>
        </tr>
          <tr>
            <td style="background:#FFF; border-left:1px solid #ddd; border-right:1px solid #ddd; border-bottom:1px solid #ddd; padding:10px 20px;">
            <p style="border-bottom:1px solid #ddd;font-size:16px; font-weight:bold; text-transform:uppercase; color:#333; padding:10px 0; margin:0;">Konfirmasi Pengiriman</p>
              <p>
              	Dear, <strong><?php echo isset($detail[0]['post_name'])?ucwords($detail[0]['post_name']):'';?></strong><br/>
              	Kami ingin menginformasikan bahwa pesanan Anda untuk <strong>Order ID #<?php echo isset($detail[0]['id'])?$detail[0]['id']:'';?></strong> dikirim ke alamat yang tercantum dalam Rincian Pengiriman di bawah menggunakan <strong><?php echo isset($detail[0]['shipped']['category']['name'])?$detail[0]['shipped']['category']['name']:'';?></strong> dengan Nomor Resi Pengiriman:
              </p>
              <h2 style="margin:30px auto; color:#F00; text-align:center"><?php echo isset($detail[0]['shipping_no'])?$detail[0]['shipping_no']:'';?></h2>
                <table width="100%" border="0" cellpadding="7" cellspacing="0" bgcolor="#f7f7f7" style="margin-top:20px; border:1px solid #ddd;">
                  	<tr>
                        <td colspan="3" style="border-bottom:1px solid #ddd;"><strong>Detail Transaksi</strong></td>
                    </tr>
                    <tr>
                        <td width="30%" style="border-bottom:1px solid #ddd;">Invoice Number</td>
                        <td width="4%" style="border-bottom:1px solid #ddd;">:</td>
                        <td width="76%" style="border-bottom:1px solid #ddd;">#<?php echo isset($detail[0]['id'])?$detail[0]['id']:'';?></td>
                    </tr>
                    <tr>
                        <td style="border-bottom:1px solid #ddd;">Tanggal Transaksi</td>
                        <td style="border-bottom:1px solid #ddd;">:</td>
                        <td style="border-bottom:1px solid #ddd;"><?php echo isset($detail[0]['datecreated'])?datetime_lang_reformat_long($detail[0]['datecreated']):'';?></td>
                    </tr>
                    <tr>
                        <td style="border-bottom:1px solid #ddd;">Metode pengiriman</td>
                        <td style="border-bottom:1px solid #ddd;">:</td>
                        <td style="border-bottom:1px solid #ddd;"><?php echo isset($detail[0]['shipped']['category']['name'])?$detail[0]['shipped']['category']['name']:'';?></td>
                    </tr>
                    <tr>
                        <td style="border-bottom:1px solid #ddd;">Metode pembayaran</td>
                        <td style="border-bottom:1px solid #ddd;">:</td>
                        <td style="border-bottom:1px solid #ddd;">Bank Transfer</td>
                    </tr>
                    <tr>
                        <td>Status Pembayaran</td>
                        <td>:</td>
                        <td>Lunas</td>
                    </tr>
                </table>
                 <table width="100%" border="0" cellpadding="7" cellspacing="0" bgcolor="#f7f7f7" style="margin-top:20px; border:1px solid #ddd;">
                  	<tr>
                        <td colspan="3" style="border-bottom:1px solid #ddd;"><strong>Detail Pengiriman</strong></td>
                    </tr>
                    <tr>
                        <td width="25%" style="border-bottom:1px solid #ddd;">Nama</td>
                        <td width="4%" style="border-bottom:1px solid #ddd;">:</td>
                        <td width="76%" style="border-bottom:1px solid #ddd;"><?php echo isset($detail[0]['post_name'])?ucwords($detail[0]['post_name']):'';?></td>
                    </tr>
                    <tr>
                        <td style="border-bottom:1px solid #ddd;">Telepon</td>
                        <td style="border-bottom:1px solid #ddd;">:</td>
                        <td style="border-bottom:1px solid #ddd;"><?php echo isset($detail[0]['post_phone'])?$detail[0]['post_phone']:'';?></td>
                    </tr>
                    <tr>
                        <td style="border-bottom:1px solid #ddd;">Alamat</td>
                        <td style="border-bottom:1px solid #ddd;">:</td>
                        <td style="border-bottom:1px solid #ddd;"><?php echo isset($detail[0]['post_address'])?$detail[0]['post_address']:'';?></td>
                    </tr>
                    <tr>
                        <td style="border-bottom:1px solid #ddd;">Kota</td>
                        <td style="border-bottom:1px solid #ddd;">:</td>
                        <td style="border-bottom:1px solid #ddd;"><?php echo isset($detail[0]['shipped']['city'])?$detail[0]['shipped']['city']:'';?></td>
                    </tr>
                    <tr>
                        <td style="border-bottom:1px solid #ddd;">Kode pos</td>
                        <td style="border-bottom:1px solid #ddd;">:</td>
                        <td style="border-bottom:1px solid #ddd;"><?php echo isset($detail[0]['post_postcode'])?$detail[0]['post_postcode']:'';?></td>
                    </tr>
                </table>
                <p>Jika kiriman Anda menggunakan <?php echo isset($detail[0]['shipped']['category']['name'])?$detail[0]['shipped']['category']['name']:'';?>, Anda dapat melacak pengiriman pesanan Anda dengan mengakses website pengiriman tersebut. Jika Anda membutuhkan bantuan, Anda dapat menghubungi kantor pengirman terdekat di daerah Anda.</p>

				<p>Untuk pertanyaan lain, Anda dapat membalas email ini dan kami akan kembali ke 1x24 jam di hari kerja. Customer Service kami selalu siap untuk membantu Anda!
                Terima kasih atas kepercayaan Anda untuk berbelanja di Snekbook.</p>
                <p>Salam hangat,<br/> 
                <strong>Snekbook</strong></p>
            </td>
          </tr>
          <tr>
            <td height="40" style="font-size:11px; color:#666;">
              <a href="<?php echo "http://".APP_DOMAIN."/"; ?>" target="_blank" style="color:#333;text-decoration:none;"><strong>shop.tyaalisha.com</strong></a><br/>
              &copy; <?php echo date('Y'); ?> <?php echo isset($this->configuration['site_title'])?$this->configuration['site_title']:'Site Name'?>. Call Center <strong><?php echo isset($this->configuration['phone'])?$this->configuration['phone']:'000 - 1234546'?></strong>
            </td>
          </tr>
        </table>
    </td>
  </tr>
</table>
</body>
</html>