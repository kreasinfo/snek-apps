<style type="text/css">
	div.jqismooth { width: 400px; }
	.frm_label { display:block; width: 140px; text-align: left; float: left;}
	p { text-align: left; }
</style>
<script type="text/javascript">
jQuery(document).ready(function($) {
	jQuery(document).ajaxStart(function() {
		$( "#loading" ).show();
	}).ajaxStop(function() {
		$( "#loading" ).hide();
	});
	$(".pagination a").click(function(e){
		// stop normal link click
		e.preventDefault(); 
		var linkUrl = jQuery(this).attr("href");
		if(linkUrl){
			jQuery('#listData').load(linkUrl.replace(/\s/g,'%20'));
		}
	});
	$('.fancybox').fancybox();
});

function changeStatus(status,orderid,gran_total,shipping) {
	
	if(status == 0) {
		var nextStatus = 2;
		var txt = "<div style='text-align: center;'><?php echo $this->lang->line('alert_payment_accepted'); ?> <label class='orid'>"+orderid+"</label> <?php echo $this->lang->line('alert_payment_accepted2'); ?><br /><br />";
			txt += "<p><label class='frm_label'><?php echo $this->lang->line('label_to_bank'); ?></label><select name='to_bank'><option value=''>-- <?php echo $this->lang->line('label_to_bank'); ?> --</option><?php if(isset($bank_lists) && count($bank_lists) > 0) { foreach($bank_lists as $l) { echo '<option value='.$l['id'].'>'.$l['name'].' ('.$l['account_rek'].')</option>'; } } ?></select></p>";
			txt += "<p><label class='frm_label'><?php echo $this->lang->line('label_from_bank'); ?></label><input type='text' class='text txt_input' name='from_bank' id='from_bank' /></p>";
			txt += "<p><label class='frm_label'><?php echo $this->lang->line('label_total_transfered'); ?></label><input type='text' class='text numeric txt_input' name='total_transfered' id='total_transfered' value='"+gran_total+"' readonly/></p>";
			txt += "<p><label class='frm_label'><?php echo $this->lang->line('label_account_name_bank'); ?></label><input type='text' class='text txt_input' name='account_name_bank' id='account_name_bank' /></p>";
			txt += "<?php echo $this->lang->line('alert_payment_accepted3'); ?>";
			txt += "<input type='hidden' id='orderid' name='orderid' value='"+orderid+"' /> <input type='hidden' id='status' name='status' value='"+nextStatus+"' /></div>";
		$.prompt(txt ,{  callback: confirmPayment, buttons: { <?php echo $this->lang->line('yes'); ?>: true, <?php echo $this->lang->line('cancel'); ?>: false },prefix:'jqismooth' });
	}
	if(status == 8) {
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/orderdetail/"+orderid,
			data: {'orderid':orderid},
			dataType: 'json',
			success: function(response) {
				//console.log(response);
				var nextStatus = 2;
				var txt = "<div style='text-align: center;'><?php echo $this->lang->line('alert_payment_accepted'); ?> <label class='orid'>"+orderid+"</label> <?php echo $this->lang->line('alert_payment_accepted2'); ?><br /><br />";
					txt += "<p><label class='frm_label'><?php echo $this->lang->line('label_to_bank'); ?></label>"+response[0].bank.name+"</p>";
					txt += "<p><label class='frm_label'><?php echo $this->lang->line('label_from_bank'); ?></label>"+response[0].client_bank_name+"</p>";
					txt += "<p><label class='frm_label'><?php echo $this->lang->line('label_total_transfered'); ?></label>Rp "+format_hasil(response[0].client_transfered)+"</p>";
					txt += "<p><label class='frm_label'><?php echo $this->lang->line('label_account_name_bank'); ?></label>"+response[0].client_account_name+"</p>";
					txt += "<?php echo $this->lang->line('alert_payment_accepted3'); ?>";
					txt += "<input type='hidden' id='orderid' name='orderid' value='"+orderid+"' /> <input type='hidden' id='status' name='status' value='"+nextStatus+"' /></div>";
				$.prompt(txt ,{  callback: confirmPayment, buttons: { <?php echo $this->lang->line('yes'); ?>: true, <?php echo $this->lang->line('cancel'); ?>: false },prefix:'jqismooth' });
			}
		})
		
	}
	if(status == 2) {
		var nextStatus = 3;
		var txt = "<?php echo $this->lang->line('label_shipping_alert'); ?><br /><br />";
			txt += "<input type='hidden' id='orderid' name='orderid' value='"+orderid+"' /> <input type='hidden' id='status' name='status' value='"+nextStatus+"' />";
			txt += "<p><label class='frm_label'><?php echo $this->lang->line('label_shipping_metode'); ?></label><input type='hidden' class='text txt_input' name='shipping_metode' id='shipping_metode' value='"+shipping+"' readonly/>"+shipping+"</p>";
			txt += "<p><label class='frm_label'><?php echo $this->lang->line('label_resi_number'); ?></label><textarea class='text txt_input' type='text' id='no_resi' name='no_resi' ></textarea></p>";
		$.prompt(txt ,{  callback: confirmSendOrder, buttons: { <?php echo $this->lang->line('yes'); ?>: true, <?php echo $this->lang->line('cancel'); ?>: false },prefix:'jqismooth' });
	}
	if(status == 3) {
		var nextStatus = 4;
		var txt = "<?php echo $this->lang->line('label_shipping_done'); ?><br /><br />";
			txt += "<input type='hidden' id='orderid' name='orderid' value='"+orderid+"' /> <input type='hidden' id='status' name='status' value='"+nextStatus+"' />";
		$.prompt(txt ,{  callback: confirmDone, buttons: { <?php echo $this->lang->line('yes'); ?>: true, <?php echo $this->lang->line('cancel'); ?>: false },prefix:'jqismooth' });
	}
	$('.numeric').numeric();
}
function setujuipayment(v,m,f){
	  if(v){
		var orderid = f.orderid;
		var status = f.status;
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/changeStatus/",
			data: {'orderid':orderid, 'status':status},
			dataType: 'json',
			success: function(response) {
				if(typeof response.error_hash != 'undefined'){
					$.each(response.error_hash, function(a,b) {
						toastr.error("", response.error_hash.error);
					});

				}
				if(typeof response.success != 'undefined') {
					toastr.success("", response.success);
				}
				$("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	  }else{
	  	$.prompt.close();
	  }
	}
function confirmPayment(v,m,f){
	  if(v){
		var orderid = f.orderid;
		var status = f.status;
		var to_bank = f.to_bank;
		var from_bank = f.from_bank;
		var total_transfered = f.total_transfered;
		var account_name_bank = f.account_name_bank;
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/changeStatus/",
			data: {'orderid':orderid, 'status':status, 'to_bank':to_bank, 'from_bank':from_bank, 'total_transfered':total_transfered, 'account_name_bank':account_name_bank },
			dataType: 'json',
			success: function(response) {
				if(typeof response.error_hash != 'undefined'){
					$.each(response.error_hash, function(a,b) {
						toastr.error("", response.error_hash.error);
					});

				}
				if(typeof response.success != 'undefined') {
					toastr.success("", response.success);
				}
				$("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	  }else{
	  	$.prompt.close();
	  }
	}
function confirmSendOrder(v,m,f){
	  if(v){
		var orderid = f.orderid;
		var status = f.status;
		var shipping_metode = f.shipping_metode;
		var no_resi = f.no_resi;
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/changeStatus/",
			data: { 'orderid':orderid, 'status':status, 'agen': shipping_metode, 'no': no_resi },
			dataType: 'json',
			success: function(response) {
				console.log(response);
				if(typeof response.error_hash != 'undefined'){
					$.each(response.error_hash, function(a,b) {
						toastr.error("", response.error_hash.error);
					});
				}
				if(typeof response.success != 'undefined') {
					toastr.success("", response.success);
				}
				$("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	  }else{
	  	$.prompt.close();
	  }
	}
function confirmDone(v,m,f){
	if(v){
		var orderid = f.orderid;
		var status = f.status;
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/changeStatus/",
			data: { 'orderid':orderid, 'status':status },
			dataType: 'json',
			success: function(response) {
				if(typeof response.error_hash != 'undefined'){
					$.each(response.error_hash, function(a,b) {
						toastr.error("", response.error_hash.error);
					});
				}
				if(typeof response.success != 'undefined') {
					toastr.success("", response.success);
				}
				$("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		});
	}else{
		$.prompt.close();
	}
}
function resetStatus(orderid) {
	var txt = "<div style='text-align: center;'><?php echo $this->lang->line('alert_resetstatus_order'); ?> "+orderid+" ?</div><input type='hidden' name='orderid' value='"+orderid+"' />";
	$.prompt(txt ,{  callback: doResetStatus, buttons: { <?php echo $this->lang->line('yes'); ?>: true, <?php echo $this->lang->line('cancel'); ?>: false },prefix:'jqismooth' });	
}
function doResetStatus(v,m,f) {
	if(v){
		var orderid = f.orderid;
		
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/resetstatus",
			data: { orderid:orderid },
			dataType: 'json',
			success: function(response) {
				if(typeof response.error_hash != 'undefined'){
					$.each(response.error_hash, function(a,b) {
						toastr.error("", response.error_hash.error);
					});
				}
				if(typeof response.success != 'undefined') {
					toastr.success("", response.success);
				}
				$("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	} else {
		$.prompt.close();
	}
}
function cancelOrder(orderid) {
	var txt = "<div style='text-align: center;'><?php echo $this->lang->line('alert_cancelstatus_order'); ?> "+orderid+" ?</div><input type='hidden' name='orderid' value='"+orderid+"' />";
	$.prompt(txt ,{  callback: docancelOrder, buttons: { <?php echo $this->lang->line('yes'); ?>: true, <?php echo $this->lang->line('cancel'); ?>: false },prefix:'jqismooth' });	
}
function docancelOrder(v,m,f) {
	if(v){
		var orderid = f.orderid;
		$.ajax({
			url: "<?php echo base_url().$this->router->class; ?>/cancelOrder",
			data: { 'orderid':orderid },
			dataType: 'json',
			success: function(response) {
				if(typeof response.error_hash != 'undefined'){
					$.each(response.error_hash, function(a,b) {
						toastr.error("", response.error_hash.error);
					});
				}
				if(typeof response.success != 'undefined') {
					toastr.success("", response.success);
				}
				$("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	} else {
		$.prompt.close();
	}
}
function format_hasil(jumlahtot){
    jumlahtot = jumlahtot.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(jumlahtot))
        jumlahtot = jumlahtot.replace(pattern, "$1.$2");
    return jumlahtot;
}
</script>

<form id="tabellistdata" method="post" action="<?php echo base_url().$this->router->class; ?>/postProcess" onsubmit="return false;">
	<div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
            	<?php if(isset($lists) && count($lists) > 0) {?>
                    <table class="table table-striped no-m">
                        <thead>
                            <tr>
                                <th width="10" class="text-center">
                                	<?php echo $this->lang->line('no'); ?>
                                </th>
                                <th>
                                	<?php echo $this->lang->line('date'); ?>
                                </th>
                                <th width="250">
                                	<?php echo $this->lang->line('no_invoice'); ?>
                                </th>
								<th>
                                	<?php echo $this->lang->line('email'); ?>
                                </th>
								<th>
                                	<?php echo $this->lang->line('status'); ?>
                                </th>
                                <th colspan="2" class="text-center" width="30">
                                	<?php echo $this->lang->line('option'); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php $i = $start_no; foreach ($lists as $list) { $i++; ?>
	                            <tr>
	                                
	                                <td class="text-center">
	                                	<?php echo $i; ?>
	                                </td>
	                                <td>
	                                	<?php echo isset($list['datecreated'])?date_lang_reformat_long($list['datecreated']):'';?>
	                                </td>
	                                <td>
	                                	<a href="<?php echo base_url().$this->router->class.'/view/'.$list['id']; ?>" class="fancybox fancybox.ajax"><b>#<?php echo $list['id']; ?></b></a>
	                                	<a href="<?php echo base_url().$this->router->class.'/view/'.$list['id']; ?>" class="fancybox fancybox.ajax">
		                                	<button class="btn btn-default btn-xs" type="button">
				                    			Detail Order
				                    		</button>
				                    	</a>
	                                </td>
	                                <td>
	                                	<?php echo $list['client_email']; ?>
	                                </td>
	                                <td>
	                                	<?php if($list['status'] == 0) {?>
	                                	<button class="btn btn-danger btn-xs" type="button" onclick="changeStatus('<?php echo isset($list['status'])?$list['status']:'';?>','<?php echo isset($list['id'])?$list['id']:''; ?>','<?php echo isset($list['gran_total'])?format_price($list['gran_total'],''):''; ?>')">
			                    			Belum bayar
			                    		</button>
			                    		<?php }else if($list['status'] == 8){ ?>
			                    		<button class="btn btn-info btn-xs" type="button" onclick="changeStatus('<?php echo isset($list['status'])?$list['status']:'';?>','<?php echo isset($list['id'])?$list['id']:''; ?>','<?php echo isset($list['gran_total'])?$list['gran_total']:''; ?>')">
			                    			Sudah konfirmasi
			                    		</button>
			                    		<?php }else if($list['status'] == 1){ ?>
			                    		<button class="btn btn-info btn-xs" type="button" onclick="changeStatus('<?php echo isset($list['status'])?$list['status']:'';?>','<?php echo isset($list['id'])?$list['id']:''; ?>','<?php echo isset($list['gran_total'])?$list['gran_total']:''; ?>')">
			                    			Konfirmasi Disetujui
			                    		</button>
			                    		<?php }else if($list['status'] == 2){ ?>
			                    		<button class="btn btn-success btn-xs" type="button" onclick="changeStatus('<?php echo isset($list['status'])?$list['status']:'';?>','<?php echo isset($list['id'])?$list['id']:''; ?>','<?php echo isset($list['gran_total'])?$list['gran_total']:''; ?>','<?php echo isset($list['shipped']['category']['name'])?$list['shipped']['category']['name']:'';?>')">
			                    			Sudah bayar
			                    		</button>
			                    		<?php }else if($list['status'] == 3){ ?>
			                    		<button class="btn btn-primary btn-xs" type="button" onclick="changeStatus('<?php echo isset($list['status'])?$list['status']:'';?>','<?php echo isset($list['id'])?$list['id']:''; ?>','<?php echo isset($list['gran_total'])?$list['gran_total']:''; ?>')">
			                    			Telah dikirim
			                    		</button>
			                    		<?php }else if($list['status'] == 4){ ?>
			                    		<button class="btn btn-default btn-xs" type="button" onclick="changeStatus('<?php echo isset($list['status'])?$list['status']:'';?>','<?php echo isset($list['id'])?$list['id']:''; ?>','<?php echo isset($list['gran_total'])?$list['gran_total']:''; ?>')">
			                    			Selesai
			                    		</button>
			                    		<?php } ?>
	                                </td>
	                                <td width='10' class="text-center">
	                                	<a href="#">
	                                	<button class="btn btn-default btn-xs" type="button" onclick="resetStatus('<?php echo isset($list['id'])?$list['id']:''; ?>')">
			                    			Reset order
			                    		</button>
							        	</a>  
	                                </td>
	                                <td width='10' class="text-center">
	                                	<?php if(isset($list['status']) && $list['status'] != 4 && $list['status'] != 5){ ?>
	                                	<button class="btn btn-default btn-xs" type="button" onclick="cancelOrder('<?php echo isset($list['id'])?$list['id']:''; ?>')">
			                    			Cancel order
			                    		</button>
			                    		<?php } ?>  
	                                </td>
	                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    
                    <div class="row footertable">
                    	<div class="col-xs-12 text-right">
                    		<ul class="pagination pagination-sm"><?php echo isset($pagination)?$pagination:""; ?></ul>
                    	</div>
                    </div>
                <?php } else { ?>
                	<p><center><?php echo $this->lang->line('no_data'); ?></center></p>
                <?php } ?>
            </div>
        </div>
    </div>
</form>