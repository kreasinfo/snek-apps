<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Konfirmasi Pembayaran</title>
</head>

<body>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#f7f7f7" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:1.4em; color:#333;">
  <tr>
    <td>
      <table width="640" border="0" cellpadding="0" cellspacing="0" style="margin:20px auto;">
          <tr style="background:#FFF;">
            <td style="text-align:center; padding:20px 0;border-left:1px solid #CCC; border-right:1px solid #CCC; border-top:1px solid #CCC;"><a href="<?php echo base_url(); ?>" target="_blank">
            <img src="<?php echo $this->webconfig['shop_template']; ?>images/logod.png" alt="Snekbook" style="border:none;" /></a>
            </td>
          </tr>
          <tr>
            <td style="background:#FFF; border-left:1px solid #ddd; border-right:1px solid #ddd; border-bottom:1px solid #ddd; padding:10px 20px;">
            <p style="border-bottom:1px solid #ddd;font-size:16px; font-weight:bold; text-transform:uppercase; color:#333; padding:10px 0; margin:0;">Konfirmasi Pembayaran</p>
              <p>
              	Dear, <strong><?php echo isset($detail[0]['post_name'])?ucwords($detail[0]['post_name']):'';?></strong><br/>
              	Kami ingin menginformasikan bahwa pembayaran untuk transaksi dengan <strong> Faktur Nomor #<?php echo isset($detail[0]['id'])?$detail[0]['id']:'';?> </strong> telah diterima. Berikut adalah rincian:
              </p>
                <table width="100%" border="0" cellpadding="7" cellspacing="0" bgcolor="#f7f7f7" style="margin-top:20px; border:1px solid #ddd;">
                  	<tr>
                    	<td colspan="3" style="border-bottom:1px solid #ddd;"><strong>Detail Transaksi</strong></td>
                    </tr>
                    <tr>
                    	<td width="30%" style="border-bottom:1px solid #ddd;">Invoice Number</td>
                        <td width="4%" style="border-bottom:1px solid #ddd;">:</td>
                        <td width="76%" style="border-bottom:1px solid #ddd;">#<?php echo isset($detail[0]['id'])?$detail[0]['id']:'';?></td>
                    </tr>
                    <tr>
                    	<td style="border-bottom:1px solid #ddd;">Tanggal Transaksi</td>
                        <td style="border-bottom:1px solid #ddd;">:</td>
                        <td style="border-bottom:1px solid #ddd;"><?php echo isset($detail[0]['datecreated'])?datetime_lang_reformat_long($detail[0]['datecreated']):'';?></td>
                    </tr>
                    <tr>
                    	<td style="border-bottom:1px solid #ddd;">Metode pengiriman</td>
                        <td style="border-bottom:1px solid #ddd;">:</td>
                        <td style="border-bottom:1px solid #ddd;"><?php echo isset($detail[0]['shipped']['category']['name'])?$detail[0]['shipped']['category']['name']:'';?></td>
                    </tr>
                    <tr>
                    	<td style="border-bottom:1px solid #ddd;">Metode pembayaran</td>
                        <td style="border-bottom:1px solid #ddd;">:</td>
                        <td style="border-bottom:1px solid #ddd;">Bank Transfer</td>
                    </tr>
                    <tr>
                    	<td>Status Pembayaran</td>
                        <td>:</td>
                        <td>Lunas</td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="7" cellspacing="0" bgcolor="#f7f7f7" style="margin-top:20px; border:1px solid #ddd;">
                    <tr>
                        <td colspan="3" style="border-bottom:1px solid #ddd;"><strong>Detail Pengiriman</strong></td>
                    </tr>
                    <tr>
                        <td width="25%" style="border-bottom:1px solid #ddd;">Nama</td>
                        <td width="4%" style="border-bottom:1px solid #ddd;">:</td>
                        <td width="76%" style="border-bottom:1px solid #ddd;"><?php echo isset($detail[0]['post_name'])?ucwords($detail[0]['post_name']):'';?></td>
                    </tr>
                    <tr>
                        <td style="border-bottom:1px solid #ddd;">Telepon</td>
                        <td style="border-bottom:1px solid #ddd;">:</td>
                        <td style="border-bottom:1px solid #ddd;"><?php echo isset($detail[0]['post_phone'])?$detail[0]['post_phone']:'';?></td>
                    </tr>
                    <tr>
                        <td style="border-bottom:1px solid #ddd;">Alamat</td>
                        <td style="border-bottom:1px solid #ddd;">:</td>
                        <td style="border-bottom:1px solid #ddd;"><?php echo isset($detail[0]['post_address'])?$detail[0]['post_address']:'';?></td>
                    </tr>
                    <tr>
                        <td style="border-bottom:1px solid #ddd;">Kota</td>
                        <td style="border-bottom:1px solid #ddd;">:</td>
                        <td style="border-bottom:1px solid #ddd;"><?php echo isset($detail[0]['shipped']['city'])?$detail[0]['shipped']['city']:'';?></td>
                    </tr>
                    <tr>
                        <td style="border-bottom:1px solid #ddd;">Kode pos</td>
                        <td style="border-bottom:1px solid #ddd;">:</td>
                        <td style="border-bottom:1px solid #ddd;"><?php echo isset($detail[0]['post_postcode'])?$detail[0]['post_postcode']:'';?></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="10" cellspacing="1" bgcolor="#ddd" style="margin-top:20px;">
                  <tr style="font-size:12px; font-weight:bold; text-transform:uppercase;">
                    <td colspan="2" bgcolor="#FFF">Detail Produk</td>
                    <td width="10%" align="center" bgcolor="#FFF">Diskon</td>
                    <td width="9%" align="center" bgcolor="#FFF">Qty</td>
                    <td width="18%" align="center" bgcolor="#FFF">Harga per Unit</td>
                    <td width="18%" align="right" bgcolor="#FFF">Sub Total</td>
                  </tr>

                  <?php  if(isset($detail[0]['details']) && count($detail[0]['details']) > 0){ //print_r($detail) ?>
                    <?php foreach ($detail[0]['details'] as $key => $value) { ?>
                       <tr>
                        <td width="11%" bgcolor="#FFF">
                          <img src="<?php echo thumb_image($value['path'],'360x203','product'); ?>" alt="image1" style="border:1px solid #ddd; width:97px;">
                        </td>
                        <td width="34%" bgcolor="#FFF">
                            <h4><?php echo $value['name']; ?></h4>
                            <p><span>Harga:</span> <?php echo $this->lang->line('currency').' '.number_format($value['price'], 0, ',', '.'); ?></p>
                            <p><span>Berat:</span> <?php echo $value['berat']; ?></p>
                            <p><span>Size:</span> <?php echo sizeName($value['size']); ?></p>
                        </td>
                        <td align="center" bgcolor="#FFF">
                         <?php echo $value['discount']; ?> %
                        </td>
                        <td align="center" bgcolor="#FFF"><?php echo isset($value['quantity'])?$value['quantity']:'';?></td>
                        <td align="center" bgcolor="#FFF"><?php echo $this->lang->line('currency').' '.number_format($value['price'] - ($value['price'] * ($value['discount']/100)), 0, ',', '.'); ?></td>
                        <td align="right" bgcolor="#FFF">
                          <?php echo $this->lang->line('currency').' '.number_format(($value['price'] - ($value['price'] * ($value['discount']/100))) * $value['quantity'], 0, ',', '.'); ?>
                          <?php 
                              $totalhrgaproduk[] = ($value['price'] - ($value['price'] * ($value['discount']/100))) * $value['quantity']; 
                              $totalberat[] = $value['berat'] * $value['quantity']; 
                          ?>
                        </td>
                      </tr>
                    <?php } ?>
                  <?php } ?>

                </table>
                <table width="100%" border="0" cellpadding="7" cellspacing="0" style="background:#f7f7f7; border:1px solid #ddd; margin-top:15px;">
                  <tr>
                    <td width="46%">Total Berat</td>
                    <td colspan="2" align="right"><?php echo number_format(ceil(array_sum($totalberat)/1000), 0, ',', '.'); ?> Kg</td>
                  </tr>
                  <tr>
                    <td>Biaya Pengiriman</td>
                    <td width="33%">
                      <?php echo number_format(ceil(array_sum($totalberat)/1000), 0, ',', '.'); ?> Kg x <?php echo number_format($detail[0]['post_price'], 0, ',', '.'); ?>
                    </td>
                    <td width="21%" align="right">
                      <?php 
                      $hargapengiriman = $detail[0]['post_price']*ceil(array_sum($totalberat)/1000);
                      echo number_format($hargapengiriman, 0, ',', '.'); 
                      ?>
                    </td>
                  </tr>
                  <tr>
                    <td style="font-size:14px; font-weight:bold; border-top:1px solid #ddd;">GRAND TOTAL</td>
                    <td colspan="2" align="right" style="font-size:14px; font-weight:bold; border-top:1px solid #ddd; color:#f2498f;">
                      <?php echo $this->lang->line('currency').' '.number_format(array_sum($totalhrgaproduk) + $hargapengiriman, 0, ',', '.'); ?>
                    </td>
                  </tr>
                </table>
                <p>Proses pengiriman akan dilakukan dalam jangka waktu minimal 1 x 24 jam. Jika pada saat itu Anda belum menerima konfirmasi pengiriman pemrosesan order Anda, silahkan hubungi kami melalui kontak di bawah ini. Terima kasih atas kepercayaan Anda untuk berbelanja di Snekbook.</p>
                <p>Salam hangat,<br/> 
                <strong>Snekbook</strong></p>
            </td>
          </tr>
          <tr>
            <td height="40" style="font-size:11px; color:#666;">
              <a href="<?php echo "http://".APP_DOMAIN."/"; ?>" target="_blank" style="color:#333;text-decoration:none;"><strong>snekbook.com</strong></a><br/>
              &copy; <?php echo date('Y'); ?> <?php echo isset($this->configuration['site_title'])?$this->configuration['site_title']:'Site Name'?>. Call Center <strong><?php echo isset($this->configuration['phone'])?$this->configuration['phone']:'000 - 1234546'?></strong>
            </td>
          </tr>
        </table>
    </td>
  </tr>
</table>
</body>
</html>