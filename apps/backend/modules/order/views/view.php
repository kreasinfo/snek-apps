<?php if(isset($lists[0]) && count($lists[0])){ //print_r($lists); ?>
<div class="col-md-12">
<section class="panel panel-default  post-comments">
    <div class="panel-heading">
    DETAIL BELANJA
    </div>
    <div class="media p15">
        <?php if(isset($lists[0]['details']) && count($lists[0]['details']) > 0) {?>
            <table class="table table-striped no-m">
                <tr valign="top">
                    <td width="80" >
                        <strong>Gambar</strong>
                    </td>
                    <td width="200">
                        <strong>Nama Produk</strong>
                    </td>
                    <td width="80" align="center">
                        <strong>Discount</strong>
                    </td>
                    <td width="80" align="center">
                        <strong>Harga</strong>
                    </td>
                    <td width="10" align="center">
                        <strong>Jml</strong>
                    </td>
                    <td width="100" align="center">
                        <strong>Total</strong>
                    </td>
                </tr>
                <?php foreach ($lists[0]['details'] as $listdata) { ?>
                    <?php 
                        $total_harga_row = $total_row[] = ($listdata['price'] - (($listdata['discount']/100)*$listdata['price']))*$listdata['quantity']; 
                        $total_berat[] = $listdata['berat'] * $listdata['quantity'];
                    ?>
                    <tr valign="top">
                        <td>
                            <img src="<?php echo thumb_image($listdata['path'],'360x203','product'); ?>" width="70" />
                        </td>
                        <td>
                            <strong><?php echo $listdata['name']; ?></strong>
                            <br/>Berat: <?php echo $listdata['berat']; ?> gram
                        </td>
                        <td align="right">
                            <?php echo $listdata['discount']; ?> % <br />[<?php echo format_price(($listdata['discount']/100)*$listdata['price'],'Rp'); ?>]
                        </td>
                        <td align="right">
                            <?php echo format_price($listdata['price'],'Rp'); ?>
                        </td>
                        <td align="center">
                            <?php echo $listdata['quantity']; ?>
                        </td>
                        <td align="right">
                            <?php echo format_price($total_harga_row,'Rp'); ?>
                        </td>
                    </tr>
                <?php } ?>
                <tr valign="top">
                    <td colspan="5" align="right">
                        SUB TOTAL
                    </td>
                    <td align="right">
                        <?php echo format_price(array_sum($total_row),'Rp'); ?>
                    </td>
                </tr>
                <tr valign="top">
                    <td colspan="5" align="right">
                        <?php 
                            $hargapengiriman = '0';
                            if($lists[0]['status_order'] == '1'){
                                $hargapengiriman = '0';
                            }elseif($lists[0]['status_order'] == '2'){ 
                                $hargapengiriman = $lists[0]['post_price'];
                            }else{
                                $hargapengiriman = $lists[0]['shipped']['price'];
                         } ?>
                        BIAYA KIRIM [<?php echo format_price($hargapengiriman,'Rp').' x '.ceil(array_sum($total_berat)/1000); ?>]
                    </td>
                    <td align="right">
                        <?php 
                            $hargakirim = $hargapengiriman * ceil(array_sum($total_berat)/1000);
                            echo isset($hargapengiriman)?format_price($hargakirim,'Rp '):'0'; 
                        ?>
                    </td>
                </tr>
                <tr valign="top">
                    <td colspan="5" align="right">
                        <strong>TOTAL</strong>
                    </td>
                    <td align="right">
                        <?php echo format_price(array_sum($total_row)+$hargakirim,'Rp'); ?>
                    </td>
                </tr>
            </table>
        <?php } ?>
    </div>
</section>

<section class="panel panel-default  post-comments">
    <div class="panel-heading">
    INFO TUJUAN PENGIRIMAN
    </div>
    <div class="media p15">
        <table class="table table-striped no-m">
            <tr valign="top">
                <td width="80">
                    <strong><?php echo $this->lang->line('label_post_name_order'); ?></strong>
                </td>
                <td width="200">
                    <?php echo $lists[0]['post_name']; ?>
                </td>
                <td width="80">
                    <strong><?php echo $this->lang->line('label_post_address_order'); ?></strong>
                </td>
                <td width="200">
                    <?php echo $lists[0]['post_address']; ?>
                </td>
            </tr>
            <tr valign="top">
                <td width="80">
                    <strong><?php echo $this->lang->line('label_post_postcode_order'); ?></strong>
                </td>
                <td>
                    <?php echo $lists[0]['post_postcode']; ?>
                </td>
                <td width="80">
                    <strong><?php echo $this->lang->line('label_post_email_order'); ?></strong>
                </td>
                <td>
                    <?php echo $lists[0]['post_email']; ?>
                </td>
            </tr>
            <tr valign="top">
                <td width="80">
                    <strong><?php echo $this->lang->line('label_post_phone_order'); ?></strong>
                </td>
                <td>
                    <?php echo $lists[0]['post_phone']; ?>
                </td>
                <td width="100">
                     <strong><?php echo $this->lang->line('label_post_price_order'); ?></strong>
                </td>
                <td>
                    <?php echo isset($lists[0]['shipped']['price'])?format_price($lists[0]['shipped']['price'],'Rp '):''; ?>
                </td>
            </tr>
            <tr valign="top">
                <td width="80">
                    <strong><?php echo $this->lang->line('label_post_city_order'); ?></strong>
                </td>
                <td>
                    <?php echo isset($lists[0]['shipped']['city'])?$lists[0]['shipped']['city']:''; ?>
                </td>
                <td width="80">&nbsp;
                     
                </td>
                <td>&nbsp;
                    
                </td>
            </tr>
        </table>

        <table class="table table-striped no-m">
            <tr valign="top">
                <td width="80">
                    <strong><?php echo $this->lang->line('label_noinvoice_order'); ?></strong>
                </td>
                <td width="200">
                    <strong>#<?php echo $lists[0]['id']; ?></strong>
                </td>
                <td width="100"><strong><?php echo $this->lang->line('label_datecreated_order'); ?></strong></td>
                <td width=""><?php echo date('d-m-Y H:i' ,strtotime($lists[0]['datecreated'])); ?></td>
            </tr>
            <tr valign="top">
                <td width="80">
                    <strong><?php echo $this->lang->line('label_shipping_agen_order'); ?></strong>
                </td>
                <td>
                    <?php if($lists[0]['status_order'] == '1'){?>
                        -
                    <?php }else{ ?>
                        <?php echo isset($lists[0]['shipped']['category']['name'])?$lists[0]['shipped']['category']['name']:'-'; ?>
                    <?php } ?>
                </td>
                <td width="80">
                    <strong><?php echo $this->lang->line('label_shipping_no_order'); ?></strong>
                </td>
                <td>
                    <?php echo isset($lists[0]['shipping_no'])?$lists[0]['shipping_no']:'-'; ?>
                </td>
            </tr>
            <tr valign="top">
                <td width="80">
                    <strong><?php echo $this->lang->line('label_from_bank'); ?></strong>
                </td>
                <td>
                    <?php echo isset($lists[0]['client_bank_name'])?$lists[0]['client_bank_name']:'-'; ?>
                </td>
                <td width="80">
                   <strong><?php echo $this->lang->line('label_account_name_bank'); ?></strong>
                </td>
                <td>
                    <?php echo isset($lists[0]['client_account_name'])?$lists[0]['client_account_name']:'-'; ?>
                </td>
            </tr>
        </table>
        
    </div>
</section>

<?php /* <section class="panel panel-default  post-comments">
    <div class="panel-heading">
    ORDER STATUS
    </div>
    <div class="media p15">
        <table class="table table-striped no-m">
            <tr valign="top">
                <td width="200">
                    <strong><?php echo $this->lang->line('label_noinvoice_order'); ?></strong>
                </td>
                <td width="100">
                    <strong>#<?php echo $lists[0]['id']; ?></strong>
                </td>
                <td width="200">
                    <strong><?php echo $this->lang->line('label_datecreated_order'); ?></strong>
                </td>
                <td>
                    <?php echo date('d-m-Y H:i' ,strtotime($lists[0]['datecreated'])); ?>
                </td>
            </tr>
            <tr valign="top">
                <td width="200">
                    <strong><?php echo $this->lang->line('label_userid_order'); ?></strong>
                </td>
                <td>
                    <?php echo $lists[0]['userid']; ?>
                </td>
                <td width="200">
                    <strong><?php echo $this->lang->line('label_status_order'); ?></strong>
                </td>
                <td>
                    <?php
                        if(isset($lists[0]['status'])){
                            echo statusOrder($lists[0]['status']);
                        }
                    ?>
                </td>
            </tr>
            <tr valign="top">
                <td width="200">
                    <strong><?php echo $this->lang->line('label_payment_order'); ?></strong>
                </td>
                <td>
                    Transfer
                </td>
                <td width="200">
                    <strong><?php echo $this->lang->line('label_shipping_order'); ?></strong>
                </td>
                <td>
                    <?php if($lists[0]['status_order'] == '1'){?>
                        Ambil di tempat
                    <?php }elseif($lists[0]['status_order'] == '2'){ ?>
                        Kirim ke tempat
                    <?php }else{ ?>
                        <?php echo $this->lang->line('label_shipping');?>
                    <?php } ?>
                </td>
            </tr>
            <tr valign="top">
                <td width="200">
                    <strong><?php echo $this->lang->line('label_shipping_agen_order'); ?></strong>
                </td>
                <td>
                    <?php if($lists[0]['status_order'] == '1'){?>
                        -
                    <?php }elseif($lists[0]['status_order'] == '2'){ ?>
                        Kurir Kami
                    <?php }else{ ?>
                        <?php echo isset($lists[0]['shipped']['category']['name'])?$lists[0]['shipped']['category']['name']:'-'; ?>
                    <?php } ?>
                </td>
                <td width="200">
                    <strong><?php echo $this->lang->line('label_shipping_no_order'); ?></strong>
                </td>
                <td>
                    <?php echo isset($lists[0]['shipping_no'])?$lists[0]['shipping_no']:'-'; ?>
                </td>
            </tr>
        </table>
    </div>
</section>


<section class="panel panel-default  post-comments">
    <div class="panel-heading">
    INFO PEMBELI
    </div>
    <div class="media p15">
        <table class="table table-striped no-m">
            <tr valign="top">
                <td width="80">
                    <strong><?php echo $this->lang->line('label_client_name_order'); ?></strong>
                </td>
                <td width="200">
                    <?php echo $lists[0]['client_name']; ?>
                </td>
                <td width="80">
                    <strong><?php echo $this->lang->line('label_client_address_order'); ?></strong>
                </td>
                <td width="200">
                    <?php echo $lists[0]['client_address']; ?>
                </td>
            </tr>
            <tr valign="top">
                <td width="80">
                    <strong><?php echo $this->lang->line('label_client_postcode_order'); ?></strong>
                </td>
                <td>
                    <?php echo $lists[0]['client_postcode']; ?>
                </td>
                <td width="80">
                    <strong><?php echo $this->lang->line('label_client_email_order'); ?></strong>
                </td>
                <td>
                    <?php echo $lists[0]['client_email']; ?>
                </td>
            </tr>
            <tr valign="top">
                <td width="80">
                    <strong><?php echo $this->lang->line('label_client_phone_order'); ?></strong>
                </td>
                <td>
                    <?php echo $lists[0]['client_phone']; ?>
                </td>
                <td width="80">&nbsp;
                     
                </td>
                <td>&nbsp;
                    
                </td>
            </tr>
        </table>
    </div>
</section>*/?>






</div>
<?php } ?>