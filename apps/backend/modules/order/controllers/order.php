<?php
class Order extends CI_Controller{
	var $webconfig;
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->helper('common');
		$this->load->helper('image');
		$this->load->helper('phpmailer');
		$this->load->sharedModel('OrderModel');
		$this->load->sharedModel('LoginModel');
		$this->load->sharedModel('BankModel');
		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login');
		}

		$this->load->sharedModel('ShopconfigurationModel');	
		$this->configuration = $this->ShopconfigurationModel->listData();
		
		$this->webconfig = $this->config->item('webconfig');
		$this->module_name = $this->lang->line('order');
	}
	function index(){
		redirect(base_url().$this->router->class.'/listing');
	}
	function listing($page = 0){
		$tdata = array();
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		$this->load->sharedView('template', $ldata);
	}
	function getAllData($page = 0){
		
		$all_data = $this->OrderModel->listDataCount();
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 3;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><span>';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->OrderModel->listData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
															));

		$listdata['bank_lists'] = $this->BankModel->listData();
		$this->load->view($this->router->class.'/list', $listdata);
	}
	function searchdata($name = 'name'){
		if($name == '-'){$name = '';}

		# PAGINATION #
		$all_data = $this->OrderModel->filterDataCount(array('name'=>trim(urldecode($name))));
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/".$this->uri->segment(3)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 4;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><span>';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->OrderModel->filterData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
																,'name' => trim(urldecode($name))
															));
		
		$this->load->view($this->router->class.'/list', $listdata);
	}

	function changeStatus() {
		$this->load->library('email');
		$tdata = array();
		$doChange = $this->OrderModel->changeStatusOrder(array(
															'orderid' => $this->input->post('orderid')
															,'status' => $this->input->post('status')
															,'shipping_agen' => $this->input->post('agen')
															,'shipping_number' => $this->input->post('no')
															,'to_bank' => $this->input->post('to_bank')
															,'from_bank' => $this->input->post('from_bank')
															,'total_transfered' => $this->input->post('total_transfered')
															,'account_name_bank' => $this->input->post('account_name_bank')
														));
		
		if($doChange == 'empty'){
			$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
		}else if($doChange == 'failed'){
			$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
		}else if($doChange == 'success'){
			$tdata['success'] = $this->lang->line('msg_success_changestatus_order');
			if($this->input->post('status') != 4) {
				$email_subject = '';
				$bodyMessage = "";
				$total_price = 0;
				$configuration = $this->ShopconfigurationModel->listData();
				$orderDetail = $this->OrderModel->listData(array('id' => $this->input->post('orderid')));
				
				switch($this->input->post('status')) {
					case 8:
						$email_subject = "Konfirmasi Pembayaran Snekbook";
						$bodyMessage = $this->load->view($this->router->class.'/email_payment_confirmation', 
																			array(
																				"detail" => isset($orderDetail)?$orderDetail:""
																				,"configuration" => isset($configuration)?$configuration:""
																			), true);
						break;
					case 2:
						$email_subject = "Konfirmasi Pembayaran Snekbook";
						$bodyMessage = $this->load->view($this->router->class.'/email_payment_confirmation', 
																			array(
																				"detail" => isset($orderDetail)?$orderDetail:""
																				,"configuration" => isset($configuration)?$configuration:""
																			), true);
						break;
					case 3:
						$email_subject = "Konfirmasi Pengiriman Snekbook";
						$bodyMessage = $this->load->view($this->router->class.'/email_shipment_confirmation', 
																			array(
																				"detail" => isset($orderDetail)?$orderDetail:""
																				,"configuration" => isset($configuration)?$configuration:""
																			), true);
						break;
				}

				$this->email->initialize(array(
				  'protocol' => 'smtp',
				  'smtp_host' => HOST_EMAIL,
				  'smtp_user' => USER_EMAIL,
				  'smtp_pass' => PASS_EMAIL,
				  'smtp_port' => PORT_EMAIL,
				  'crlf' => "\r\n",
				  'newline' => "\r\n",
				  'mailtype'  => 'html', 
		          'charset'   => 'iso-8859-1'
				));

				$this->email->subject($email_subject);
				$this->email->from('noreply@snekbook.com', 'Snekbook Admin');
				$this->email->to(isset($orderDetail[0]['client_email'])?$orderDetail[0]['client_email']:"");
				$this->email->message($bodyMessage);
				$this->email->send();

			}
		}

		$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode($tdata));
	}

	function resetstatus() {
		$tdata = array();
		$orderDetail = $this->OrderModel->listData(array('id' => $this->input->post('orderid')));
		if(isset($orderDetail[0]['status']) && $orderDetail[0]['status'] == '5'){
			/* Restock Barang ke order*/
			$this->__backstockOrderBarang($this->input->post('orderid'));
			/* End */
		}
	
		$doReset = $this->OrderModel->resetStatusOrder($this->input->post('orderid'));
		if($doReset == 'empty'){
			$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
		}else if($doReset == 'failed'){
			$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
		}else if($doReset == 'success'){
			$tdata['success'] = $this->lang->line('msg_success_changestatus_order');
		}
		$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode($tdata));
	}

	function cancelOrder() {
		$this->load->library('email');
		$tdata = array();

		$doReset = $this->OrderModel->cancelStatusOrder($this->input->get('orderid'));
		if($doReset == 'empty'){
			$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
		}else if($doReset == 'failed'){
			$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
		}else if($doReset == 'success'){
			$tdata['success'] = $this->lang->line('msg_success_changestatus_order');

			$email_params = array();
			$bodyMessage = "";
			$configuration = $this->ShopconfigurationModel->listData();
			$orderDetail = $this->OrderModel->listData(array('id' => $this->input->get('orderid')));
			/* Restock Barang */
			$this->__restockBarang($this->input->get('orderid'));
			/* End */

			$email_subject = "Pembatalan Order Snekbook";
			$bodyMessage = $this->load->view($this->router->class.'/email_cancel_order', 
																			array(
																				"detail" => isset($orderDetail)?$orderDetail:""
																				,"configuration" => isset($configuration)?$configuration:""
																			), true);

			$this->email->initialize(array(
				  'protocol' => 'smtp',
				  'smtp_host' => HOST_EMAIL,
				  'smtp_user' => USER_EMAIL,
				  'smtp_pass' => PASS_EMAIL,
				  'smtp_port' => PORT_EMAIL,
				  'crlf' => "\r\n",
				  'newline' => "\r\n",
				  'mailtype'  => 'html', 
		          'charset'   => 'iso-8859-1'
				));

				$this->email->subject($email_subject);
				$this->email->from('noreply@snekbook.com', 'Snekbook Admin');
				$this->email->to(isset($orderDetail[0]['client_email'])?$orderDetail[0]['client_email']:"");
				$this->email->message($bodyMessage);
				$this->email->send();
		}
		$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode($tdata));
	}

	function __restockBarang($orderId = 0) {
		$this->OrderModel->reStock($orderId);
	}

	function __backstockOrderBarang($orderId = 0) {
		$this->OrderModel->updateStock($orderId);
	}

	function view($id = 0){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}
		
		$tdata['lists'] = $this->OrderModel->listData(array('id' => $id));
		
		$this->load->view($this->router->class.'/view', $tdata);
	}

	function orderdetail(){
		$listdata = $this->OrderModel->listData(array('id' => $_POST['orderid']));
		foreach ($listdata as $key => $value) {
			$listdata[$key]['gran_total'] = number_format($value['gran_total'], 0, ',', '');
			$listdata[$key]['client_transfered'] = number_format($value['client_transfered'], 0, ',', '');
		}
		$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode($listdata));
	}

}