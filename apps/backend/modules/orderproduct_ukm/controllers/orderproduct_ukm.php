<?php
class Orderproduct_ukm extends CI_Controller{
	var $webconfig;
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('common');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->sharedModel('Orderproduct_ukmModel');
		$this->load->sharedModel('UkmModel');
		$this->load->sharedModel('LoginModel');
		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login');
		}
		
		$this->webconfig = $this->config->item('webconfig');
		$this->module_name = $this->lang->line('orderproduct_ukm');
	}
	function index(){
		redirect(base_url().$this->router->class.'/listing');
	}
	function listing($page = 0){
		$tdata = array();
		$tdata['ukm'] = $this->UkmModel->listData();
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		$this->load->sharedView('template', $ldata);
	}
	function getAllData($page = 0){
		
		$all_data = $this->Orderproduct_ukmModel->listDataCount();
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 3;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><span>';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->Orderproduct_ukmModel->listData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
															));
		
		$this->load->view($this->router->class.'/list', $listdata);
	}
	function searchdata($id_ukm = ''){
		if($id_ukm == '-'){$id_ukm = '';}

		# PAGINATION #
		$all_data = $this->Orderproduct_ukmModel->filterDataCount(array('id_ukm'=>trim(urldecode($id_ukm))));
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/".$this->uri->segment(3)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 4;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><span>';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->Orderproduct_ukmModel->filterData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
																,'id_ukm' => trim(urldecode($id_ukm))
															));
		$this->load->view($this->router->class.'/list', $listdata);
	}

	public function postProcessBayar(){
		$postvalue = $this->input->post('datacek');
		if($postvalue != ''){
			foreach($postvalue as $data){
				$this->__bayar($data);
			}
			echo "success";
		}else{
			echo "nodata";
		}
	}
	private function __bayar($id){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}
		$doUpdate = $this->Orderproduct_ukmModel->ubahDataBayar($id);
    	return $doUpdate;
	}

	public function postProcessBelumBayar(){
		$postvalue = $this->input->post('datacek');
		if($postvalue != ''){
			foreach($postvalue as $data){
				$this->__belumbayar($data);
			}
			echo "success";
		}else{
			echo "nodata";
		}
	}
	private function __belumbayar($id){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}
		$doUpdate = $this->Orderproduct_ukmModel->ubahDataBelumBayar($id);
    	return $doUpdate;
	}
}