<script type="text/javascript">
	jQuery(document).ready(function($) {
		$("#architectures").select2();
		jQuery("#listData").load('<?php echo base_url().$this->router->class; ?>/getAllData/');
	});
	function searchThis(){
		var frm = document.searchform;
		var id_architectures = frm.id_architectures.value;
		if(id_architectures == ''){ id_architectures = '-'; }
		jQuery("#listData").load("<?php echo base_url().$this->router->class; ?>/searchdata/"+id_architectures.replace(/\s/g,'%20'));
	}
</script>
<section class="main-content">
 
    <div class="content-wrap">
     
        <div class="wrapper">
            <ol class="breadcrumb">
	            <li>
	            	<a href="<?php echo base_url('/'); ?>"><i class="ti-home mr5"></i><?php echo $this->lang->line('dashboard'); ?></a>
	            </li>
	            <li class="active"><?php echo ucfirst($this->module_name); ?></li>
            </ol>

            <div class="panel">
            	<header class="panel-heading">
        			<div class="row">
						<div class="col-xs-10">
							<h5 class="text-uppercase no-m"><?php echo strtoupper($this->module_name); ?></h5>
		            		<small><?php echo $this->lang->line('orderproduct_ukm_teks'); ?></small>
						</div>
					</div>
        		</header>
            	<div class="panel-body">
		        	<div class="row widget bg-primary">
		        		<div class="col-xs-12 widget-body">
							<form id="searchform" name="searchform" method="POST" action="<?php echo base_url().$this->router->class; ?>/searchdata/" onsubmit="searchThis(); return false;">
								<select id="architectures" name="id_architectures" class="form-select col-sm-3">
                                    <option value="">-- <?php echo $this->lang->line('cari_ukm'); ?> --</option>
                                    <?php
                                    if (isset($ukm) && count($ukm)>0) {
                                        foreach ($ukm as $key => $value) {
                                            ?>
                                            <option value="<?php echo $value['id'] ?>" <?php if(isset($id_architectures) && $id_architectures == $value['id']){echo "selected='selected'; ";} ?> ><?php echo $value['name']; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>  
                                </select>
								<div class="col-xs-9">
									<button class="btn btn-sm btn-color" type="button" onclick="searchThis()"><i class="ti-search"></i> <?php echo $this->lang->line('search'); ?></button>
								</div>
							</form>
						</div>
					</div>
		    		<div id="listData"><center><img src='<?php echo $this->webconfig['back_base_template']; ?>img/loading.gif' align='middle' style="margin:5px;" /></center></div>
    			</div>
    		</div>
        </div>
     
    </div>
 
	<a class="exit-offscreen"></a>
</section>