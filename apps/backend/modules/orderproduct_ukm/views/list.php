<script type="text/javascript">
jQuery(document).ready(function($) {
	$('#checkall').click(function () {
		$('#tabellistdata .check-all').attr('checked', this.checked);
	});
	$(".pagination a").click(function(e){
		// stop normal link click
		e.preventDefault(); 
		var linkUrl = jQuery(this).attr("href");
		if(linkUrl){
			jQuery('#listData').load(linkUrl.replace(/\s/g,'%20'));
		}
	});
});

function bayar(code){
	var cekdata = jQuery('#tabellistdata').serialize();
	if(cekdata ==''){
		var txt = "<?php echo $this->lang->line('alert_checklist'); ?> ";
		jQuery.prompt(txt ,{ buttons: { <?php echo $this->lang->line('ok'); ?>: true },prefix:'jqismooth' });
	}else{
		var txt = "<?php echo $this->lang->line('alert_info_bayar'); ?> <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
		jQuery.prompt(txt ,{  callback: submitkiebayar, buttons: { <?php echo $this->lang->line('ok'); ?>: true, <?php echo $this->lang->line('cancel'); ?>: false },prefix:'jqismooth' });
	}
}
function submitkiebayar(){
	jQuery().ajaxStart(function($) {
		$('#loading').show();
		$('#result').hide();
	}).ajaxStop(function($) {
		$('#loading').hide();
		$('#result').fadeIn('slow');	
		$("#tabellistdata")[0].reset();
	});
	
	jQuery.ajax({
			type: 'POST',
			url: '<?php echo base_url().$this->router->class; ?>/postProcessBayar',
			data: jQuery('#tabellistdata').serialize(),
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_bayar'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_failed_bayar'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				
				jQuery("#listData").load('<?php echo $_SERVER["REQUEST_URI"]; ?>');
			}
		});
		return false;
}

function belumbayar(code){
	var cekdata = jQuery('#tabellistdata').serialize();
	if(cekdata ==''){
		var txt = "<?php echo $this->lang->line('alert_checklist'); ?> ";
		jQuery.prompt(txt ,{ buttons: { <?php echo $this->lang->line('ok'); ?>: true },prefix:'jqismooth' });
	}else{
		var txt = "<?php echo $this->lang->line('alert_info_belumbayar'); ?> <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
		jQuery.prompt(txt ,{  callback: submitkiebelumbayar, buttons: { <?php echo $this->lang->line('ok'); ?>: true, <?php echo $this->lang->line('cancel'); ?>: false },prefix:'jqismooth' });
	}
}
function submitkiebelumbayar(){
	jQuery().ajaxStart(function($) {
		$('#loading').show();
		$('#result').hide();
	}).ajaxStop(function($) {
		$('#loading').hide();
		$('#result').fadeIn('slow');	
		$("#tabellistdata")[0].reset();
	});
	
	jQuery.ajax({
			type: 'POST',
			url: '<?php echo base_url().$this->router->class; ?>/postProcessBelumBayar',
			data: jQuery('#tabellistdata').serialize(),
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_belumbayar'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_failed_belumbayar'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				
				jQuery("#listData").load('<?php echo $_SERVER["REQUEST_URI"]; ?>');
			}
		});
		return false;
}

</script>

<form id="tabellistdata" method="post" action="" onsubmit="return false;">
	<div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
            	<?php if(isset($lists) && count($lists) > 0) {?>
                    <table class="table table-striped no-m">
                        <thead>
                            <tr>
                            	<th width="10" class="text-center">
                                	<input type="checkbox" id="checkall" />
                                </th>
                                <th width="10" class="text-center">
                                	<?php echo $this->lang->line('no'); ?>
                                </th>
                                <th width="250">
                                	<?php echo $this->lang->line('date'); ?>
                                </th>
                                <th>
                                	<b><?php echo $this->lang->line('no_invoice'); ?></b>
                                </th>
                                <th>
                                	<?php echo $this->lang->line('name_product'); ?>
                                </th>
                                <th class="text-center">
                                	<?php echo $this->lang->line('quantity'); ?>
                                </th>
                                <th>
                                	<?php echo $this->lang->line('margin'); ?>
                                </th>
                                <th>
                                	<?php echo $this->lang->line('harga'); ?>
                                </th>
                                <th>
                                	<?php echo $this->lang->line('harga_total'); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php $totalbayar = 0; $totalbelumbayar = 0; $i = $start_no; foreach ($lists as $list) { $i++; ?>
	                            <tr>
	                            	<td class="text-center">
	                                	<input type="checkbox" name="datacek[]" class="check-all" value="<?php echo $list['id']; ?>" />
	                                </td>
	                                <td class="center_th_td">
	                                	<?php echo $i; ?>
	                                </td>
	                                <td>
	                                	<?php echo isset($list['orders']['datecreated'])?date_lang_reformat_long($list['orders']['datecreated']):'';?>
	                                </td>
	                                <td>
	                                	<b>#<?php echo isset($list['order_id'])?$list['order_id']:'';?></b>
	                                </td>
	                                <td>
	                                	<?php echo isset($list['product']['name'])?$list['product']['name']:'';?>
	                                </td>
	                                <td class="text-center">
	                                	<?php echo isset($list['quantity'])?$list['quantity']:'';?>
	                                </td>
	                                <td>
	                                	<?php echo isset($list['productdetail']['size'])?$list['productdetail']['size']:'';?>
	                                </td>
	                                <td>
	                                	<?php echo isset($list['price'])?format_price($list['price'],'Rp'):'';?>
	                                </td>
	                                <td>
	                                	<?php 
	                                		$hargatotal = $list['price']*$list['quantity'];
	                                	?>
	                                	<?php echo isset($hargatotal)?format_price($hargatotal,'Rp'):'';?>
	                                	<!-- <?php if(isset($list['status']) && $list['status'] == '0'){?>
	                                		<b>Belum bayar</b>
	                                	<?php }else if(isset($list['status']) && $list['status'] == '1'){ ?>
	                                		<b>Sudah bayar</b>
	                                	<?php } ?> -->
	                                </td>
	                            </tr>
	                            <?php 
	                            	/*if($list['status'] == '1'){
	                            		$subtotalbayarprice = $list['price'] * $list['quantity'];
	                            		$subtotalbayarmargin = $list['price_margin'] * $list['quantity'];
	                            		$subtotalbayar = $subtotalbayarprice - $subtotalbayarmargin;
	                            		$totalbayar += $subtotalbayar;
	                            	} 
	                            	if($list['status'] == '0'){
	                            		$subtotalbelumbayarprice = $list['price'] * $list['quantity'];
	                            		$subtotalbelumbayarmargin = $list['price_margin'] * $list['quantity'];
	                            		$subtotalbelumbayar = $subtotalbelumbayarprice - $subtotalbelumbayarmargin;
	                            		$totalbelumbayar += $subtotalbelumbayar;
	                            	}*/
	                            ?>
                            <?php } ?>
                            <?php /*<tr>
                            	<td colspan="7" class="text-right">
                            		<b>Total sudah bayar</b>
                            	</td>
                            	<td colspan="2">
                            		<?php //echo format_price($totalbayar,'Rp');?>
                            	</td >
	                        </tr>
                            <tr>
                            	<td colspan="7" class="text-right">
                            		<b>Total belum bayar</b>
                            	</td>
                            	<td colspan="2">
                            		<?php //echo format_price($totalbelumbayar,'Rp');?>
                            	</td>
	                        </tr>*/?>
                        </tbody>
                    </table>
                    <div class="row footertable">
                    	<!-- <div class="col-xs-2">
                    		<button class="btn btn-color btn-xs" type="button" onclick="bayar()">
                    			<i class="ti-trash"></i>
                				Bayar
                    		</button>
                    		<button class="btn btn-color btn-xs" type="button" onclick="belumbayar()">
                    			<i class="ti-trash"></i>
                				Belum bayar
                    		</button>
                    	</div> -->
                    	<div class="col-xs-10 text-right">
                    		<ul class="pagination pagination-sm"><?php echo isset($pagination)?$pagination:""; ?></ul>
                    	</div>
                    </div>
                <?php } else { ?>
                	<p><center><?php echo $this->lang->line('no_data'); ?></center></p>
                <?php } ?>
            </div>
        </div>
    </div>
</form>