<?php
class Informationpage extends CI_Controller{
	var $webconfig;
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->sharedModel('InformationpageModel');
		$this->load->sharedModel('LoginModel');
		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login');
		}
		
		$this->webconfig = $this->config->item('webconfig');
		$this->module_name = $this->lang->line('informationpage');
	}
	function index(){
		redirect(base_url().$this->router->class.'/listing');
	}
	function listing($page = 0){
		$tdata = array();
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		$this->load->sharedView('template', $ldata);
	}
	function getAllData($page = 0){
		
		$all_data = $this->InformationpageModel->listDataCount();
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 3;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><span>';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->InformationpageModel->listData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
															));
		
		$this->load->view($this->router->class.'/list', $listdata);
	}
	function searchdata($name = 'name'){
		if($name == '-'){$name = '';}

		# PAGINATION #
		$all_data = $this->InformationpageModel->filterDataCount(array('name'=>trim(urldecode($name))));
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/".$this->uri->segment(3)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 4;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><span>';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->InformationpageModel->filterData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
																,'name' => trim(urldecode($name))
															));
		$this->load->view($this->router->class.'/list', $listdata);
	}
	function add(){
		$tdata = array();
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
		if($_POST)
		{
			$validator = new FormValidator();
		    $validator->addValidation("name","req",$this->lang->line('error_empty_name_informationpage'));
		    $validator->addValidation("body","req",$this->lang->line('error_empty_body_informationpage'));
			
		    if($validator->ValidateForm())
		    {

		    	// untuk validasi image tinymce
				$open = preg_quote( 'tinymce/' );
				$close = preg_quote( '.jpg' );
				$pattern =  "~$open(.+)$close~misU";
				$image_in_tinymce = "";
				
				preg_match_all( $pattern, $_POST['body'], $matches);
				if(isset($matches) && count($matches) > 0){
					foreach ($matches[1] as $key => $value) {
						$image_in_tinymce[] = $value;
					}
				}

				if(isset($image_in_tinymce) && $image_in_tinymce !=''){
					$result = array_diff($_POST['image_tinymce'] , $image_in_tinymce );
				}
				
				if (isset($result) && count($result) > 0) {
					foreach ($result as $key => $value) {
						//jika input hidden image tidak ada di isi tinymce , maka file di hapus
						@unlink($this->webconfig["media-path-tinymce"].$value.".jpg");
					}
				}

		        $doInsert = $this->InformationpageModel->entriData(array(
														        'name'=>$this->input->post('name')
														        ,'slug'=>$this->input->post('slug')
														        ,'body'=>$this->input->post('body')
														        ,'status'=>$this->input->post('status')
														        ,'seo_title'=>$this->input->post('seo_title')
														        ,'seo_description'=>$this->input->post('seo_description')
														  ));
				if($doInsert == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doInsert == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doInsert == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_entry');
				}
				$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {
		        $tdata['name']     = $this->input->post('name');
			    $tdata['slug'] = $this->input->post('slug');
			    $tdata['body'] = $this->input->post('body');
			    $tdata['status'] = $this->input->post('status');
			    $tdata['seo_title'] = $this->input->post('seo_title');
			    $tdata['seo_description'] = $this->input->post('seo_description');
		    	$tdata['error_hash'] = $validator->GetErrors();

				$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{
			$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
			$this->load->sharedView('template', $ldata);
		}
	}
	function modif($id = 0){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}

		$tdata['lists'] = $this->InformationpageModel->listData(array('id'=>$id));
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
		if($_POST)
		{
			$validator = new FormValidator();

		    $validator->addValidation("name","req",$this->lang->line('error_empty_name_informationpage'));
		    $validator->addValidation("body","req",$this->lang->line('error_empty_body_informationpage'));
			
		    if($validator->ValidateForm())
		    {

		    	// hapus gambar yang ada di public
				$open = preg_quote( 'tinymce/' );
				$close = preg_quote( '.jpg' );
				$data =  "~$open(.+)$close~misU";
				// $image_in_tinymce = "";
				preg_match_all( $data, $tdata['lists'][0]['body'] , $sama);
			
			
				// untuk validasi image tinymce
				$open = preg_quote( 'tinymce/' );
				$close = preg_quote( '.jpg' );
				$pattern =  "~$open(.+)$close~misU";
				$image_in_tinymce = array();
				preg_match_all( $pattern, $_POST['body'], $matches);
				if(count($matches) > 0){
					foreach ($matches[1] as $key => $value) {
						$image_in_tinymce[] = $value;
						
					}
				}
				// lanjutan hapus gambar yang ada di public
				if(isset($image_in_tinymce) && $image_in_tinymce !=''){
					$delfile = array_diff($sama[1],$image_in_tinymce);
					foreach ($delfile as $key => $list) {
						@unlink($this->webconfig["media-path-tinymce"].$list.".jpg");
					}
				}
				
				if(isset($image_in_tinymce) && $image_in_tinymce !=''){
					$result = array_diff($_POST['image_tinymce'] , $image_in_tinymce );
				}
				
				if (isset($result) && count($result) > 0) {
					foreach ($result as $key => $value) {
						//jika input hidden image tidak ada di isi tinymce , maka file di hapus
						@unlink($this->webconfig["media-path-tinymce"].$value.".jpg");
					}
				}
		    	
		        $doUpdate = $this->InformationpageModel->updateData(array(
														        'id' => $id
		        												,'name'=>$this->input->post('name')
														        ,'slug'=>$this->input->post('slug')
														        ,'body'=>$this->input->post('body')
														        ,'status'=>$this->input->post('status')
														        ,'seo_title'=>$this->input->post('seo_title')
														        ,'seo_description'=>$this->input->post('seo_description')
														  ));

				if($doUpdate == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doUpdate == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doUpdate == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_update');
					$tdata['lists'] = $this->InformationpageModel->listData(array('id'=>$id));
				}
				$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {
		        $tdata['name'] = $this->input->post('name');
			    $tdata['slug'] = $this->input->post('slug');
			    $tdata['body'] = $this->input->post('body');
			    $tdata['status'] = $this->input->post('status');
			    $tdata['seo_title'] = $this->input->post('seo_title');
			    $tdata['seo_description'] = $this->input->post('seo_description');
		    	$tdata['error_hash'] = $validator->GetErrors();

				$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{
			$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
			$this->load->sharedView('template', $ldata);
		}
	}
	function view($id = 0){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}
		$tdata['lists'] = $this->InformationpageModel->listData(array('id'=>$id));
		$this->load->view($this->router->class.'/view',$tdata);
	}
	public function deleteThis(){
		$doDelete = $this->__delete($this->input->post('dataid'));

		if($doDelete == 'failed'){
			echo "nodata";
		}else if($doDelete == 'success'){
			echo "success";
		}
	}
	public function publish($id = 0){
		$doThis = $this->__doPublish($this->input->post('dataid'));
		
		if($doThis == 'failed'){
			echo "nodata";
		}else if($doThis == 'success'){
			echo "success";   	
		}
	}
	public function unpublish($id = 0){
		$doThis = $this->__doUnpublish($this->input->post('dataid'));
		
		if($doThis == 'failed'){
			echo "nodata";
		}else if($doThis == 'success'){
			echo "success";   	
		}
	}
	public function postProcess(){
		$postvalue = $this->input->post('datacek');
		if($postvalue != ''){
			foreach($postvalue as $data){
				$this->__delete($data);
			}
			echo "success";
		}else{
			echo "nodata";
		}
	}
	private function __delete($id){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}
		$doDelete = $this->InformationpageModel->deleteData($id);
    	return $doDelete;
	}
	private function __doPublish($kodeid){
		if($kodeid == 0){
			redirect(base_url().$this->router->class."/");
		}
		$doPublish = $this->InformationpageModel->doPublish($kodeid);
		return $doPublish;
	}
	private function __doUnpublish($kodeid){
		if($kodeid == 0){
			redirect(base_url().$this->router->class."/");
		}
		$doUnublish = $this->InformationpageModel->doUnublish($kodeid);
		return $doUnublish;
	}


}