<script type="text/javascript">
jQuery(document).ready(function($) {
    $('input#seo_title').limit('<?php echo $this->webconfig["limit_seo_title"];?>','#charsLeft');
    $('textarea#seo_description').limit('<?php echo $this->webconfig["limit_seo_description"];?>','#charsLeft_textarea');
    tinymce.init({
        selector: "textarea#body",
        theme: "modern",
        height: 200,
        plugins: [
             "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
             "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
             "save table contextmenu directionality emoticons template paste textcolor jbimages"
       ],
       toolbar: "template pagebreak | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages | print preview fullpage | forecolor backcolor emoticons",
       style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}}
        ],


      content_css : "<?php echo $this->webconfig['frontend_template']; ?>css/nicdark_style.css?ver=0,<?php echo $this->webconfig['frontend_template']; ?>css/nicdark_responsive.css?ver=0,<?php echo $this->webconfig['frontend_template']; ?>css/custome_back.css?ver=0",
      templates : [
                {
                    title: 'Template 1', 
                    description: 'Template halaman dengan list gambar yang menarik.', 
                    url: '<?php echo base_url(); ?>template/template_1/'
                },
                {
                    title: 'Template 2', 
                    description: 'Template halaman dengan satu gambar di tengah.', 
                    url: '<?php echo base_url(); ?>template/template_2/'
                },
                {
                    title: 'Template 3', 
                    description: 'Template halaman dengan list gambar di samping.', 
                    url: '<?php echo base_url(); ?>template/template_3/'
                },
        ],
        template_popup_width:900,

        
        relative_urls : false,
     });
});
</script>
<section class="main-content">
 
    <div class="content-wrap">
     
        <div class="wrapper">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo base_url('/'); ?>"><i class="ti-home mr5"></i><?php echo $this->lang->line('dashboard'); ?></a>
                </li>
                <li>
                    <a href="<?php echo base_url().$this->router->class; ?>"><?php echo ucfirst($this->module_name); ?></a>
                </li>
                <li class="active"><?php echo $this->lang->line('add'); ?></li>
            </ol>

            <div class="panel">
                <header class="panel-heading">
                    <div class="row">
                        <div class="col-xs-10">
                            <h5 class="text-uppercase no-m"><?php echo strtoupper($this->module_name); ?></h5>
                            <small><?php echo $this->lang->line('informationpage_teks'); ?></small>
                        </div>
                        <div class="col-xs-2 text-right">
                            <a class="btn btn-warning btn-xs" href="<?php echo base_url().$this->router->class; ?>"  title="<?php echo $this->lang->line('add'); ?>">
                                <i class="ti-plus"></i>
                                <?php echo $this->lang->line('list'); ?>
                            </a>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal" name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>/add" enctype="multipart/form-data">
                        <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
                            <?php foreach($error_hash as $inp_err){ ?>
                                <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                                    });
                                </script>
                            <?php } ?>
                        <?php } ?>
                        
                        <?php if(isset($success)){ ?>
                            <script type="text/javascript">
                            jQuery(document).ready(function($) {
                                toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                                });
                            </script>
                        <?php } ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('title'); ?> <span>*</span></label>
                            <div class="col-sm-5">
                                <input type="text" id="name" name="name" value='<?php echo isset($name)?$name:''; ?>' size="50" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('slug'); ?> </label>
                            <div class="col-sm-6">
                                <input type="text" name="slug" value='<?php echo isset($slug)?$slug:''; ?>' readonly="readonly" class="form-control ArticleSlug">
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $("#name").slug();
                                });
                            </script>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('seo_title'); ?> </label>
                            <div class="col-sm-5">
                                <input type="text" id="seo_title" name="seo_title" value='<?php echo isset($seo_title)?$seo_title:''; ?>' class="form-control">
                                <?php echo $this->lang->line('you_have'); ?> <b><span id="charsLeft"></span></b> <?php echo $this->lang->line('char_left'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('seo_description'); ?> </label>
                            <div class="col-sm-6">
                                <textarea type="text"  id="seo_description" name="seo_description" class="form-control"><?php echo isset($seo_description)?$seo_description:''; ?></textarea>
                                <?php echo $this->lang->line('you_have'); ?> <b><span id="charsLeft_textarea"></span></b> <?php echo $this->lang->line('char_left'); ?>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('body'); ?> <span>*</span></label>
                            <div class="col-sm-10">
                                <textarea type="text" id="body" name="body" class="form-control"><?php echo isset($body)?$body:''; ?></textarea>
                                <div id="image_tinymce">
                                    <?php
                                        if(isset($image_tinymce) && count($image_tinymce) > 0){
                                            foreach ($image_tinymce as $key => $value) {
                                            ?>
                                                <input type="hidden" name="image_tinymce[]" value='<?php echo $value; ?>' >
                                            <?php
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('status'); ?> <span>*</span></label>
                            <div class="col-sm-4 radio">
                                <label>
                                    <input type="radio" name="status" value="1" <?php if(isset($status) && $status == 1){ echo "checked=checked"; }?> />
                                    <?php echo $this->lang->line('aktif'); ?>
                                </label>
                                &nbsp
                                <label>
                                    <input type="radio" name="status" value="0" <?php if(!isset($status)){ echo "checked=checked"; } ?> <?php if(isset($status) && $status == 0){ echo "checked=checked"; }?> />
                                    <?php echo $this->lang->line('nonaktif'); ?> 
                                </label>
                            </div>
                        </div>
                        <div class="form-group footertable">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-5 text-left">
                                <input class="btn btn-danger btn-sm mr5" type="submit" value="<?php echo $this->lang->line('navigation_kirim'); ?>">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
     
    </div>
 
    <a class="exit-offscreen"></a>
</section>