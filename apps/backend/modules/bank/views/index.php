<script type="text/javascript">
	jQuery(document).ready(function($) {
		jQuery("#listData").load('<?php echo base_url().$this->router->class; ?>/getAllData/');
	});
	function searchThis(){
		var frm = document.searchform;
		var name = frm.name.value;
		if(name == ''){ name = '-'; }
		jQuery("#listData").load("<?php echo base_url().$this->router->class; ?>/searchdata/"+name.replace(/\s/g,'%20'));
	}
</script>
<section class="main-content">
 
    <div class="content-wrap">
     
        <div class="wrapper">
            <ol class="breadcrumb">
	            <li>
	            	<a href="<?php echo base_url('/'); ?>"><i class="ti-home mr5"></i><?php echo $this->lang->line('dashboard'); ?></a>
	            </li>
	            <li class="active"><?php echo ucfirst($this->module_name); ?></li>
            </ol>

            <div class="panel">
            	<header class="panel-heading">
        			<div class="row">
						<div class="col-xs-10">
							<h5 class="text-uppercase no-m"><?php echo strtoupper($this->module_name); ?></h5>
		            		<small><?php echo $this->lang->line('bank_teks'); ?></small>
						</div>
						<div class="col-xs-2 text-right">
							<a class="btn btn-warning btn-xs" href="<?php echo base_url().$this->router->class; ?>/add"  title="<?php echo $this->lang->line('add'); ?>">
								<i class="ti-plus"></i>
								<?php echo $this->lang->line('add'); ?>
							</a>
						</div>
					</div>
        		</header>
            	<div class="panel-body">
		        	<div class="row widget bg-primary">
		        		<div class="col-xs-12 widget-body">
							<form id="searchform" name="searchform" method="POST" action="<?php echo base_url().$this->router->class; ?>/searchdata/" onsubmit="searchThis(); return false;">
								<div class="col-xs-3">
									<input type="text" class="form-control" name="name" placeholder="<?php echo $this->lang->line('name_bank'); ?>">
								</div>
								<div class="col-xs-9">
									<button class="btn btn-color" type="button" onclick="searchThis()"><i class="ti-search"></i> <?php echo $this->lang->line('search'); ?></button>
								</div>
							</form>
						</div>
					</div>
		    		<div id="listData"><center><img src='<?php echo $this->webconfig['back_base_template']; ?>img/loading.gif' align='middle' style="margin:5px;" /></center></div>
    			</div>
    		</div>
        </div>
     
    </div>
 
	<a class="exit-offscreen"></a>
</section>