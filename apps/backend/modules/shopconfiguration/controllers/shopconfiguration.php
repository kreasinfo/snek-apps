<?php
class Shopconfiguration extends CI_Controller{
	var $webconfig;
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('text');
		$this->load->library('session');
		$this->load->sharedModel('ShopconfigurationModel');
		$this->load->library('pagination');
		$this->load->sharedModel('LoginModel');
		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login');
		}
		if($this->session->userdata('role') != 0 && $this->session->userdata('role') != 1){
			redirect(base_url().'dashboard');
		}
		$this->webconfig = $this->config->item('webconfig');
		$this->module_name = $this->lang->line('shopconfiguration');
	}
	function index(){
		$tdata = array();					
		$webconfig = $this->config->item('webconfig');
		$tdata['base_template'] = $webconfig['base_template'];
		$tdata['lists'] = $this->ShopconfigurationModel->listData();
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
		if($_POST)
		{
			
			$validator = new FormValidator();
		    $validator->addValidation("meta_title","req",$this->lang->line('error_empty_meta_title_shopconfiguration'));
			$validator->addValidation("site_title","req",$this->lang->line('error_empty_site_title_shopconfiguration'));
			$validator->addValidation("tagline_text","req",$this->lang->line('error_empty_tagline_text_shopconfiguration'));
			$validator->addValidation("meta_description","req",$this->lang->line('error_empty_meta_description_shopconfiguration'));
			$validator->addValidation("email_contact","req",$this->lang->line('error_empty_email_contact_shopconfiguration'));
			$validator->addValidation("address","req",$this->lang->line('error_empty_address_shopconfiguration'));
			$validator->addValidation("phone","req",$this->lang->line('error_empty_phone_shopconfiguration'));
			$validator->addValidation("email_contact","email",$this->lang->line('error_empty_email_contact_valid_shopconfiguration'));
			$validator->addValidation("harga_kirim","req",$this->lang->line('error_empty_harga_kirim_shopconfiguration'));
			
		    if($validator->ValidateForm())
		    {
		        if($this->input->post('id') == ''){
					$doUpdate = $this->ShopconfigurationModel->entriData(array(
																'meta_title'=>$this->input->post('meta_title')
																,'site_title'=>$this->input->post('site_title')
																,'tagline_text'=>$this->input->post('tagline_text')
																,'meta_description'=>$this->input->post('meta_description')
																,'fb_url'=>$this->input->post('fb_url')
																,'email_contact'=>$this->input->post('email_contact')
																,'twitter_url'=>$this->input->post('twitter_url')
																,'gplus_url'=>$this->input->post('gplus_url')
																,'address'=>$this->input->post('address')
																,'phone'=>$this->input->post('phone')
																,'instagram_url'=>$this->input->post('instagram_url')
																,'harga_kirim'=>$this->input->post('harga_kirim')
														  ));
				}else{
					$doUpdate = $this->ShopconfigurationModel->updateData(array(
														        'id' =>$this->input->post('id')
																,'meta_title'=>$this->input->post('meta_title')
		        												,'site_title'=>$this->input->post('site_title')
		        												,'tagline_text'=>$this->input->post('tagline_text')
																,'meta_description'=>$this->input->post('meta_description')
																,'fb_url'=>$this->input->post('fb_url')
																,'email_contact'=>$this->input->post('email_contact')
																,'twitter_url'=>$this->input->post('twitter_url')
																,'gplus_url'=>$this->input->post('gplus_url')
																,'address'=>$this->input->post('address')
																,'phone'=>$this->input->post('phone')
																,'instagram_url'=>$this->input->post('instagram_url')
																,'harga_kirim'=>$this->input->post('harga_kirim')
														  ));
				}
				
		    	
				if($doUpdate == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doUpdate == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doUpdate == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_entry');	        	
				}

				$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
				$this->load->sharedView('template', $ldata);
				
		    }
		    else
		    {	
				$tdata['id'] 	   = $this->input->post('id');
				$tdata['meta_title'] 	   = $this->input->post('meta_title');
				$tdata['site_title'] 	   = $this->input->post('site_title');
				$tdata['tagline_text'] 	   = $this->input->post('tagline_text');
				$tdata['meta_description'] 	   = $this->input->post('meta_description');
				$tdata['email_contact'] 	   = $this->input->post('email_contact');
				$tdata['fb_url'] 	   = $this->input->post('fb_url');
				$tdata['twitter_url'] 	   = $this->input->post('twitter_url');
				$tdata['gplus_url'] 	   = $this->input->post('gplus_url');
				$tdata['address'] 	   = $this->input->post('address');
				$tdata['phone'] 	   = $this->input->post('phone');
				$tdata['instagram_url'] 	   = $this->input->post('instagram_url');
				$tdata['harga_kirim'] 	   = $this->input->post('harga_kirim');
				
		    	$tdata['error_hash'] = $validator->GetErrors();
		    	$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{
			$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
			$this->load->sharedView('template', $ldata);
		}
	}
	
}