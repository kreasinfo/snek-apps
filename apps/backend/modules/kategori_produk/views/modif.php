<section class="main-content">
 
    <div class="content-wrap">
     
        <div class="wrapper">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo base_url('/'); ?>"><i class="ti-home mr5"></i><?php echo $this->lang->line('dashboard'); ?></a>
                </li>
                <li>
                    <a href="<?php echo base_url().$this->router->class; ?>"><?php echo ucfirst($this->module_name); ?></a>
                </li>
                <li class="active"><?php echo $this->lang->line('modif'); ?></li>
            </ol>

            <div class="panel">
                <header class="panel-heading">
                    <div class="row">
                        <div class="col-xs-10">
                            <h5 class="text-uppercase no-m"><?php echo strtoupper($this->module_name); ?></h5>
                            <small><?php echo $this->lang->line('kategory_teks'); ?></small>
                        </div>
                        <div class="col-xs-2 text-right">
                            <a class="btn btn-warning btn-xs" href="<?php echo base_url().$this->router->class; ?>"  title="<?php echo $this->lang->line('add'); ?>">
                                <i class="ti-plus"></i>
                                <?php echo $this->lang->line('list'); ?>
                            </a>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal" name="form" method="POST" action="" enctype="multipart/form-data">
                        <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
                            <?php foreach($error_hash as $inp_err){ ?>
                                <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                                    });
                                </script>
                            <?php } ?>
                        <?php } ?>
                        
                        <?php if(isset($success)){ ?>
                            <script type="text/javascript">
                            jQuery(document).ready(function($) {
                                toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                                });
                            </script>
                        <?php } ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('induk_kategori'); ?> <span>*</span></label>
                            <div class="col-sm-5">
                                <select name="id_parent" class="form-control" id="id_parent" style="padding: 3px;">
                                    <option value="">-- <?php echo $this->lang->line('pilih_kategori'); ?> --</option>
                                    <option value="0"><?php echo $this->lang->line('induk_kategori'); ?></option>
                                    <?php foreach ($categories as $key => $value){ ?>
                                        <option <?php echo (isset($lists[0]['id_parent']) && $lists[0]['id_parent'] == $value['id'])?"selected='selected'":""; ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('name'); ?> <span>*</span></label>
                            <div class="col-sm-5">
                                <input type="text" id="name" name="name" value='<?php echo isset($lists[0]['name'])?$lists[0]['name']:''; ?>' size="50" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('slug'); ?> </label>
                            <div class="col-sm-6">
                                <input type="text" name="slug" value='<?php echo isset($lists[0]['slug'])?$lists[0]['slug']:''; ?>' readonly="readonly" class="form-control ArticleSlug">
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $("#name").slug();
                                });
                            </script>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('description'); ?> </label>
                            <div class="col-sm-8">
                                <textarea type="text" name="description" class="form-control"><?php echo isset($lists[0]['description'])?$lists[0]['description']:''; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group footertable">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-5 text-left">
                                <input class="btn btn-danger btn-sm mr5" type="submit" value="<?php echo $this->lang->line('navigation_kirim'); ?>">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
     
    </div>
 
    <a class="exit-offscreen"></a>
</section>