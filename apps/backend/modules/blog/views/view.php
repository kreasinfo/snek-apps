<!DOCTYPE html>
<html>
<head>
   
</head>
<div style="min-width:700px;max-width:700px">
	<section class="panel position-relative">
		<div class="panel-body">
			<h2><?php echo isset($lists[0]['title'])?$lists[0]['title']:''; ?></h2>
			<hr>
			<p class="small">
				<?php echo isset($lists[0]['description'])?stripcslashes($lists[0]['description']):''; ?>
			</p>
			<hr>
			<p class="text-center">
				<?php if(isset($lists[0]['path']) && $lists[0]['path'] != '' && count($lists[0]['path']) > 0){ ?>
                  <img src="<?php echo thumb_image($lists[0]['path'],'', 'blog'); ?>" alt="thumbs" width="600" border="0" />
                <?php }else{?>
                  <img src="<?php echo $this->webconfig['base_template']; ?>img/no_image_news.jpg" width="600"/>
                <?php }?>
                <br/>
                <em><?php echo isset($lists[0]['img_text'])?stripcslashes($lists[0]['img_text']):''; ?></em>
			</p>
			<hr>
			<p class="small">
				<?php echo isset($lists[0]['body'])?stripcslashes($lists[0]['body']):''; ?>
			</p>
		</div>
	</section>
</div>
</html>

	