<?php
$lang['app_logo'] = "Snekbook CMS";

## MENU ##
$lang['login_system'] = "Login System";
$lang['dashboard'] = "Dashboard";
$lang['logs'] = "Catatan Aplikasi";
$lang['logs_teks'] = "Catatan aktivitas aplikasi";
$lang['peoples'] = "Pengguna";
$lang['users'] = "Pengguna Aplikasi";
$lang['users_teks'] = "Pengelolaan pengguna aplikasi";
$lang['utility'] = "Utilitas";
$lang['references'] = "Referensi";

$lang['no_content'] = "Tidak Ada Konten";
$lang['no_data'] = "Tidak Ada Data";
$lang['date'] = "Tanggal";
$lang['month'] = "Bulan";
$lang['year'] = "Tahun";
$lang['yes'] = "Ya";
$lang['no'] = "Tidak";
$lang['ip'] = "Alamat IP";
$lang['username'] = "ID Pengguna";
$lang['module'] = "Modul aplikasi";
$lang['detail'] = "Keterangan";
$lang['description'] = "Deskripsi";
$lang['seo_description'] = "Deskripsi seo";
$lang['slug'] = "Slug";
$lang['gambar'] = "Gambar";
$lang['name'] = "Nama";
$lang['title'] = "Title";
$lang['seo_title'] = "Title seo";
$lang['datecreated'] = "Tgl. Pembuatan";
$lang['datemodified'] = "Tgl. Perubahan";
$lang['body'] = "Isi Konten";
$lang['tags'] = "Tag";
$lang['tagline'] = "Tagline";
$lang['password'] = "Kata Sandi";
$lang['retype_password'] = "Ulangi Sandi Pengguna";
$lang['group'] = "Group";
$lang['categories'] = "Kategori";
$lang['label_logout'] = "Keluar";
$lang['label_setting'] = "Konfigurasi";
$lang['login'] = "Masuk";
$lang['birthday'] = "Birthday";
$lang['address'] = "Alamat";
$lang['size'] = "Size";
$lang['budget'] = "Budget";
$lang['style'] = "Style";
$lang['email'] = "Email";
$lang['berat'] = "Berat (gr)";
$lang['gender'] = "Jenis kelamin";
$lang['chose_gender'] = "- Pilih -";
$lang['laki_laki'] = "Laki-laki";
$lang['wanita'] = "Wanita";
$lang['postcode'] = "Kode pos";
$lang['phone'] = "Telepon";
$lang['join'] = "Bergabung";
$lang['created'] = "Dibuat";
$lang['status'] = "Status";
$lang['label_info_ukuran'] = "Silahkan upload gambar dengan ukuran file maksimum";
$lang['label_info_dimensi'] = "dan dimensi gambar";
$lang['search_categories'] = "Cari Kategori";
$lang['tipe'] = "Tipe";
$lang['url'] = "URL";
$lang['nonaktif'] = "Non-aktif";
$lang['aktif'] = "Aktif";
$lang['fb_url'] = "Facebook Url";
$lang['twitter_url'] = "Twitter Url";
$lang['gplus_url'] = "Gplus Url";
$lang['instagram_url'] = "Instagram Url";
$lang['img_txt'] = "Deskripsi Gambar";
$lang['icon'] = "Icon";
$lang['email_contact'] = "Kontak Email";
$lang['color'] = "Warna";
$lang['ecommerce'] = "E-commerce";
$lang['search_category'] = "Cari kategori";
$lang['msg_exist_name'] = "Nama yang anda masukan sudah ada";
$lang['city'] = "Kota";
$lang['price'] = "Harga";
$lang['discount'] = "Diskon";
$lang['size'] = "Ukuran";
$lang['stock'] = "Stok";
$lang['profil'] = "Foto Profil";
$lang['sizename'] = "Nama Ukuran";
$lang['nama_product'] = "Nama Produk";
$lang['harga_total'] = "Harga Total";
$lang['harga'] = "Harga";


$lang['no'] = "No.";
$lang['option'] = "Opsi";
$lang['choose'] = "Pilih";
$lang['view'] = "Detail";
$lang['look_web'] = "Lihat website";
$lang['look_shop'] = "Lihat Online Shop";


$lang['add'] = "Tambah Data";
$lang['list'] = "Daftar Data";
$lang['search'] = "Cari";
$lang['modif'] = "Ubah";
$lang['delete'] = "Hapus";
$lang['navigation_publish'] = "Aktifkan";
$lang['navigation_unpublish'] = "Nonaktifkan";
$lang['navigation_download'] = "Download";
$lang['navigation_crop'] = "Crop";
$lang['navigation_reset_search'] = "Reset";
$lang['navigation_data_aktif'] = "Data aktif";
$lang['navigation_data_nonaktif'] = "Data tidak aktif";
$lang['navigation_kirim'] = "Kirim";


$lang['error_notif'] = "Pesan Error";
$lang['success_notif'] = "Pesan Sukses";

$lang['msg_empty_send'] = "Silahkan pilih salah satu data";
$lang['msg_success_send'] = "Newsletter telah berhasil di kirim ke subscriber";
$lang['msg_empty_delete'] = "Silahkan pilih salah satu data";
$lang['msg_success_delete'] = "Data telah dihapus dari database";
$lang['msg_error_database'] = "Maaf, telah terjadi kesalahan pada database. Silahkan mencoba beberapa saat lagi, atau menghubungi Administrator.";
$lang['msg_error_empty_field'] = "Silahkan memeriksa kembali form isian Anda.";
$lang['msg_error_empty_file_image'] = "Silahkan mengunggah file gambar";
$lang['msg_error_empty_article'] = "Silahkan masukkan minimal satu artikel";
$lang['msg_success_entry'] = "Data telah ditambahkan ke dalam database.";
$lang['msg_success_update'] = "Data telah disimpan ke dalam database.";
$lang['msg_success_publish'] = "Data telah diaktifkan.";
$lang['msg_empty_publish'] = "Data gagal diaktifkan.";
$lang['msg_success_unpublish'] = "Data telah dinonaktifkan.";
$lang['msg_empty_unpublish'] = "Data gagal dinonaktifkan.";
$lang['msg_success_entry_info'] = "Data telah disimpan ke dalam database.";
$lang['msg_error_interval1_empty'] = "Silahkan mengisi tanggal awal";
$lang['msg_error_interval2_empty'] = "Silahkan mengisi tanggal akhir";
$lang['msg_error_interval'] = "Tanggal mulai tidak boleh lebih besar dari tanggal akhir";
$lang['msg_error_minimum_dimensi_gambar'] = "Silahkan memilih gambar dengan dimensi yang lebih besar";
$lang['msg_error_extension'] = "Silahkan periksa kembali ekstensi file Anda.";
$lang['msg_error_empty_file'] = "File tidak ditemukan, silahkan upload file.";
$lang['msg_error_dimension'] = "Silahkan upload file dengan dimensi yang benar.";
$lang['label_error_ukuran'] = "File terlalu besar, silahkan upload file dengan ukuran yang lebih kecil";
$lang['label_error_dimensi'] = "Dimensi gambar terlalu kecil, silahkan upload gambar dengan dimensi yang sesuai";
$lang['label_error_dimensi_tertentu'] = "Dimensi gambar harus sesuai dengan yang telah ditentukan";

$lang['error_username_alfanumeric_users'] = "Silahkan mengisi id pengguna berupa huruf, angka, _ dan -";

$lang['alert_delete'] = "Apakah yakin akan menghapus data ini?";
$lang['alert_delete_img'] = "Apakah anda yakin akan menghapus gambar ini?";
$lang['alert_publish'] = "Apakah yakin akan mengaktifkan data ini?";
$lang['alert_unpublish'] = "Apakah yakin akan me-non aktifkan data ini?";
$lang['alert_info_del_img'] = "Jika anda ingin menghapus gambar ini maka masukan gambar yang lain terlebih dahulu.";
$lang['ok'] = "Benar";
$lang['cancel'] = "Batal";
$lang['alert_date_greater'] = "Tanggal mulai lebih besar dari tanggal akhir, silahkan tentukan tanggal kembali.";
$lang['alert_date_empty'] = "Tanggal mulai atau tanggal akhir tidak boleh kosong.";

$lang['empty_userid'] = "Tamu";
$lang['empty_name'] = "Nama Kosong";

$lang['user_menu_profile'] = "Profil";
$lang['user_menu_message'] = "Pesan";

$lang['user_menu_visit'] = "Lihat Website";
$lang['upload_btn'] = "Pilih File";
$lang['upload_image'] = "Upload gambar";
$lang['body'] = "Body";

$lang['btn_slider_gambar'] = "Crop slider";
$lang['btn_news_gambar'] = "Crop berita";
$lang['btn_free_gambar'] = "Crop bebas";
$lang['btn_product_gambar'] = "Crop produk";
$lang['add_images'] = "Tambah Gambar";

$lang['label_button_save'] = "Simpan";
$lang['label_interval_mulai'] = "Mulai Tanggal";
$lang['label_interval_akhir'] = "Sampai Dengan";
$lang['label_btn_tambahkan'] = "Tambahkan";
$lang['label_image_exist'] = "Gambar sudah dipilih";
$lang['label_pilih_lokasi'] = "-- Pilih Lokasi --";
$lang['label_pilih_kategori'] = "-- Pilih Kategori --";
$lang['label_pilih_style'] = "-- Pilih Style --";
$lang['label_ekstensi_gambar'] = "File harus berekstensi : .jpg / .jpeg / .png / .gif";
$lang['label_semua_kategori'] = "Semua Kategori";
$lang['label_aktif'] = "Aktif";
$lang['label_tidak_aktif'] = "Tidak Aktif";
$lang['label_min_password'] = "Minimal 5 karakter";
$lang['label_info_icon2'] = "File harus berekstensi : .png";
$lang['label_info_icon'] = "Silahkan upload ikon maksimum lebar/width 50px.";
$lang['label_info_icon_kategori_hotel'] = "Silahkan upload ikon maksimum lebar/width 100px.";
$lang['label_icon'] = "Ikon";
$lang['label_search_result'] = "Hasil Penelusuran";
$lang['label_undifined'] = "(Tidak terdefinisi)";
$lang['label_tgl_awal'] = "Tanggal awal";
$lang['label_tgl_akhir'] = "Tanggal akhir";
$lang['label_info_namakategori'] = "Maksimal 20 karakter";
$lang['label_image_notavailable'] = "Gambar tidak tersedia";
$lang['label_summary'] = "Ringkasan";
$lang['msg_empty_summary'] = "Silahkan mengisi ringkasan";

$lang['btn_recomended'] = "Merekomendasikan";
$lang['btn_unrecomended'] = "Hapus Recomendasi";
$lang['you_have'] = "Anda memiliki";
$lang['char_left'] = "karakter yang tersisa.";



## MODUL LOGS ##
$lang['logs_delete_logs'] = "Menghapus catatan aplikasi";

## MODUL ORDER ##
$lang['order'] = "Order";
$lang['order_teks'] = "Pengelolaan Order";
$lang['no_invoice'] = "No. Invoice";
$lang['alert_payment_accepted'] = "Apakah anda yakin pembayaran pesanan dengan nomor invoice";
$lang['alert_payment_accepted2'] = "telah anda terima?";
$lang['alert_payment_accepted3'] = "Sistem secara otomatis akan mengirimkan email kepada pelanggan yang bersangkutan bahwa pembayaran pesanan ini telah diterima";
$lang['logs_changestatus_order'] = "Status order di ubah menjadi";
$lang['logs_resetstatus_order'] = "Status pesanan umum di reset menjadi 0";
$lang['logs_cancelstatus_order'] = "Pesanan di batalkan";
$lang['alert_resetstatus_order'] = "Apakah anda yakin akan me-reset pesanan dengan nomor invoice";
$lang['alert_cancelstatus_order'] = "Apakah anda yakin akan membatalkan pesanan dengan nomor invoice";
$lang['msg_success_changestatus_order'] = "Berhasil mengubah status order";
$lang['msg_success_reset_order'] = "Berhasil me-reset status order";
$lang['label_shipping_alert'] = "Silahkan masukkan detail pengiriman yang digunakan";
$lang['label_shipping_done'] = "Apakah anda yakin transaksi dengan pelanggan ini sudah berhasil?";
$lang['label_all_statusorder'] = "Semua Status";
$lang['label_to_bank'] = "Transfered To";
$lang['label_from_bank'] = "From Bank";
$lang['label_total_transfered'] = "Total Transfered";
$lang['label_account_name_bank'] = "Account Name";

$lang['label_notprocess'] = "Belum diproses";
$lang['label_done'] = "Selesai";
$lang['label_waiting_payment'] = "Menunggu Pembayaran";
$lang['label_payment_accepted'] = "Pembayaran Diterima";
$lang['label_on_shipping'] = "Telah Dikirim";
$lang['label_batal'] = "Dibatalkan Manual";
$lang['label_batal_machine'] = "Dibatalkan Otomatis";
$lang['label_not_confirmed'] = "Belum dikonfirmasi";
$lang['label_waiting_confirmation'] = "Menunggu Konfirmasi Pembayaran";
$lang['label_shipping_metode'] = "Metode Pengiriman";
$lang['label_resi_number'] = "No. Resi";

$lang['msg_success_changestatus_order'] = "Berhasil mengubah status order";
$lang['msg_success_reset_order'] = "Berhasil me-reset status order";

$lang['label_noinvoice_order'] = "No.Invoice";
$lang['label_datecreated_order'] = "Tanggal";
$lang['label_datemodified_order'] = "Tanggal Update";
$lang['label_shipping_order'] = "Pengiriman";
$lang['label_shipping_agen_order'] = "Agen Pengiriman";
$lang['label_shipping_no_order'] = "Resi Pengiriman";
$lang['label_payment_order'] = "Pembayaran";
$lang['label_status_order'] = "Status";
$lang['label_userid_order'] = "ID User";
$lang['label_client_name_order'] = "Nama";
$lang['label_client_address_order'] = "Alamat";
$lang['label_client_postcode_order'] = "Kode Pos";
$lang['label_client_email_order'] = "Email";
$lang['label_client_phone_order'] = "Telepon";
$lang['label_post_name_order'] = "Nama";
$lang['label_post_address_order'] = "Alamat";
$lang['label_post_postcode_order'] = "Kode Pos";
$lang['label_post_email_order'] = "Email";
$lang['label_post_phone_order'] = "Telepon";
$lang['label_post_price_order'] = "Harga Kirim (Kg)";
$lang['label_post_city_order'] = "Kota";
$lang['label_product_id_order'] = "ID Produk";
$lang['label_shipping'] = "Pengiriman Umum";

## MODUL BANK ##
$lang['bank'] = "Bank";
$lang['bank_teks'] = "Pengelolaan bank";
$lang['logs_entry_bank'] = "Menambahkan bank";
$lang['logs_modif_bank'] = "Mengubah bank";
$lang['logs_delete_bank'] = "Menghapus bank";
$lang['error_empty_name_bank'] = "Silahkan mengisi nama bank";
$lang['error_empty_account_name_bank'] = "Silahkan mengisi nama akun";
$lang['error_empty_rekening_bank'] = "Silahkan mengisi No. Rekening";
$lang['error_empty_branch_bank'] = "Silahkan mengisi cabang";
$lang['account_name'] = "Nama akun";
$lang['rekening'] = "No. Rekening";
$lang['branch'] = "Cabang";
$lang['name_bank'] = "Nama bank";

## MODUL UKM ##
$lang['ukm'] = "Ukm";
$lang['ukm_teks'] = "Pengelolaan ukm";
$lang['logs_entry_ukm'] = "Menambahkan ukm";
$lang['logs_modif_ukm'] = "Mengubah ukm";
$lang['logs_delete_ukm'] = "Menghapus ukm";
$lang['error_empty_name_ukm'] = "Silahkan mengisi nama ukm";
$lang['error_empty_address_ukm'] = "Silahkan mengisi alamat";
$lang['error_empty_email_ukm'] = "Silahkan mengisi email";
$lang['error_empty_phone_ukm'] = "Silahkan mengisi telepon";
$lang['name_ukm'] = "Nama ukm";
$lang['pilih_ukm'] = "Pilih ukm";
$lang['logs_confirm_ukm'] = "Konfirmasi ukm";
$lang['logs_notconfirm_ukm'] = "Belum Konfirmasi ukm";
$lang['konfirmasi'] = "Konfirmasi ukm";
$lang['navigation_confirm'] = "Sudah Konfirmasi";
$lang['navigation_unconfirm'] = "Belum Konfirmasi";
$lang['alert_confirm'] = "Apakah anda yakin akan mengubah status ke sudah konfirmasi ukm ini?";
$lang['alert_unconfirm'] = "Apakah anda yakin akan mengubah status ke belum konfirmasi ukm ini?";
$lang['msg_empty_confirm'] = "Data gagal dikonfirmasi.";
$lang['msg_success_confirm'] = "Data berhasil dikonfirmasi.";
$lang['msg_success_unconfirm'] = "Data berhasil menjadi belum konfirmasi.";
$lang['msg_empty_unconfirm'] = "Data gagal menjadi belum konfirmasi.";

## MODUL ORDER PRODUK UKM ##
$lang['orderproduct_ukm'] = "Laporan per item";
$lang['orderproduct_ukm_teks'] = "Pengelolaan order produk ukm";
$lang['name_product'] = "Nama produk";
$lang['quantity'] = "Quantity";
$lang['laporan'] = "Laporan";
$lang['date_trx'] = "Tanggal transaksi";
$lang['margin'] = "Margin";
$lang['pembayaran'] = "Pembayaran";
$lang['alert_info_bayar'] = "Apakah anda yakin akan mengubah status ke sudah bayar";
$lang['alert_info_belumbayar'] = "Apakah anda yakin akan mengubah status ke belum bayar";
$lang['alert_checklist'] = "Silahkan checklist salah satu data";
$lang['logs_modif_bayar'] = "Mengubah status bayar";
$lang['logs_modif_belumbayar'] = "Mengubah status belum bayar";
$lang['msg_success_bayar'] = "Status sudah diubah menjadi sudah bayar";
$lang['msg_failed_bayar'] = "Status gagal diubah menjadi sudah bayar";;
$lang['msg_success_belumbayar'] = "Status sudah diubah menjadi belum bayar";
$lang['msg_failed_belumbayar'] = "Status gagal diubah menjadi belum bayar";
$lang['cari_ukm'] = "Cari ukm";

## MODUL SLIDERS ##
$lang['sliders'] = "Sliders";
$lang['sliders_teks'] = "Pengelolaan gambar sliders di homepage";
$lang['logs_entry_sliders'] = "Menambahkan sliders";
$lang['logs_modif_sliders'] = "Mengubah sliders";
$lang['logs_delete_sliders'] = "Menghapus sliders";
$lang['logs_publish_sliders'] = "Mengaktifkan sliders";
$lang['logs_unpublish_sliders'] = "Non aktifkan sliders";
$lang['error_empty_name_sliders'] = "Silahkan mengisi nama slider";

## MODUL PROMO IMG ##
$lang['promoimg'] = "Gambar Promo";
$lang['promoimg_teks'] = "Pengelolaan gambar promo di homepage";
$lang['logs_entry_promoimg'] = "Menambahkan gambar promo";
$lang['logs_modif_promoimg'] = "Mengubah gambar promo";
$lang['logs_delete_promoimg'] = "Menghapus gambar promo";
$lang['logs_publish_promoimg'] = "Mengaktifkan gambar promo";
$lang['logs_unpublish_promoimg'] = "Non aktifkan gambar promo";
$lang['error_empty_name_promoimg'] = "Silahkan mengisi nama gambar promo";

## MODUL SHOPSLIDERS ##
$lang['shopsliders'] = "Shop Sliders";
$lang['shopsliders_teks'] = "Pengelolaan gambar shop sliders di homepage";
$lang['logs_entry_shopsliders'] = "Menambahkan shop sliders";
$lang['logs_modif_shopsliders'] = "Mengubah shop sliders";
$lang['logs_delete_shopsliders'] = "Menghapus shop sliders";
$lang['logs_publish_shopsliders'] = "Mengaktifkan shops liders";
$lang['logs_unpublish_shopsliders'] = "Non aktifkan shop sliders";
$lang['error_empty_name_shopsliders'] = "Silahkan mengisi nama shop sliders";

## MODUL PRODUK ##
$lang['produk'] = "Produk";
$lang['produk_teks'] = "Pengelolaan produk";
$lang['logs_entry_produk'] = "Menambahkan produk";
$lang['logs_modif_produk'] = "Mengubah produk";
$lang['logs_delete_produk'] = "Menghapus produk";
$lang['logs_delete_img_produk'] = "Menghapus gambar produk";
$lang['logs_publish_produk'] = "Mengaktifkan produk";
$lang['logs_unpublish_produk'] = "Non aktifkan produk";
$lang['error_empty_name_produk'] = "Silahkan mengisi nama produk";
$lang['error_empty_slug_produk'] = "Silahkan mengisi slug produk";
$lang['error_empty_price_produk'] = "Silahkan mengisi harga produk";
$lang['error_empty_berat_produk'] = "Silahkan mengisi berat produk";
$lang['error_empty_description_produk'] = "Silahkan mengisi deskripsi produk";
$lang['error_empty_body_produk'] = "Silahkan mengisi konten produk";
$lang['error_empty_id_kategori_produk_produk'] = "Silahkan mengisi kategori produk";
$lang['logs_entry_imgproduk'] = "Menambahkan gambar produk";
$lang['success_delete_imgproduk'] = "Data gambar telah berhasil dihapus";
$lang['tambah_gambar'] = "Tambah gambar";
$lang['sembunyikan_gambar'] = "Sembunyikan gambar";
$lang['error_empty_id_ukm_produk'] = "Silahkan mengisi ukm";
$lang['error_empty_id_lokasi_ukm_produk'] = "Silahkan mengisi lokasi ukm";
$lang['rasa'] = "Nama rasa";
$lang['status_produk'] = "Status produk";
$lang['dalam_luar_kota'] = "Dalam dan luar kota";
$lang['dalam_kota'] = "Dalam kota";

## MODUL KATEGORI PRODUK ##
$lang['kategori_produk'] = "Kategori Produk";
$lang['kategori_produk_teks'] = "Pengelolaan kategori produk";
$lang['logs_entry_kategori_produk'] = "Menambahkan kategori produk";
$lang['logs_modif_kategori_produk'] = "Mengubah kategori produk";
$lang['logs_delete_kategori_produk'] = "Menghapus kategori produk";
$lang['error_empty_name_kategori_produk'] = "Silahkan mengisi nama kategori produk";
$lang['induk_kategori'] = "Induk kategori";
$lang['pilih_kategori'] = "Pilih kategori";

## MODUL PENGIRIMAN ##
$lang['pengiriman'] = "Pengiriman";
$lang['pengiriman_teks'] = "Pengelolaan Pengiriman";
$lang['logs_entry_pengiriman'] = "Menambahkan Pengiriman";
$lang['logs_modif_pengiriman'] = "Mengubah Pengiriman";
$lang['logs_delete_pengiriman'] = "Menghapus Pengiriman";
$lang['error_empty_city_pengiriman'] = "Silahkan mengisi kota";
$lang['error_empty_price_pengiriman'] = "Silahkan mengisi Harga";
$lang['msg_exist_city_category'] = "Kota dan kategori yang anda masukan sudah ada";
$lang['error_empty_id_category_pengiriman'] = "Silahkan mengisi kategori pengiriman";

## MODUL KATEGORI PENGIRIMAN ##
$lang['kategori_pengiriman'] = "Kategori Pengiriman";
$lang['kategori_pengiriman_teks'] = "Pengelolaan Kategori Pengiriman";
$lang['logs_entry_kategori_pengiriman'] = "Menambahkan Kategori Pengiriman";
$lang['logs_modif_kategori_pengiriman'] = "Mengubah Kategori Pengiriman";
$lang['logs_delete_kategori_pengiriman'] = "Menghapus Kategori Pengiriman";
$lang['error_empty_name_kategori_pengiriman'] = "Silahkan mengisi nama kategori pengiriman";

## MODUL LOKASI UKM ##
$lang['lokasi_ukm'] = "Lokasi Ukm";
$lang['lokasi_ukm_teks'] = "Pengelolaan Lokasi Ukm";
$lang['logs_entry_lokasi_ukm'] = "Menambahkan Lokasi Ukm";
$lang['logs_modif_lokasi_ukm'] = "Mengubah Lokasi Ukm";
$lang['logs_delete_lokasi_ukm'] = "Menghapus Lokasi Ukm";
$lang['error_empty_name_lokasi_ukm'] = "Silahkan mengisi nama Lokasi Ukm";
$lang['pilih_lokasi_ukm'] = "Pilih Lokasi Ukm";

## MODUL BLOG ##
$lang['blog'] = "Blog";
$lang['blog_teks'] = "Pengelolaan blog";
$lang['logs_entry_blog'] = "Menambahkan blog";
$lang['logs_modif_blog'] = "Mengubah blog";
$lang['logs_delete_blog'] = "Menghapus blog";
$lang['logs_publish_blog'] = "Mengaktifkan blog";
$lang['logs_unpublish_blog'] = "Non aktifkan blog";
$lang['error_empty_title_blog'] = "Silahkan mengisi title ";
$lang['error_empty_seo_title_blog'] = "Silahkan mengisi title seo";
$lang['error_empty_body_blog'] = "Silahkan mengisi body blog ";
$lang['error_empty_description_blog'] = "Silahkan mengisi deskripsi blog ";
$lang['error_empty_seo_description_blog'] = "Silahkan mengisi deskripsi seo blog ";
$lang['error_empty_id_category_blog'] = "Silahkan mengisi kategori blog ";

## MODUL TENTANG KAMI ##
$lang['tentangkami'] = "Tentang Kami";
$lang['tentangkami_teks'] = "Pengelolaan tentang kami";
$lang['logs_entry_tentangkami'] = "Menambahkan tentang kami";
$lang['logs_modif_tentangkami'] = "Mengubah tentang kami";
$lang['logs_delete_tentangkami'] = "Menghapus tentang kami";
$lang['logs_unpublish_statispages'] = "Non aktifkan tentang kami";
$lang['error_empty_name_tentangkami'] = "Silahkan mengisi nama ";
$lang['error_empty_body_tentangkami'] = "Silahkan mengisi deskripsi ";
$lang['error_empty_seo_title_tentangkami'] = "Silahkan mengisi title seo ";
$lang['error_empty_seo_description_tentangkami'] = "Silahkan mengisi deskripsi seo";

## MODUL LAYANAN ##
$lang['layanan'] = "Layanan";
$lang['layanan_teks'] = "Pengelolaan layanan";
$lang['logs_entry_layanan'] = "Menambahkan layanan";
$lang['logs_modif_layanan'] = "Mengubah layanan";
$lang['logs_delete_layanan'] = "Menghapus layanan";
$lang['logs_publish_statispages'] = "Mengaktifkan layanan";
$lang['logs_unpublish_statispages'] = "Non aktifkan layanan";
$lang['error_empty_name_layanan'] = "Silahkan mengisi nama ";
$lang['error_empty_body_layanan'] = "Silahkan mengisi deskripsi ";
$lang['error_empty_seo_title_layanan'] = "Silahkan mengisi title seo ";
$lang['error_empty_seo_description_layanan'] = "Silahkan mengisi deskripsi seo";

## MODUL INFORMATION PAGE ##
$lang['informationpage'] = "Informasi";
$lang['informationpage_teks'] = "Pengelolaan halaman informasi";
$lang['logs_entry_informationpage'] = "Menambahkan halaman informasi";
$lang['logs_modif_informationpage'] = "Mengubah halaman informasi";
$lang['logs_delete_informationpage'] = "Menghapus halaman informasi";
$lang['logs_publish_statispages'] = "Mengaktifkan halaman informasi";
$lang['logs_unpublish_statispages'] = "Non aktifkan halaman informasi";
$lang['error_empty_name_informationpage'] = "Silahkan mengisi nama ";
$lang['error_empty_body_informationpage'] = "Silahkan mengisi deskripsi ";
$lang['error_empty_seo_title_informationpage'] = "Silahkan mengisi title seo ";
$lang['error_empty_seo_description_informationpage'] = "Silahkan mengisi deskripsi seo";

## MODUL CUSTOMER SERVICE PAGE ##
$lang['customerservicepage'] = "Customer Service";
$lang['customerservicepage_teks'] = "Pengelolaan halaman customer service";
$lang['logs_entry_customerservicepage'] = "Menambahkan halaman customer service";
$lang['logs_modif_customerservicepage'] = "Mengubah halaman customer service";
$lang['logs_delete_customerservicepage'] = "Menghapus halaman customer service";
$lang['logs_publish_statispages'] = "Mengaktifkan halaman customer service";
$lang['logs_unpublish_statispages'] = "Non aktifkan halaman customer service";
$lang['error_empty_name_customerservicepage'] = "Silahkan mengisi nama ";
$lang['error_empty_body_customerservicepage'] = "Silahkan mengisi deskripsi ";
$lang['error_empty_seo_title_customerservicepage'] = "Silahkan mengisi title seo ";
$lang['error_empty_seo_description_customerservicepage'] = "Silahkan mengisi deskripsi seo";

## MODUL PROGRAM HOME ##
$lang['program'] = "Program";
$lang['program_teks'] = "Pengelolaan program untuk di halaman home";
$lang['logs_entry_program'] = "Menambahkan program";
$lang['logs_modif_program'] = "Mengubah program";
$lang['logs_delete_program'] = "Menghapus program";
$lang['error_empty_name_program'] = "Silahkan mengisi nama ";
$lang['error_empty_icon_program'] = "Silahkan mengisi ilustrasi icon";
$lang['error_empty_description_program'] = "Silahkan mengisi deskripsi program";

## MODUL CONFIGURATION ##
$lang['configuration'] = "Konfigurasi Web";
$lang['configuration_teks'] = "Pengelolaan konfigurasi website";
$lang['meta_title'] = "Meta title";
$lang['site_title'] = "Site title";
$lang['meta_description'] = "Meta description";
$lang['project_page'] = "Teks halaman project";
$lang['profesional_page'] = "Teks halaman profesional";
$lang['blog_page'] = "Teks halaman blog";
$lang['error_empty_meta_title_configuration'] = "Silahkan mengisi meta title";
$lang['error_empty_site_title_configuration'] = "Silahkan mengisi site title";
$lang['error_empty_meta_description_configuration'] = "Silahkan mengisi meta description";
$lang['error_empty_email_contact_configuration'] = "Silahkan mengisi kontak email";
$lang['error_empty_phone_configuration'] = "Silahkan mengisi no telepon";
$lang['error_empty_address_configuration'] = "Silahkan mengisi alamat";
$lang['error_empty_email_contact_valid_configuration'] = "Silahkan mengisi kontak email dengan benar";

## MODUL SHOPCONFIGURATION ##
$lang['shopconfiguration'] = "Konfigurasi Shop";
$lang['shopconfiguration_teks'] = "Pengelolaan konfigurasi e-commerce";
$lang['tagline_text'] = "Tagline";
$lang['error_empty_meta_title_shopconfiguration'] = "Silahkan mengisi meta title";
$lang['error_empty_site_title_shopconfiguration'] = "Silahkan mengisi site title";
$lang['error_empty_tagline_text_shopconfiguration'] = "Silahkan mengisi tagline";
$lang['error_empty_meta_description_shopconfiguration'] = "Silahkan mengisi meta description";
$lang['error_empty_email_contact_shopconfiguration'] = "Silahkan mengisi kontak email";
$lang['error_empty_phone_shopconfiguration'] = "Silahkan mengisi no telepon";
$lang['error_empty_address_shopconfiguration'] = "Silahkan mengisi alamat";
$lang['error_empty_email_contact_valid_shopconfiguration'] = "Silahkan mengisi kontak email dengan benar";
$lang['error_empty_harga_kirim_shopconfiguration'] = "Silahkan mengisi harga kirim ke tempat";

## MODUL USERS ##
$lang['logs_entry_users'] = "Menambahkan pengguna";
$lang['logs_modif_users'] = "Mengubah pengguna";
$lang['logs_delete_users'] = "Menghapus pengguna";
$lang['error_empty_name_users'] = "Silahkan mengisi nama pengguna";
$lang['error_empty_username_users'] = "Silahkan mengisi id pengguna";
$lang['error_empty_password_users'] = "Silahkan mengisi sandi pengguna";
$lang['error_empty_password_5_users'] = "Silahkan mengisi sandi minimal 5 karakter";
$lang['error_empty_retype_password_users'] = "Silahkan mengulang sandi pengguna";
$lang['error_empty_retype_password_true_users'] = "Silahkan mengulang sandi pengguna dengan benar";
$lang['error_duplicate_username'] = "Username telah digunakan, silahkan menggunakan username lain";
$lang['label_edit_users'] = "Ubah data pengguna";
$lang['label_add_users'] = "Tambah data pengguna";


## MODUL MEMBERS ##
$lang['members'] = "Members";
$lang['members_teks'] = "Pengelolaan members aplikasi";
$lang['error_empty_name_members'] = "Silahkan mengisi nama members";
$lang['error_empty_status_members'] = "Silahkan mengisi status members";
$lang['error_empty_birthday_members'] = "Silahkan mengisi tanggal lahir members";
$lang['error_empty_address_members'] = "Silahkan mengisi alamat members";
$lang['error_empty_postcode_members'] = "Silahkan mengisi kode pos members";
$lang['error_empty_gender_members'] = "Silahkan mengisi jenis kelamin members";
$lang['error_empty_phone_members'] = "Silahkan mengisi telepon members";
$lang['error_empty_email_members'] = "Silahkan mengisi email members";
$lang['error_wrong_email_members'] = "Format email yang anda masukan salah";
$lang['error_empty_password_members'] = "Silahkan mengisi password members";
$lang['error_duplicate_email'] = "Email telah digunakan, silahkan menggunakan email lain";
$lang['error_password_5_members'] = "Silahkan mengisi sandi minimal 5 karakter";
$lang['logs_entry_members'] = "Menambahkan pengguna";
$lang['logs_modif_members'] = "Mengubah pengguna";
$lang['logs_delete_members'] = "Menghapus pengguna";
$lang['aktif'] = "Aktif";
$lang['nonaktif'] = "Tidak aktif";

## LOGIN ##
$lang['error_login_failed_input'] = "Silahkan periksa kembali ID Pengguna dan Kata Sandi Anda.";
$lang['error_login_failed_login'] = "Login gagal, silahkan periksa ID Pengguna atau Kata Sandi Anda.";