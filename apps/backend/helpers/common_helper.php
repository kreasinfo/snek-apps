<?php
if ( ! function_exists('idParent')) {
	function idParent($id_parent_categories='') {
		$CI =& get_instance();
		$CI->load->sharedModel('ChildkategoriModel');

		$result = $CI->ChildkategoriModel->listData(array('id'=>$id_parent_categories));

		$data = isset($result['0']['id_parent_categories'])?$result['0']['id_parent_categories']:"";

		return $data;
	}
}


if ( ! function_exists('GetImage')) {
	function GetImage($id='') {
		$CI =& get_instance();
		$CI->load->sharedModel('GambarModel');

		$result = $CI->GambarModel->listData(array('id'=>$id));

		$data = isset($result['0']['path'])?$result['0']['path']:"";

		return $data;
	}
}


if ( ! function_exists('statproduk')) {
	function statproduk($produk ='') {
		$data = '';
		if($produk !=''){
			if($produk == '0'){
				$data = 'Dalam kota';
			}else{
				$data = 'Dalam dan Luar kota';
			}
		}
		
		return $data;
	}
}



if ( ! function_exists('statusOrder')) {
	function statusOrder($status = 0) {
		$result = "";
		$CI =& get_instance();
		switch ($status) {
			case 0:
				$result = $CI->lang->line('label_notprocess');
				break;
			case 1:
				$result = $CI->lang->line('label_waiting_payment');
				break;
			case 2:
				$result = $CI->lang->line('label_payment_accepted');
				break;
			case 3:
				$result = $CI->lang->line('label_on_shipping');
				break;
			case 4:
				$result = $CI->lang->line('label_done');
				break;
			case 5:
				$result = $CI->lang->line('label_batal_machine');
				break;
			case 6:
				$result = $CI->lang->line('label_not_confirmed');
				break;
			case 7:
				$result = $CI->lang->line('label_done');
				break;
			case 8:
				$result = $CI->lang->line('label_waiting_confirmation');
				break;
			default:
				//No Action
				break;
		}

		return $result;
	}
}