<?php
function cutting_words($string, $max_length){  
    if (strlen($string) > $max_length){  
        $string = substr($string, 0, $max_length);  
        $pos = strrpos($string, " ");  
        if($pos === false) {  
                return substr($string, 0, $max_length)."...";  
        }  
            return substr($string, 0, $pos)."...";  
    }else{  
        return $string;  
    }  
} 
function clean_str($string){
    $string = str_replace(array("\\r\\n", "\\r", "\\n"), "", $string);
    $string = stripslashes($string);
    return $string;
}

function clean_str_html($string){
    $string = str_replace(array("\\r\\n", "\\r", "\\n"), "", $string);
    $string = htmlentities(stripslashes($string));
    return $string;
}

function format_price($price, $code) {
    return $code.' '.number_format($price, 0, ',', '.');
}

if ( ! function_exists('getChannelChild')) {
    function getChannelChild($result = array(), $selected_id = '', $space = '&nbsp;&nbsp;') {
        $CI =& get_instance(); 
        $CI->load->database();
        $tdata = '';    
        $selected = ''; 
        
        foreach ($result as $val_child) {
            $selected = "";
            
            if($selected_id != '' && $selected_id == $val_child['id']) { $selected = "selected='selected'"; }
            $tdata .= "<option ".$selected." value='".$val_child['id']."'>".$space." >>> ".$val_child['name']."</option>";
            
            if(isset($val_child['child']) && count($val_child['child']) > 0) {
                $tdata .= getChannelChild($val_child['child'], $selected_id,($space.'&nbsp;&nbsp;'));
            }
        }
        return $tdata;

    }
}
