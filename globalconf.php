<?php
date_default_timezone_set('Asia/Jakarta');
## DATABASE CONFIGURATION ##
DEFINE("APP_HOST", "localhost");
DEFINE("APP_USERNAME", "root");
DEFINE("APP_PASSWORD", "");
DEFINE("APP_DBNAME", "snekbook");

## URL CONFIG ##
DEFINE("APP_DOMAIN", "localhost:8080/kreasinfo/snekbook");
DEFINE("APP_DOMAIN_SHOP", "localhost:8080/kreasinfo/snekbook/shop");
DEFINE("APP_DOMAIN_BACK", "localhost:8080/kreasinfo/snekbook/admincms");
DEFINE("APP_PATH", "D:/xampp/htdocs/kreasinfo/snekbook/public/");

## EMAIL ##
DEFINE("HOST_EMAIL", "mail.ruangjualan.com");
DEFINE("PORT_EMAIL", "26");
DEFINE("SENDER_EMAIL", "noreply@ruangjualan.com");
DEFINE("USER_EMAIL", "noreply@ruangjualan.com");
DEFINE("PASS_EMAIL", "noreply123");

### LINK ##
DEFINE("URL_1", "#");
DEFINE("URL_2", "#");
DEFINE("URL_3", "#");
DEFINE("URL_4", "#");


## CONFIG ##
$servername = "http://".APP_DOMAIN."/";
$pathbase              =  APP_PATH;
$servernamemedia       = $servername.'public/';

$config["webconfig"]["count_data"] = 15;
$config["webconfig"]["count_data_project"] = 12;
$config["webconfig"]["count_data_profesionals"] = 4;
$config["webconfig"]["limit_seo_title"] = 60;
$config["webconfig"]["limit_seo_description"] = 160;
$config["webconfig"]["limit_data_popular"] = 5;
$config["webconfig"]["count_data_blog"] = 9;
$config["webconfig"]["base_site"] = $servername;
$config["webconfig"]["site_url"] = "www.".APP_DOMAIN;
$config["webconfig"]["base_template"] = $servernamemedia."template/backend_template/";
$config["webconfig"]["images"] = $servernamemedia."template/backend_template/img/";
$config["webconfig"]["back_base_template"] = $servernamemedia."template/backend_template/";
$config["webconfig"]["back_images"] = $servernamemedia."template/backend_template/img/";

$config["webconfig"]["media-server"] = $servernamemedia."media/";
$config["webconfig"]["media-server-embeded"] = $servernamemedia."media/embeded/";

## FRONT END ##
$config["webconfig"]["frontend_template"] = $servernamemedia."template/frontend_template/";
$config["webconfig"]["shop_template"] = $servernamemedia."template/shop_template/";

$config["webconfig"]["max_foto_filesize"] = 1; //MB MAX 32MB
$config["webconfig"]["width_resize"] = 600; // 600px
$config["webconfig"]["image_sliders"] = array('1600x1066');
$config["webconfig"]["image_shopsliders"] = array('870x373');
$config["webconfig"]["image_promoimg"] = array('270x196');
$config["webconfig"]["image_blog"] = array('850x638','500x375', '300x225','124x93');
$config["webconfig"]["image_projectmember"] = array('850x638','500x375', '300x225','124x93');
$config["webconfig"]["image_cover"] = array('1200x300');
$config["webconfig"]["image_profile"] = array('200x200');
$config["webconfig"]["image_profesionalcategory"] = array('100x100');
$config["webconfig"]["image_product"] = array('555x311','360x203');

# GENERAL TINIMCE #
$config["webconfig"]["media-server-tinymce"] = $servernamemedia."media/tinymce/";
$config["webconfig"]["media-path-tinymce"] = $pathbase."media/tinymce/";

# GENERAL IMAGES #
$config["webconfig"]["media-server-images-original"] = $servernamemedia."media/images/original/";
$config["webconfig"]["media-path-images-original"] = $pathbase."media/images/original/";
$config["webconfig"]["media-server-images-thumb"] = $servernamemedia."media/images/thumb/";
$config["webconfig"]["media-path-images-thumb"] = $pathbase."media/images/thumb/";

# GENERAL PROFESIONAL #
$config["webconfig"]["media-server-profesionalcategory"] = $servernamemedia."media/profesionalcategory/";
$config["webconfig"]["media-path-profesionalcategory"] = $pathbase."media/profesionalcategory/";
$config["webconfig"]["media-server-profesionalcategory-thumb"] = $servernamemedia."media/profesionalcategory/thumb/";
$config["webconfig"]["media-path-profesionalcategory-thumb"] = $pathbase."media/profesionalcategory/thumb/";


# GENERAL PROMOIMG #
$config["webconfig"]["media-server-promoimg"] = $servernamemedia."media/promoimg/";
$config["webconfig"]["media-path-promoimg"] = $pathbase."media/promoimg/";
$config["webconfig"]["media-server-promoimg-thumb"] = $servernamemedia."media/promoimg/thumb/";
$config["webconfig"]["media-path-promoimg-thumb"] = $pathbase."media/promoimg/thumb/";

# GENERAL SHOP SLIDERS #
$config["webconfig"]["media-server-shopsliders"] = $servernamemedia."media/shopsliders/";
$config["webconfig"]["media-path-shopsliders"] = $pathbase."media/shopsliders/";
$config["webconfig"]["media-server-shopsliders-thumb"] = $servernamemedia."media/shopsliders/thumb/";
$config["webconfig"]["media-path-shopsliders-thumb"] = $pathbase."media/shopsliders/thumb/";

# GENERAL SLIDERS #
$config["webconfig"]["media-server-sliders"] = $servernamemedia."media/sliders/";
$config["webconfig"]["media-path-sliders"] = $pathbase."media/sliders/";
$config["webconfig"]["media-server-sliders-thumb"] = $servernamemedia."media/sliders/thumb/";
$config["webconfig"]["media-path-sliders-thumb"] = $pathbase."media/sliders/thumb/";

# GENERAL BLOG #
$config["webconfig"]["media-server-blog"] = $servernamemedia."media/blog/";
$config["webconfig"]["media-path-blog"] = $pathbase."media/blog/";
$config["webconfig"]["media-server-blog-thumb"] = $servernamemedia."media/blog/thumb/";
$config["webconfig"]["media-path-blog-thumb"] = $pathbase."media/blog/thumb/";

# GENERAL COVER #
$config["webconfig"]["media-server-cover"] = $servernamemedia."media/cover/";
$config["webconfig"]["media-path-cover"] = $pathbase."media/cover/";
$config["webconfig"]["media-server-cover-thumb"] = $servernamemedia."media/cover/thumb/";
$config["webconfig"]["media-path-cover-thumb"] = $pathbase."media/cover/thumb/";

# GENERAL PROFILE #
$config["webconfig"]["media-server-profile"] = $servernamemedia."media/profile/";
$config["webconfig"]["media-path-profile"] = $pathbase."media/profile/";
$config["webconfig"]["media-server-profile-thumb"] = $servernamemedia."media/profile/thumb/";
$config["webconfig"]["media-path-profile-thumb"] = $pathbase."media/profile/thumb/";

# GENERAL PRODUCT #
$config["webconfig"]["media-server-product"] = $servernamemedia."media/product/";
$config["webconfig"]["media-path-product"] = $pathbase."media/product/";
$config["webconfig"]["media-server-product-thumb"] = $servernamemedia."media/product/thumb/";
$config["webconfig"]["media-path-product-thumb"] = $pathbase."media/product/thumb/";

# GENERAL CACHE #
$config["webconfig"]["media-server-cache"] = $servernamemedia."media/cache/";
$config["webconfig"]["media-path-cache"] = $pathbase."media/cache/";